import 'dart:async';
import 'package:intl/intl.dart';
import 'package:micro_utils/data_helper.dart';
import 'package:db_partnerum/db_partnerum.dart';
import 'package:partnerum/utils/brain_module.dart';

@DataHelper()
class OrdersHelper {
  var notStated = 'N/A';
  var acceptedOffers = <Offer>[];

  Future<List<Offer>> requests(Request request) async {
    var resultOffers = [];
    var offers = request.offers.values.toList();
    return offers;

    for (var element in offers) {
      if (element?.offerId?.value != null) {
        var ownerStatus = (await root.chatTopic.offer2Room[element.offerId.value].conversationUsers.usersStatuses.ownerUserStatus.future).value;
        if ((ownerStatus == 'request_accept') || (ownerStatus == 'request_booked')) {
          resultOffers.add(element);
        }
      }
    }
    return resultOffers;
  }

  Future<List<ConversationUsers>> acceptedOffersConversationUsers(Request request) async {
    var resultOfferConversationUsers = <ConversationUsers>[];
    var offers = request.offers.values.toList();
    // return offers;
    // print('offers: ${offers}');
    for (var element in offers) {
      // resultOfferConversationUsers.add(element);
      // continue;
      // print('element?.offerId?.value: ${element?.offerId?.value}');
      if (element?.offerId?.value != null) {
        var conversationUsers = (await root.chatTopic.offer2Room[element.offerId.value].conversationUsers.future);
        var ownerStatus = conversationUsers.usersStatuses.ownerUserStatus.value.split(';').first;
        var requestUserStatus = conversationUsers.usersStatuses.requestUserStatus.value;
        // print('ownerStatus: ${ownerStatus}');
        // print('element.offerId.value: ${element.offerId.value}');
        print('Conversation users: ${conversationUsers.ownerUser.value.split(';')[0]} ${conversationUsers.requestUser.value}');
        if (conversationUsers.ownerUser.value.split(';')[0] !=
                    conversationUsers.requestUser.value &&
            ( (ownerStatus == 'request_accept') ||
                (ownerStatus == 'request_booked') ||
                (ownerStatus == 'request_denied') ||
                (requestUserStatus == 'house_request_sended') ||
                (requestUserStatus == 'request_booked'))) {
          resultOfferConversationUsers.add(conversationUsers);
          acceptedOffers.add(element);
        }
      }
    }
    print('resultOfferConversationUsers: ${resultOfferConversationUsers} ${request.key} ');
    return resultOfferConversationUsers;
  }

  String requestDatesRange(Request request) {
    var firstDate = request.requestInformation.datesRange.first.value.inMilliseconds;
    var lastDate = request.requestInformation.datesRange.last.value.inMilliseconds;

    String formatDate(int timestamp) {
      return DateFormat.yMMMd('ru').format(DateTime.fromMillisecondsSinceEpoch(timestamp));
    }

    var title = '${(firstDate == 'null') ? notStated : formatDate(firstDate)}'
        ' - '
        '${(lastDate == 'null') ? notStated : formatDate(lastDate)}';

    return title;
  }

  List requestStations(Request request) {
    // var stations = request?.requestInformation?.stations?.values?.map((e) => e.key)?.toList();
    var stations = [];
    var stationsMap = request?.requestInformation?.stations;
    stations = stationsMap?.values?.map((e) => e?.value)?.toList() ?? [];
    // print('stations: ${request?.requestInformation?.stations.runtimeType} ${request?.requestInformation?.stations?.values}');
    return stations;
  }

  void removeFromArchive(Request request) {
    request.requestInformation.isArchived.set(false);
  }
}

@DataHelper()
class ChoseListHelper {
  var photosList = [];
  Future<HouseInformation> loadHouseInformation(String ownerId, String houseId) {
    print('ownerId, houseId: ${ownerId} ${houseId}');
    return root.userData[ownerId].houses[houseId].houseInformation.future.then((value) async {
      var photosIds = value?.photos?.values?.toList();
      for (var photo in photosIds){
        photosList.add(await root.imagesContainer[photo].future);
      }
      return value;
    });
  }

  Future loadOwnerInformation(String ownerId) {
    return root.userData[ownerId].userInformation.future;
  }
}
