import 'package:db_partnerum/db_partnerum.dart';
import 'package:partnerum/head_module/gen_extensions/generated_str_extension.dart';
import 'package:partnerum/pages/request/request_create/request_create_page.dart';
import 'package:partnerum/utils/brain_module.dart';
import 'package:partnerum/utils/theme_constants.dart';
import 'package:partnerum/widgets/house_card/house_card_widget.dart';
import 'package:partnerum/widgets/partnerum_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';

import 'order_detail_helper.dart';

class OrderDetailPage extends StatefulWidget {
  final RequestInformation requestCopy;
  final House house;

  /// request parameter is used with editable (true)
  final Request request;
  final bool editable;

  final Widget extraActions;

  const OrderDetailPage(
      {Key key,
      this.requestCopy,
      this.house,
      this.request,
      this.editable,
      this.extraActions})
      : super(key: key);

  @override
  _OrderDetailPageState createState() => _OrderDetailPageState();
}

class _OrderDetailPageState extends State<OrderDetailPage>
    with OrderDetailHelper {
  DateFormat formatDates;

  @override
  void initState() {
    if (widget.editable) {
      document = widget.request.requestInformation;
    } else {
      document = widget.requestCopy;
    }
    super.initState();
    initializeDateFormatting();
  }

  @override
  Widget build(BuildContext context) {
    // print('RequestId: ${widget.requestId}');

    print('Request object: ${document.isArchived.value}');
    return Scaffold(
      appBar: _buildAppBar(),
      backgroundColor: Colors.white,
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: widget.editable
          ? FloatingActionButton(
              onPressed: () {
                Navigator.push(
                  context,
                  CupertinoPageRoute(
                    builder: (context) => RequestCreate(
                      requestId: widget.request.key,
                    ),
                  ),
                ).then((v) {
                  setState(() {});
                });
              },
              child: Icon(
                Partnerum.edit,
                color: Theme.of(context).primaryColor,
              ),
            )
          : Container(),
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            _buildCity(),
            _buildMetro(),
            _buildDistance(),
            _buildPrice(),
            _buildRooms(),
            _buildGuests(),
            _buildComission(),
            _buildInfo(),
            if (widget?.house != null)
              FutureBuilder(
                future: loadHouseInformation(widget.house),
                builder: (context, photoSnapshot) {
                  var photosList = photoSnapshot.data;
                  return Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: HouseCard(
                      house: widget.house,
                      noEdit: true,
                      photos: photosList,
                    ),
                  );
                }
              ),
            if (widget.extraActions != null) widget.extraActions
          ],
        ),
      ),
    );
  }

  Widget _buildAppBar() {
    return AppBar(
      leading: IconButton(
        icon: Theme.of(context).platform == TargetPlatform.iOS
            ? Icon(Icons.arrow_back_ios)
            : Icon(Icons.arrow_back),
        onPressed: () => Navigator.of(context).pop(),
      ),
      title: Text(requestDatesRange(context)),
      iconTheme: IconThemeData(
        color: Colors.black,
      ),
      centerTitle: false,
      textTheme: TextTheme(
          title: TextStyle(
        color: Colors.black,
        fontSize: 20.0,
      )),
      actions: <Widget>[],
      backgroundColor: Colors.white,
    );
  }

  Widget _buildAction() {
    return IconButton(
      icon: Icon(
        Partnerum.edit,
        color: Colors.black,
      ),
      onPressed: () {},
    );
  }

  Widget _buildInfo() {
    return ExpansionTile(
      leading: Icon(Partnerum.info, color: Theme.of(context).primaryColor),
      title: Text(Strings.pages.ordersPage.orderDetail.info.l(context)),
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: Align(
            alignment: Alignment.topLeft,
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 8),
              child: requestDescription(context),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildComission() {
    return ListTile(
      leading: Icon(Partnerum.percent, color: Theme.of(context).primaryColor),
      title:
          Text(Strings.pages.ordersPage.orderDetail.comissionSize.l(context)),
      subtitle: requestCommission(context),
    );
  }

  Widget _buildGuests() {
    return ListTile(
      leading: Icon(Partnerum.guest, color: Theme.of(context).primaryColor),
      title: Text(Strings.pages.ordersPage.orderDetail.guestsAmount.l(context)),
      subtitle: requestGuests(context),
    );
  }

  Widget _buildRooms() {
    return ListTile(
      leading: Icon(Partnerum.rooms, color: Theme.of(context).primaryColor),
      title: Text(Strings.pages.ordersPage.orderDetail.roomsAmount.l(context)),
      subtitle: Container(
        child: requestRooms(context),
      ),
    );
  }

  Widget _buildPrice() {
    return ListTile(
        leading: Icon(Partnerum.ruble, color: Theme.of(context).primaryColor),
        title: Text(Strings.pages.ordersPage.orderDetail.price.l(context)),
        subtitle: requestPrices(context));
  }

  Widget _buildCity() {
    return ListTile(
      leading: Icon(Partnerum.city, color: Theme.of(context).primaryColor),
      title: Text(Strings.pages.ordersPage.orderDetail.city.l(context)),
      subtitle: requestCity(context),
    );
  }

  Widget _buildMetro() {
    var stationsList = requestStations();

    if(stationsList.isEmpty) {
      return ListTile(
        leading: Icon(Partnerum.metro, color: Theme.of(context).primaryColor),
        title: Text(
            '${Strings.pages.ordersPage.orderDetail.metroStations.l(context)} ${stationsList.length}'),
      );
    }
    return ExpansionTile(
      leading: Icon(Partnerum.metro, color: Theme.of(context).primaryColor),
      title: Text(
          '${Strings.pages.ordersPage.orderDetail.metroStations.l(context)} ${stationsList.length}'),

      children: <Widget>[

        Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: Align(
            alignment: Alignment.topLeft,
            child: Wrap(
              spacing: 0,
              runSpacing: 0,
              children: <Widget>[
                for (var i = 0; i < stationsList.length; i++)
                  Container(
                    child: RawChip(
                        padding: EdgeInsets.zero,
                        labelPadding: EdgeInsets.all(3),
                        label: Text(
                          '${stationsList[i]}',
                          style: TextStyle(fontSize: 14, color: Colors.grey),
                        )),
                  ),
              ],
            ),
          ),
        ),
      ],
    );


  }

  Widget _buildDistance() {
    return ListTile(
        leading: Icon(Partnerum.walk, color: Theme.of(context).primaryColor),
        title:
            Text(Strings.pages.ordersPage.orderDetail.timeToMetro.l(context)),
        subtitle: requestTimeToMetro(context));
  }
}
