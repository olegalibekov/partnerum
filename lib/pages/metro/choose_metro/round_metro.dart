import 'package:diffutil_sliverlist/diffutil_sliverlist.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:partnerum/models/city_models.dart';
import 'package:partnerum/utils/brain_module.dart';
import 'package:partnerum/utils/theme_constants.dart';
import 'choose_metro_helper.dart';

class RoundMetro extends StatefulWidget {
  final JsonCity metroJson;
  final Set<String> stations;
  final String city;
  final String searchString;

  const RoundMetro({Key key, this.metroJson, this.stations, this.city, this.searchString}) : super(key: key);

  @override
  _RoundMetroState createState() => _RoundMetroState();
}

class _RoundMetroState extends State<RoundMetro> with ChooseMetroHelper {
  Map<String, Lines> linesList;

  var lineStationsList = <String, List>{};
  var lineStationsDisplay = <String, bool>{};
  var resultSliverList = <Widget>[];

  @override
  void initState() {
    // = widget.metroJson.toList();
    linesList = mindClass.getRoundsAsync(widget.city, widget.metroJson);

    if (linesList != null) {
      for (var line in linesList.keys) {
        print(line);
        lineStationsDisplay[line] = false;
        lineStationsList[line] = [];
        for (var station in linesList[line].stations) {
          lineStationsList[line].add(station);
        }
      }
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    resultSliverList = [];
    // return Container(color: Colors.yellow,);

    if (linesList == null) {
      return Center(child: Text('Доступно только для Москвы'));
    }
    for (var district in linesList.keys) {
      resultSliverList.add(SliverToBoxAdapter(
        child: GestureDetector(
          key: ValueKey(district),
          behavior: HitTestBehavior.translucent,
          onTap: () {
            setState(() {
              lineStationsDisplay[district] = !lineStationsDisplay[district];
            });
          },
          child: Container(
            child: Padding(
              padding: EdgeInsets.all(16.0),
              child: Row(children: [
                Flexible(child: Text(district)),
                Checkbox(
                    value: isRoundContains(district, widget.stations, linesList),
                    // value: false,
                    onChanged: (bool val) {
                      setState(() {
                        if (val) {
                          linesList[district].stations.forEach((v) {
                            if (v.name.toLowerCase().contains(widget.searchString)) {
                              widget.stations.add(v.name);
                            }
                          });
                        } else {
                          linesList[district].stations.forEach((v) {
                            if (v.name.toLowerCase().contains(widget.searchString)) {
                              widget.stations.remove(v.name);
                            }
                          });
                        }
                      });
                    }),
                Icon(lineStationsDisplay[district] ? Icons.arrow_drop_up : Icons.arrow_drop_down, color: Colors.black87),
              ]),
            ),
          ),
        ),
      ));
      resultSliverList.add(DiffUtilSliverList<Stations>(
        equalityChecker: (line1, line2) {
          // print('${line1.name} ${line2.name}');
          return line1.name == line2.name;
        },
        items: List.from(lineStationsDisplay[district] ? lineStationsList[district] : []),
        builder: (context, item) {
          print('build GestureDetector');
          if (item.name.toLowerCase().contains(widget.searchString)) {
            return GestureDetector(
              key: ValueKey(item.name),
              child: Container(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(32, 0, 8, 16),
                  child: Row(children: [
                    Flexible(child: Text(item.name)),
                    Checkbox(
                        value: widget.stations.contains(item.name),
                        // value: false,
                        onChanged: (bool val) {
                          setState(() {
                            if (val) {
                              widget.stations.add(item.name);
                            } else {
                              widget.stations.remove(item.name);
                            }
                          });
                        }),
                  ]),
                ),
              ),
            );
          } else {
            return Container();
          }
        },
        insertAnimationBuilder: (context, animation, child) => FadeTransition(
          opacity: animation,
          child: child,
        ),
        removeAnimationBuilder: (context, animation, child) => FadeTransition(
          opacity: animation,
          child: child,
        ),
        removeAnimationDuration: const Duration(milliseconds: 100),
        insertAnimationDuration: const Duration(milliseconds: 500),
      ));
    }
    resultSliverList.add(SliverToBoxAdapter(
      child: Container(
        child: Padding(
          padding: EdgeInsets.all(30),
        ),
      ),
    ));

    if (linesList.isNotEmpty) {
      return ScrollConfiguration(
        behavior: noHighlightScrollBehavior(context),
        child: CustomScrollView(
          slivers: [for (var item in resultSliverList) item],
        ),
      );
    } else {
      return Container(color: Colors.grey);
    }
  }
}
