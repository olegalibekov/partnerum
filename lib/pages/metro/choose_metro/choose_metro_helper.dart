import 'dart:async';
import 'package:micro_utils/data_helper.dart';
import 'package:db_partnerum/db_partnerum.dart';
import 'package:partnerum/models/city_models.dart';
import 'package:partnerum/utils/brain_module.dart';

@DataHelper()
class ChooseMetroHelper {

  // var sliverLines = <Lines>[Lines('1')];

  bool isFullContains(Lines line, Set<String> stations) {
    for (var station in line.stations) {
      // print('station: ${station.name}');
      if (!stations.contains(station.name)) {
        return false;
      }
    }
    return true;
  }

  bool isRoundContains(String round, Set<String> stations, Map<String, Lines> linesList) {
    for (var station in linesList[round].stations) {
      // print('station: ${station.name}');
      if (!stations.contains(station.name)) {
        return false;
      }
    }
    return true;
  }
}
