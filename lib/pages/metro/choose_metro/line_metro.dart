import 'package:diffutil_sliverlist/diffutil_sliverlist.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:partnerum/models/city_models.dart';
import 'package:partnerum/utils/theme_constants.dart';
import 'choose_metro_helper.dart';

class LineMetro extends StatefulWidget {
  final List metroLines;
  final Set<String> stations;
  final String searchString;

  const LineMetro({Key key, this.metroLines, this.stations, this.searchString}) : super(key: key);

  @override
  _LineMetroState createState() => _LineMetroState();
}

class _LineMetroState extends State<LineMetro> with ChooseMetroHelper {
  List linesList;

  var lineStationsList = <String, List>{};
  var lineStationsDisplay = <String, bool>{};
  var resultSliverList = <Widget>[];

  @override
  void initState() {
    linesList = widget.metroLines.toList();
    for (var line in linesList) {
      lineStationsDisplay[line.name] = false;
      lineStationsList[line.name] = [];
      for (var station in line.stations) {
        lineStationsList[line.name].add(station);
      }
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    resultSliverList = [];
    for (var line in linesList) {
      resultSliverList.add(SliverToBoxAdapter(
        child: GestureDetector(
          key: ValueKey(line.name),
          behavior: HitTestBehavior.translucent,
          onTap: () {
            // print('opened stations');
            setState(() {
              lineStationsDisplay[line.name] = !lineStationsDisplay[line.name];
            });
          },
          child: Container(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.0),
              child: Row(children: [
                Flexible(child: Text(line.name)),
                Checkbox(
                    value: isFullContains(line, widget.stations),
                    onChanged: (bool val) {
                      setState(() {
                        if (val) {
                          line.stations.forEach((v) {
                            if (v.name.toLowerCase().contains(widget.searchString)) {
                              widget.stations.add(v.name);
                            }
                          });
                        } else {
                          line.stations.forEach((v) {
                            if (v.name.toLowerCase().contains(widget.searchString)) {
                              widget.stations.remove(v.name);
                            }
                          });
                        }
                      });
                    }),
                Icon(lineStationsDisplay[line.name] ? Icons.arrow_drop_up : Icons.arrow_drop_down, color: Colors.black87),
              ]),
            ),
          ),
        ),
      ));
      resultSliverList.add(DiffUtilSliverList<Stations>(
        equalityChecker: (line1, line2) {
          // print('${line1.name} ${line2.name}');
          return line1.name == line2.name;
        },
        items: List.from(lineStationsDisplay[line.name] ? lineStationsList[line.name] : []),
        builder: (context, item) {
          print('build GestureDetector');
          if (item.name.toLowerCase().contains(widget.searchString)) {
            return GestureDetector(
              key: ValueKey(item.name),
              child: Container(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(32, 0, 8, 16),
                  child: Row(children: [
                    Flexible(child: Text(item.name)),
                    Checkbox(
                        value: widget.stations.contains(item.name),
                        onChanged: (bool val) {
                          setState(() {
                            if (val) {
                              widget.stations.add(item.name);
                            } else {
                              widget.stations.remove(item.name);
                            }
                          });
                        }),
                  ]),
                ),
              ),
            );
          }
          else{
            return Container();
          }
        },
        insertAnimationBuilder: (context, animation, child) => FadeTransition(
          opacity: animation,
          child: child,
        ),
        removeAnimationBuilder: (context, animation, child) => FadeTransition(
          opacity: animation,
          child: child,
        ),
        removeAnimationDuration: const Duration(milliseconds: 100),
        insertAnimationDuration: const Duration(milliseconds: 500),
      ));
    }
    resultSliverList.add(SliverToBoxAdapter(
      child: Container(
        child: Padding(
          padding: EdgeInsets.all(30),
        ),
      ),
    ));
    if (linesList.isNotEmpty) {
      return ScrollConfiguration(
        behavior: noHighlightScrollBehavior(context),
        child: CustomScrollView(
          slivers: [for (var item in resultSliverList) item],
        ),
      );
    } else {
      return Container(color: Colors.grey);
    }
  }
}
