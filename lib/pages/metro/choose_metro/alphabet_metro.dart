import 'package:diffutil_sliverlist/diffutil_sliverlist.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:partnerum/models/city_models.dart';
import 'package:partnerum/utils/theme_constants.dart';
import 'choose_metro_helper.dart';

class AlphabetMetro extends StatefulWidget {
  final List metroLines;
  final Set<String> stations;
  final String searchString;

  const AlphabetMetro(
      {Key key, this.metroLines, this.stations, this.searchString})
      : super(key: key);

  @override
  _AlphabetMetroState createState() => _AlphabetMetroState();
}

class _AlphabetMetroState extends State<AlphabetMetro> with ChooseMetroHelper {
  List linesList;

  var lineStationsList = <String, List>{};
  var lineStationsDisplay = <String, bool>{};
  var resultSliverList = <Widget>[];
  var allStations = <String>[];

  @override
  void initState() {
    linesList = widget.metroLines;

    for (var line in linesList) {
      for (var item in line.stations) {
        allStations.add(item.name);
      }
    }
    allStations.sort();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    resultSliverList = [];

    for (var station in allStations.toSet().toList()) {
      if (station.toLowerCase().contains(widget.searchString)) {
        resultSliverList.add(SliverToBoxAdapter(
          child: Padding(
            key: ValueKey(station),
            padding: EdgeInsets.symmetric(horizontal: 16.0),
            child: Row(children: [
              Flexible(child: Text(station)),
              Checkbox(
                  value: widget.stations.contains(station),
                  onChanged: (bool val) {
                    setState(() {
                      if (val) {
                        widget.stations.add(station);
                      } else {
                        widget.stations.remove(station);
                      }
                    });
                  }),
            ]),
          ),
        ));
      }
    }
    resultSliverList.add(SliverToBoxAdapter(
      child: Container(
        child: Padding(
          padding: EdgeInsets.all(30),
        ),
      ),
    ));

    if (linesList.isNotEmpty) {
      return ScrollConfiguration(
        behavior: noHighlightScrollBehavior(context),
        child: CustomScrollView(
          slivers: [for (var item in resultSliverList) item],
        ),
      );
    } else {
      return Container(color: Colors.grey);
    }
  }
}
