import 'package:after_layout/after_layout.dart';
import 'package:diffutil_sliverlist/diffutil_sliverlist.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:partnerum/head_module/gen_extensions/generated_str_extension.dart';
import 'package:partnerum/models/city_models.dart';
import 'package:partnerum/utils/brain_module.dart';
import 'package:partnerum/utils/theme_constants.dart';
import 'dart:math' as math;

import 'line_metro.dart';
import 'alphabet_metro.dart';
import 'choose_metro_helper.dart';
import 'round_metro.dart';

class ChooseMetro extends StatefulWidget {
  final String city;
  final Set<String> stations;

  const ChooseMetro({Key key, @required this.city, this.stations = const <String>{}}) : super(key: key);

  @override
  _ChooseMetroState createState() => _ChooseMetroState();
}

class _ChooseMetroState extends State<ChooseMetro> with AfterLayoutMixin<ChooseMetro>, ChooseMetroHelper {
  final _alphForm = GlobalKey<FormState>();
  final _lineForm = GlobalKey<FormState>();
  final _districtForm = GlobalKey<FormState>();
  var metroLines = <Lines>[];

  final TextEditingController _metroNameController = TextEditingController();
  Set<String> stations = {};
  String searchString;

  bool selected = false;

  Map<int, Widget> children = <int, Widget>{
    0: Text(Strings.pages.metro.chooseMetro.byAlphabet, style: TextStyle(fontSize: 14)),
    1: Text(Strings.pages.metro.chooseMetro.byLines, style: TextStyle(fontSize: 14)),
    2: Text(Strings.pages.metro.chooseMetro.byDistricts, style: TextStyle(fontSize: 14)),
  };

  Map<int, Widget> getChildren() {
    return <int, Widget>{0: Form(key: _alphForm, child: _buildAlph())};
  }

  final int _sharedValue = 0;

  @override
  Future<void> initState() {
    // TODO: implement initState
    super.initState();
    stations = widget.stations;
    // print(stations);

    mindClass.getCityMetros(widget.city).then((value) {
      for (var line in value.lines) {
        metroLines.add(line);
      }
    });

    // for (var line in allLines2) {
    //   sliverLines.add(line);
    // }

    _hideButtonController = ScrollController();
    _hideButtonController.addListener(() {
      if (_hideButtonController.position.userScrollDirection == ScrollDirection.reverse) {
        if (_isVisible == true) {
          /* only set when the previous state is false
             * Less widget rebuilds
             */

          setState(() {
            _isVisible = false;
          });
        }
      } else {
        if (_hideButtonController.position.userScrollDirection == ScrollDirection.forward) {
          if (_isVisible == false) {
            /* only set when the previous state is false
               * Less widget rebuilds
               */

            setState(() {
              _isVisible = true;
            });
          }
        }
      }
    });
  }

  generateList() async {
    toDraw = getChildren()[_sharedValue];
    setState(() {
      pageLoaded = true;
    });
  }

  Widget toDraw;
  ScrollController _hideButtonController;

  @override
  Widget build(BuildContext context) {
    print('redraw');
    // print(pageLoaded);
    var expanded = true;

    return DefaultTabController(
        length: 3,
        child: Scaffold(
            floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
            key: _scaffoldKey,
            appBar: AppBar(
                backgroundColor: Colors.white,
                leading: IconButton(
                    icon: Theme.of(context).platform == TargetPlatform.iOS ? Icon(Icons.arrow_back_ios) : Icon(Icons.arrow_back),
                    onPressed: () {
                      Navigator.of(context).pop();
//                if (stations == null || stations.length == 0) {
//                  Fluttertoast.showToast(
//                    textColor: Colors.white,
//                    backgroundColor: Colors.black54,
//                    msg: Strings.pages.metro.chooseMetro.chooseStations.l(context),
//                    toastLength: Toast.LENGTH_SHORT,
//                    gravity: ToastGravity.BOTTOM,
//                    timeInSecForIos: 1,
//                  );
//                  return;
//                }
                    }),
                bottom: TabBar(
                    indicatorColor: Theme.of(context).primaryColor,
                    tabs: [Tab(text: Strings.pages.metro.chooseMetro.byAlphabet.l(context)), Tab(text: Strings.pages.metro.chooseMetro.byLines.l(context)), Tab(text: Strings.pages.metro.chooseMetro.byDistricts.l(context))]),
                title: TextField(
                    decoration: InputDecoration(border: InputBorder.none, hintText: Strings.pages.metro.chooseMetro.searchByName.l(context)),
                    controller: _metroNameController,
                    onChanged: (value) {
                      setState(() {
                        searchString = value.toLowerCase();
                      });
                    }),
                automaticallyImplyLeading: true,
                iconTheme: IconThemeData(color: Colors.black),
                textTheme: TextTheme(headline6: TextStyle(color: Colors.black, fontSize: 20.0)),
                actions: <Widget>[
                  IconButton(
                      icon: Icon(Icons.clear),
                      onPressed: () {
                        _metroNameController.clear();
                      })
                ]),
            body: FutureBuilder(
                initialData: null,
                future: mindClass.getCityMetros(widget.city),
                builder: (context, snapshot) {
                  metroLines = snapshot?.data?.lines ?? [];
                  // print('sliverLines.length: ${metroLines.length}');
                  // print ('opened stations: ${stations}');
                  return TabBarView(key: ValueKey(metroLines.length), children: [
                    AlphabetMetro(metroLines: metroLines, stations: stations, searchString: searchString ?? ''),
                    LineMetro(metroLines: metroLines, stations: stations, searchString: searchString ?? ''),
                    RoundMetro(metroJson: snapshot?.data ?? JsonCity(), stations: stations, city: widget.city, searchString: searchString ?? ''),
                  ]);
                }),

            floatingActionButton: AnimatedOpacity(
                duration: Duration(milliseconds: 300),
                opacity: _isVisible ? 1 : 0,
                child: IgnorePointer(
                    ignoring: !_isVisible,
                    child: FloatingActionButton.extended(
                        label: Text(Strings.pages.metro.chooseMetro.addStations.l(context)),
                        //width: MediaQuery.of(context).size.width,

                        backgroundColor: Theme.of(context).primaryColor,
                        onPressed: () {
                          if (stations == null || stations.isEmpty) {
                            _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(Strings.pages.metro.chooseMetro.chooseStations.l(context)), duration: Duration(milliseconds: 300)));
                            return;
                          }
                          print('sent stations: ${stations}');
                          stations.removeWhere((value) => value == '');
                          Navigator.pop(context, stations.toList());
//            Fluttertoast.showToast(
//              textColor: Colors.white,
//              backgroundColor: Colors.black54,
//              msg: "${Strings.pages.metro.chooseMetro.metro.l(context)} (${stations.length})",
//              toastLength: Toast.LENGTH_SHORT,
//              gravity: ToastGravity.BOTTOM,
//              timeInSecForIos: 1,
//            );
                        })))));
  }

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  bool _isVisible = true;

  Map<String, ExpandableController> panels = {};

  ExpandablePanel _buildLineItem(BuildContext context, Lines line, index, [expand = false]) {
    var allStations = <Stations>[];
    if (_metroNameController.text.isNotEmpty) {
      var search = _metroNameController.text.toLowerCase();
      for (var i = 0; i < line.stations.length; i++) {
        var city = line.stations[i].name.toLowerCase();
        if (city.contains(search) || search.contains(city)) {
          allStations.add(line.stations[i]);
        }
      }
    } else {
      allStations = line.stations;
    }
    if (!panels.containsKey(line.name)) {
      panels[line.name] = ExpandableController(initialExpanded: false);
    }

    bool isLine() {
      for (var i = 0; i < allStations.length; i++) {
        if (!stations.contains(allStations[i].name)) return false;
      }
      return true;
    }

    return ExpandablePanel(
        //collapsed: Text('N', softWrap: true, maxLines: 2, overflow: TextOverflow.ellipsis,),
        header: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(children: <Widget>[
              Flexible(child: Text(line.name)),
              Checkbox(
                  value: isLine(),
                  onChanged: (bool val) {
                    setState(() {
                      if (val) {
                        allStations.forEach((v) {
                          stations.add(v.name);
                        });

                        print('1-> $stations');
                      } else {
                        allStations.forEach((v) {
                          stations.remove(v.name);
                        });
                        print('1-> $stations');
                      }
                      selected = val;
                    });
                  })
            ])),
        controller: panels[line.name],
        //isExpanded: expand ? roundsExpanded[index] : linesExpanded[index],
        expanded: ListView(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            children: <Widget>[for (var i = 0; i < allStations.length; i++) _buildSearchItem(context, allStations[i])]));
  }

  Widget _buildSearchItem(BuildContext context, Stations document) {
    return ListTile(
        leading: Container(
            height: 25,
            width: 25,
            decoration: BoxDecoration(color: document.color, shape: BoxShape.circle, border: Border.all(color: Colors.transparent))),
        title: Text('${document.name}', overflow: TextOverflow.fade),
        trailing: Checkbox(
            activeColor: Theme.of(context).primaryColor,
            value: stations.contains(document.name),
            onChanged: (bool val) {
              if (val) {
                stations.add(document.name);
                print('2-> $stations');
              } else {
                stations.remove(document.name);
                print('2-> $stations');
              }
              selected = val;
              setState(() {});
              // setState(() {
              //
              // });
            }));
  }

  Widget _buildAlph() {
    print('alphabet');
    return FutureBuilder<JsonCity>(
        future: mindClass.getCityMetros(widget.city),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Container(
                alignment: FractionalOffset.center,
                child: Center(child: Theme.of(context).platform == TargetPlatform.iOS ? CupertinoActivityIndicator() : CircularProgressIndicator()));
          }
          var allStations = <Stations>[];
          for (var i = 0, l = snapshot.data.lines.length; i < l; i++) {
            snapshot.data.lines[i].stations.forEach((value) {
              var _station = value;
              _station.color = Color(int.parse('0xff' + snapshot.data.lines[i].hexColor));
              _station.number = snapshot.data.lines[i].id;
              allStations.add(value);
            });
          }
          allStations.sort((s1, s2) {
            return s1.name.compareTo(s2.name);
          });
          return Padding(
              padding: const EdgeInsets.only(bottom: 40.0),
              child: ListView.builder(
                  shrinkWrap: true,
                  physics: ClampingScrollPhysics(),
                  itemCount: allStations.length,
                  itemBuilder: (context, index) => _buildSearchItem(context, allStations[index])));
        });
  }

  getMetros() {}

  bool pageLoaded = false;

  @override
  void afterFirstLayout(BuildContext context) {
//    setState(() {
//      print("yeah");
//      pageLoaded = true;
//    });
  }
}
