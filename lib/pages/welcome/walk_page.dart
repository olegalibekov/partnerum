import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:partnerum/models/walkthrough.dart';
import 'package:partnerum/pages/etc/login_page.dart';

import 'package:partnerum/head_module/gen_extensions/generated_files_extension.dart';


///TODO change with flutter_serve
/// text & icons
class WalkPage extends StatefulWidget {
  final List<Walkthrough> pages = [
    Walkthrough(
        title: 'Трудитесь в посуточной аренде квартир?',
        color: Colors.white,
        description: 'Тогда это приложение поможет Вам!',
        illustrationPath: files.images.undraw.apartmentRentSvg
        // illustration: UnDrawIllustration.apartment_rent
    ),
    Walkthrough(
        title: 'Привлекайте клиентов',
        color: Colors.white,
        description:
            'Работайте с максимальной заполняемостью квартир и зарабатывайте больше.',
        illustrationPath: files.images.undraw.confirmedSvg
        // illustration: UnDrawIllustration.confirmed
    ),
    Walkthrough(
        title: 'Зарабатывайте больше',
        color: Colors.white,
        description:
            'Зарабатывайте на лишних заявках 10-20% с общей суммы проживания.',
        illustrationPath: files.images.undraw.makeItRainSvg
        // illustrationPath: UnDrawIllustration.make_it_rain
    )
  ];

  @override
  _WalkPageState createState() => _WalkPageState();
}

class _WalkPageState extends State<WalkPage> {
  List<Widget> widgets;

  @override
  Widget build(BuildContext context) {
    widgets ??= pages(context);
    return Scaffold(
        body: Swiper.children(
            autoplay: false,
            index: 0,
            loop: false,
            pagination: SwiperPagination(
                margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 40.0),
                builder: DotSwiperPaginationBuilder(
                    color: Colors.black26,
                    activeColor: Colors.black,
                    size: 6.5,
                    activeSize: 8.0)),
            control: SwiperControl(iconPrevious: null, iconNext: null),
            children: widgets));
  }

  List<Widget> pages(BuildContext context) {
    var widgets = <Widget>[];
    for (var i = 0, l = widget.pages.length; i < l; i++) {
      var page = widget.pages[i];
      var pageWidget = Container(
          color: page.color,
          child: Stack(children: <Widget>[
            ///Виджет с картинкой
            Padding(
              padding: const EdgeInsets.all(48.0),
              child: Container(
                  child: Align(
                      alignment: Alignment.center,
                      child:
                      SvgPicture.asset(page.illustrationPath)
                      // UnDraw(
                      //     color: Theme.of(context).primaryColor,
                      //     illustration: page.illustration)
                  )),
            ),

            ///Виджет с титульным заголовком
            Container(
                margin: EdgeInsets.symmetric(vertical: 100),
                child: Align(
                    alignment: Alignment.topCenter,
                    child: Text(page.title,
                        softWrap: true,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Theme.of(context).primaryColor,
                            decoration: TextDecoration.none,
                            fontSize: 24.0,
                            fontWeight: FontWeight.w700,
                            fontFamily: 'Roboto')))),

            ///Виджет со вторым титульным заголовком
            Container(
                margin: EdgeInsets.symmetric(vertical: 100, horizontal: 16),
                child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Text(page.description,
                        softWrap: true,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.black,
                            decoration: TextDecoration.none,
                            fontSize: 15.0,
                            fontWeight: FontWeight.w300,
                            fontFamily: 'Roboto'))))
          ]));
      widgets.add(pageWidget);
    }
    widgets.add(Container(
        color: Colors.white,
        child: Stack(children: <Widget>[
          Container(
              margin: EdgeInsets.symmetric(vertical: 100),
              child: Align(
                  alignment: Alignment.topCenter,
                  child: Text('Добавляйте свои квартиры БЕСПЛАТНО прямо сейчас',
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          decoration: TextDecoration.none,
                          fontSize: 24.0,
                          fontWeight: FontWeight.w700,
                          fontFamily: 'Roboto')))),
          Padding(
            padding: const EdgeInsets.all(48.0),
            child: Container(
                child: Align(
                    alignment: Alignment.center,
                    child: SvgPicture.asset(
                        files.images.undraw.updateSvg)
                    // child: UnDraw(
                    //     color: Theme.of(context).primaryColor,
                    //     illustration: UnDrawIllustration.update)
                )),
          ),
          Container(
              margin: EdgeInsets.symmetric(vertical: 180),
              child: Align(
                  alignment: Alignment.topCenter,
                  child: Text('и получайте заявки от коллег',
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.black,
                          decoration: TextDecoration.none,
                          fontSize: 15.0,
                          fontWeight: FontWeight.w300,
                          fontFamily: 'Roboto')))),
          Container(
              margin: EdgeInsets.symmetric(vertical: 100),
              child: Align(
                  alignment: Alignment.bottomCenter,
                  child: FlatButton(
                      onPressed: () =>
                          Navigator.of(context).pushNamed('$LoginPage'),
                      color: Theme.of(context).primaryColor,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0)),
                      child: Text('Жмите',
                          style: TextStyle(
                              color: Colors.white,
                              decoration: TextDecoration.none,
                              fontSize: 18)))))
        ])));
    return widgets;
  }
}
