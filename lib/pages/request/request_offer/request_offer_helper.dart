import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:micro_utils/data_helper.dart';
import 'package:db_partnerum/db_partnerum.dart';
import 'package:partnerum/head_module/gen_extensions/generated_str_extension.dart';
import 'package:partnerum/utils/brain_module.dart';
import 'package:path/path.dart' as path;

class RequestStatus {
  String houseRequestSended = 'house_request_sended';
  String houseRequestCanceled = 'house_request_canceled';
  String houseRequestBooked = 'house_request_booked';

  String requestAccept = 'request_accept';
  String requestDenied = 'request_denied';
  String requestBooked = 'request_booked';
}

@DataHelper()
class RequestOfferHelper {
  OfferObject offerObject;

  RequestStatus requestStatus = RequestStatus();

  void initRequestOffer() {
    offerObject = root.chatTopic.offer2Room.push;
  }

  Future<void> uploadRequestOffer({Offer offer, RequestInformation requestInformation, String requestId}) async {
    var ownerId = offer.userId.value;
    var houseId = offer.houseId.value;
    initRequestOffer();
    await setRequestId(offerObject, requestId);
    await setConversationUsers(ownerId, houseId);
    addOfferObjectToRequest(offer, offerObject.key);
    setRequestUserStatus(requestStatus.houseRequestSended);
    // await createChat(ownerId);
    // setRequestCopy(requestInformation);
    mentionOfferObjectId(offer, offerObject.key);
    /*
    await setHouseCopy(houseInformation);
    addHouseRequests(ownerId, houseId);
    */
  }

  void mentionOfferObjectId(Offer offer, String offerObjectId) {
    offer.offerId.set(offerObjectId);
  }

  Future<void> declineRequestOffer(String offerId) async {
    root.chatTopic.offer2Room[offerId].conversationUsers.usersStatuses.requestUserStatus = 'house_request_canceled';
    // offer.offerId.remove();
  }

  Future<void> declineRequestOfferByKey(String offerObjectId) async {
    root.chatTopic.offer2Room[offerObjectId].conversationUsers.usersStatuses.requestUserStatus = 'house_request_canceled';
    // offerObject.offerId.remove();
  }

  Future<void> offerRequestByKey(String offerObjectId) async {
    root.chatTopic.offer2Room[offerObjectId].conversationUsers.usersStatuses.requestUserStatus = 'house_request_sended';
    // offerObject.offerId.remove();
  }

  Future<void> setConversationUsers(String ownerID, String houseId) async {
    await setRequestUser(mindClass.user.uid);
    setOwnerUser(ownerID, houseId);
  }

  Future<void> setRequestUser(String userId) async {
    await offerObject.conversationUsers.requestUser.set(userId);
  }

  void setOwnerUser(String userId, String houseId) {
    offerObject.conversationUsers.ownerUser = userId + ';' + houseId;
  }

  void setRequestUserStatus(String requestUserStatus) {
    offerObject.conversationUsers.usersStatuses.requestUserStatus = requestUserStatus;
  }

  void requestUserStatusByOfferId(String offerObjectId, String requestUserStatus) {
    root.chatTopic.offer2Room[offerObjectId].conversationUsers.usersStatuses.requestUserStatus = requestUserStatus;
  }

  Future<String> createChat(String ownerId, String offerObjectId) async {
    var existChat = await root?.chatTopic?.offer2Room[offerObjectId]?.conversationUsers?.chatId?.future;

    if (existChat?.value == null) {
      var newChat = root.chatTopic.userToUser.push;
      await newChat.header.type.set('offer2room');

      newChat.header.offerObjectId = offerObjectId;
      root.chatTopic.offer2Room[offerObjectId].conversationUsers.chatId = newChat.key;
      return newChat.key;
    }
    else{
      return existChat.value;
    }
  }

  void enterInChat(String chatId) {
    root.userData[mindClass.user.uid].activeChats[chatId].set(true);
    root.userData[mindClass.user.uid].userChats.otr[chatId].status.set('read');
    root.userData[mindClass.user.uid].unreadChats[chatId].remove();
  }

  void addHouseRequests(String userID, String houseID) {
    var newObject = root.userData[userID].houses[houseID].houseRequests.push;
    newObject.set(offerObject.key);
  }

  void setStations(var object, Iterable stations) {
    // var stationNum = 0;
    for (DbString station in stations) {
      object.stations[station.key] = station.key;
      // stationNum += 1;
    }
  }

  Future<void> setRequestId(OfferObject offerObject, String requestId) async {
    print('requestId: ${requestId}');
    await offerObject.requestId.set(requestId);
  }

  void setRequestCopy(RequestInformation requestInformation) {
    offerObject.requestCopy.city = requestInformation.city.value;
    offerObject.requestCopy.commision = requestInformation.commision.value;
    offerObject.requestCopy.datesRange.first = requestInformation.datesRange.first.value.inMilliseconds;
    offerObject.requestCopy.datesRange.last = requestInformation.datesRange.last.value.inMilliseconds;
    offerObject.requestCopy.description = requestInformation.description.value;
    offerObject.requestCopy.guests = requestInformation.guests.value;
    offerObject.requestCopy.timeToMetro = requestInformation.timeToMetro.value.inMilliseconds;
    offerObject.requestCopy.position.latitude = requestInformation.position.latitude.value;
    offerObject.requestCopy.position.longitude = requestInformation.position.longitude.value;
    offerObject.requestCopy.rooms = requestInformation.rooms.value;
    setStations(offerObject.requestCopy, requestInformation.stations.values);
    offerObject.requestCopy.isArchived = requestInformation.isArchived.value;
    offerObject.requestCopy.priceRange.startPosition = requestInformation.priceRange.startPosition.value;
    offerObject.requestCopy.priceRange.endPosition = requestInformation.priceRange.endPosition.value;
    offerObject.requestCopy.isArchived = false;
  }

/*
  Future<void> setHouseCopy(HouseInformation houseInformation) async {
    offerObject.houseCopy.address = houseInformation.address.value;
    offerObject.houseCopy.city = houseInformation.city.value;
    offerObject.houseCopy.description = houseInformation.description.value;
    offerObject.houseCopy.timeToMetro =
        houseInformation.timeToMetro.value.inMinutes;
    offerObject.houseCopy.priceRange.startPosition =
        houseInformation.priceRange.startPosition.value;
    offerObject.houseCopy.priceRange.endPosition =
        houseInformation.priceRange.endPosition.value;

    houseInformation.photos.values.forEach((element) async {
      var photoFuture = await element.future;
      var photoCopy = offerObject.houseCopy.photos.push;
      await photoCopy.set(FileWrapper(path.basename(photoFuture.bigImg.value),
          photoFuture.bigImg.uint8list));
    });

    offerObject.houseCopy.rooms = houseInformation.rooms.value;
    setStations(offerObject.houseCopy, houseInformation.stations.values);
    offerObject.houseCopy.isArchived = houseInformation.isArchived.value;
  }
*/
  void addOfferObjectToRequest(Offer offer, String offerObjectKey) {
    offer.offerId.set(offerObjectKey);
  }

  void setOwnerStatus(OfferObject offerObject, String status) {
    offerObject.conversationUsers.usersStatuses.ownerUserStatus = status;
  }

  void archiveRequest(OfferObject offerObject) {
    offerObject.requestCopy.isArchived = true;
    var conversationUsers = offerObject.conversationUsers;

    if (conversationUsers.usersStatuses.requestUserStatus.value != 'house_request_booked' &&
        conversationUsers.usersStatuses.ownerUserStatus.value.split(';').first != 'request_booked') {
      setOwnerStatus(offerObject, 'request_denied');
    }
  }

  void setUnArchived(OfferObject offerObject) {
    offerObject.requestCopy.isArchived = false;
  }

  Future<ConversationUsers> getOwnerStatus(String offerId) {
    // print('offerId: ${offerId}');
    return root.chatTopic.offer2Room[offerId].conversationUsers.future;
  }

  Stream<ConversationUsers> conversationUsers(String offerId) {
    // print('offerId: ${offerId}');
    return root.chatTopic.offer2Room[offerId].conversationUsers.stream;
  }

  Stream ownerStatusStream(String offerId) {
    return root.chatTopic.offer2Room[offerId].conversationUsers.usersStatuses.ownerUserStatus.stream;
  }

  Stream chatIdStream(String offerId) {
    return db.root.chatTopic.offer2Room[offerId].conversationUsers.chatId.stream;
  }

  // Stream chatId(String offerId) {
  //   return db
  //       .root.chatTopic.offer2Room[offerId].conversationUsers.chatId.stream;
  // }

  Stream requestUserStatusStream(String offerId) {
    return root.chatTopic.offer2Room[offerId].conversationUsers.usersStatuses.requestUserStatus.stream;
  }

  // messageOfSharing(House house) {
  //   var sharingMessage = housesModel.HouseInfo(
  //     city: house.houseInformation.city.value,
  //     stations: stationsToList(house.houseInformation.stations),
  //     extraInformation: house.houseInformation.description.value,
  //     priceRange: [
  //       house.houseInformation.priceRange.startPosition.value,
  //       house.houseInformation.priceRange.endPosition.value
  //     ],
  //     address: house.houseInformation.address.value,
  //     rooms: house.houseInformation.rooms.value.toString(),
  //     // photoUrls: ,
  //     minutesToMetro:
  //         house.houseInformation.timeToMetro.value.inMilliseconds.toString(),
  //     // latLon:
  //   ).toExportableJson();
  //
  //   return sharingMessage;
  // }

  String messageOfSharing(HouseInformation houseInformation, BuildContext context) {
    var listOfStations = (houseInformation.stations.values.map((e) => e.key)).toList();
    var export = '';
    if (houseInformation.description.value.isNotEmpty) {
      export += '${Strings.pages.requestOfferPage.requestOfferHelper.aboutThePlace.l(context)}: ${houseInformation.description.value}\n';
    }
    export += '${Strings.pages.requestOfferPage.requestOfferHelper.city.l(context)}: ${houseInformation.city.value}\n';
    if (listOfStations != null && listOfStations.isNotEmpty) {
      export += '${Strings.pages.requestOfferPage.requestOfferHelper.nearbyStations.l(context)}: ${listOfStations.join(', ')}\n';
      export += '${houseInformation.timeToMetro.value.inMinutes} ${Strings.pages.requestOfferPage.requestOfferHelper.timeToMetro.l(context)}\n';
    }
    if (houseInformation.address.value != null &&
        houseInformation.address.value.isNotEmpty &&
        houseInformation.address.value != Strings.pages.requestOfferPage.requestOfferHelper.noAddress.l(context)) {
      export += '${Strings.pages.requestOfferPage.requestOfferHelper.address.l(context)}: ${houseInformation.address.value}\n';
    }
    if (houseInformation.rooms.value != 5) {
      export += '${Strings.pages.requestOfferPage.requestOfferHelper.rooms.l(context)}: ${houseInformation.rooms.value}\n';
    } else {
      export += '${Strings.pages.requestOfferPage.requestOfferHelper.moreThanXRooms.l(context)}\n';
    }
    return export;
  }

  Future<List<Uint8List>> uint8ListOfImages(requestImages) async {
    if (requestImages == null || requestImages.isEmpty) return [];
    var imagesUint8List = <Uint8List>[];

    for (DbString requestImage in requestImages) {
      var imageFuture = await root?.imagesContainer[requestImage.value]?.bigImg?.future;
      var imageBytes = imageFuture?.uint8list;
      // print('Current data is: ${imageBytes}');
      if (imageBytes != null) imagesUint8List.add(imageBytes);
    }
    return imagesUint8List;
  }

  Widget decoratedImage(ImageProvider imageProvider) {
    return Container(
        width: 50,
        height: 50,
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(image: imageProvider, fit: BoxFit.cover, colorFilter: ColorFilter.mode(Colors.white, BlendMode.colorBurn))));
  }

// Future<List<Offer>> filterOffers(Request request) async {
//   if(request?.offers?.values == null) return null;
//
//   var filteredOffers = request?.offers?.values?.toList();
//
//   var requestCheckIn = request.requestInformation?.datesRange?.first?.value;
//   var requestCheckOut = request.requestInformation?.datesRange?.last?.value;
//
//
//   var intRequestCheckIn = int.parse(requestCheckIn);
//   var intRequestCheckOut = int.parse(requestCheckOut);
//
//   var offerIndex = 0;
//
//   for (var offer in request.offers.values) {
//     offerIndex += 1;
//
//     /// element.userId???
//     var houseBookingDates = await root.userData[offer.userId.value]
//         .houses[offer.houseId.value]?.bookingDates?.future;
//
//     if (houseBookingDates?.values != null) {
//       for (var houseBookingDate in houseBookingDates.values) {
//         var elementCheckIn = houseBookingDate.checkIn.value.inMilliseconds;
//         var elementCheckOut = houseBookingDate.checkOut.value.inMilliseconds;
//         if (intRequestCheckIn < elementCheckOut &&
//             elementCheckIn < intRequestCheckOut) {
//           filteredOffers.removeAt(offerIndex);
//           break;
//         }
//       }
//     }
//   }
//
//   return filteredOffers;
// }

  Future ifIntersection() {

  }
}
