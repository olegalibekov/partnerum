import 'dart:io';
import 'dart:typed_data';

import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:partnerum/head_module/gen_extensions/generated_str_extension.dart';
import 'package:partnerum/utils/theme_constants.dart';

enum ShareType { all, text, images }

share(text, List<Uint8List> imagesByteList, BuildContext context) async {
  Future<void> _share(ShareType shareType) async {
    if (shareType == ShareType.text) {
      Share.text('', text, 'text/plain');
      return;
    }
    var files = <String, Uint8List>{};
    for (var i = 0; i < imagesByteList.length; i++) {
      // var request = await HttpClient().getUrl(Uri.parse(urls[i]));
      // var response = await request.close();
      // var bytes = await consolidateHttpClientResponseBytes(response);
      files['file_$i.jpg'] = imagesByteList[i];
    }
    if (shareType == ShareType.images) {
      await Share.files('about', files, 'image/jpg');
      return;
    }
    await Share.files('about', files, 'image/jpg', text: text);
  }

  if (imagesByteList.length == 1) {
    await _share(ShareType.all);
    return;
  } else {
    await showModalBottomSheet(
        shape: RoundedRectangleBorder(borderRadius: borderRadius),
        context: context,
        builder: (_context) {
          return Padding(
              padding: const EdgeInsets.all(16.0),
              child: Container(
                  child:
                      Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                ListTile(
                    title: Text(Strings.pages.requestOfferPage.share.title.l(context)),
                    //contentPadding: EdgeInsets.all(8),
                    subtitle: Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: Text(Strings.pages.requestOfferPage.share.whatsappIssue.l(context)))),
                SizedBox(height: 16 * 2.0),
                Container(
                    width: double.maxFinite,
                    child: Wrap(
                        alignment: WrapAlignment.end,
                        spacing: 8,
                        children: <Widget>[
                          FlatButton(
                              color: Theme.of(context).primaryColor,
                              child: Text(Strings.pages.requestOfferPage.share.fullExport.l(context),
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.white)),
                              onPressed: () async {
                                await _share(ShareType.all);
                                Navigator.of(context).pop();
                              }),
                          FlatButton(
                              color: Theme.of(context).primaryColor,
                              onPressed: () => _share(ShareType.images),
                              child: Text(Strings.pages.requestOfferPage.share.onlyPhotos.l(context),
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.white))),
                          FlatButton(
                              color: Theme.of(context).primaryColor,
                              onPressed: () => _share(ShareType.text),
                              child: Text(Strings.pages.requestOfferPage.share.onlyText.l(context),
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.white)))
                        ]))
              ])));
        });
  }
}

// share(text, List<String> urls, BuildContext context) async {
//   _share(ShareType shareType) async {
//     if (shareType == ShareType.text) {
//       Share.text('', text, 'text/plain');
//       return;
//     }
//     var files = <String, Uint8List>{};
//     for (var i = 0; i < urls.length; i++) {
//       var request = await HttpClient().getUrl(Uri.parse(urls[i]));
//       var response = await request.close();
//       var bytes = await consolidateHttpClientResponseBytes(response);
//       files['file_$i.jpg'] = bytes;
//     }
//     if (shareType == ShareType.images) {
//       await Share.files('about', files, 'image/jpg');
//       return;
//     }
//     await Share.files('about', files, 'image/jpg', text: text);
//   }
//
//   if (urls.length == 1) {
//     await _share(ShareType.all);
//     return;
//   } else {
//     showModalBottomSheet(
//         shape: RoundedRectangleBorder(borderRadius: borderRadius),
//         context: context,
//         builder: (_context) {
//           return Padding(
//               padding: const EdgeInsets.all(16.0),
//               child: Container(
//                   child:
//                       Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
//                 ListTile(
//                     title: Text('Экспорт нескольких фотографий и текста'),
//                     //contentPadding: EdgeInsets.all(8),
//                     subtitle: Padding(
//                         padding: const EdgeInsets.only(top: 8.0),
//                         child: Text(
//                             'К сожалению, если Ваш клиент находится в Whatsapp, мы не можем ему сразу отправить несколько фотографий, а также текст. Поэтому, если это так воспользуйтесь раздельным поиском ниже. В противном случае, можете использовать полный экспорт.'))),
//                 SizedBox(height: 16 * 2.0),
//                 Container(
//                     width: double.maxFinite,
//                     child: Wrap(
//                         alignment: WrapAlignment.end,
//                         spacing: 8,
//                         children: <Widget>[
//                           FlatButton(
//                               color: Theme.of(context).primaryColor,
//                               child: Text('Экспорт всего',
//                                   style: TextStyle(
//                                       fontSize: 16, color: Colors.white)),
//                               onPressed: () async {
//                                 await _share(ShareType.all);
//                                 Navigator.of(context).pop();
//                               }),
//                           FlatButton(
//                               color: Theme.of(context).primaryColor,
//                               onPressed: () => _share(ShareType.images),
//                               child: Text('Экспорт только фотографий',
//                                   style: TextStyle(
//                                       fontSize: 16, color: Colors.white))),
//                           FlatButton(
//                               color: Theme.of(context).primaryColor,
//                               onPressed: () => _share(ShareType.text),
//                               child: Text('Экспорт только текста',
//                                   style: TextStyle(
//                                       fontSize: 16, color: Colors.white)))
//                         ]))
//               ])));
//         });
//   }
// }
