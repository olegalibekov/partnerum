import 'dart:async';
import 'dart:io';
import 'package:db_partnerum/db_partnerum.dart';
import 'package:flutter/rendering.dart';
import 'package:partnerum/head_module/gen_extensions/generated_str_extension.dart';
import 'package:partnerum/pages/extra/get_city_name.dart';
import 'package:partnerum/pages/extra/get_metro_name.dart';
import 'package:partnerum/pages/metro/choose_metro/choose_metro_page.dart';
import 'package:partnerum/utils/brain_module.dart';
import 'package:partnerum/utils/theme_constants.dart';
import 'package:partnerum/widgets/partnerum_icons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:date_range_picker/date_range_picker.dart' as DateRagePicker;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/date_symbol_data_local.dart';

import 'request_create_helper.dart';

class RequestCreate extends StatefulWidget {
  ///mindClass.writeUserRequest();

  final String requestId;
  final bool firstLoad;
  final Request request;

  const RequestCreate(
      {Key key, @required this.requestId, this.firstLoad = true, this.request})
      : super(key: key);

  @override
  _RequestCreateState createState() => _RequestCreateState();
}

class _RequestCreateState extends State<RequestCreate>
    with RequestCreateHelper {
//  final Firestore _db = Firestore.instance;
//  final FirebaseMessaging _fcm = FirebaseMessaging();
//
//  StreamSubscription iosSubscription;

  bool firstLoad = true;
  final _formKey = GlobalKey<FormState>();

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  String query = '';

  int _startPrice = 0;

  int _endPrice = 0;

  String _phoneNumber;

  String searchString;

  final String _roomsCount = '1';
  final int _countValue = 0;

  bool circleLoading;

  SharedPreferences prefs;
  String userImage = '';
  bool isLoading = false;
  File file;

  DateFormat formatDates;
  bool _isVisible = true;

  String title;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    circleLoading = false;
    initializeDateFormatting();
    initRequest(widget.requestId);
    _hideButtonController = ScrollController();
    _hideButtonController.addListener(() {
      if (_hideButtonController.position.userScrollDirection ==
          ScrollDirection.reverse) {
        if (_isVisible == true) {
          /* only set when the previous state is false
             * Less widget rebuilds
             */

          setState(() {
            _isVisible = false;
          });
        }
      } else {
        if (_hideButtonController.position.userScrollDirection ==
            ScrollDirection.forward) {
          if (_isVisible == false) {
            /* only set when the previous state is false
               * Less widget rebuilds
               */

            setState(() {
              _isVisible = true;
            });
          }
        }
      }
    });
  }

  setupIt() {}
  ScrollController _hideButtonController;

  @override
  Widget build(BuildContext context) {
    Request tmpRequest;
    if (widget.request != null) {
      tmpRequest = widget.request;
    }
    return FutureBuilder(
        /*
        future: mindClass.getUserRequest(widget.requestId),

       */
        // future: requestFuture(requestID: widget.requestId, newRequest: widget.firstLoad),
        future: futureRequest,
        initialData: tmpRequest,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Container(
                color: Colors.white,
                alignment: FractionalOffset.center,
                child: Center(child: Text('${Strings.globalOnes.error.l(context)}: ${snapshot.error}')));
          }
          // document = snapshot.data;


          if (document != null) {
            if (document.requestInformation != null) {
              /*
              title =
                  '${DateFormat('dd MMM', 'ru').format(dateFormat.parse(document.info.datesRange.first)).toString()} - ${DateFormat('dd MMM', 'ru').format(dateFormat.parse(document.info.datesRange.last)).toString()}';

               */

              if (firstLoad) {
                /*
                _orderCityNameController.text = document.info.city;
                _orderMetroNameController.text =
                    document.info.stations.join(', ');
                _timeValue = int.parse(document.info.minutesToMetro ?? '0');
                roomsList = document.info.rooms.toList();
                _guestsValue = (int.parse(document.info.guests));
                 */

                title = requestTitle(context);
                orderCityNameController.text = requestCity();
                roomsList = requestRoomsList();
                guestsValue = requestGuests();
                guestsCount =
                    (guestsValue - 1 < 4) ? guestsValue.toString() : Strings.pages.requestsPage.requestCreatePage.moreThan4.l(context);
                stations = requestStations();
                orderMetroNameController.text = stations.join(', ');
                distanceTimeValue = requestTimeToMetro();
                // orderArriveDateController.text = requestStartDates();

                var startDates = requestStartDates();
                if (startDates == '') {
                  orderArriveDateController.text = '';
                } else {
                  orderArriveDateController.text = DateFormat('dd MMM', 'ru')
                      .format(DateTime.fromMillisecondsSinceEpoch(
                          int.parse(startDates)))
                      .toString();
                }

                var endDates = requestEndDates();
                if (endDates == '') {
                  orderDepartureDateController.text = '';
                } else {
                  orderDepartureDateController.text = DateFormat('dd MMM', 'ru')
                      .format(DateTime.fromMillisecondsSinceEpoch(
                          int.parse(endDates)))
                      .toString();
                }

                // orderDepartureDateController.text = requestEndDates();
                orderStartPriceController.text = requestStartPrice();
                orderEndPriceController.text = requestEndPrice();
                if (orderStartPriceController.text != ''){
                  _startPrice = int.parse(orderStartPriceController.text);
                  _endPrice = int.parse(orderEndPriceController.text);
                }
                orderInfoFieldController.text = requestDescription();
                commissionValue = requestCommission();

                compareOrderCityName = orderCityNameController.text;
                compareOrderRoomsList = [];
                compareOrderRoomsList.addAll(roomsList);
                compareOrderGuestsCount = '0';
                compareOrderStations = stations;
                compareOrderDistanceTimeValue = 0;
                compareOrderArriveDate = orderArriveDateController.text;
                compareOrderDepartureDate = orderDepartureDateController.text;
                compareOrderStartPrice = orderStartPriceController.text;
                compareOrderEndPrice = orderEndPriceController.text;
                compareOrderInfoField = orderInfoFieldController.text;
                compareOrderCommissionValue = commissionValue;

                firstLoad = false;
              }
            }
          }

          // return Container(color: Colors.red);

          print('Title is: ${title}');
          return Scaffold(
              key: _scaffoldKey,
              backgroundColor: Colors.white,
              body: ScrollConfiguration(
                  behavior: noHighlightScrollBehavior(context),
                  child: CustomScrollView(slivers: [
                    SliverAppBar(
                        backgroundColor: Colors.white,
                        pinned: true,
                        leading: IconButton(
                            icon: Icon(Icons.close),
                            onPressed: () => resetEverything()),
                        title: Text(title ?? Strings.pages.requestsPage.requestCreatePage.requestCreateHelper.newRequest.l(context)),
                        centerTitle: true,
                        iconTheme: IconThemeData(color: blackColor),
                        textTheme: TextTheme(
                            title: TextStyle(
                                color: Colors.black, fontSize: 20.0))),
                    SliverToBoxAdapter(
                        child: Form(
                            key: _formKey,
                            child: Column(children: <Widget>[
                              if (circleLoading) loadingChats(context),
                              _buildCityName(),
                              _buildMetroName(document),
                              Container(
                                margin:
                                    const EdgeInsets.symmetric(vertical: 5.0),
                                child: _buildDistanceTime(),
                              ),
                              Container(
                                  margin:
                                      const EdgeInsets.symmetric(vertical: 5.0),
                                  child: _buildRoomsCount()),
                              Container(
                                  margin:
                                      const EdgeInsets.symmetric(vertical: 5.0),
                                  child: _buildGuestsCount()),
                              _buildDate(context),
                              Container(
                                  margin: const EdgeInsets.symmetric(
                                      horizontal: 16.0),
                                  child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: <Widget>[
                                        Flexible(
                                            child: _buildStartPriceField()),
                                        Flexible(child: _buildEndPriceField())
                                      ])),
                              Container(
                                  margin: const EdgeInsets.symmetric(
                                      horizontal: 16.0),
                                  child: _buildInfoField()),
                              _buildCommissionSize(),
                              SizedBox(height: 80)
                            ])))
                  ])),
              floatingActionButtonLocation:
                  FloatingActionButtonLocation.centerFloat,
              floatingActionButton: AnimatedOpacity(
                  duration: Duration(milliseconds: 300),
                  opacity: _isVisible ? 1 : 0,
                  child: IgnorePointer(
                      ignoring: !_isVisible,
                      child: FloatingActionButton.extended(

                          //width: MediaQuery.of(context).size.width,

                          backgroundColor: Theme.of(context).primaryColor,
                          onPressed: () async {
                            print('_startPrice, _endPrice: ${_startPrice} $_endPrice');
                            if (_startPrice > _endPrice) {
                              _scaffoldKey.currentState.showSnackBar(SnackBar(
                                  content: Text(Strings.pages.requestsPage.requestCreatePage.priceIssueSnackBar.l(context))));
                            } else {
                              // loadingChats;

                              if (_formKey.currentState.validate()) {
                                setState(() {
                                  circleLoading = true;
                                });
                                await saveWithOrderToken();
                                // uploadDoc();
                                Navigator.of(context).pop();

                                await Fluttertoast.showToast(
                                    textColor: Colors.white,
                                    backgroundColor: Colors.black54,
                                    msg: Strings.pages.requestsPage.requestCreatePage.requestAddedSnackBar.l(context),
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.BOTTOM,
                                    timeInSecForIosWeb: 1);
                              } else {
                                _scaffoldKey.currentState.showSnackBar(SnackBar(
                                    content: Text(
                                        Strings.pages.requestsPage.requestCreatePage.notEnoughInfo.l(context))));
                              }
                            }
                          },
                          label: Text(widget.requestId.isEmpty
                              ? Strings.pages.requestsPage.requestCreatePage.addRequest.l(context).toUpperCase()
                              : Strings.pages.requestsPage.requestCreatePage.changeRequest.l(context).toUpperCase())))));
        });
  }

  Widget _buildCityName() {
    return ListTile(
        leading: Icon(Partnerum.city, color: blackColor),
        title: InkWell(
            child: IgnorePointer(
                child: TextFormField(
          autofocus: false,
          onFieldSubmitted: (term) {
            SystemChannels.textInput.invokeMethod('TextInput.hide');
          },
          decoration: InputDecoration(hintText: Strings.pages.requestsPage.requestCreatePage.city.l(context)),
          controller: orderCityNameController,
          validator: (value) {
            if (value.isEmpty) {
              return Strings.pages.requestsPage.requestCreatePage.necessarily.l(context);
            }
            return null;
          },
        ))),
        onTap: () async {
          var city = await Navigator.push(
              context, CupertinoPageRoute(builder: (context) => GetCityName()));
          if (city.isNotEmpty) {
            orderCityNameController.text = city;
            orderMetroNameController.text = '';
            _scaffoldKey.currentState.showSnackBar(SnackBar(
                content: Text('${Strings.pages.requestsPage.requestCreatePage.choseCityMessage.l(context)}: ${orderCityNameController.text}'),
                duration: Duration(milliseconds: 300)));
            _formKey.currentState.validate();
          }
          setState(() {});
        });
  }

  Widget _buildMetroName(Request request) {
    if (orderMetroNameController.text.isEmpty) {
      if (orderCityNameController.text.isEmpty) return Container();
      if (!mindClass.cityMetro.containsKey(orderCityNameController.text)) {
        return Container();
      }
    }
    return ListTile(
        leading: Icon(Partnerum.metro, color: blackColor),
        title: InkWell(
            child: IgnorePointer(
                child: TextFormField(
                    maxLines: null,
                    autofocus: false,
                    onFieldSubmitted: (term) {
                      SystemChannels.textInput.invokeMethod('TextInput.hide');
                    },
                    decoration: InputDecoration(hintText: Strings.pages.requestsPage.requestCreatePage.metro.l(context)),
                    controller: orderMetroNameController,
                    validator: (value) {
                      if (value.isEmpty) {
                        return Strings.pages.requestsPage.requestCreatePage.necessarily.l(context);
                      }
                      return null;
                    }))),
        onTap: () async {
          var _stations = await Navigator.push(
              context,
              CupertinoPageRoute(
                // builder: (context) => GetMetroName(city: _orderCityNameController.text, stations: stations.toList()),
                builder: (context) => ChooseMetro(
                    city: orderCityNameController.text,
                    stations: stations.toSet()),
              ));
          if (_stations == null) return;
          if (_stations.length > 0) stations = _stations;
          if (stations.join(', ').isNotEmpty) {
            _scaffoldKey.currentState.showSnackBar(SnackBar(
                content: Text('${Strings.pages.requestsPage.requestCreatePage.choseMetroMessage.l(context)}: ${stations.join(', ')}'),
                duration: Duration(milliseconds: 300)));
          }
          orderMetroNameController.text = stations.join(', ');
          _formKey.currentState.validate();
        });
  }

  Widget _buildDistanceTime() {
    if (orderCityNameController.text.isEmpty) return Container();
    if (!mindClass.cityMetro.containsKey(orderCityNameController.text)) {
      return Container();
    }

    return ExpansionTile(
        leading: Icon(Partnerum.walk, color: blackColor),
        title: Text('${Strings.pages.requestsPage.requestCreatePage.timeToMetro.l(context)} - $distanceTimeValue ${Strings.pages.requestsPage.requestCreatePage.timeMeasure.l(context)}',
            style: TextStyle(fontSize: 16, color: Colors.black)),
        trailing: Icon(Icons.arrow_drop_down, color: blackColor),
        children: <Widget>[
          Center(
              child: SizedBox(
                  height: 80.0,
                  child: ListView(
                      physics: ClampingScrollPhysics(),
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      children: <Widget>[
                        Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Radio(
                                  activeColor: Theme.of(context).primaryColor,
                                  value: 5,
                                  groupValue: distanceTimeValue,
                                  onChanged: _handleTimeValueChange),
                              Text('5')
                            ]),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Radio(
                                  activeColor: Theme.of(context).primaryColor,
                                  value: 10,
                                  groupValue: distanceTimeValue,
                                  onChanged: _handleTimeValueChange),
                              Text('10')
                            ]),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Radio(
                                  activeColor: Theme.of(context).primaryColor,
                                  value: 15,
                                  groupValue: distanceTimeValue,
                                  onChanged: _handleTimeValueChange),
                              Text('15')
                            ]),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Radio(
                                  activeColor: Theme.of(context).primaryColor,
                                  value: 20,
                                  groupValue: distanceTimeValue,
                                  onChanged: _handleTimeValueChange),
                              Text('20')
                            ]),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Radio(
                                  activeColor: Theme.of(context).primaryColor,
                                  value: 25,
                                  groupValue: distanceTimeValue,
                                  onChanged: _handleTimeValueChange),
                              Text('25')
                            ])
                      ])))
        ]);
  }

  void _handleTimeValueChange(int value) {
    print(value);
    setState(() {
      distanceTimeValue = value;
    });
  }

  Widget _buildRoomsCount() {
    return ExpansionTile(
        leading: Icon(
          Partnerum.rooms,
          color: blackColor,
        ),
        title: roomsList.isNotEmpty
            ? Text('${Strings.pages.requestsPage.requestCreatePage.roomsAmount.l(context)} - ${roomsList.join(', ')}',
                style: TextStyle(fontSize: 16, color: Colors.black))
            : Text('${Strings.pages.requestsPage.requestCreatePage.roomsAmount.l(context)} - 0',
                style: TextStyle(fontSize: 16, color: Colors.black)),
        trailing: Icon(Icons.arrow_drop_down, color: blackColor),
        children: <Widget>[
          Center(
              child: SizedBox(
                  height: 80.0,
                  child: ListView(
                      physics: ClampingScrollPhysics(),
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      children: <Widget>[
                        checkboxRow('1'),
                        checkboxRow('2'),
                        checkboxRow('3'),
                        checkboxRow('4'),
                        checkboxRow(Strings.pages.requestsPage.requestCreatePage.neverMind.l(context)),
                      ])))
        ]);
  }

  Widget _buildGuestsCount() {
    return ExpansionTile(
        leading: Icon(Partnerum.guest, color: blackColor),
        title: Text('${Strings.pages.requestsPage.requestCreatePage.guestsAmount.l(context)} - $guestsCount',
            style: TextStyle(fontSize: 16, color: Colors.black)),
        trailing: Icon(Icons.arrow_drop_down, color: blackColor),
        children: <Widget>[
          Center(
              child: SizedBox(
                  height: 80.0,
                  child: ListView(
                      physics: ClampingScrollPhysics(),
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      children: <Widget>[
                        guestValueChangeRow(value: 1),
                        guestValueChangeRow(value: 2),
                        guestValueChangeRow(value: 3),
                        guestValueChangeRow(value: 4),
                        guestValueChangeRow(value: 5),
                      ])))
        ]);
  }

  Row checkboxRow(String s) {
    return Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
      Checkbox(
          activeColor: Theme.of(context).primaryColor,
          value: roomsList.contains(s),
          onChanged: (bool val) {
            setState(() {
              if (val) {
                roomsList.add(s);
              } else {
                roomsList.remove(s);
              }
              roomSelected = val;
            });
          }),
      Text(s)
    ]);
  }

  Row guestValueChangeRow({int value}) {
    const moreValueText = 5;
    return Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
      Radio(
          activeColor: Theme.of(context).primaryColor,
          value: value,
          groupValue: guestsValue,
          onChanged: _handleGuestsCountValueChange),
      Text((value != moreValueText) ? value.toString() : Strings.pages.requestsPage.requestCreatePage.more.l(context))
    ]);
  }

  Row commissionValueChangeRow({int value}) {
    return Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
      Radio(
          activeColor: Theme.of(context).primaryColor,
          value: value,
          groupValue: commissionValue,
          onChanged: _handleCommissionValueChange),
      Text(value.toString())
    ]);
  }

  void _handleGuestsCountValueChange(int value) {
    const edgeValue = 4;
    setState(() {
      guestsValue = value;
      guestsCount = (guestsValue - 1 < edgeValue)
          ? guestsValue.toString()
          : '${Strings.pages.requestsPage.requestCreatePage.more.l(context)} ${edgeValue}';
    });
  }

  Widget _buildCommissionSize() {
    return ExpansionTile(
        leading: Icon(Partnerum.percent, color: blackColor),
        title: Text('${Strings.pages.requestsPage.requestCreatePage.comissionSize.l(context)} - $commissionValue %',
            style: TextStyle(fontSize: 16, color: Colors.black)),
        trailing: Icon(Icons.arrow_drop_down, color: blackColor),
        children: <Widget>[
          Center(
              child: SizedBox(
                  height: 60.0,
                  child: ListView(
                      physics: ClampingScrollPhysics(),
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      children: <Widget>[
                        commissionValueChangeRow(value: 10),
                        commissionValueChangeRow(value: 15),
                        commissionValueChangeRow(value: 20),
                        commissionValueChangeRow(value: 25),
                      ])))
        ]);
  }

  void _handleCommissionValueChange(int value) {
    setState(() {
      commissionValue = value;
    });
  }

  Widget _buildStartPriceField() {
    return TextFormField(
        autofocus: false,
        onFieldSubmitted: (term) {
          SystemChannels.textInput.invokeMethod('TextInput.hide');
        },
        keyboardType: TextInputType.number,
        inputFormatters: [
          WhitelistingTextInputFormatter(RegExp(r'^[()\d -]{1,15}$'))
        ],
        controller: orderStartPriceController,
        decoration: InputDecoration(
            icon: Icon(Partnerum.ruble, color: blackColor),
            labelText: Strings.pages.requestsPage.requestCreatePage.priceFrom.l(context)),
        validator: (value) {
          if (value.isEmpty) {
            return Strings.pages.requestsPage.requestCreatePage.necessarily.l(context);
          }
          return null;
        },
        onChanged: (value) {
          _formKey.currentState.validate();
          setState(() {
            _startPrice = int.parse(value);
          });
        },
        onSaved: (value) {
          setState(() {
            _startPrice = int.parse(value);
          });
        });
  }

  Widget _buildEndPriceField() {
    return Padding(
        padding: const EdgeInsets.only(left: 16.0),
        child: TextFormField(
            autofocus: false,
            onFieldSubmitted: (term) {
              SystemChannels.textInput.invokeMethod('TextInput.hide');
            },
            keyboardType: TextInputType.number,
            inputFormatters: [
              FilteringTextInputFormatter.allow(RegExp(r'^[()\d -]{1,15}$'))
            ],
            controller: orderEndPriceController,
            decoration: InputDecoration(
//       icon: Icon(MyIcons.rouble),
//          hintText: '4200',
                labelText: Strings.pages.requestsPage.requestCreatePage.priceTo.l(context)),
            validator: (value) {
              if (value.isEmpty) {
                return Strings.pages.requestsPage.requestCreatePage.necessarily.l(context);
              }
              return null;
            },
            onChanged: (value) {
              _formKey.currentState.validate();
              setState(() {
                _endPrice = int.parse(value);
              });
            },
            onSaved: (value) {
              setState(() {
                _endPrice = int.parse(value);
              });
            }));
  }

  Widget _buildDate(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                  height: 40,
                  decoration: BoxDecoration(
                      border:
                          Border.all(color: Theme.of(context).primaryColor)),
                  child: MaterialButton(
                      onPressed: () async {
                        SystemChannels.textInput.invokeMethod('TextInput.hide');
                        var timedPickedList =
                            await DateRagePicker.showDatePicker(
                                context: context,
                                initialFirstDate: pickedList.isEmpty
                                    ? DateTime.now()
                                    : pickedList[0],
                                initialLastDate: pickedList.isEmpty
                                    ? (DateTime.now()).add(Duration(days: 7))
                                    : pickedList[1],
                                firstDate:
                                    DateTime.now().subtract(Duration(days: 1)),
                                lastDate: DateTime(DateTime.now().year + 20));
                        if (timedPickedList != null) {
                          pickedList = timedPickedList;
                          if (pickedList.length < 2) {
                            pickedList.add(pickedList[0]);
                          }
                        }
                        if (pickedList != null && pickedList.isNotEmpty) {
                          orderArriveDateController.text =
                              DateFormat('dd MMM', 'ru')
                                  .format(pickedList[0])
                                  .toString();
                          orderDepartureDateController.text =
                              DateFormat('dd MMM', 'ru')
                                  .format(pickedList[1])
                                  .toString();

                          orderArriveDateTimestamp =
                              pickedList[0].millisecondsSinceEpoch;
                          orderDepartureDateTimestamp =
                              pickedList[1].millisecondsSinceEpoch;

                          _formKey.currentState.validate();
                        }
                      },
                      child: Text(Strings.pages.requestsPage.requestCreatePage.chooseDate.l(context),
                          style: TextStyle(color: Colors.black)))),
              Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 58),
                  child: Row(children: <Widget>[
                    Flexible(
                        child: IgnorePointer(
                            child: TextFormField(
                                autofocus: false,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: Strings.pages.requestsPage.requestCreatePage.entry.l(context)),
                                controller: orderArriveDateController,
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return Strings.pages.requestsPage.requestCreatePage.necessarily.l(context);
                                  }
                                  return null;
                                }))),
                    Flexible(
                        child: IgnorePointer(
                            child: TextFormField(
                                autofocus: false,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: Strings.pages.requestsPage.requestCreatePage.departure.l(context)),
                                controller: orderDepartureDateController,
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return Strings.pages.requestsPage.requestCreatePage.departure.l(context);
                                  }
                                  return null;
                                })))
                  ]))
            ]));
  }

  Widget _buildCustomerPhoneField() {
    return TextFormField(
        autofocus: false,
        onFieldSubmitted: (term) {
          SystemChannels.textInput.invokeMethod('TextInput.hide');
        },
        keyboardType: TextInputType.phone,
        inputFormatters: [
          WhitelistingTextInputFormatter(RegExp(r'^[()\d -]{1,15}$'))
        ],
        controller: orderCustomerPhoneController,
        decoration: InputDecoration(
            icon: Icon(
              Partnerum.phone,
              color: blackColor,
            ),
            hintText: Strings.pages.requestsPage.requestCreatePage.askForPhoneNumber.l(context),
            labelText: Strings.pages.requestsPage.requestCreatePage.phoneNumber.l(context),
            suffixIcon: IconButton(
                icon: Icon(Icons.help_outline),
                onPressed: () {
                  _displaySnackBar(context);
                  print('onPressed SnackBar');
                })),
        validator: (value) {
          if (value.isEmpty) {
            return Strings.pages.requestsPage.requestCreatePage.necessarily.l(context);
          }
          return null;
        },
        onSaved: (value) {
          setState(() {
            _phoneNumber = value;
          });
        });
  }

  Widget _buildInfoField() {
    return TextFormField(
        autofocus: false,
        onFieldSubmitted: (term) {
          SystemChannels.textInput.invokeMethod('TextInput.hide');
        },
        maxLines: null,
        //minLines: 3,
        //maxLines: 3,
        decoration: InputDecoration(
            icon: Icon(Partnerum.info, color: blackColor),
            hintText: Strings.pages.requestsPage.requestCreatePage.requestInfoWidget.addExtraInfo.l(context),
            labelText: Strings.pages.requestsPage.requestCreatePage.requestInfoWidget.extraInfo.l(context)),
        controller: orderInfoFieldController,
        keyboardType: TextInputType.text);
  }

  void _addOrder(document, BuildContext context) {
//    if (Platform.isIOS) {
//      iosSubscription = _fcm.onIosSettingsRegistered.listen((data) {
//        _saveWithOrderToken(document, context);
//      });
//      _fcm.requestNotificationPermissions(IosNotificationSettings());
//    } else {
//      _saveWithOrderToken(document, context);
//    }
  }

  void resetEverything() {
    orderArriveDateController.text = '';
    orderDepartureDateController.text = '';
    orderCityNameController.text = '';
    orderMetroStationController.text = '';
    orderStartPriceController.text = '';
    orderStartPriceController.text = '';
    orderCustomerPhoneController.text = '';
    orderInfoFieldController.text = '';
    roomsList.clear();
    pickedList.clear();

    setState(() {
      Navigator.of(context).pop();
    });
  }

  _displaySnackBar(BuildContext context) {
    final snackBar = SnackBar(
        content: Text(
            Strings.pages.requestsPage.requestCreatePage.phoneInfoSnackBar.l(context)));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  Widget loadingChats(context) {
    return Padding(
      padding:
      const EdgeInsets.only(top: 12.0, bottom: 16),
      child: Container(
          alignment: FractionalOffset.center,
          child: Center(
              child: Theme.of(context).platform == TargetPlatform.iOS
                  ? CupertinoActivityIndicator()
                  : linearProgressIndicator)),
    );
  }
}
