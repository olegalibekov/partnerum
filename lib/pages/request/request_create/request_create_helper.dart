import 'dart:async';
import 'dart:io';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:micro_utils/data_helper.dart';
import 'package:db_partnerum/db_partnerum.dart';
import 'package:partnerum/head_module/gen_extensions/generated_str_extension.dart';
import 'package:partnerum/utils/brain_module.dart';

@DataHelper()
class RequestCreateHelper {
  Request document;
  Future<Request> futureRequest;
  var notStated = 'not stated';
  int distanceTimeValue = 5;
  String guestsCount = '1';
  int guestsValue = 1;
  int commissionValue = 10;
  List roomsList = ['1'];
  bool roomSelected = true;
  List<String> stations = [];
  List<DateTime> pickedList = [];

  TextEditingController orderArriveDateController = TextEditingController();
  TextEditingController orderDepartureDateController = TextEditingController();
  TextEditingController orderCityNameController = TextEditingController();
  TextEditingController orderMetroNameController = TextEditingController();
  TextEditingController orderMetroStationController = TextEditingController();
  TextEditingController orderStartPriceController = TextEditingController();
  TextEditingController orderEndPriceController = TextEditingController();
  TextEditingController orderInfoFieldController = TextEditingController();
  TextEditingController orderCustomerPhoneController = TextEditingController();

  String compareOrderArriveDate;
  String compareOrderDepartureDate;
  String compareOrderCityName;
  String compareOrderStartPrice;
  String compareOrderEndPrice;
  String compareOrderInfoField;
  List compareOrderRoomsList;
  String compareOrderGuestsCount;
  List compareOrderStations;
  int compareOrderDistanceTimeValue;
  int compareOrderCommissionValue;
  int orderArriveDateTimestamp;
  int orderDepartureDateTimestamp;

  void initRequest(String requestID) {
    if (requestID.isNotEmpty) {
      document = root.userData[mindClass.user.uid].requests[requestID];
      futureRequest =
          root.userData[mindClass.user.uid].requests[requestID].future;
    } else {
      document = root.userData[mindClass.user.uid].requests.push;
      document.settings.packet.on();
      // futureRequest = document.future;
    }
  }

  Future<Request> requestFuture({String requestID, bool newRequest}) {
    if (newRequest) {
      return root.userData[mindClass.user.uid].requests.push.future;
    } else {
      return root.userData[mindClass.user.uid].requests[requestID].future;
    }
  }

  String requestTitle(BuildContext context) {
    String formatDate(int timestamp) {
      return DateFormat('dd MMM', 'ru')
          .format(DateTime.fromMillisecondsSinceEpoch(timestamp));
    }

    var firstDate =
        document.requestInformation?.datesRange?.first?.value?.inMilliseconds;
    var lastDate =
        document.requestInformation?.datesRange?.last?.value?.inMilliseconds;




    if (firstDate == 0 || lastDate == 0) {
      return Strings.pages.requestsPage.requestCreatePage.requestCreateHelper.newRequest.l(context);
    }
    var title = '${formatDate(firstDate)} - ${formatDate(lastDate)}';

    // var title =
    //     '${(firstDate == 'null') || (firstDate.inMilliseconds == 0) ? notStated : formatDate(firstDate.inMilliseconds)}' +
    //         ' - ' +
    //         '${(lastDate == 'null') || (lastDate.inMilliseconds == 0) ? notStated : formatDate(lastDate.inMilliseconds)}';


    return title;
  }

  String requestCity() => document.requestInformation.city.value;

  List<String> requestStations() {
    // return ['-'];
    var stationsDB = document.requestInformation.stations;
    List<String> emptyList = [];
    var stations = document?.requestInformation?.stations?.values
            ?.map((e) => e.key)
            ?.toList() ??
        [];
    return stations;
  }

  // requestRooms() {
  //   var roomsDB = document.requestInformation.rooms;
  //   // var stations = [];
  //   // print('stationsDB: ${stationsDB.list}');
  //   var stations = (stationsDB == null || stationsDB.isEmpty) ? notStated : stationsDB.values.map((e) => e.value).join(', ');
  //   return stations;
  // }

  int requestGuests() {
    var guests = document.requestInformation.guests;
    // return (guests == null) ? 1 : guests.value;
    print('guests: ${guests == null}');
    return guests?.value ?? 1;
  }

  int requestTimeToMetro() {
    var timeToMetroValue =
        document?.requestInformation?.timeToMetro?.value;
    return timeToMetroValue.inMilliseconds == 0 ? 5 : timeToMetroValue.inMinutes;
  }

  String requestStartPrice() {
    var price = document.requestInformation.priceRange.startPosition;
    return ((price == null) || (price.value == null) || (price.value == 'null'))
        ? ''
        : price.value;
  }

  String requestEndPrice() {
    var price = document.requestInformation.priceRange.endPosition;
    return ((price == null) || (price.value == null) || (price.value == 'null'))
        ? ''
        : price.value;
  }

  String requestStartDates() {
    var date = document.requestInformation.datesRange.first;
    return ((date == null) ||
            (date.value == null) ||
            (date.value == 'null') ||
            (date.value.inMilliseconds == 0))
        ? ''
        : date.value.inMilliseconds.toString();
  }

  String requestEndDates() {
    var date = document.requestInformation.datesRange.last;
    return ((date == null) ||
            (date.value == null) ||
            (date.value == 'null') ||
            (date.value.inMilliseconds == 0))
        ? ''
        : date.value.inMilliseconds.toString();
  }

  String requestDescription() {
    var description = document.requestInformation.description;
    return ((description == null) ||
            (description.value == null) ||
            (description.value == 'null'))
        ? ''
        : description.value;
  }

  int requestCommission() {
    var commission = document.requestInformation.commision;
    return ((commission == null) || (commission.value == null))
        ? 10
        : commission.value;
    // return 10;
  }

  List requestRoomsList() {
    var roomsList = document.requestInformation.rooms;
    return ((roomsList == null) ||
            (roomsList.value == null) ||
            (roomsList.value == 'null'))
        ? ['1']
        : roomsList.value.split(', ');
  }

// void uploadDoc(){
//   document.upload();
//   document.requestInformation.upload();
//   document.set(document.value);
// }

  Future<void> saveWithOrderToken() async {
    // document.requestInformation.stations = stations;
    if (compareStations(stations)) {
      setStations(stations);
    }
    if (compareOrderCityName != orderCityNameController.text) {
      document.requestInformation.city = orderCityNameController.text;
    }
    if (compareOrderGuestsCount != guestsValue.toString()) {
      document.requestInformation.guests = guestsValue;
    }
    if (compareOrderRoomsList != roomsList) {
      document.requestInformation.rooms = roomsList.join(', ');
    }
    if (compareOrderDistanceTimeValue != distanceTimeValue) {
      document.requestInformation.timeToMetro = distanceTimeValue * 60000;
    }
    if (compareOrderCommissionValue != commissionValue) {
      document.requestInformation.commision = commissionValue;
    }
    if (compareOrderInfoField != orderInfoFieldController.text) {
      document.requestInformation.description = orderInfoFieldController.text;
    }
    if (compareOrderStartPrice != orderStartPriceController.text) {
      document.requestInformation.priceRange.startPosition =
          orderStartPriceController.text;
    }
    if (compareOrderEndPrice != orderEndPriceController.text) {
      document.requestInformation.priceRange.endPosition =
          orderEndPriceController.text;
    }
    if (compareOrderArriveDate != orderArriveDateController.text) {
      // document.requestInformation.datesRange.first =
      //     orderArriveDateController.text;
      document.requestInformation.datesRange.first = orderArriveDateTimestamp;
    }
    if (compareOrderDepartureDate != orderDepartureDateController.text) {
      // document.requestInformation.datesRange.last =
      //     orderDepartureDateController.text;
      document.requestInformation.datesRange.last = orderDepartureDateTimestamp;
    }
    document.requestInformation.lastChange =
        DateTime.now().millisecondsSinceEpoch;
    await document.upload();
  }

  void setStations(List stationsList) {
    var stations = document?.requestInformation?.stations;
    var oldStationsSet = <dynamic>{};
    if (stations?.values != null) {
      oldStationsSet = stations.values.map((e) => e.key).toSet();
    }
    // print('stationsList.toSet(): ${stationsList.toSet()}');
    // print('oldStationsSet: ${oldStationsSet}');
    oldStationsSet = oldStationsSet.difference(stationsList.toSet());
    // stations.clear();
    // stations.remove();
    for (var metroStation in stationsList) {
      // stations[stationNum.toString()].remove();
      stations[metroStation] = (metroStation);
    }
    for (var oldStation in oldStationsSet) {
      // print('oldStation: ${oldStation}');
      stations[oldStation].remove();
    }
    // for (var i = stationNum; i < oldStationsListLength; i++) {
    //   stations[i.toString()].remove();
    // }
    // house.houseInformation.rooms = 5;
  }

  bool compareStations(List newStations) {
    var documentStations = document?.requestInformation?.stations?.values;
    if (documentStations != null) {
      var oldStations = documentStations.map((e) => e.value).toList();
      var oldStationsSet = oldStations.toSet();
      var newStationsSet = newStations.toSet();
      return !(oldStationsSet.containsAll(newStations) &&
          newStationsSet.containsAll(oldStations));
    }
    return true;
  }
}
