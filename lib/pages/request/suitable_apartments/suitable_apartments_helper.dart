import 'package:db_partnerum/db_partnerum.dart';
import 'package:intl/intl.dart';
import 'package:micro_utils/data_helper.dart';
import 'package:partnerum/utils/brain_module.dart';

@DataHelper()
class SearchingOfSuitableApartmentsHelper {

  Map bookedDates = {};

  Stream<List<Offer>> requestsStream(String requestID) {
    return root.userData[mindClass.user.uid].requests[requestID].offers.stream.asBroadcastStream().map((event) => event?.values?.toList() ?? []);
  }

  OffersMap offersMap(String requestID) {
    return root.userData[mindClass.user.uid].requests[requestID].offers;
  }

  UserInformation loadUserInformation(String userID) {
    return root.userData[userID].userInformation;
  }

  // Stream<UserInformation> loadUserInformation(String userID){
  //   return root.userData[userID].userInformation.stream;
  // }

  HouseInformation loadHouseInformation(Offer offer) {
    var houseId = offer.houseId.value;
    var userId = offer.userId.value;
    return root.userData[userId].houses[houseId].houseInformation;
  }


  Future<List<PhotoImage>> loadHouseImages(HouseInformation houseInformation) async {;
    var photosList = <PhotoImage>[];
    for (var photoId in houseInformation.photos.values.toList()) {
      photosList.add(await root.imagesContainer[photoId.value].future);
    }
    return photosList;

  }
  BookingDatesMap loadHouseBookingDates(Offer offer) {
    var houseId = offer.houseId.value;
    var userId = offer.userId.value;
    return root.userData[userId].houses[houseId].bookingDates;
  }

  Future<String> datesIntersectionMessage(DatesRange requestDatesRange, Offer offer) async {
    String formatDate(int timestamp) {
      return DateFormat.yMMMd('ru').format(DateTime.fromMillisecondsSinceEpoch(timestamp));
    }

    var intRequestCheckIn = requestDatesRange?.first?.value?.inMilliseconds;
    var intRequestCheckOut = requestDatesRange?.last?.value?.inMilliseconds;

    var houseBookingDates;

    /// Uncomment below to show alert when it needs
    // var houseBookingDates = await root.userData[offer.userId.value]
    //     .houses[offer.houseId.value]?.bookingDates?.future;

    String intersectionMessage() {
      print('intersectionMessage() invoked');
      for (var houseBookingDate in houseBookingDates.values) {
        var elementCheckIn = houseBookingDate.checkIn.value;
        var elementCheckOut = houseBookingDate.checkOut.value;

        if (intRequestCheckIn <= elementCheckOut.inMilliseconds && elementCheckIn.inMilliseconds <= intRequestCheckOut) {
          var message = 'Бронирование квартиры недоступно в даты: \n'
              '${formatDate(elementCheckIn.inMilliseconds)} - ${formatDate(elementCheckOut.inMilliseconds)}';
          return message;
        }
      }
      return null;
    }

    // print('HouseBooking dates: ${houseBookingDates?.values?.first}');
    if (houseBookingDates?.values?.first != null) return intersectionMessage();

    return null;
  }

  ConversationUsers conversationUsers(String offerId) {
    if (offerId == null) return null;
    return root.chatTopic?.offer2Room[offerId]?.conversationUsers;
  }
}
