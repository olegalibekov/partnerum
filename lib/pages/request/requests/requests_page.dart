import 'dart:async';

import 'package:async/async.dart';
import 'package:db_partnerum/db_partnerum.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_svg/svg.dart';
import 'package:partnerum/head_module/gen_extensions/generated_str_extension.dart';
import 'package:partnerum/orders/orders/orders_page.dart';
import 'package:partnerum/pages/chat/chats/chats_page.dart';
import 'package:partnerum/utils/brain_module.dart';
import 'package:partnerum/utils/theme_constants.dart';

import 'requests_helper.dart';

class RequestsPage extends StatefulWidget {
  const RequestsPage({Key key, @required this.hideFab, @required this.showFab}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _OrdersPage();

  final Function hideFab;
  final Function showFab;
}

class _OrdersPage extends State<RequestsPage> with AutomaticKeepAliveClientMixin, RequestsHelper {
  StreamSubscription _streamSubscription;
  Stream<List<Request>> requestsStream;

  @override
  void initState() {
    initStreams();
    requestsStream = root.userData[mindClass.user.uid].requests.stream.map((event) {
      return event?.values?.toList() ?? [];
    }).asBroadcastStream();
    _streamSubscription = mindClass.eventSteam.outStream.where((event) => event == 'updateRequestScreen').listen((_) {
      if (mounted) {
        setState(() {});
      }
    });
    manualUpdate.add(false);
    super.initState();
  }

  @override
  void dispose() {
    manualUpdate.close();
    _streamSubscription.cancel();
    RequestCard.keys = {};
    super.dispose();
  }

  bool isArchive = false;
  bool containsNeededRequests = false;

  toggleArchive() {
    isArchive = !isArchive;
    if (isArchive) {
      widget.hideFab();
    } else {
      widget.showFab();
    }
    manualUpdate.add(isArchive);
    Future.delayed(Duration(seconds: 1));
    if (mounted) {
      setState(() {});
    }
  }

  var globalRequests = [];

  var requestsGlobalKeys = {};

  @override
  Widget build(BuildContext context) {
    // var requestsAmount = 0;
    return StreamBuilder(
        initialData: [],
        // stream: requestsLoader(),
        stream: requestsStream,
        builder: (BuildContext buildContext, AsyncSnapshot requestsSnapshot) {

          if (requestsSnapshot.hasData && !requestsSnapshot.hasError && requestsSnapshot.data.length > 0) {
            globalRequests = (requestsSnapshot.data);
          }
          requests = List.from(globalRequests
              .where((element) {
                return ((!isArchive) && (element.requestInformation.isArchived.value != !isArchive)) ||
                    (isArchive && (element.requestInformation.isArchived.value == isArchive));
              })
              .toList()
              .reversed);

          var requestNumber = requests.length;

          /*
          var requestNumber = 0;
          for (var request in requests) {
            requestNumber += 1;
            () async {
              var requestOffers = await request?.offers?.future;
              if (requestOffers?.values != null) {
                for (var requestOffer in requestOffers.values) {
                  if (requestOffer.userId.value == mindClass.user.uid) {
                    request.offers.remove(requestOffer);
                    requests[requestNumber].offers.remove(requestOffer);
                  }
                }
              }
            }();
          }

           */

          requests.removeWhere((element) => element.requestInformation.isArchived.value == !isArchive);

          // request.offers.future.then((requestsValue){
          //
          //   requests.removeWhere((request) => false);
          //   requests.removeWhere((key, value) => mindClass.user.uid == value.userId.value);
          // });

          requests.forEach((element) {
            requestsGlobalKeys[element.key] ??= GlobalKey(debugLabel: element.key);
          });

          containsNeededRequests = requests.isNotEmpty;

          getKey() {
            return Key('zero');
            // if (asyncSnapshot.hasData && !asyncSnapshot.hasError && asyncSnapshot.data.length > 0) {
            //   requestsAmount = asyncSnapshot.data.length;
            //   if (asyncSnapshot.data.first.runtimeType == 0.runtimeType) {
            //     return Key('loading');
            //   }
            //   return Key('>zero' + isArchive.toString());
            // } else {
            //   return Key('zero');
            // }
          }

          return Scaffold(
              key: getKey(),
              backgroundColor: Colors.white,
              body: GestureDetector(
                  child: RefreshIndicator(
                      displacement: 80,
                      onRefresh: () {
                        setState(() {});
                        return Future.delayed(Duration(milliseconds: 300));
                      },
                      child: ScrollConfiguration(
                          behavior: noHighlightScrollBehavior(context),
                          child: CustomScrollView(slivers: [
                            SliverAppBar(
                                centerTitle: true,
                                pinned: true,
                                title: Text(
                                    isArchive
                                        ? Strings.pages.requestsPage.archivedTitleText.l(context)
                                        : Strings.pages.requestsPage.titleText.l(context),
                                    style: TextStyle(fontSize: 20),
                                    key: Key(isArchive.toString())),
                                leading: !isArchive
                                    ? Container()
                                    : IconButton(
                                        icon: Icon(Icons.arrow_back),
                                        onPressed: () {
                                          toggleArchive();
                                          setState(() {});
                                          // Navigator.pop(context);
                                        }),
                                actions: <Widget>[
                                  isArchive
                                      ? Container()
                                      : IconButton(
                                          icon: Icon(Icons.archive),
                                          onPressed: () {
                                            toggleArchive();
                                            setState(() {});
                                          })
                                ]),

                            if (containsNeededRequests)
                              SliverList(
                                  delegate: SliverChildBuilderDelegate((context, index) {
                                return Slidable(
                                    enabled: true,
                                    actionPane: SlidableDrawerActionPane(),
                                    key: requestsGlobalKeys[requests[index].key],
                                    // key: Key(requestIdByIndex(index)
                                    //     // requests[index].requestId.toString()
                                    //     ),
                                    direction: Axis.horizontal,
                                    dismissal: SlidableDismissal(
                                        onWillDismiss: (item) async {
                                          //   /*
                                          //   return await archiveItem(index);
                                          //    */

                                          await archiveRequest(index);
                                          await requestsStream.first;
                                          // setState((){});
                                          return true;
                                        },
                                        onDismissed: (actionType) {
                                          // setState(() {
                                          // requests.removeAt(index);
                                          // });
                                        },
                                        child: SlidableDrawerDismissal()),
                                    actions: <Widget>[action(index)],
                                    secondaryActions: <Widget>[action(index)],
                                    child: Padding(
                                        padding: EdgeInsets.symmetric(horizontal: 16.0).add(EdgeInsets.only(top: 16)),
                                        child: Builder(builder: (BuildContext _context) {
                                          return RequestCard(
                                              request: requests[index],
                                              isArchive: isArchive,
                                              onArchive: () {
                                                Slidable.of(_context).dismiss();
                                              });

                                          /*
                                                      return RequestCard(
                                                          request: requests[index],
                                                          isArchive: isArchive,
                                                          onArchive: () {
                                                            Slidable.of(_context).dismiss();
                                                          });

                                                       */
                                        })));
                              }, childCount: requests.length)),

                            if (containsNeededRequests) SliverToBoxAdapter(child: SizedBox(width: 30, height: 30)),
                            if (!containsNeededRequests) noRequestsWidget(context)
//                                 Builder(builder: (BuildContext buildContext) {
//                                   var noRequestsWidget = SliverToBoxAdapter(
//                                       child: ListView(
//                                           shrinkWrap: true, children: <Widget>[
//                                         Container(
//                                             margin: EdgeInsets.all(20),
//                                             child: Column(
//                                                 crossAxisAlignment: CrossAxisAlignment
//                                                     .start, children: <Widget>[
//                                               Padding(
//                                                   padding: const EdgeInsets
//                                                       .only(top: 16.0),
//                                                   child: Center(
//                                                       child: Container(
//                                                           width: 300,
//                                                           height: 300,
//                                                           child: SvgPicture
//                                                               .asset(
//                                                               'assets/images/zero_orders.svg',
//                                                               semanticsLabel: 'Zero orders')))),
//                                               Padding(
//                                                   padding: const EdgeInsets
//                                                       .only(top: 16.0),
//                                                   // child: Text('requestsAmount: ${requestsAmount}   isArchive: ${isArchive}   showImage: ${showImage}'),
//                                                   child: Text(
//                                                       Strings.pages.requestsPage.emptyRequests.l(context),
//                                                       textAlign: TextAlign
//                                                           .center,
//                                                       style: TextStyle(
//                                                           color: Colors.black,
//                                                           fontSize: 14.0))
//                                               )
//                                             ]))
//                                       ]));
//                                   // return SliverToBoxAdapter(child: Container(width: 120, height: 120, color: Colors.black87,),);
//
//                                   if (asyncSnapshot.hasData &&
//                                       !asyncSnapshot.hasError &&
//                                       asyncSnapshot.data.length > 0) {
//                                     if (asyncSnapshot.data.first.runtimeType ==
//                                         0.runtimeType) {
//                                       /*
//                                       return SliverToBoxAdapter(
//
//                                         child: Container(
//                                           color: Colors.yellow,
//                                           width: 10,
//                                           height: 10,
//                                         ),
//                                       );
//
//                                        */
//                                       return SliverToBoxAdapter(
//                                           child: Stack(children: <Widget>[
//                                             (!asyncSnapshot.hasData)
//                                                 ? Align(
//                                                 alignment: Alignment.topCenter,
//                                                 child: SizedBox(height: 2,
//                                                     child: linearProgressIndicator))
//                                                 : Container(),
//                                             Container(
//                                                 margin: EdgeInsets.all(20),
//                                                 child: Column(
//                                                     crossAxisAlignment: CrossAxisAlignment
//                                                         .start,
//                                                     mainAxisSize: MainAxisSize
//                                                         .min,
//                                                     children: <Widget>[
//                                                       Padding(
//                                                           padding: const EdgeInsets
//                                                               .only(top: 16.0),
//                                                           child: Center(
//                                                               child: Container(
//                                                                   width: 300,
//                                                                   height: 300,
//                                                                   child:
//                                                                   SvgPicture
//                                                                       .asset(
//                                                                       'assets/images/zero_orders.svg',
//                                                                       semanticsLabel: 'Zero orders')))),
//                                                       Padding(
//                                                           padding: const EdgeInsets
//                                                               .only(top: 16.0),
//                                                           child: Text(
//                                                               Strings.pages.requestsPage.emptyRequests.l(context),
//                                                               textAlign: TextAlign
//                                                                   .center,
//                                                               style: TextStyle(
//                                                                   color: Colors
//                                                                       .black,
//                                                                   fontSize: 14.0)))
//                                                     ]))
//                                           ]));
//                                     }
//
//
//
//
//
//                                     // for (var request in requests) {
//                                     //   if (request.requestInformation.isArchived.value == isArchive) {
//                                     //     containsNeededRequests = true;
//                                     //     break;
//                                     //   }
//                                     // }
//
//
//                                     /*
//                                     Future archiveItem(int index) async {
//
//                                      */
//                                     // FutureOr<bool> archiveItem(int index) async {
//                                     //   //return false;
//                                     //   await archiveRequest(index);
//                                     //   return true;
//                                     //   /*
//                                     //   var result =
//                                     //       await mindClass.archiveRequest(
//                                     //           data[index].requestId,
//                                     //           !data[index].archive);
//                                     //   //await Future.delayed(Duration(milliseconds: 600));
//                                     //   return result;
//                                     //
//                                     //    */
//                                     // }
//
//                                     Builder action(int index) {
//                                       return Builder(
//                                           builder: (BuildContext buildContext) {
//                                             return IconSlideAction(
//                                                 caption: isArchive
//                                                     ? 'Strings.globalOnes.unArchive.l(context)
//                                                     : Strings.globalOnes.archive
//                                                     .l(context),
//                                                 color: Colors.transparent,
//                                                 foregroundColor: blackColor,
//                                                 icon: isArchive ? Icons
//                                                     .unarchive : Icons.archive,
//                                                 onTap: () {
//                                                   Slidable.of(buildContext)
//                                                       .dismiss();
//                                                 });
//                                           });
//                                     }
// /*
//                                     return SliverToBoxAdapter(
//                                       child: Container(
//                                         color: Colors.green,
//                                         width: 10,
//                                         height: 10,
//                                       ),
//                                     );
// */
//
//                                     if (containsNeededRequests) {
//                                       return SliverList(
//                                           delegate: SliverChildBuilderDelegate(
//                                                   (context, index) {
//                                                 if (index < requests.length) {
//                                                   return Slidable(
//                                                       enabled: true,
//                                                       actionPane: SlidableDrawerActionPane(),
//                                                       key: Key(requestIdByIndex(
//                                                           index)
//                                                         /*
//                                                     requests[index].requestId.toString()
//                                                */
//                                                       ),
//                                                       dismissal: SlidableDismissal(
//                                                           onWillDismiss: (
//                                                               item) async {
//                                                             //   /*
//                                                             //   return await archiveItem(index);
//                                                             //    */
//                                                             await archiveRequest(
//                                                                 index);
//                                                             await requestsStream
//                                                                 .first;
//                                                             return true;
//                                                           },
//                                                           onDismissed: (
//                                                               actionType) {
//                                                             // setState(() {
//                                                             //   requests.removeAt(index);
//                                                             // });
//                                                           },
//                                                           child: SlidableDrawerDismissal()),
//                                                       actions: <Widget>[
//                                                         action(index)
//                                                       ],
//                                                       secondaryActions: <
//                                                           Widget>[
//                                                         action(index)
//                                                       ],
//                                                       child: Padding(
//                                                           padding: EdgeInsets
//                                                               .symmetric(
//                                                               horizontal: 16.0)
//                                                               .add(
//                                                               EdgeInsets.only(
//                                                                   top: 16)),
//                                                           child: Builder(
//                                                               builder: (
//                                                                   BuildContext _context) {
//                                                                 if (isArchived(
//                                                                     index) ==
//                                                                     isArchive) {
//                                                                   return RequestCard(
//                                                                       request: requests[index],
//                                                                       isArchive: isArchive,
//                                                                       onArchive: () {
//                                                                         Slidable
//                                                                             .of(
//                                                                             _context)
//                                                                             .dismiss();
//                                                                       });
//                                                                 } else {
//                                                                   return Container();
//                                                                 }
//                                                                 /*
//                                                       return RequestCard(
//                                                           request: requests[index],
//                                                           isArchive: isArchive,
//                                                           onArchive: () {
//                                                             Slidable.of(_context).dismiss();
//                                                           });
//
//                                                        */
//                                                               })));
//                                                 }
//                                                 else {
//                                                   return Padding(
//                                                       padding: EdgeInsets.all(
//                                                           15.0));
//                                                 }
//                                               },
//                                               childCount: requests.length));
//                                     } else {
//                                       return noRequestsWidget;
//                                     }
//                                   } else {
//                                     return noRequestsWidget;
//                                     // return Container();
//                                   }
//                                   // else {
//                                   //   /*
//                                   //   return SliverToBoxAdapter(
//                                   //     child: Container(
//                                   //       color: Colors.blue,
//                                   //       width: 10,
//                                   //       height: 10,
//                                   //     ),
//                                   //   );
//                                   //
//                                   //    */
//                                   //   // return SliverToBoxAdapter(child: Container());
//                                   //   return SliverToBoxAdapter(
//                                   //       child: ListView(shrinkWrap: true, children: <Widget>[
//                                   //     Container(
//                                   //         margin: EdgeInsets.all(20),
//                                   //         child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
//                                   //           Padding(
//                                   //               padding: const EdgeInsets.only(top: 16.0),
//                                   //               child: Center(
//                                   //                   child: Container(
//                                   //                       width: 300,
//                                   //                       height: 300,
//                                   //                       child: SvgPicture.asset('assets/images/zero_orders.svg', semanticsLabel: 'Zero orders')))),
//                                   //           Padding(
//                                   //               padding: const EdgeInsets.only(top: 16.0),
//                                   //               child: Text(Strings.pages.requestsPage.emptyRequests.l(context),
//                                   //                   textAlign: TextAlign.center, style: TextStyle(color: Colors.black, fontSize: 14.0)))
//                                   //         ]))
//                                   //   ]));
//                                   // }
//
//                                 }),
                            // if (((requestsAmount == 0) && !isArchive) ||
                            //     ((requestsAmount == asyncSnapshot.data.length) && isArchive))

                            //   SliverToBoxAdapter(
                            //       child: ListView(shrinkWrap: true, children: <Widget>[
                            //     Container(
                            //         margin: EdgeInsets.all(20),
                            //         child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                            //           Padding(
                            //               padding: const EdgeInsets.only(top: 16.0),
                            //               child: Center(
                            //                   child: Container(
                            //                       width: 300,
                            //                       height: 300,
                            //                       child: SvgPicture.asset('assets/images/zero_orders.svg', semanticsLabel: 'Zero orders')))),
                            //           Padding(
                            //               padding: const EdgeInsets.only(top: 16.0),
                            //           // child: Text('requestsAmount: ${requestsAmount}   isArchive: ${isArchive}   showImage: ${showImage}'),
                            //               child: Text(Strings.pages.requestsPage.emptyRequests.l(context),
                            //                   textAlign: TextAlign.center, style: TextStyle(color: Colors.black, fontSize: 14.0))
                            //           )
                            //         ]))
                            //   ]))
                            // SliverToBoxAdapter(child: Container(color: Colors.red, width: 10, height: 10,),)
                          ])))));
        });
  }

  SliverToBoxAdapter noRequestsWidget(context) {
    return SliverToBoxAdapter(
        child: ListView(shrinkWrap: true, children: <Widget>[
      Container(
          margin: EdgeInsets.all(20),
          child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
            Padding(
                padding: const EdgeInsets.only(top: 16.0),
                child: Center(
                    child:
                        Container(width: 300, height: 300, child: SvgPicture.asset('assets/images/zero_orders.svg', semanticsLabel: 'Zero orders')))),
            Padding(
                padding: const EdgeInsets.only(top: 16.0),
                // child: Text('requestsAmount: ${requestsAmount}   isArchive: ${isArchive}   showImage: ${showImage}'),
                child: Text(Strings.pages.requestsPage.emptyRequests.l(context),
                    textAlign: TextAlign.center, style: TextStyle(color: Colors.black, fontSize: 14.0)))
          ]))
    ]));
  }

  /*
  var noRequestsWidget = SliverToBoxAdapter(
      child: ListView(shrinkWrap: true, children: <Widget>[
    Container(
        margin: EdgeInsets.all(20),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                  padding: const EdgeInsets.only(top: 16.0),
                  child: Center(
                      child: Container(
                          width: 300,
                          height: 300,
                          child: SvgPicture.asset(
                              'assets/images/zero_orders.svg',
                              semanticsLabel: 'Zero orders')))),
              Padding(
                  padding: const EdgeInsets.only(top: 16.0),
                  // child: Text('requestsAmount: ${requestsAmount}   isArchive: ${isArchive}   showImage: ${showImage}'),
                  child: Text(
                      Strings.pages.requestsPage.emptyRequests.l(context),
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.black, fontSize: 14.0)))
            ]))
  ]));
  */
  Builder action(int index) {
    return Builder(builder: (BuildContext buildContext) {
      return IconSlideAction(
          caption: isArchive ? Strings.globalOnes.unarchive.l(context) : Strings.globalOnes.archive.l(context),
          color: Colors.transparent,
          foregroundColor: blackColor,
          icon: isArchive ? Icons.unarchive : Icons.archive,
          onTap: () {
            Slidable.of(buildContext).dismiss();
          });
    });
  }

  @override
  bool get wantKeepAlive => true;
}
