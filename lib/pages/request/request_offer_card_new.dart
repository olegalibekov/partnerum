import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';
import 'dart:math' as math;
import 'package:db_partnerum/db_partnerum.dart';
import 'package:intl/intl.dart';

import 'package:after_layout/after_layout.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:diffutil_sliverlist/diffutil_sliverlist.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart' hide ExpansionTile;
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:partnerum/head_module/gen_extensions/generated_str_extension.dart';
import 'package:partnerum/models/requests_model.dart';

import 'package:partnerum/models/user_info_model.dart';
import 'package:partnerum/pages/house/house_details/house_details_page.dart';
import 'package:partnerum/orders/orders/orders_page.dart';
import 'package:partnerum/pages/request/share.dart';
import 'package:partnerum/utils/brain_module.dart';
import 'package:partnerum/utils/theme_constants.dart';
import 'package:partnerum/widgets/user_title/user_tile.dart';

import 'package:scroll_to_index/scroll_to_index.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import '../chat/messages/messages_page.dart';
import 'request_create/request_create_page.dart';
import 'suitable_apartments/suitable_apartments_page.dart';
import 'request_offer_card.dart';

class RequestOfferCardNew extends StatelessWidget {
  final String id;
  final bool isReady;
  final list;
  final Function show;
  final Future future;
  final HouseInformation houseInformation;
  final String messId;
  final String rType;
  final Function close;
  final UserModel user;

  const RequestOfferCardNew(
      {Key key,
      this.id,
      this.isReady = false,
      this.list = const [],
      this.show,
      this.future,
      this.messId,
      this.rType,
      this.houseInformation,
      this.close,
      this.user})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var idSplit;
    String type;
    String messageInstanceId;
    if (future == null) {
      idSplit = id?.split(';');
      type = '';
      messageInstanceId = '';
      if (list.length > 0) {
        type = list[3];
        messageInstanceId = list[2];
      }
    } else {
      messageInstanceId = messId;
      type = rType;
    }
    /*
    String uid = (idSplit as List)?.elementAt(0) ?? house?.uid;

     */
    String uid = (idSplit as List)?.elementAt(0) ?? houseInformation?.key;
    Widget houseElement(HouseInformation houseInformation, [special = false]) {
      //if(house.houseId.isEmpty) return Container();
      if (special) {
        return GestureDetector(
            onTap: () {
              Navigator.push(context, CupertinoPageRoute(builder: (context) => OfferDetailsPage(edit: false, houseInformation: houseInformation)));
            },
            child: AnimatedSwitcher(
                duration: Duration(milliseconds: 300),
                child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                  /*
                      FutureBuilder<UserModel>(
                          future: mindClass.userInfo(uid),
                          builder: (context, snapshot) {
                            //&& snapshot.data.isUsernameExist()
                            if (snapshot.hasData &&
                                snapshot.data.username.isNotEmpty) {
                              return AnimatedSwitcher(
                                  transitionBuilder:
                                      (Widget child, Animation<
                                      double> animation) {
                                    return SizeTransition(
                                      axis: Axis.vertical,
                                      sizeFactor: animation,
                                      child: child,
                                    );
                                  },
                                  duration: kThemeAnimationDuration,
                                  child: Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 15),
                                      child: UserTile(
                                          uid: uid, user: snapshot.data))
                              );
                            }
                            return AnimatedSwitcher(
                                duration: kThemeAnimationDuration,
                                child: SizedBox());
                          }),

                       */
                  LayoutBuilder(builder: (context, constraints) {
                    return SizedBox(
                        height: MediaQuery.of(context).size.height * 1 / 6,
                        child: ListView(physics: BouncingScrollPhysics(), scrollDirection: Axis.horizontal, shrinkWrap: true, children: [
                          /*
                          for (var url in house.houseInformation.photos.list)
                            Padding(
                                padding: EdgeInsets.only(right: 8),
                                child: ClipRRect(
                                    borderRadius: BorderRadius.circular(16),
                                    child: Stack(children: <Widget>[
                                      CachedNetworkImage(fit: BoxFit.cover, width: constraints.maxWidth, imageUrl: url),
                                      Positioned.fill(child: Container(color: Colors.black.withOpacity(0.05)))
                                    ]))),

                           */

                          ///TODO добавить, если будет надо
//                                    if (house.info.photoUrls.length < 2)
//                                      Container(
//                                        height:
//                                            MediaQuery.of(context).size.height *
//                                                1 /
//                                                6,
//                                        width:
//                                            MediaQuery.of(context).size.width *
//                                                    2 /
//                                                    3 -
//                                                80,
//                                        child: Column(
//                                          mainAxisAlignment:
//                                              MainAxisAlignment.center,
//                                          crossAxisAlignment:
//                                              CrossAxisAlignment.center,
//                                          children: <Widget>[
//                                            Icon(
//                                              Icons.add_a_photo,
//                                              color: Theme.of(context)
//                                                  .primaryColor,
//                                            ),
//                                            Text("Запросить больше фото"),
//                                            Text(
//                                                "Попросите владельца добавить больше фото"),
//                                          ],
//                                        ),
//                                      )
                        ]));
                  }),
                  Padding(
                      padding: const EdgeInsets.all(8.0).add(
                        EdgeInsets.symmetric(horizontal: 15),
                      ),
                      child: Theme(
                          data: ThemeData(iconTheme: IconThemeData(color: Colors.grey.shade500)),
                          child: Column(mainAxisSize: MainAxisSize.min, crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                            Text(houseInformation.stations.values.join(', '), style: Theme.of(context).textTheme.subtitle1),
                            spacer(0, 8),
                            if (houseInformation.address.value.isNotEmpty)
                              Text(houseInformation.address.value, style: Theme.of(context).textTheme.subtitle2),
                            spacer(0, 4),
                            /*
                                    if (house.info.rating != null)
                                      Row(
                                          mainAxisSize: MainAxisSize.min,
                                          children: <Widget>[
                                            Text(house.info.rating.toString()),
                                            Text(house.info.reviews.length
                                                .toString())
                                          ]
                                      )
                                    else
                                      Text('Нет ни оценок, не отзывов'),

                                     */
                            Text('От ${houseInformation.priceRange.startPosition} до ${houseInformation.priceRange.endPosition} руб/сут',
                                style: Theme.of(context).textTheme.caption)
                          ]))),
                  /*
                      if (house.info.extraInformation != 'yours')

                       */
                  if (houseInformation.description.value != 'yours')
                    Container(
                        width: double.maxFinite,
                        child: Wrap(alignment: WrapAlignment.end, children: <Widget>[
                          /*
                                  if (house.status.canShare)
                                    FlatButton(
                                        padding: EdgeInsets.all(5),
                                        onPressed: () async {
                                          await share(
                                              house.info.toExportableJson(),
                                              house.info.photoUrls, context);
                                        },
                                        child: Icon(Icons.share)),
                                  if (house.status.canChat)
                                    FlatButton(
                                        padding: EdgeInsets.all(5),
                                        onPressed: () {
                                          Navigator.push(
                                              context,
                                              CupertinoPageRoute(
                                                  builder: (context) =>
                                                      Messages(
                                                          messageInstanceId: messageInstanceId,
                                                          noInfo: true
                                                      )
                                              )
                                          );
                                        },
                                        child: Text(
                                            'Чат',
                                            style:
                                            TextStyle(color: Colors.black,
                                                fontSize: 16)
                                        )
                                    ),

                                   */
                          ListTile(
                              title: Align(
                                  alignment: Alignment.centerRight,
                                  child: show == null
                                      ? Text('Закрыть окно',
                                          style: Theme.of(context).textTheme.button.merge(TextStyle(color: Colors.orange, fontSize: 16)))
                                      : Text(isReady ? 'Отменить заявку' : Strings.pages.suitableApartmentsPage.makeOfferText.l(context),
                                          style: Theme.of(context).textTheme.button.merge(TextStyle(color: Colors.orange, fontSize: 16)))),
                              onTap: () {
                                if (show != null) {
                                  show(idSplit);
                                } else {
                                  close();
                                }
                              })
                        ])),
                  SizedBox(height: 8)
                ])));
      }
      final _counter = ValueNotifier<int>(0);
      return Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: Column(mainAxisSize: MainAxisSize.min, children: [
            GestureDetector(
                onTap: () {
                  Navigator.push(context, CupertinoPageRoute(builder: (context) => OfferDetailsPage(edit: false, houseInformation: houseInformation)));
                },
                child: AnimatedSwitcher(
                    duration: Duration(milliseconds: 300),
                    child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        child: Card(
                            key: Key(id),
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(16))),
                            color: Colors.white,
                            clipBehavior: Clip.antiAlias,
                            child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                              /*
                                      FutureBuilder<UserModel>(
                                          future: mindClass.userInfo(uid),
                                          builder: (context, snapshot) {
                                            if (snapshot.hasData &&
                                                snapshot.data.username
                                                    .isNotEmpty) {
                                              return ListTile(
                                                  onTap: () =>
                                                      openUserInfo(
                                                          uid, snapshot.data,
                                                          context),
                                                  contentPadding:
                                                  EdgeInsets.symmetric(
                                                      vertical: 5)
                                                      .copyWith(left: 16),
                                                  title: Text.rich(TextSpan(
                                                      children: <InlineSpan>[
                                                        TextSpan(
                                                            text: snapshot.data
                                                                .username),
                                                        if (DateTime
                                                            .now()
                                                            .millisecondsSinceEpoch -
                                                            snapshot.data
                                                                .lastOnline <=
                                                            60000)
                                                          WidgetSpan(
                                                              alignment: PlaceholderAlignment
                                                                  .top,
                                                              child: ClipOval(
                                                                  child: Container(
                                                                      width: 8,
                                                                      height: 8,
                                                                      color: Colors
                                                                          .greenAccent
                                                                  ))
                                                          ),
                                                        TextSpan(
                                                            text:
                                                            ' • ${snapshot.data
                                                                ?.isConfirmedHumanReadable()}')
                                                      ]
                                                  )),
                                                  subtitle: Text('Рейтинг'),
                                                  leading: CircleAvatar(
                                                      backgroundColor: Colors
                                                          .grey,
                                                      backgroundImage: CachedNetworkImageProvider(
                                                          snapshot.data.avatar)
                                                  )
                                              );
                                            }
                                            return SizedBox();
                                          }),

                                       */
                              LayoutBuilder(builder: (context, constraints) {
                                return SizedBox(
                                    height: MediaQuery.of(context).size.height * 1 / 6,
                                    child: Stack(overflow: Overflow.visible, children: [
                                      Positioned(
                                          top: -11,
                                          bottom: 0,
                                          left: 0,
                                          right: 0,
                                          child: ListView(
                                              physics: BouncingScrollPhysics(),
                                              scrollDirection: Axis.horizontal,
                                              shrinkWrap: true,
                                              children: [
                                                /*
                                                for (var url in house.houseInformation.photos.list)
                                                  Padding(
                                                      padding: EdgeInsets.only(right: 8),
                                                      child: ClipRRect(
                                                          borderRadius: BorderRadius.circular(16),
                                                          child: Stack(children: <Widget>[
                                                            CachedNetworkImage(fit: BoxFit.cover, width: constraints.maxWidth, imageUrl: url),
                                                            Positioned.fill(child: Container(color: Colors.black.withOpacity(0.05)))
                                                          ]))),
*/
                                                ///TODO добавить, если будет надо
//                                    if (house.info.photoUrls.length < 2)
//                                      Container(
//                                        height:
//                                            MediaQuery.of(context).size.height *
//                                                1 /
//                                                6,
//                                        width:
//                                            MediaQuery.of(context).size.width *
//                                                    2 /
//                                                    3 -
//                                                80,
//                                        child: Column(
//                                          mainAxisAlignment:
//                                              MainAxisAlignment.center,
//                                          crossAxisAlignment:
//                                              CrossAxisAlignment.center,
//                                          children: <Widget>[
//                                            Icon(
//                                              Icons.add_a_photo,
//                                              color: Theme.of(context)
//                                                  .primaryColor,
//                                            ),
//                                            Text("Запросить больше фото"),
//                                            Text(
//                                                "Попросите владельца добавить больше фото"),
//                                          ],
//                                        ),
//                                      )
                                              ]))
                                    ]));
                              }),
                              Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Theme(
                                      data: ThemeData(iconTheme: IconThemeData(color: Colors.grey.shade500)),
                                      child: Column(mainAxisSize: MainAxisSize.min, crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                                        Text(houseInformation.stations.values.join(', '),
                                            style: Theme.of(context).textTheme.subtitle1, maxLines: 3, overflow: TextOverflow.ellipsis),
                                        spacer(0, 8),
                                        if (houseInformation.address.value.isNotEmpty)
                                          Text(houseInformation.address.value, style: Theme.of(context).textTheme.subtitle2),
                                        spacer(0, 4),
                                        /*
                                                    if (house.info.rating !=
                                                        null)
                                                      Row(
                                                          mainAxisSize: MainAxisSize
                                                              .min,
                                                          children: <Widget>[
                                                            Text(house.info
                                                                .rating
                                                                .toString()),
                                                            Text(house.info
                                                                .reviews.length
                                                                .toString())
                                                          ]
                                                      )
                                                    else
                                                      Text(
                                                          'Нет ни оценок, не отзывов'),

                                                     */
                                        Text(
                                            'От ${houseInformation.priceRange.startPosition} до ${houseInformation.priceRange.endPosition} руб/сут',
                                            style: Theme.of(context).textTheme.caption)
                                      ]))),
                              /*
                                      if (house.info.extraInformation != 'yours')

                                       */
                              if (houseInformation.description.value != 'yours')
                                Container(
                                    width: double.maxFinite,
                                    child: Wrap(alignment: WrapAlignment.end, children: <Widget>[
                                      true //TODO
                                          ? FlatButton(
                                              padding: EdgeInsets.all(5),
                                              /*
                                                      onPressed: () async {
                                                        await share(
                                                            house.info
                                                                .toExportableJson(),
                                                            house.info
                                                                .photoUrls,
                                                            context);
                                                      },

                                                       */
                                              child: Icon(Icons.share))
                                          : Container(),
                                      true //TODO
                                          ? FlatButton(
                                              padding: EdgeInsets.all(5),
                                              onPressed: () {
                                                Navigator.push(
                                                    context,
                                                    CupertinoPageRoute(
                                                        builder: (context) => Messages(messageInstanceId: messageInstanceId, isOwner: true)));
                                              },
                                              child: Text('Чат', style: TextStyle(color: Colors.black, fontSize: 16)))
                                          : Container(),
                                      Divider(),
                                      ListTile(
                                          title: Align(
                                              alignment: Alignment.centerRight,
                                              child: show == null
                                                  ? Text('Закрыть окно',
                                                      style: Theme.of(context).textTheme.button.merge(TextStyle(color: Colors.orange, fontSize: 16)))
                                                  : Text(isReady ? 'Отменить заявку' : Strings.pages.suitableApartmentsPage.makeOfferText.l(context),
                                                      style:
                                                          Theme.of(context).textTheme.button.merge(TextStyle(color: Colors.orange, fontSize: 16)))),
                                          onTap: () {
                                            if (show != null) {
                                              show(idSplit);
                                            } else {
                                              close();
                                            }
                                          })
                                    ])),
                              SizedBox(height: 8)
                            ]))))),
            /*
                if (!house.info
                    .possibleToBook(CurrentRequestWidget
                    .of(context)
                    .request))
                  Padding(
                      padding: const EdgeInsets.only(top: 8.0).add(
                          EdgeInsets.symmetric(horizontal: 15)
                      ),
                      child: Card(
                          child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Text(
                                        'К сожалению в выбраннные Вами даты невозможно запросить'
                                            ' бронь, квартира забронирована в следующеи даты.',
                                        style: Theme
                                            .of(context)
                                            .textTheme
                                            .bodyText1
                                    ),
                                    Wrap(
                                        alignment: WrapAlignment.start,
                                        crossAxisAlignment: WrapCrossAlignment
                                            .start,
                                        children: [
                                          Chip(label: Text('23.05.23-23.06.23'))
                                        ]
                                    ),
                                    Text(
                                        'Хотите изменить даты у себя в заявке?',
                                        style: Theme
                                            .of(context)
                                            .textTheme
                                            .subtitle2,
                                        textAlign: TextAlign.start
                                    ),
                                    Align(
                                        alignment: Alignment.bottomRight,
                                        child: FlatButton(
                                            onPressed: () {
                                              Navigator.push(
                                                  context,
                                                  CupertinoPageRoute(
                                                      builder: (_) =>
                                                          RequestCreate(
                                                              requestId:
                                                              CurrentRequestWidget
                                                                  .of(context)
                                                                  .request
                                                                  .requestId
                                                          )
                                                  )
                                              ).then((v) {
                                                mindClass.updateEvent(
                                                    'update_suitable_apartments_page');
                                              });
                                            },
                                            child: Text('ДА')))
                                  ]
                              )
                          )
                      )
                  )

                 */
          ]));
    }

    if (houseInformation != null) return houseElement(houseInformation, true);
    //if(idSplit[0] == mindClass.user.uid) return Container();
    /*
    return FutureBuilder(
        future: future ??
            (isReady
                ? mindClass.userHouseByMessageInstance(list[2])
                : mindClass.userHouse(idSplit[1], idSplit[0])),
        builder: (BuildContext context, snapShot) {
          if (snapShot.hasData) {
            House house = snapShot.data;
            return houseElement(house);
          }
          return Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                  elevation: 3,
                  child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                      child: AnimatedSwitcher(
                          duration: Duration(milliseconds: 1000),
                          child: Container(
                              key: Key(messageInstanceId + 'shimmer'),
                              child: getShimmer(context))))));
        });

     */
  }
}
