import 'package:after_layout/after_layout.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:partnerum/models/city_models.dart';
import 'package:partnerum/utils/brain_module.dart';
import 'package:partnerum/utils/theme_constants.dart';

class GetMetroName extends StatefulWidget {
  final String city;
  final List<String> stations;

  const GetMetroName({Key key, @required this.city, this.stations = const <String>[]}) : super(key: key);

  @override
  _GetMetroNameState createState() => _GetMetroNameState();
}

class _GetMetroNameState extends State<GetMetroName> with AfterLayoutMixin<GetMetroName> {
  final _alphForm = GlobalKey<FormState>();
  final _lineForm = GlobalKey<FormState>();
  final _districtForm = GlobalKey<FormState>();

  final TextEditingController _metroNameController = TextEditingController();

  List<String> stations = [];

  String searchString;

  bool selected = false;

  Map<int, Widget> children = <int, Widget>{
    0: Text('По алфавиту', style: TextStyle(fontSize: 14)),
    1: Text('По линиям', style: TextStyle(fontSize: 14)),
    2: Text('По округам', style: TextStyle(fontSize: 14)),
  };

  Map<int, Widget> getChildren() {
    return <int, Widget>{0: Form(key: _alphForm, child: _buildAlph())};
  }

  final int _sharedValue = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    stations = widget.stations;
    print(stations);
    _hideButtonController = ScrollController();
    _hideButtonController.addListener(() {
      if (_hideButtonController.position.userScrollDirection == ScrollDirection.reverse) {
        if (_isVisible == true) {
          /* only set when the previous state is false
             * Less widget rebuilds
             */

          setState(() {
            _isVisible = false;
          });
        }
      } else {
        if (_hideButtonController.position.userScrollDirection == ScrollDirection.forward) {
          if (_isVisible == false) {
            /* only set when the previous state is false
               * Less widget rebuilds
               */

            setState(() {
              _isVisible = true;
            });
          }
        }
      }
    });
  }

  generateList() async {
    toDraw = getChildren()[_sharedValue];
    setState(() {
      pageLoaded = true;
    });
  }

  Widget toDraw;
  ScrollController _hideButtonController;

  @override
  Widget build(BuildContext context) {
    print('redraw');
    // print(pageLoaded);

    return DefaultTabController(
        length: 3,
        child: Scaffold(
            floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
            key: _scaffoldKey,
            appBar: AppBar(
                backgroundColor: Colors.white,
                leading: IconButton(
                    icon: Theme.of(context).platform == TargetPlatform.iOS ? Icon(Icons.arrow_back_ios) : Icon(Icons.arrow_back),
                    onPressed: () {
                      Navigator.of(context).pop();
//                if (stations == null || stations.length == 0) {
//                  Fluttertoast.showToast(
//                    textColor: Colors.white,
//                    backgroundColor: Colors.black54,
//                    msg: "Выберите метро",
//                    toastLength: Toast.LENGTH_SHORT,
//                    gravity: ToastGravity.BOTTOM,
//                    timeInSecForIos: 1,
//                  );
//                  return;
//                }
                    }),
                bottom: TabBar(
                    indicatorColor: Theme.of(context).primaryColor,
                    tabs: [Tab(text: 'По алфавиту'), Tab(text: 'По линиям'), Tab(text: 'По округам')]),
                title: TextField(
                    decoration: InputDecoration(border: InputBorder.none, hintText: 'Поиск по названию'),
                    controller: _metroNameController,
                    onChanged: (value) {
                      setState(() {
                        searchString = value.toLowerCase();
                      });
                    }),
                automaticallyImplyLeading: true,
                iconTheme: IconThemeData(color: Colors.black),
                textTheme: TextTheme(headline6: TextStyle(color: Colors.black, fontSize: 20.0)),
                actions: <Widget>[
                  IconButton(
                      icon: Icon(Icons.clear),
                      onPressed: () {
                        _metroNameController.clear();
                      })
                ]),
            body: TabBarView(children: [
              Builder(
                  key: Key('0p'),
                  builder: (BuildContext context) {
                    return FutureBuilder<List<Stations>>(
                        initialData: [],
                        future: mindClass.getMetroFromCity(widget.city),
                        builder: (context, snapshot) {
                          if (!snapshot.hasData) {
                            return AnimatedSwitcher(
                                duration: Duration(milliseconds: 300),
                                child: ListView(
                                    children: <Widget>[SizedBox(key: Key(pageLoaded.toString()), height: 2, child: linearProgressIndicator)]));
                          }
                          var allStations = <Stations>[];
                          if (_metroNameController.text.isNotEmpty) {
                            var search = _metroNameController.text.toLowerCase();
                            for (var i = 0; i < snapshot.data.length; i++) {
                              var city = snapshot.data[i].name.toLowerCase();
                              if (city.contains(search) || search.contains(city)) {
                                allStations.add(snapshot.data[i]);
                              }
                            }
                          } else {
                            allStations = snapshot.data;
                          }
                          return AnimatedSwitcher(
                              duration: Duration(milliseconds: 300),
                              child: ListView.builder(
                                  key: Key(_metroNameController.text + '0p'),
                                  //shrinkWrap: true,
                                  controller: _hideButtonController,
                                  physics: BouncingScrollPhysics(),
                                  itemCount: allStations.length,
                                  itemBuilder: (context, index) => _buildSearchItem(context, allStations[index])));
                        });
                  }),
              Builder(
                  key: Key('1p'),
                  builder: (BuildContext context) {
                    return FutureBuilder<JsonCity>(
                        initialData: null,
                        future: mindClass.getCityMetros(widget.city),
                        builder: (context, snapshot) {
                          if (!snapshot.hasData) {
                            return AnimatedSwitcher(
                                duration: Duration(milliseconds: 300),
                                child: ListView(
                                    children: <Widget>[SizedBox(key: Key(pageLoaded.toString()), height: 2, child: linearProgressIndicator)]));
                          }
                          var allLines2 = <Lines>[];
                          allLines2 = snapshot.data.lines;
                          return AnimatedSwitcher(
                              duration: Duration(milliseconds: 300),
                              child: ListView.builder(
                                  controller: _hideButtonController,
                                  key: Key(_metroNameController.text + '1p'),
                                  physics: BouncingScrollPhysics(),
                                  //shrinkWrap: true,,
                                  itemBuilder: (BuildContext context, int index) {
                                    return _buildLineItem(context, allLines2[index], index);
                                  },
                                  itemCount: allLines2.length));
                        });
                  }),
              Builder(
                  key: Key('2p'),
                  builder: (BuildContext context) {
                    return FutureBuilder<Map<String, Lines>>(
                        initialData: null,
                        future: mindClass.getRounds(widget.city),
                        builder: (context, snapshot) {
                          //if (widget.city != 'Москва') return Container();
                          if (!snapshot.hasData) {
                            return AnimatedSwitcher(
                                duration: Duration(milliseconds: 300),
                                child: ListView(
                                    children: <Widget>[SizedBox(key: Key(pageLoaded.toString()), height: 2, child: linearProgressIndicator)]));
                          }
                          var allLines = <Lines>[];
                          allLines = snapshot.data.values.toList();

                          var count = 0;
                          return AnimatedSwitcher(
                              duration: Duration(milliseconds: 300),
                              child: ListView.builder(
                                  controller: _hideButtonController,
                                  key: Key(_metroNameController.text + '1p'),
                                  physics: BouncingScrollPhysics(),
                                  //shrinkWrap: true,,
                                  itemBuilder: (BuildContext context, int index) {
                                    return _buildLineItem(context, allLines[index], index, true);
                                  },
                                  /*
                        itemCount: allLines.length

                           */
                                  itemCount: 0));
                        });
                  })
            ]),
//      bottomNavigationBar: BottomAppBar(
//        child: DefaultTabController(
//          child: TabBar(tabs: [
//            Tab(
//              text: "По алфавиту",
//            ),
//            Tab(
//              text: "По линиям",
//            )
//          ]),
//          length: 2,
//        ),
//      ),
            floatingActionButton: AnimatedOpacity(
                duration: Duration(milliseconds: 300),
                opacity: _isVisible ? 1 : 0,
                child: IgnorePointer(
                    ignoring: !_isVisible,
                    child: FloatingActionButton.extended(
                        label: Text('ДОБАВИТЬ СТАНЦИИ'),
                        //width: MediaQuery.of(context).size.width,

                        backgroundColor: Theme.of(context).primaryColor,
                        onPressed: () {
                          if (stations == null || stations.isEmpty) {
                            _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text('Выберите метро'), duration: Duration(milliseconds: 300)));
                            return;
                          }
                          Navigator.pop(context, stations);
//            Fluttertoast.showToast(
//              textColor: Colors.white,
//              backgroundColor: Colors.black54,
//              msg: "Метро (${stations.length})",
//              toastLength: Toast.LENGTH_SHORT,
//              gravity: ToastGravity.BOTTOM,
//              timeInSecForIos: 1,
//            );
                        })))));
  }

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  bool _isVisible = true;

  Map<String, ExpandableController> panels = {};

  ExpandablePanel _buildLineItem(BuildContext context, Lines line, index, [expand = false]) {
    var allStations = <Stations>[];
    if (_metroNameController.text.isNotEmpty) {
      var search = _metroNameController.text.toLowerCase();
      for (var i = 0; i < line.stations.length; i++) {
        var city = line.stations[i].name.toLowerCase();
        if (city.contains(search) || search.contains(city)) {
          allStations.add(line.stations[i]);
        }
      }
    } else {
      allStations = line.stations;
    }
    if (!panels.containsKey(line.name)) {
      panels[line.name] = ExpandableController(initialExpanded: false);
    }

    bool isLine() {
      for (var i = 0; i < allStations.length; i++) {
        if (!stations.contains(allStations[i].name)) return false;
      }
      return true;
    }

    return ExpandablePanel(
        //collapsed: Text('N', softWrap: true, maxLines: 2, overflow: TextOverflow.ellipsis,),
        header: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(children: <Widget>[
              Flexible(child: Text(line.name)),
              Checkbox(
                  value: isLine(),
                  onChanged: (bool val) {
                    setState(() {
                      if (val) {
                        allStations.forEach((v) {
                          stations.add(v.name);
                        });

                        print('1-> $stations');
                      } else {
                        allStations.forEach((v) {
                          stations.remove(v.name);
                        });
                        print('1-> $stations');
                      }
                      selected = val;
                    });
                  })
            ])),
        controller: panels[line.name],
        //isExpanded: expand ? roundsExpanded[index] : linesExpanded[index],
        expanded: ListView(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            children: <Widget>[for (var i = 0; i < allStations.length; i++) _buildSearchItem(context, allStations[i])]));
  }

  Widget _buildSearchItem(BuildContext context, Stations document) {
    return ListTile(
        leading: Container(
            height: 25,
            width: 25,
            decoration: BoxDecoration(color: document.color, shape: BoxShape.circle, border: Border.all(color: Colors.transparent))),
        title: Text('${document.name}', overflow: TextOverflow.fade),
        trailing: Checkbox(
            activeColor: Theme.of(context).primaryColor,
            value: stations.contains(document.name),
            onChanged: (bool val) {
              if (val) {
                stations.add(document.name);
                print('2-> $stations');
              } else {
                stations.remove(document.name);
                print('2-> $stations');
              }
              selected = val;
              setState(() {});
              // setState(() {
              //
              // });
            }));
  }

  Widget _buildAlph() {
    print('alphabet');
    return FutureBuilder<JsonCity>(
        future: mindClass.getCityMetros(widget.city),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Container(
                alignment: FractionalOffset.center,
                child: Center(child: Theme.of(context).platform == TargetPlatform.iOS ? CupertinoActivityIndicator() : CircularProgressIndicator()));
          }
          var allStations = <Stations>[];
          for (var i = 0, l = snapshot.data.lines.length; i < l; i++) {
            snapshot.data.lines[i].stations.forEach((value) {
              var _station = value;
              _station.color = Color(int.parse('0xff' + snapshot.data.lines[i].hexColor));
              _station.number = snapshot.data.lines[i].id;
              allStations.add(value);
            });
          }
          allStations.sort((s1, s2) {
            return s1.name.compareTo(s2.name);
          });
          return Padding(
              padding: const EdgeInsets.only(bottom: 40.0),
              child: ListView.builder(
                  shrinkWrap: true,
                  physics: ClampingScrollPhysics(),
                  itemCount: allStations.length,
                  itemBuilder: (context, index) => _buildSearchItem(context, allStations[index])));
        });
  }

  getMetros() {}

  bool pageLoaded = false;

  @override
  void afterFirstLayout(BuildContext context) {
//    setState(() {
//      print("yeah");
//      pageLoaded = true;
//    });
  }
}
