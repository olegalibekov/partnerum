import 'package:db_partnerum/db_partnerum.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:partnerum/models/city_models.dart';
import 'package:partnerum/utils/brain_module.dart';
import 'package:partnerum/utils/theme_constants.dart';

class GetCityName extends StatefulWidget {
  @override
  _GetCityNameState createState() => _GetCityNameState();
}

class _GetCityNameState extends State<GetCityName> {
  final TextEditingController _cityNameController = TextEditingController();

  String searchString;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(50.0),
            child: AppBar(
                backgroundColor: Colors.white,
                leading: IconButton(
                    icon: Theme.of(context).platform == TargetPlatform.iOS
                        ? Icon(Icons.arrow_back_ios)
                        : Icon(Icons.arrow_back),
                    onPressed: () {
                      if (_cityNameController.text == '') {
                        Fluttertoast.showToast(
                          textColor: Colors.white,
                          backgroundColor: Colors.black54,
                          msg: "Выберите город",
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.BOTTOM,
                          timeInSecForIosWeb: 1,
                        );
                        return;
                      }
                      Navigator.pop(context, _cityNameController.text);
                    }),
                title: TextField(
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Поиск по названию'),
                    controller: _cityNameController,
                    onChanged: (value) {
                      setState(() {
                        searchString = value.toLowerCase();
                      });
                    }),
                automaticallyImplyLeading: true,
                iconTheme: IconThemeData(color: Colors.black),
                textTheme: TextTheme(
                    headline6: TextStyle(color: Colors.black, fontSize: 20.0)),
                actions: <Widget>[
                  IconButton(
                      icon: Icon(Icons.clear),
                      onPressed: () {
                        _cityNameController.clear();
                        Navigator.pop(context);
                      })
                ])),
        backgroundColor: Colors.white,
        body: FutureBuilder<List<JsonCity>>(
            future: mindClass.getCities(),
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return Container(
                    alignment: FractionalOffset.center,
                    child: Center(
                        child: Theme.of(context).platform == TargetPlatform.iOS
                            ? CupertinoActivityIndicator()
                            : CircularProgressIndicator()));
              }
              var cities = <JsonCity>[];
              if (_cityNameController.text.isNotEmpty) {
                var search = _cityNameController.text.toLowerCase();
                for (var i = 0; i < snapshot.data.length; i++) {
                  var city = snapshot.data[i].name.toLowerCase();
                  if (city.contains(search) || search.contains(city)) {
                    cities.add(snapshot.data[i]);
                  }
                }
              } else {
                cities = snapshot.data;
              }
              return AnimatedSwitcher(
                  duration: Duration(milliseconds: 600),
                  child: ScrollConfiguration(
                    behavior: noHighlightScrollBehavior(context),
                    child: ListView.builder(
                        key: Key(_cityNameController.text),
                        physics: ClampingScrollPhysics(),
                        itemCount: cities.length,
                        itemBuilder: (context, index) =>
                            _buildSearchItem(context, cities[index].name)),
                  ));
            }));
  }

  Widget _buildSearchItem(BuildContext context, String document) {
    return Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
      ListTile(
          title: Text('${document}'),
          onTap: () {
            _cityNameController.text = document;
            if (_cityNameController.text == null ||
                _cityNameController.text.isEmpty) {
              return;
            }
            Navigator.pop(context, _cityNameController.text);
            Fluttertoast.showToast(
              textColor: Colors.white,
              backgroundColor: Colors.black54,
              msg: "${_cityNameController.text}",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
            );
          }),
//         Divider(),
    ]);
  }
}
