import 'dart:async';

import 'package:db_partnerum/db_partnerum.dart';
import 'package:flutter/cupertino.dart';
import 'package:micro_utils/data_helper.dart';
import 'package:partnerum/head_module/gen_extensions/generated_str_extension.dart';
import 'package:partnerum/utils/brain_module.dart';

@DataHelper()
class NotificationHelper {
  List notifications = [];

  // Future notificationsUpload() =>
  //     root.userData[mindClass.user.uid].notifications.future
  //         .then((value) => value.values.toList());

  Stream notificationsUpload() =>
      root.userData[mindClass.user.uid].notifications.stream
          .map((event) => event?.values?.toList() ?? []);

  String getHumanReadableTitle(int index, BuildContext context) {
    var title = notifications[index]?.notification?.title?.value;
    return (title != null) ? title : Strings.common.error.l(context);
    //common.error
  }

  String getHumanReadableTitleByObject(
      UserNotification userNotification, BuildContext context) {
    var title = userNotification?.notification?.title?.value;
    return (title != null) ? title : Strings.common.error.l(context);
  }

  Future<String> getApartmentFirstPhoto(
      UserNotification userNotification) async {
    var photoName = userNotification?.notification?.image?.value;
    var photoObj = await root.imagesContainer[photoName].bigImg.future;

    return photoObj.urlWithToken;
  }

  bool isRead(int index) {
    var readValue = notifications[index]?.read?.value;
    return readValue ?? true;
  }

  bool isReadByObject(UserNotification userNotification) =>
      userNotification.read.value;

  int notificationTime(int index) =>
      notifications[index].timestamp.value.inMilliseconds;

  int notificationTimeByObject(UserNotification userNotification) =>
      userNotification.timestamp.value.inMilliseconds;

  String notificationBody(int index, BuildContext context) {
    var body = notifications[index]?.notification?.body?.value;
    return (body != null) ? body : Strings.globalOnes.error.l(context);
  }

  String notificationTitleByObject(
      UserNotification userNotification, BuildContext context) {
    var body = userNotification?.notification?.title?.value;
    return (body != null) ? body : Strings.globalOnes.error.l(context);
  }

  String notificationBodyByObject(
      UserNotification userNotification, BuildContext context) {
    var body = userNotification?.notification?.body?.value;
    return (body != null) ? body : Strings.globalOnes.error.l(context);
  }

  bool isShowImage(int index) {
    // => ((notifications[index].notification.image.value.isNotEmpty) &&
    //      (isRead(index)));
    var image = notifications[index].notification?.image?.value;
    if ((image != null) && (image.isNotEmpty) && (isRead(index))) {
      return true;
    }
    return false;
  }

  void readNotification(int index) {
    root.userData[mindClass.user.uid].notifications[notifications[index].key]
        .read
        .set(true);
  }

  void readNotificationByKey(String notificationKey) {
    root.userData[mindClass.user.uid].notifications[notificationKey].read
        .set(true);
  }

  Future<UserNotification> notificationById(String notificationKey) async {
    var notification = await root
        ?.userData[mindClass.user.uid]?.notifications[notificationKey]?.future;
    return notification;
  }

  Future<void> clearNotifications() async {
    for (var notification in notifications) {
      notification.remove();
    }
  }

  Future<void> deleteNotification(int index) async {
    var notId = notifications[index].key;
    await root.userData[mindClass.user.uid].notifications[notId].remove();
  }

  FutureOr<void> setUserToken(String token) async {
    await root.userData[mindClass.user.uid].userInformation.tokens[token]
        .set('0');
  }

  Future<void> removeToken() async {
    if (mindClass?.user?.uid != null) {
      await root.userData[mindClass.user.uid]?.userInformation
          ?.tokens[mindClass.currentNotificationToken]
          ?.remove();
    }
  }
}
