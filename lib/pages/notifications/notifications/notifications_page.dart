import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:partnerum/head_module/gen_extensions/generated_str_extension.dart';
import 'package:partnerum/pages/chat/messages/messages_page.dart';
import 'package:partnerum/tools/firebase_notifcation_handler.dart';
import 'package:partnerum/tools/firebase_notification_handler_original.dart';
import 'package:partnerum/utils/brain_module.dart';
import 'package:partnerum/utils/theme_constants.dart';
import 'package:db_partnerum/db_partnerum.dart';
import 'package:shimmer/shimmer.dart';
import '../message_instance_page.dart';
import 'notifications_helper.dart';
import 'package:partnerum/head_module/gen_extensions/generated_files_extension.dart';

class NotificationsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _NotificationsPage();
}

class _NotificationsPage extends State<NotificationsPage>
    with TickerProviderStateMixin, NotificationHelper {
  var notificationsStream;

  Widget _buildZeroNotifications(BuildContext context) {
    return SvgPicture.asset(files.images.undraw.notifySvg);
  }

  @override
  void initState() {
    super.initState();
    notificationsStream = root.userData[mindClass.user.uid].notifications.stream
        .map((event) => event?.values?.toList())
        .asBroadcastStream();
    mindClass.eventSteam.outStream.listen((value) {
      if (value == 'updateBottom') setState(() {});
    });
  }

  final ValueNotifier<bool> _loaderVisibility = ValueNotifier<bool>(true);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: StreamBuilder(
            /*
            future: mindClass.getNotifications(),
           */
            // future: notificationsUpload(),
            // stream: notificationsUpload(),
            stream: notificationsStream,
            // stream: root.userData[mindClass.user.uid].notifications.stream,
            builder: (context, snapshot) {
              Key getKey() {
                if (snapshot.hasData) {
                  // UserNotification().timestamp.value
                  notifications = snapshot.data;
                  notifications.sort(
                      (a, b) => b.timestamp.value.compareTo(a.timestamp.value));
                  if (notifications.isEmpty) return Key('notifications_zero');

                  return Key('notifications_loaded');
                }
                return Key('notifications_loading');
              }

              return RefreshIndicator(
                  key: getKey(),
                  displacement: 80,
                  onRefresh: () {
                    setState(() {});
                    return Future.delayed(Duration(milliseconds: 300));
                  },
                  child: ScrollConfiguration(
                      behavior: noHighlightScrollBehavior(context),
                      child: CustomScrollView(slivers: [
                        SliverAppBar(
                            pinned: true,
                            title: Text(Strings
                                .pages.notificationsPage.notifications
                                .l(context)),
                            centerTitle: true,
                            bottom: PreferredSize(
                                preferredSize: Size.fromHeight(2),
                                child: ValueListenableBuilder(
                                    valueListenable: _loaderVisibility,
                                    builder: (BuildContext context, bool value,
                                            Widget child) =>
                                        AnimatedOpacity(
                                            duration:
                                                Duration(milliseconds: 300),
                                            opacity: value ? 1 : 0,
                                            child: linearProgressIndicator))),
                            actions: [
                              IconButton(
                                  icon: Icon(Icons.delete_outline),
                                  onPressed: () async {
                                    await showModalBottomSheet(
                                        shape: RoundedRectangleBorder(
                                            borderRadius: borderRadius),
                                        context: context,
                                        builder: (_context) {
                                          return Padding(
                                              padding:
                                                  const EdgeInsets.all(16.0),
                                              child: Container(
                                                  child: Column(
                                                      mainAxisSize:
                                                          MainAxisSize.min,
                                                      children: <Widget>[
                                                    ListTile(
                                                        title: Text(Strings
                                                            .pages
                                                            .notificationsPage
                                                            .clearNotifications
                                                            .l(context)),
                                                        subtitle: Text(Strings
                                                            .pages
                                                            .notificationsPage
                                                            .clearAlertWarning
                                                            .l(context))),
                                                    SizedBox(height: 16 * 2.0),
                                                    Container(
                                                        width: double.maxFinite,
                                                        child: Wrap(
                                                            alignment:
                                                                WrapAlignment
                                                                    .end,
                                                            spacing: 8,
                                                            children: <Widget>[
                                                              FlatButton(
                                                                  color: Colors
                                                                          .grey[
                                                                      300],
                                                                  onPressed: () =>
                                                                      Navigator.pop(
                                                                          _context),
                                                                  child: Text(
                                                                      Strings
                                                                          .pages
                                                                          .notificationsPage
                                                                          .later
                                                                          .l(
                                                                              context),
                                                                      style: TextStyle(
                                                                          fontSize:
                                                                              16,
                                                                          color:
                                                                              Colors.white))),
                                                              FlatButton(
                                                                  color: Theme.of(
                                                                          context)
                                                                      .primaryColor,
                                                                  onPressed:
                                                                      () async {
                                                                    await clearNotifications();
                                                                    /*
                                                                        await mindClass
                                                                            .removeAllNotifications();

                                                                         */
                                                                    Navigator.pop(
                                                                        context);
                                                                  },
                                                                  child: Text(
                                                                      Strings
                                                                          .pages
                                                                          .notificationsPage
                                                                          .agreement
                                                                          .l(
                                                                              context),
                                                                      style: TextStyle(
                                                                          fontSize:
                                                                              16,
                                                                          color:
                                                                              Colors.white)))
                                                            ]))
                                                  ])));
                                        });
                                    setState(() {});
                                  })
                            ]),
                        Builder(builder: (context) {
                          if (snapshot.hasData) {
                            // var notifications = snapshot.data;
                            if (notifications.isEmpty) {
                              Timer(kThemeChangeDuration,
                                  () => _loaderVisibility.value = false);
                              return SliverToBoxAdapter(
                                  child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                    Padding(
                                        padding: const EdgeInsets.only(
                                            left: 48.0, right: 48.0, top: 48.0),
                                        child: Container(
                                            width: 300,
                                            height: 300,
                                            child: _buildZeroNotifications(
                                                context))),
                                    Container(
                                        // margin: EdgeInsets.symmetric(vertical: 100, _buildZeroNotificationshorizontal: 16),
                                        child: Text(
                                            Strings.pages.notificationsPage
                                                .emptyNotifications
                                                .l(context),
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 14.0)))
                                  ]));
                              // return SliverToBoxAdapter(
                              //     child: Container(
                              //         margin: EdgeInsets.all(20),
                              //         child: Stack(children: <Widget>[
                              //           Column(
                              //               crossAxisAlignment:
                              //                   CrossAxisAlignment.center,
                              //               children: <Widget>[
                              //                 _buildZeroNotifications(context),
                              //                 Padding(
                              //                     padding:
                              //                         const EdgeInsets.only(
                              //                             top: 16.0),
                              //                     child: Text(
                              //                         Strings.pages.notificationsPage.emptyNotifications.l(context),
                              //                         textAlign:
                              //                             TextAlign.center,
                              //                         style: TextStyle(
                              //                             color: Colors.black,
                              //                             fontSize: 14.0)))
                              //               ])
                              //         ])));
                            }
                            Builder action(int index) {
                              return Builder(
                                  builder: (BuildContext buildContext) {
                                return IconSlideAction(
                                    caption: Strings
                                        .pages.notificationsPage.delete
                                        .l(context),
                                    color: Colors.red,
                                    icon: Icons.delete_outline,
                                    onTap: () {
                                      deleteNotification(index);
                                      setState(() {});
                                      Slidable.of(buildContext).dismiss();
                                    });
                              });
                            }

                            return SliverList(
                                delegate: SliverChildBuilderDelegate(
                                    (context, index) {
                              return Slidable(
                                actions: <Widget>[action(index)],
                                secondaryActions: <Widget>[action(index)],
                                actionPane: SlidableDrawerActionPane(),
                                actionExtentRatio: 0.25,
                                key: Key(notifications[index].key),
                                dismissal: SlidableDismissal(
                                    child: SlidableDrawerDismissal(),
                                    onWillDismiss: (item) async {
                                      /*
                                              return await mindClass.removeNotification(
                                                  notifications[index].key);
                                               */
                                    },
                                    onDismissed: (actionType) {
                                      setState(() {
                                        notifications.removeAt(index);
                                      });
                                    }),
                                child: Padding(
                                    padding: const EdgeInsets.only(
                                        top: 8, left: 16, right: 16),
                                    child: Card(
                                        clipBehavior: Clip.antiAlias,
                                        child: ListTile(
                                            title: Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Flexible(
                                                    child: Text(
                                                      getHumanReadableTitle(
                                                          index, context),
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      /*
                                                                notifications[index]
                                                                .getHumanReadableType()

                                                                 */
                                                    ),
                                                  ),
                                                  if (
                                                  /*
                                                          !notifications[index]
                                                              .read

                                                           */
                                                  !isRead(index))
                                                    ClipOval(
                                                        child: Container(
                                                            width: 8,
                                                            height: 8,
                                                            color: Colors.red)),
                                                  SizedBox(width: 8),
                                                  Text(
                                                      DateFormat(
                                                              'yyyy.MM.dd HH:mm')
                                                          .format(DateTime
                                                              .fromMillisecondsSinceEpoch(
                                                                  /*
                                                                          notifications[
                                                                                  index]
                                                                              .timestamp

                                                                 */
                                                                  notificationTime(
                                                                      index))),
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .caption)
                                                ]),
                                            contentPadding: EdgeInsets.all(8),
                                            subtitle: Column(
                                                mainAxisSize: MainAxisSize.min,
                                                children: [
                                                  Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              top: 4.0),
                                                      child: Align(
                                                        alignment: Alignment
                                                            .centerLeft,
                                                        child: Text(
                                                            /*
                                                                    notifications[index]
                                                                        .body

                                                                     */
                                                            notificationBody(
                                                                index,
                                                                context)),
                                                      )),
                                                  // if (notificationImage != null)
                                                  Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              top: 8.0),
                                                      child: AnimatedSwitcher(
                                                          duration:
                                                              kThemeAnimationDuration,
                                                          child: notifications[
                                                                          index]
                                                                      ?.notification
                                                                      ?.image
                                                                      ?.value !=
                                                                  null
                                                              ? ClipRRect(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              16),
                                                                  child: CachedNetworkImage(
                                                                      imageUrl: notifications[index]?.notification?.image?.value,
                                                                      imageBuilder: (context, imageProvider) => Container(height: 125, decoration: BoxDecoration(image: DecorationImage(image: imageProvider, fit: BoxFit.cover))),
                                                                      placeholder: (context, url) => Shimmer.fromColors(
                                                                            highlightColor:
                                                                                Colors.orange,
                                                                            baseColor:
                                                                                Colors.orangeAccent,
                                                                            child:
                                                                                Container(height: 125)
                                                                          ),
                                                                      errorWidget: (context, url, error) {
                                                                        return SizedBox(height: 125, child: Center(child: Icon(Icons.error)));
                                                                      }))
                                                              : SizedBox.shrink()))
                                                ]),
                                            // subtitle: Column(mainAxisSize: MainAxisSize.min,children: [Container(color: Colors.red, height: 100,)]),
                                            onTap: () async {
                                              /*
                                                      await mindClass.readNotification(
                                                          notifications[index].key);

                                                       */
                                              readNotification(index);
                                              setState(() {});
                                              var offerObjectId =
                                                  notifications[index]
                                                      ?.offerObjectId
                                                      ?.value;
                                              var chatId = notifications[index]
                                                  ?.chatId
                                                  ?.value;

                                              if (offerObjectId != null) {
                                                await Navigator.push(
                                                    context,
                                                    CupertinoPageRoute(
                                                        builder: (context) =>
                                                            InstancePage(
                                                                message:
                                                                    notifications[
                                                                        index],
                                                                offerObjectId:
                                                                    offerObjectId)));
                                              } else if (chatId != null) {
                                                await Navigator.push(
                                                    context,
                                                    CupertinoPageRoute(
                                                        builder: (context) =>
                                                            Messages(
                                                                messageInstanceId:
                                                                    chatId))).then(
                                                    (v) {
                                                  mindClass.ramData[
                                                      'last_notification_instance'] = '';
                                                });
                                              }

                                              /*
                                                      launchNotification(
                                                          notifications[index],
                                                          context);

                                                       */
                                            }))),
                              );
                            }, childCount: notifications.length));
                          }
                          Timer(kThemeChangeDuration,
                              () => _loaderVisibility.value = false);
                          return SliverToBoxAdapter(
                              child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                Padding(
                                    padding: const EdgeInsets.only(
                                        left: 48.0, right: 48.0, top: 48.0),
                                    child: Container(
                                        width: 300,
                                        height: 300,
                                        child:
                                            _buildZeroNotifications(context))),
                                Container(

                                    // margin: EdgeInsets.symmetric(vertical: 100, _buildZeroNotificationshorizontal: 16),
                                    child: Text(
                                        Strings.pages.notificationsPage
                                            .emptyNotifications
                                            .l(context),
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 14.0)))
                              ]));

                          // return SliverToBoxAdapter(
                          //     child: Stack(children: <Widget>[
                          //   Align(
                          //       child: SizedBox(
                          //           height: 2, child: linearProgressIndicator),
                          //       alignment: Alignment.topCenter),
                          //   Container(
                          //       margin: EdgeInsets.all(20),
                          //       child: Column(
                          //           crossAxisAlignment:
                          //               CrossAxisAlignment.center,
                          //           children: <Widget>[
                          //             _buildZeroNotifications(context),
                          //             Padding(
                          //                 padding:
                          //                     const EdgeInsets.only(top: 16.0),
                          //                 child: Text(
                          //                     Strings.pages.notificationsPage.emptyNotifications.l(context),
                          //                     textAlign: TextAlign.center,
                          //                     style: TextStyle(
                          //                         color: Colors.black,
                          //                         fontSize: 14.0)))
                          //           ]))
                          // ]));
                        })
                      ])));
            }));
  }
}
