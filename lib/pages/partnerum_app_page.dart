import 'package:flutter/material.dart';
import 'package:partnerum/utils/app_localizations.dart';
import 'package:partnerum/utils/brain_module.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:partnerum/utils/router.dart';
import 'package:partnerum/utils/theme_constants.dart';

import '../main.dart';

class PartnerumApp extends StatefulWidget {
  PartnerumApp()
      : assert(() {
    debugMode = true;
    return true;
  }());

  @override
  State<StatefulWidget> createState() => _PartnerumApp();
}

class _PartnerumApp extends State<PartnerumApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        initialRoute: mindClass.isUserExist() ? 'walk' : 'home',
        localizationsDelegates: [
          AppLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        onGenerateRoute: FluroRouter.router.generator,
        localeResolutionCallback: (locale, supportedLocales) {
          for (var supportedLocale in supportedLocales) {
            if (supportedLocale.languageCode == locale.languageCode) {
              return supportedLocale;
            }
          }
          return supportedLocales.first;
        },
        supportedLocales: [const Locale('ru', 'RU')],
        title: 'Partnerum',
        theme: mainTheme,
        debugShowCheckedModeBanner: false);
  }
}
