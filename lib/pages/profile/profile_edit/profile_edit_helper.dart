import 'dart:io';
import 'dart:typed_data';
import 'package:db_partnerum/db_partnerum.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:micro_utils/data_helper.dart';
import 'package:db_partnerum/db_partnerum.dart';
import 'package:partnerum/utils/brain_module.dart';
import 'package:path/path.dart' as path;

@DataHelper()
class ProfileEditHelper {
  File image;

  Future<void> uploadImage(image) async {
    var img = root.userData[mindClass.user.uid].userInformation.avatar;
    var image8List = await testCompressFile(image);
    await img.set(FileWrapper(path.basename(image.path), image8List));
    loadToContainer(image);
  }

  void loadToContainer(image) async {
    var image8List = await testCompressFile(image);
    var containerImg = root.imagesContainer[mindClass.user.uid].smallImg;
    await containerImg.set(FileWrapper(path.basename(image.path), image8List));
    root.userData[mindClass.user.uid].userInformation.avatarId.set(mindClass.user.uid);
  }

  Future<Uint8List> testCompressFile(File file) async {
    var result = await FlutterImageCompress.compressWithFile(
      file.path,
      quality: 10,
      rotate: 0,
    );
    return result;
  }
}
