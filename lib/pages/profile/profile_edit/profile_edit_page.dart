import 'dart:io';

import 'package:db_partnerum/db_partnerum.dart';
import 'package:partnerum/head_module/gen_extensions/generated_str_extension.dart';
import 'package:partnerum/utils/brain_module.dart';
import 'package:partnerum/utils/theme_constants.dart';
import 'package:partnerum/widgets/partnerum_icons.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';
import 'profile_edit_helper.dart';

class ProfileEditPage extends StatefulWidget {
  @override
  _ProfileEditPageState createState() => _ProfileEditPageState();
}

class _ProfileEditPageState extends State<ProfileEditPage> with AutomaticKeepAliveClientMixin, ProfileEditHelper {
  SharedPreferences prefs;
  String userImage = '';
  bool isLoading = false;
  File userFile;

  TextEditingController _userNameController = TextEditingController();

  var showImageUpload = false;
  var imageIsLoaded = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,

          title: Text(Strings.pages.profileEditPage.title.l(context), style: TextStyle(fontSize: 20)),
          automaticallyImplyLeading: true,
          centerTitle: true,
          iconTheme: IconThemeData(color: Colors.black),
          textTheme: TextTheme(title: TextStyle(color: Colors.black, fontSize: 22)),
          backgroundColor: Colors.white,
          leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.black),
              onPressed: () {
                Navigator.pop(context, image);
              }),
//        actions: <Widget>[
//           IconButton(
//            icon:  Icon(Icons.check, color: Colors.black),
//            onPressed: () {},
//          ),
//        ],
        ),
        backgroundColor: Colors.white,
        body: FutureBuilder(
            /*
          future: mindClass.userInfo(),

         */
            future: root.userData[mindClass.user.uid].userInformation.future,
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                return Container(
                    color: Colors.white,
                    alignment: FractionalOffset.center,
                    child: Center(child: Text('${Strings.globalOnes.error.l(context)}: ${snapshot.error}')));
              }
              if (!snapshot.hasData) {
                return Container(
                    color: Colors.white,
                    alignment: FractionalOffset.center,
                    child:
                        Center(child: Theme.of(context).platform == TargetPlatform.iOS ? CupertinoActivityIndicator() : CircularProgressIndicator()));
              }

              final UserInformation document = snapshot.data;

              // print(_userNameController.text + document.username.value);
              if (_userNameController.text != document.username.value) if ((document.username.value != null) &&
                  (document.username.value.isNotEmpty)) {
                _userNameController.text = document.username.value;
              }

              return ScrollConfiguration(
                behavior: noHighlightScrollBehavior(context),
                child: ListView(children: <Widget>[
                  Container(
                      height: MediaQuery.of(context).size.height / 4,
                      child: Align(
                          alignment: Alignment.center,
                          child: Stack(children: <Widget>[
                            if (!showImageUpload && image == null) _buildImage(document, context),
                            if (showImageUpload) CircularProgressIndicator(),
                            if ((!showImageUpload && imageIsLoaded && image != null)) decoratedImage(FileImage(image)),
                            if (!showImageUpload)
                              Positioned(
                                  top: 0.0,
                                  right: 0.0,
                                  child: Container(
                                      decoration: BoxDecoration(
                                          border: Border.all(color: Colors.white, width: 3), borderRadius: BorderRadius.all(Radius.circular(40))),
                                      child: ClipOval(
                                          child: Material(
                                              color: Theme.of(context).primaryColor,
                                              // button color
                                              child: InkWell(
                                                splashColor: Colors.black,
                                                // inkwell color
                                                onTap: () async {
                                                  setState(() => showImageUpload = true);
                                                  await pickImage();

                                                  showImageUpload = false;
                                                  setState(() => imageIsLoaded = true);
                                                },
                                                child: SizedBox(width: 35, height: 35, child: Icon(Partnerum.edit, size: 20, color: Colors.white)),
                                              )))))
                          ]))),
                  ListTile(
                      leading: Icon(Partnerum.profile, color: Theme.of(context).primaryColor),
                      title: TextFormField(
                          enableInteractiveSelection: true,
                          controller: _userNameController,
                          cursorColor: Theme.of(context).primaryColor,
                          textInputAction: TextInputAction.go,
                          decoration: InputDecoration(
                              hintText: Strings.pages.profileEditPage.enterName.l(context),
                              suffixIcon: IconButton(
                                  icon: Icon(Icons.check),
                                  onPressed: () async {
                                    //SystemChannels.textInput
                                    //   .invokeMethod('TextInput.hide');

                                    if ((_userNameController.text != null) && (_userNameController.text.isNotEmpty)) {
                                      /*
                              await mindClass.writeUserInfo(
                                  _userNameController.text, document.document);

                               */

                                      document.username = _userNameController.text;

                                      // setState(() {});
                                      await Fluttertoast.showToast(
                                          textColor: Colors.white,
                                          backgroundColor: Colors.black87,
                                          msg: '${Strings.pages.profileEditPage.changedNameMessage.l(context)} - ${_userNameController.text}',
                                          toastLength: Toast.LENGTH_SHORT,
                                          gravity: ToastGravity.BOTTOM,
                                          timeInSecForIosWeb: 1);

                                      // await document.username.upload();
                                    }
                                  }))))
                ]),
              );
            }));
  }

  Widget decoratedImage(ImageProvider imageProvider) {
    return Container(
        width: 120,
        height: 120,
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
                image: imageProvider,
                // image: FileImage(image),
                fit: BoxFit.cover,
                colorFilter: ColorFilter.mode(Colors.white, BlendMode.colorBurn))));
  }

  Widget _buildImage(UserInformation document, BuildContext context) {
    if (document.username != null && document.avatar.value != null && document.avatar.value.isNotEmpty) {
      // print('Avatar url: ${document.avatar.}');
      // print('Image path: ${image?.path}');

      print('Image value: ${image}');
      print('imageIsLoaded value: ${imageIsLoaded}');
      return CachedNetworkImage(
          imageUrl: document.avatar.url,
          httpHeaders: document.avatar.syncHttpHeaders,
          imageBuilder: (context, imageProvider) => decoratedImage(imageProvider),
          // placeholder: (context, url) =>
          //     Theme.of(context).platform == TargetPlatform.iOS
          //         ? CupertinoActivityIndicator()
          //         : CircularProgressIndicator(),
          errorWidget: (context, url, error) => Container(
              width: 120,
              height: 120,
              decoration: BoxDecoration(shape: BoxShape.circle, border: Border.all(color: Theme.of(context).primaryColor)),
              child: Center(child: Icon(Icons.info_outline, color: Theme.of(context).primaryColor))));
    } else {
      return Container(
          width: 120,
          height: 120,
          decoration: BoxDecoration(shape: BoxShape.circle, border: Border.all(color: Theme.of(context).primaryColor)),
          child: Center(child: Icon(Icons.person_outline, color: Theme.of(context).primaryColor, size: 24.0 * 2)));
    }
  }

  Future<void> pickImage() async {
    var file = await ImagePicker.pickImage(source: ImageSource.gallery);

    if (file != null) {
      image = file;
      await uploadImage(image);
    }
  }

  // @override
  // // TODO: implement wantKeepAlive
  // bool get wantKeepAlive => throw UnimplementedError();
  // }

//   Future _getUserImage(UserInformation document) async {
//     var image = await ImagePicker.pickImage(source: ImageSource.gallery);
//     if (image != null) {
//       setState(() {
//         userFile = image;
//         _uploadUserFile(userFile, document);
//       });
//     }
//   }
//
//   Future _uploadUserFile(File file, UserInformation document) async {
//     var uuid = Uuid().v1();
//
//     /// changed code
//     var ref = null;
//     var uploadTask = ref.putFile(file);
//     var storageTaskSnapshot;
//     uploadTask.then((value) {
//       /// TODO check value errors
//       if (true) {
//         storageTaskSnapshot = value;
//         storageTaskSnapshot.ref.getDownloadURL().then((downloadUrl) async {
//           userImage = downloadUrl;
//           print(downloadUrl);
//           print('uh');
//           /*
//           await mindClass.writeUserInfo(document.username, downloadUrl);
//
//            */
//           document.avatar = downloadUrl;
//           await document.avatar.upload();
//           setState(() {});
// //          Firestore.instance
// //              .collection('users')
// //              .document(mindClass.user.uid)
// //              .updateData({
// //            'userImage': userImage,
// //          }).then((data) async {
// //            await prefs.setString('userImage', userImage);
// //          });
//         });
//       }
//     });
//     Fluttertoast.showToast(
//         textColor: Colors.white,
//         backgroundColor: Colors.black87,
//         msg: Strings.pages.profileEditPage.photoUploaded.l(context),
//         toastLength: Toast.LENGTH_SHORT,
//         gravity: ToastGravity.BOTTOM,
//         timeInSecForIosWeb: 1);
//   }

  @override
  bool get wantKeepAlive => true;
}
