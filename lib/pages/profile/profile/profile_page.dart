import 'dart:async';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:db_partnerum/db_partnerum.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:partnerum/head_module/gen_extensions/generated_str_extension.dart';
import 'package:partnerum/models/user_info_model.dart' hide UserModel;
import 'package:partnerum/utils/brain_module.dart';
import 'package:partnerum/utils/theme_constants.dart';
import 'package:partnerum/widgets/partnerum_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';

import '../../../main.dart';
import '../profile_edit/profile_edit_page.dart';
import 'profile_helper.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> with ProfileHelper {
  SharedPreferences prefs;
  String userPasImage = '';
  bool isLoading = false;
  File userPasFile;

  File image;

  // ValueNotifier<bool> showDocumentLoading = ValueNotifier<bool>(false);
  var showDocumentLoading = false;
  var blockDocumentUploading = false;

  @override
  Widget build(BuildContext context) {
    Widget profileAppBar(bool disableButtons) {
      return AppBar(
          title: Text(Strings.pages.profilePage.profile.l(context),
              style: TextStyle(fontSize: 20)),
          elevation: 0,
          automaticallyImplyLeading: false,
          centerTitle: true,
          iconTheme: IconThemeData(color: Colors.black),
          textTheme:
              TextTheme(title: TextStyle(color: Colors.black, fontSize: 22)),
          backgroundColor: Colors.white,
          actions: <Widget>[
            IconButton(
                icon:
                    Icon(Icons.restore_from_trash_rounded, color: Colors.black),
                onPressed: disableButtons
                    ? null
                    : () {
                        _removeAccountAlert(context);
                      }),
            IconButton(
                icon: Icon(Icons.exit_to_app, color: Colors.black),
                onPressed: disableButtons
                    ? null
                    : () {
                        _signOut(context);
                      })
          ]);
    }

    return FutureBuilder(
        /*
            future: mindClass.userInfo(),
           */
        future: root.userData[mindClass.user.uid].userInformation.future,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            print('Error');
            return Container(
                color: Colors.white,
                alignment: FractionalOffset.center,
                child: Center(
                    child: Text(
                        '${Strings.pages.profilePage.error.l(context)}: ${snapshot.error}')));
          }
          // if (!snapshot.hasData) {
          //   // print('has no data');
          //   return Theme.of(context).platform == TargetPlatform.iOS ? CupertinoActivityIndicator() : linearProgressIndicator;
          // }

          /*
              final UserModel document = snapshot.data;

               */
          UserInformation userInformationData = snapshot.data;
          // print(userInformationData.runtimeType);
          //print(document);
          // if (userInformationData == null) {
          //   return Container(color: Colors.yellow, width: 10, height: 20);
          // }

          if (!snapshot.hasData) {
            return Scaffold(
                appBar: profileAppBar(true),
                backgroundColor: Colors.white,
                body: ScrollConfiguration(
                  behavior: noHighlightScrollBehavior(context),
                  child: ListView(children: <Widget>[
                    Theme.of(context).platform == TargetPlatform.iOS
                        ? CupertinoActivityIndicator()
                        : linearProgressIndicator,
                  ]),
                ));
          }
          return Scaffold(
              appBar: profileAppBar(false),
              backgroundColor: Colors.white,
              body: ScrollConfiguration(
                behavior: noHighlightScrollBehavior(context),
                child: ListView(children: <Widget>[
                  Container(
                      height: MediaQuery.of(context).size.height / 2.5,
                      child: Stack(children: <Widget>[
                        Align(
                            alignment: Alignment.topRight,
                            child: Padding(
                                padding: const EdgeInsets.all(16),
                                child: ClipOval(
                                    child: Material(
                                        color: Theme.of(context)
                                            .primaryColor, // button color
                                        child: InkWell(
                                            splashColor: Colors.black,
                                            // inkwell color
                                            child: SizedBox(
                                                width: 35,
                                                height: 35,
                                                child: Icon(Partnerum.edit,
                                                    size: 20,
                                                    color: Colors.white)),
                                            onTap: () async {
                                              image = await Navigator.push(
                                                  context,
                                                  CupertinoPageRoute(
                                                    builder: (context) =>
                                                        ProfileEditPage(),
                                                    //fullscreenDialog: true,
                                                  ));
                                              setState(() {});
                                            }))))),
                        Align(
                            alignment: Alignment.center,
                            child: _buildImage(userInformationData)),
                        if (debugMode) SelectableText(mindClass.user.uid),
                        Align(
                            alignment: Alignment.bottomCenter,
                            child: ListTile(
                                title: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                  Flexible(
                                    child: Text(
                                        userInformationData?.username?.value ??
                                            Strings.pages.profilePage.anonymous
                                                .l(context),
                                        // Text('username',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontSize: 22,
                                            fontWeight: FontWeight.bold))
                                  ),
                                  /*
                                          document.document.isNotEmpty

                                       */
                                  /*
                                      document.username.isSync

                                           */

                                  FutureBuilder(
                                      future: db
                                          .root
                                          .userData[mindClass.user.uid]
                                          .document
                                          .future,
                                      builder: (context, userDocumentSnapshot) {
                                        var userDocument =
                                            userDocumentSnapshot.data;
                                        return Padding(
                                            padding: EdgeInsets.only(
                                                left: userDocument
                                                            ?.status?.value ==
                                                        'confirmed'
                                                    ? 8.0
                                                    : 0.0),
                                            child:
                                                userDocument?.status?.value ==
                                                        'confirmed'
                                                    ? Icon(Partnerum.correct,
                                                        color: Colors.green)
                                                    : SizedBox.shrink()
                                            // Icon(
                                            //     userDocument?.status?.value ==
                                            //             'confirmed'
                                            //         ? Partnerum.correct
                                            //         : null,
                                            //     color: Colors.green)
                                            );
                                      })
                                ])))
                      ])),
                  _buildConfirm(userInformationData),
                  if (blockDocumentUploading)
                    Center(
                        child: Column(
                      children: [
                        OutlineButton(
                            onPressed: null,
                            borderSide:
                                BorderSide(width: 3, color: Colors.orange),
                            child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 8.0, vertical: 13),
                                child: Text(
                                    Strings.pages.profilePage.uploadDocument
                                        .l(context),
                                    style: TextStyle(
                                        fontSize: 16, color: Colors.grey)))),
                        SizedBox(height: 8),
                        Text(Strings.pages.profilePage.inProcess.l(context),
                            style: TextStyle(fontSize: 14, color: Colors.grey))
                      ],
                    )),
                  if (!blockDocumentUploading)
                    FutureBuilder(
                        future: db
                            .root.userData[mindClass.user.uid].document.future,
                        builder: (context, snapshot) {
                          Document document = snapshot?.data;

                          if (snapshot.hasData && !snapshot.hasError) {
                            var photo = document?.photo?.value;
                            var status = document?.status?.value;
                            var timestamp =
                                document?.timestamp?.value?.inMilliseconds;

                            return Center(
                                child: Column(
                              children: [
                                OutlineButton(
                                    onPressed: showDocumentLoading
                                        ? null
                                        : photo == null ||
                                                status == null ||
                                                timestamp == null
                                            ? () => _getUserPasImage(context)
                                            : null,
                                    borderSide: BorderSide(
                                        width: 3, color: Colors.orange),
                                    child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 8.0, vertical: 13),
                                        child: Text(
                                            /*
                                              document.document.isEmpty

                                             */
                                            /*
                                            document.isSync

                                             */
                                            true
                                                ? Strings.pages.profilePage
                                                    .uploadDocument
                                                    .l(context)
                                                : Strings.pages.profilePage
                                                    .updateDocument
                                                    .l(context),
                                            style: TextStyle(
                                                fontSize: 16,
                                                color: showDocumentLoading
                                                    ? null
                                                    : photo == null ||
                                                            status == null ||
                                                            timestamp == 0
                                                        ? Colors.orange
                                                        : Colors.grey)))),
                                SizedBox(height: 8),
                                showDocumentLoading
                                    ? Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 16.0),
                                        child: linearProgressIndicator)
                                    : Container(),
                                if (photo != null &&
                                    status != null &&
                                    timestamp != null)
                                  Text(
                                      '${Strings.pages.profilePage.status.l(context)}: ${statusToString(status, context)}',
                                      style: TextStyle(
                                          fontSize: 14, color: Colors.grey))
                              ],
                            ));
                          }

                          return Container();
                        })
                ]),
              ));
        });
  }

  Future _removeAccountAlert(BuildContext _context) {
    return showDialog<Null>(
        context: _context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return SimpleDialog(
              title: Text(Strings.pages.profilePage.deleteAccount.l(context)),
              children: <Widget>[
                ListTile(
                    leading:
                        Icon(Icons.delete_sweep_outlined, color: Colors.black),
                    title: Text(
                        Strings.pages.profilePage.deleteWarning.l(context),
                        style: TextStyle(color: Colors.black54, fontSize: 14))),
                Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      FlatButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                            _confirmAccountRemoveAlert(context);
                          },
                          child: Text(
                              Strings.pages.profilePage.yes
                                  .l(context)
                                  .toUpperCase(),
                              style: TextStyle(color: Colors.redAccent))),
                      FlatButton(
                          onPressed: () => Navigator.of(context).pop(),
                          child: Text(
                              Strings.pages.profilePage.no
                                  .l(context)
                                  .toUpperCase(),
                              style: TextStyle(color: Colors.black)))
                    ])
              ]);
        });
  }

  Future _confirmAccountRemoveAlert(BuildContext _context) async {
    String _smsCode;
    String _verificationId;

    await mindClass.auth.verifyPhoneNumber(
        phoneNumber: mindClass.user.phoneNumber,
        codeSent: (String verificationId, int resendToken) async {
          _verificationId = verificationId;
          // Sign the user in (or link) with the credential
          // await auth.signInWithCredential(credential);
        },
        verificationFailed: (FirebaseAuthException error) {},
        codeAutoRetrievalTimeout: (String verificationId) {},
        verificationCompleted: (PhoneAuthCredential phoneAuthCredential) {});

    return await showDialog(
        context: _context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return SimpleDialog(
              title: Text(Strings.pages.profilePage.confirm.l(context)),
              children: <Widget>[
                ListTile(
                    leading: Icon(Icons.sms_outlined, color: Colors.black),
                    title: Text(
                        Strings.pages.profilePage.sendCodeMessage.l(context),
                        style: TextStyle(color: Colors.black54, fontSize: 14))),
                Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 36.0),
                    child: TextField(
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.center,
                        onChanged: (value) => _smsCode = value)),
                Row(mainAxisAlignment: MainAxisAlignment.end, children: <
                    Widget>[
                  FlatButton(
                      onPressed: () async {
                        // Navigator.of(context).pop();

                        var userCredential = await mindClass
                            .reauthenticateViaPhone(_verificationId, _smsCode);

                        if (userCredential != null) {
                          await removeAccount();
                          await mindClass.deleteUser();
                          await Navigator.pushNamedAndRemoveUntil(
                              context, 'walk', (Route<dynamic> route) => false);
                        }
                      },
                      child: Text(
                          Strings.pages.profilePage.yes
                              .l(context)
                              .toUpperCase(),
                          style: TextStyle(color: Colors.redAccent))),
                  FlatButton(
                      onPressed: () => Navigator.of(context).pop(),
                      child: Text(
                          Strings.pages.profilePage.no.l(context).toUpperCase(),
                          style: TextStyle(color: Colors.black)))
                ])
              ]);
        });
  }

/*
  Widget _buildImage(UserModel document) {

 */
  Widget decoratedImage(ImageProvider imageProvider) {
    return Container(
        width: 120,
        height: 120,
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
                image: imageProvider,
                fit: BoxFit.cover,
                colorFilter:
                    ColorFilter.mode(Colors.white, BlendMode.colorBurn))));
  }

  Widget _buildImage(UserInformation document) {
    //if (document == null) return Container();
    //if ((document).document.isNotEmpty) return Container();

    if (true) {
      //if (false) {
      if (image != null) {
        return decoratedImage(FileImage(image));
      }
      return CachedNetworkImage(
          imageUrl: document.avatar.url,
          httpHeaders: document.avatar.syncHttpHeaders,
          imageBuilder: (context, imageProvider) =>
              decoratedImage(imageProvider),
          placeholder: (context, url) =>
              Theme.of(context).platform == TargetPlatform.iOS
                  ? CupertinoActivityIndicator()
                  : CircularProgressIndicator(),
          errorWidget: (context, url, error) => Container(
              width: 120,
              height: 120,
              decoration: BoxDecoration(
                  border: Border.all(color: Theme.of(context).primaryColor),
                  shape: BoxShape.circle),
              child: Center(
                  child: Icon(Icons.info_outline,
                      color: Theme.of(context).primaryColor))));
    } else {
      return Container(
          width: 120,
          height: 120,
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(color: Theme.of(context).primaryColor)),
          child: Center(
              child: Icon(Icons.person_outline,
                  color: Theme.of(context).primaryColor, size: 24.0 * 2)));
    }
  }

  Widget _buildConfirm(UserInformation document) {
    /*
    if (document.document.isEmpty) {
      return Container(
          margin: const EdgeInsets.all(20),
          decoration: BoxDecoration(
              color: Theme.of(context).primaryColor.withOpacity(0.3),
              borderRadius: BorderRadius.all(Radius.circular(20))),
          child: Padding(
              padding: const EdgeInsets.all(8),
              child: ListTile(
                  subtitle: Text(
                      Strings.pages.profilePage.askForConfirm.l(context),
                      style: TextStyle(color: Colors.black),
                      textAlign: TextAlign.center))));
    } else if (document.isConfirmed) {
      return Container(
          margin: const EdgeInsets.all(20),
          decoration: BoxDecoration(
              color: Colors.green.withOpacity(0.3),
              borderRadius: BorderRadius.all(Radius.circular(20))),
          child: Padding(
              padding: const EdgeInsets.all(8),
              child: ListTile(
                  subtitle: Text(Strings.pages.profilePage.uploadDocumentStatus.l(context),
                      style: TextStyle(color: Colors.black),
                      textAlign: TextAlign.center))));
    } else {
      return Container(margin: const EdgeInsets.all(20));
    }
    */
    return Container(margin: const EdgeInsets.all(20));
  }

  Future _getUserPasImage(BuildContext context) async {
    await showModalBottomSheet(
        elevation: 1,
        shape: RoundedRectangleBorder(borderRadius: borderRadius),
        clipBehavior: Clip.antiAlias,
        context: context,
        builder: (context) {
          getPasImage([ImageSource source = ImageSource.gallery]) async {
            var image = await ImagePicker.pickImage(source: source);
            if (image != null) {
              // setState(() {
              //
              //
              // });

              userPasFile = image;
              Navigator.pop(context);

              await _uploadUserPasFile(userPasFile);
            }
          }

          return Container(
              color: Colors.transparent,
              child: Padding(
                  padding: const EdgeInsets.only(bottom: 16.0 * 2),
                  child:
                      Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                    ListTile(
                        title: Text(Strings
                            .pages.profilePage.askForUploadDocument
                            .l(context)),
                        subtitle: Text(Strings
                            .pages.profilePage.askForUploadDocumentSubtitle
                            .l(context))),
                    Wrap(alignment: WrapAlignment.end, spacing: 8, children: <
                        Widget>[
                      FlatButton(
                          onPressed: () => getPasImage(ImageSource.camera),
                          color: Theme.of(context).primaryColor,
                          child: Text(
                              Strings.pages.profilePage.makePhoto.l(context),
                              style: TextStyle(
                                  fontSize: 16, color: Colors.white))),
                      FlatButton(
                          onPressed: () => getPasImage(ImageSource.gallery),
                          color: Theme.of(context).primaryColor,
                          child: Text(
                              Strings.pages.profilePage.chooseInGallery
                                  .l(context),
                              style:
                                  TextStyle(fontSize: 16, color: Colors.white)))
                    ])
                  ])));
        });
  }

  Future _uploadUserPasFile(File file) async {
    setState(() => showDocumentLoading = true);

    await uploadDocument(file);

    await Fluttertoast.showToast(
        textColor: Colors.black,
        backgroundColor: Colors.black.withOpacity(0.1),
        msg: Strings.pages.profilePage.documentUploaded.l(context),
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1);

    showDocumentLoading = false;
    setState(() => blockDocumentUploading = true);
    // setState(() {});
    // showDocumentLoading.value = false;
    /*
    var uuid = Uuid().v1();

    /// changed code
    var ref = null;
    var uploadTask = ref.putFile(file);
    var storageTaskSnapshot;
    await uploadTask.then((value) async {
      /// TODO check value to errors
      if (true) {
        storageTaskSnapshot = value;
        await storageTaskSnapshot.ref
            .getDownloadURL()
            .then((downloadUrl) async {
          userPasImage = downloadUrl;



          /*
          await mindClass.writeUserDocument(downloadUrl);

           */
//          Firestore.instance
//              .collection('users')
//              .document(mindClass.user.uid)
//              .updateData({
//            'userPasImage': ,
//            'isConfirmed': false,
//          }).then((data) async {
//            await prefs.setString('userPasImage', userPasImage);
//          });
        });
      } else {}
    });

     */
  }

  _signOut(BuildContext context) async {
    try {
      await mindClass.signOut();
      await Navigator.pushNamedAndRemoveUntil(
          context, 'walk', (Route<dynamic> route) => false);
    } catch (e) {
      print(e);
    }
  }

// Future<void> _deleteUser(BuildContext context) async {
//   try {
//     await mindClass.deleteUser();
//     await removeAccount();
//     await Navigator.pushNamedAndRemoveUntil(
//         context, 'walk', (Route<dynamic> route) => false);
//   } catch (e) {
//     print(e);
//   }
// }
}
