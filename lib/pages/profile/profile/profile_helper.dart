import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:micro_utils/data_helper.dart';
import 'package:db_partnerum/db_partnerum.dart';
import 'package:partnerum/head_module/gen_extensions/generated_str_extension.dart';
import 'package:partnerum/utils/brain_module.dart';
import 'package:path/path.dart' as path;

@DataHelper()
class ProfileHelper {
  Future<void> uploadDocument(File image) async {
    var doc = root.userData[mindClass.user.uid].document;
    var img = doc.photo;
    var image8List = await testCompressFile(image);
    await img.set(FileWrapper(path.basename(image.path), image8List));
    await doc.timestamp.set(DateTime.now().millisecondsSinceEpoch);
    await doc.status.set(Keywords.inProgress);
  }

  Future<Uint8List> testCompressFile(File file) async {
    var result = await FlutterImageCompress.compressWithFile(
      file.path,
      quality: 10,
      rotate: 0,
    );
    return result;
  }

  String statusToString(String status, BuildContext context) {
    //profile_helper
    if (status == Keywords.inProgress.short) return Strings.pages.profilePage.profileHelper.inProgress.l(context);
    if (status == Keywords.confirmed.short) return Strings.pages.profilePage.profileHelper.accepted.l(context);
    return 'null';
  }

  Future<void> removeAccount() async {
    await root.userData[mindClass.user.uid]?.userInformation
        ?.tokens[mindClass.currentNotificationToken]
        ?.remove();

   await root.userData[mindClass.user.uid].userInformation.isRemoved.set(true);
  }
}
