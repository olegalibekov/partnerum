import 'package:db_partnerum/db_partnerum.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:partnerum/head_module/gen_extensions/generated_str_extension.dart';
import 'package:partnerum/main.dart';

import 'package:partnerum/utils/brain_module.dart';
import 'package:partnerum/widgets/calendar_date_range_picker.dart';
import 'package:partnerum/widgets/partnerum_icons.dart';
import 'package:cached_network_image/cached_network_image.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:partnerum/widgets/water_mark.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import '../house_create/house_create_page.dart';
import 'house_details_helper.dart';

class OfferDetailsPage extends StatefulWidget {
  final String userId;
  final bool edit;
  final HouseInformation houseInformation;
  final String houseId;

  OfferDetailsPage({this.userId, this.houseInformation, this.edit = true, this.houseId});

  @override
  _OfferDetailsPageState createState() => _OfferDetailsPageState();
}

class _OfferDetailsPageState extends State<OfferDetailsPage> with HouseDetailsHelper {
  int photoIndex = 0;
  ScrollController _scrollController;
  double _appBarHeight;
  List images = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    houseInformation = widget.houseInformation;
  }

  @override
  Widget build(BuildContext context) {
    // print('NewVersionHouseDetails: ${widget.house}');
    return NewVersionHouseDetails(
      userId: mindClass.user.uid,
      edit: widget.edit,
      houseInformation: widget.houseInformation,
      houseId: widget.houseId,
    );
  }

  Widget _buildAppBar(user) {
    return AppBar(
        leading: IconButton(
            icon: Theme.of(context).platform == TargetPlatform.iOS ? Icon(Icons.arrow_back_ios) : Icon(Icons.arrow_back),
            onPressed: () => Navigator.of(context).pop()),
        title: Text(Strings.pages.housesPage.houseDetailsPage.aboutFlat.l(context)),
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
        centerTitle: false,
        textTheme: TextTheme(
            title: TextStyle(
          color: Colors.black,
          fontSize: 20.0,
        )),
        backgroundColor: Colors.white,
        actions: <Widget>[
//        _buildAction(user, offerId)
        ]);
  }

  Widget _buildBody() {
    if (images == null || images.isEmpty) {
      return _buildSubBody();
    } else {
      return NestedScrollView(
        physics: BouncingScrollPhysics(),
        controller: _scrollController,
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          _appBarHeight = MediaQuery.of(context).size.height / 3;
          return [
            SliverAppBar(
                automaticallyImplyLeading: false,
                floating: false,
                expandedHeight: _appBarHeight,
                pinned: false,
                backgroundColor: Colors.white,
                flexibleSpace: FlexibleSpaceBar(
                    background: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                  Container(
                      height: MediaQuery.of(context).size.height / 3,
                      width: MediaQuery.of(context).size.width,
                      child: ListView.builder(
                          physics: BouncingScrollPhysics(),
                          itemCount: photosAmount(),
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (context, index) {
                            return Padding(
                                padding: const EdgeInsets.only(bottom: 16, top: 16, left: 8, right: 8),
                                child: ClipRRect(
                                    borderRadius: BorderRadius.circular(16),
                                    child: CachedNetworkImage(
                                        imageUrl: photoByIndex(index),
                                        errorWidget: (context, url, error) => Icon(Icons.error),
                                        placeholder: (context, url) => Center(
                                            child: Theme.of(context).platform == TargetPlatform.iOS
                                                ? CupertinoActivityIndicator()
                                                : CircularProgressIndicator()),
                                        fadeOutDuration: Duration(seconds: 1),
                                        fadeInDuration: Duration(seconds: 3),
                                        fit: BoxFit.cover)));
                          }))
                ])))
          ];
        },
        body: _buildSubBody(),
      );
    }
  }

  Widget _buildSubBody() {
    return SingleChildScrollView(
        child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[_buildCity(), _buildMetro(), _buildDistance(), _buildAddress(), _buildPrice(), _buildRooms(), _buildInfo()]));
  }

  Widget _buildInfo() {
    var info = houseInfo();
    return ExpansionTile(
        leading: Icon(Partnerum.info, color: Theme.of(context).primaryColor),
        title: Text(Strings.pages.housesPage.houseDetailsPage.info.l(context)),
        children: <Widget>[
          Container(
              margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              child: Align(
                  alignment: Alignment.topLeft,
                  child: info.isNotEmpty ? Text(info) : Text(Strings.pages.housesPage.houseDetailsPage.notStated.l(context))))
        ]);
  }

  Widget _buildRooms() {
    var roomsNumber = roomsAmount();
    return ListTile(
        leading: Icon(Partnerum.rooms, color: Theme.of(context).primaryColor),
        title: Text(Strings.pages.housesPage.houseDetailsPage.roomsAmount.l(context)),
        subtitle: Text(roomsNumber == 5 ? Strings.pages.housesPage.houseDetailsPage.moreThan4.l(context) : roomsNumber.toString()));
  }

  Widget _buildPrice() {
    return ListTile(
        leading: Icon(Partnerum.ruble, color: Theme.of(context).primaryColor),
        title: Text(Strings.pages.housesPage.houseDetailsPage.price.l(context)),
        subtitle: Text(
            '${Strings.pages.housesPage.houseDetailsPage.priceFrom.l(context)} ${startPrice()} ${Strings.pages.housesPage.houseDetailsPage.priceTo.l(context)} ${endPrice()} ${Strings.pages.housesPage.houseDetailsPage.priceMeasure.l(context)}'));
  }

  Widget _buildAddress() {
    return ListTile(
        leading: Icon(Partnerum.address, color: Theme.of(context).primaryColor),
        title: Text(Strings.pages.housesPage.houseDetailsPage.address.l(context)),
        subtitle: Text('${address()}'));
  }

  Widget _buildDistance() {
    if (isAnyStations()) {
      return ListTile(
          leading: Icon(Partnerum.walk, color: Theme.of(context).primaryColor),
          title: Text(Strings.pages.housesPage.houseDetailsPage.timeToMetro.l(context)),
          subtitle: Text('${timeToMetro()} ${Strings.pages.housesPage.houseDetailsPage.timeMeasure.l(context)}'));
    } else {
      return Container();
    }
  }

  Widget _buildCity() {
    return ListTile(
        leading: Icon(Partnerum.city, color: Theme.of(context).primaryColor),
        title: Text(Strings.pages.housesPage.houseDetailsPage.city.l(context)),
        subtitle: Text('${city()}'));
  }

  Widget _buildMetro() {
    return isAnyStations()
        ? ExpansionTile(
            leading: Icon(Partnerum.metro, color: Theme.of(context).primaryColor),
            title: Text('${Strings.pages.housesPage.houseDetailsPage.metroStations.l(context)} (${stationsAmount()})'),
            children: <Widget>[
                Container(
                    margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                    child: Align(
                        alignment: Alignment.topLeft,
                        child: Wrap(runSpacing: 2, spacing: 2, children: <Widget>[
                          for (var i = 0, l = stationsAmount(); i < l; i++)
                            Chip(padding: EdgeInsets.zero, label: Text('${stationByIndex(i)}', style: TextStyle(fontSize: 14, color: Colors.grey)))
                        ])))
              ])
        : Container();
  }

  void _previousImage() {
    setState(() {
      photoIndex = photoIndex > 0 ? photoIndex - 1 : 0;
    });
  }

  void _nextImage() {
    setState(() {
      photoIndex = photoIndex < images.length - 1 ? photoIndex + 1 : photoIndex;
    });
  }
}

class SelectedPhoto extends StatelessWidget {
  final int numberOfDots;
  final int photoIndex;

  SelectedPhoto({this.numberOfDots, this.photoIndex});

  Widget _inactivePhoto() {
    return Container(
        child: Padding(
            padding: const EdgeInsets.only(left: 3.0, right: 3.0),
            child: Container(height: 8.0, width: 8.0, decoration: BoxDecoration(color: Colors.grey, borderRadius: BorderRadius.circular(4.0)))));
  }

  Widget _activePhoto() {
    return Container(
        child: Padding(
            padding: EdgeInsets.only(left: 3.0, right: 3.0),
            child: Container(
                height: 10.0,
                width: 10.0,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(5.0),
                    boxShadow: [BoxShadow(color: Colors.grey, spreadRadius: 0.0, blurRadius: 2.0)]))));
  }

  List<Widget> _buildDots() {
    var dots = <Widget>[];

    for (var i = 0; i < numberOfDots; ++i) {
      dots.add(i == photoIndex ? _activePhoto() : _inactivePhoto());
    }

    return dots;
  }

  @override
  Widget build(BuildContext context) {
    return Center(child: Row(mainAxisAlignment: MainAxisAlignment.center, children: _buildDots()));
  }
}

///15 sec
class NewVersionHouseDetails extends StatefulWidget {
  final String userId;
  final bool edit;
  final HouseInformation houseInformation;
  final String houseId;

  const NewVersionHouseDetails({Key key, this.userId, this.edit = true, this.houseInformation, this.houseId}) : super(key: key);

  @override
  _NewVersionHouseDetailsState createState() => _NewVersionHouseDetailsState();
}

class _NewVersionHouseDetailsState extends State<NewVersionHouseDetails>
    with SingleTickerProviderStateMixin<NewVersionHouseDetails>, HouseDetailsHelper {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  bool datesAutoBooking;

  bool blockSwitchTile = false;

  @override
  Widget build(BuildContext context) {
    if (houseInformation == null) {
      return Container(
        color: Colors.white,
        alignment: FractionalOffset.center,
        child: Center(
          child: Text(Strings.globalOnes.error.l(context)),
        ),
      );
    }

    // images = photosList();
    BorderRadiusGeometry radius = BorderRadius.only(
      topLeft: Radius.circular(24.0),
      topRight: Radius.circular(24.0),
    );

    return FutureBuilder(
        future: photosList(widget.houseInformation),
        builder: (context, photosSnapshot) {
          var images = photosSnapshot?.data ?? [];
          return Stack(children: [
            Scaffold(
              key: scaffoldKey,
              body: SlidingUpPanel(
                color: Colors.transparent,
                panelBuilder: (ScrollController sc) => Container(
                    decoration: BoxDecoration(color: Colors.white, borderRadius: radius), clipBehavior: Clip.antiAlias, child: _buildBody(sc)),
                body: images.isEmpty
                    ? Center(child: CircularProgressIndicator())
                    : PageView.builder(
                        physics: BouncingScrollPhysics(),
                        // itemCount: photosAmount(),
                        itemCount: images.length,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index) {
                          return Stack(
                            children: [
                              Positioned.fill(
                                child: InteractiveViewer(
                                  onInteractionStart: (_) {},
                                  maxScale: 10,
                                  minScale: 0.0001,
                                  child: CachedNetworkImage(
                                    imageUrl: images[index].bigImg.url,
                                    httpHeaders: images[index].bigImg.syncHttpHeaders,
                                    errorWidget: (context, url, error) => Icon(Icons.error),
                                    // placeholder: (context, url) => Center(
                                    //     child: Theme.of(context).platform ==
                                    //             TargetPlatform.iOS
                                    //         ? CupertinoActivityIndicator()
                                    //         : CircularProgressIndicator()),
                                    fadeOutDuration: Duration(seconds: 1),
                                    fadeInDuration: Duration(seconds: 3),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              )
                            ],
                          );
                        }),
              ),
              floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
              floatingActionButton: widget.edit
                  ? FloatingActionButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          CupertinoPageRoute(
                            builder: (context) => HouseCreatePage(
                                /*
                                  houseId: house.houseId,

                                   */
                                houseId: widget.houseId,
                                houseInformation: widget.houseInformation,
                                photosList: images),
                          ),
                        ).then((v) async {
                          /*
                              var id = house.houseId;
                              house = await mindClass.userHouse(house.houseId);
                              house.houseId = id;
                               */
                          setState(() {});
                        });
                      },
                      child: Icon(
                        Partnerum.edit,
                        color: Theme.of(context).primaryColor,
                      ),
                    )
                  : Container(),
            ),
            Positioned(
              top: 32,
              left: 16,
              child: FloatingActionButton(
                mini: true,
                heroTag: Strings.pages.housesPage.newVersionHouseDetails.back.l(context),
                child: Icon(
                  Icons.arrow_back,
                  color: Theme.of(context).primaryColor,
                ),
                onPressed: () => Navigator.pop(context),
              ),
            ),
            if (widget.edit)
              Positioned(
                  top: 32,
                  right: 16,
                  child: FloatingActionButton(
                      mini: true,
                      heroTag: Strings.pages.housesPage.newVersionHouseDetails.setClosingDates.l(context),
                      child: Icon(
                        Icons.calendar_today,
                        color: Theme.of(context).primaryColor,
                      ),
                      onPressed: () async {
                        await showModalBottomSheet(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
                            ),
                            clipBehavior: Clip.antiAlias,
                            context: context,
                            builder: (BuildContext context) {
                              var bookingPeriod = root?.userData[mindClass.user.uid]?.houses[widget.houseId]?.bookingDates;
                              return DraggableScrollableSheet(
                                  expand: false,
                                  maxChildSize: .7,
                                  //MediaQuery.of(context).size.height*0.5,
                                  builder: (BuildContext context, ScrollController scrollController) {
                                    return SingleChildScrollView(
                                        controller: scrollController,
                                        child: Theme(
                                            data: Theme.of(context)
                                                .copyWith(appBarTheme: AppBarTheme(actionsIconTheme: IconThemeData(color: Colors.red, opacity: 1))),
                                            child: StatefulBuilder(builder: (context, setState) {
                                              return Padding(
                                                  padding: const EdgeInsets.all(16.0),
                                                  child: Column(mainAxisSize: MainAxisSize.min, children: [
                                                    Padding(
                                                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                                                      child: Text(Strings.pages.housesPage.newVersionHouseDetails.editBookDates.l(context),
                                                          style: Theme.of(context).textTheme.headline5),
                                                    ),
                                                    Divider(),
                                                    FutureBuilder(
                                                        future: root.userData[mindClass.user.uid]?.apartmentsAutoBookingDates?.future,
                                                        builder: (context, snapshot) {
                                                          if (snapshot.hasData && !snapshot.hasError) {
                                                            var dbDatesAutoBooking = snapshot?.data?.value;
                                                            if (dbDatesAutoBooking == null) {
                                                              datesAutoBooking = false;
                                                            } else {
                                                              datesAutoBooking = dbDatesAutoBooking;
                                                            }
                                                            return IgnorePointer(
                                                              ignoring: blockSwitchTile,
                                                              child: SwitchListTile(
                                                                  title: Text(
                                                                      '${datesAutoBooking ? Strings.pages.housesPage.newVersionHouseDetails.turnedOn.l(context) : Strings.pages.housesPage.newVersionHouseDetails.turnedOff.l(context)} ${Strings.pages.housesPage.newVersionHouseDetails.autoDatesSettingsTitle.l(context)}'),
                                                                  value: datesAutoBooking,
                                                                  onChanged: (value) async {
                                                                    setState(() => blockSwitchTile = true);
                                                                    await toggleAutoBookingSetup(datesAutoBooking);
                                                                    setState(() {
                                                                      datesAutoBooking = value;
                                                                      blockSwitchTile = false;
                                                                    });
                                                                  }),
                                                            );
                                                          }
                                                          return Container();
                                                        }),

                                                    //     StreamBuilder<Object>(
                                                    //         stream: mindClass.eventSteam.outStream
                                                    //             .where((event) => event == 'automaticHouseBookingDatesPlaceReady'),
                                                    //         builder: (context, snapshot) {
                                                    //
                                                    //           /*
                                                    // return SwitchListTile(
                                                    //     title: Text(
                                                    //         "${mindClass.automaticHouseBookingDatesPlace ? "Strings.pages.housesPage.newVersionHouseDetails.turnedOn.l(context)" : "Strings.pages.housesPage.newVersionHouseDetails.turnedOff.l(context)"} ${Strings.pages.housesPage.newVersionHouseDetails.autoDatesSettingsTitle.l(context)}"),
                                                    //     value: mindClass
                                                    //         .automaticHouseBookingDatesPlace,
                                                    //     onChanged: (value) =>
                                                    //         setState(() {
                                                    //           mindClass
                                                    //                   .automaticHouseBookingDatesPlace =
                                                    //               value;
                                                    //         }));
                                                    //
                                                    //  */
                                                    //         }),
                                                    Divider(),
                                                    FutureBuilder(
                                                        future: bookingPeriod.future,
                                                        initialData: bookingPeriod,
                                                        builder: (context, snapshot) {
                                                          // print('Booking periods: ${snapshot?.data}');
                                                          // print('House id: ${widget.houseId}');
                                                          if (snapshot.hasData && !snapshot.hasError && snapshot?.data != null) {
                                                            var bookingPeriods = snapshot.data;

                                                            // print('Booking periods: $bookingPeriods');
                                                            // print(bookingPeriods.length);
                                                            // for (var bookingPeriod in bookingPeriods
                                                            //     .values)
                                                            // print(
                                                            //     bookingPeriod
                                                            //         .checkIn
                                                            //         .value);
                                                            return Wrap(children: [
                                                              // Container(
                                                              //     color: Colors
                                                              //         .green,
                                                              //     width:
                                                              //     100,
                                                              //     height:
                                                              //     100),

                                                              // if (apartmentBookingDates(
                                                              //     widget.house.key,
                                                              //     asyncSnapshot:
                                                              //     snapshot) !=
                                                              //     null)

                                                              // for (DateTimeRange d
                                                              // in apartmentBookingDates(
                                                              //     widget
                                                              //         .house.key,
                                                              //     asyncSnapshot:
                                                              //     snapshot))

                                                              if (bookingPeriods?.values != null)
                                                                for (var bookingPeriod in bookingPeriods.values)
                                                                  Chip(
                                                                    label: Text(
                                                                        "${DateFormat.yMMMd("ru").format(DateTime.fromMillisecondsSinceEpoch(bookingPeriod.checkIn.value.inMilliseconds))} - ${DateFormat.yMMMd("ru").format(DateTime.fromMillisecondsSinceEpoch(bookingPeriod.checkOut.value.inMilliseconds))}"),
                                                                    onDeleted: () async {
                                                                      await removeBookingPeriod(widget.houseId, bookingPeriod.key);
                                                                      setState(() {});
                                                                    },
                                                                  ),
                                                              ActionChip(
                                                                avatar: Icon(Icons.add),
                                                                label:
                                                                    Text(Strings.pages.housesPage.newVersionHouseDetails.addNewBookPeriod.l(context)),
                                                                onPressed: () async {
                                                                  var dateTimeRange = await showDateRangePicker(
                                                                      context: context,
                                                                      firstDate: DateTime.now(),
                                                                      lastDate: DateTime.now().add(Duration(days: 360 * 10)));
                                                                  if (await addApartmentBookingDates(
                                                                      widget.houseId, dateTimeRange, (bookingPeriods?.values ?? []).toList())) {
                                                                    await Fluttertoast.showToast(
                                                                        textColor: Colors.white,
                                                                        backgroundColor: Colors.black87,
                                                                        msg: 'Даты бронирования пересеаются',
                                                                        toastLength: Toast.LENGTH_SHORT,
                                                                        gravity: ToastGravity.BOTTOM,
                                                                        timeInSecForIosWeb: 1);
                                                                  }
                                                                  setState(() {});
                                                                },
                                                              )
                                                            ]);
                                                          }
                                                          return Container();
                                                        }),
                                                    /*
                                                    ActionChip(
                                                      avatar: Icon(Icons.add),
                                                      label: Text(Strings.pages.housesPage.newVersionHouseDetails.addNewBookPeriod.l(context)),
                                                      onPressed: () async {
                                                        var dateTimeRange = await showDateRangePicker(
                                                            context: context,
                                                            firstDate: DateTime.now(),
                                                            lastDate: DateTime.now().add(Duration(days: 360 * 10)));

                                                        await addApartmentBookingDates(widget.houseId, dateTimeRange);
                                                        setState(() {});
                                                      },
                                                    ),
                                                    */
                                                    /////////////////
                                                    // StreamBuilder<Object>(
                                                    //     stream: mindClass
                                                    //         .eventSteam
                                                    //         .outStream
                                                    //         .where((event) =>
                                                    //             event is List<
                                                    //                 DateTimeRange> ||
                                                    //             event ==
                                                    //                 'automaticHouseBookingDatesPlaceReady'),
                                                    //     builder: (context,
                                                    //         snapshot) {
                                                    //       return Wrap(
                                                    //           children: [
                                                    //             Container(
                                                    //                 color: Colors
                                                    //                     .green,
                                                    //                 width:
                                                    //                     100,
                                                    //                 height:
                                                    //                     100),
                                                    //             // if (mindClass.houseBookingDates(
                                                    //             //         widget.house.houseId,
                                                    //             //         asyncSnapshot:
                                                    //             //             snapshot) !=
                                                    //             //     null)
                                                    //             //   for (DateTimeRange d
                                                    //             //       in mindClass
                                                    //             //           .houseBookingDates(
                                                    //             //               widget
                                                    //             //                   .house.houseId,
                                                    //             //               asyncSnapshot:
                                                    //             //                   snapshot))
                                                    //             //     Chip(
                                                    //             //       label: Text(
                                                    //             //           "${DateFormat.yMMMd("ru").format(d.start)} - ${DateFormat.yMMMd("ru").format(d.end)}"),
                                                    //             //       onDeleted: () async {
                                                    //             //         await mindClass
                                                    //             //             .houseBookingDates(
                                                    //             //                 widget.house
                                                    //             //                     .houseId,
                                                    //             //                 date: d,
                                                    //             //                 task: Classic
                                                    //             //                     .remove);
                                                    //             //         mindClass.updateEvent(
                                                    //             //             "automaticHouseBookingDatesPlaceReady");
                                                    //             //       },
                                                    //             //     ),
                                                    //             // ActionChip(
                                                    //             //   avatar: Icon(Icons.add),
                                                    //             //   label: Text(
                                                    //             //       Strings.pages.housesPage.newVersionHouseDetails.addNewBookPeriod.l(context)),
                                                    //             //   onPressed: () async {
                                                    //             //     DateTimeRange dateTimeRange =
                                                    //             //         await showDateRangePicker(
                                                    //             //             context: context,
                                                    //             //             firstDate:
                                                    //             //                 DateTime.now(),
                                                    //             //             lastDate: DateTime
                                                    //             //                     .now()
                                                    //             //                 .add(Duration(
                                                    //             //                     days: 360 *
                                                    //             //                         10)));
                                                    //             //     mindClass.houseBookingDates(
                                                    //             //         widget.house.houseId,
                                                    //             //         date: dateTimeRange);
                                                    //             //   },
                                                    //             // ),
                                                    //           ]);
                                                    //     })
                                                  ]));
                                            })));
                                  });
                            });

                        setState(() {});
                      }))
          ]);
        });
  }

  int photoIndex = 0;
  ScrollController _scrollController;
  double _appBarHeight;
  List images = [];

  @override
  void initState() {
    houseInformation = widget.houseInformation;
    FlutterStatusbarcolor.setStatusBarColor(Colors.transparent);
    super.initState();
  }

  @override
  void dispose() {
    FlutterStatusbarcolor.setStatusBarColor(Colors.white);
    super.dispose();
  }

  Widget _buildBody(ScrollController sc) {
    return _buildSubBody(sc);
  }

  Widget _buildSubBody(ScrollController sc) {
    return SingleChildScrollView(
      controller: sc,
      physics: BouncingScrollPhysics(),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: 16,
              ),
              Container(width: 38, height: 4, decoration: BoxDecoration(borderRadius: BorderRadius.circular(16), color: Colors.grey.shade300)),
              SizedBox(
                height: 18,
              ),
              Text(
                Strings.pages.housesPage.newVersionHouseDetails.moreAboutOffer.l(context),
                style: Theme.of(context).textTheme.headline6.copyWith(color: Colors.grey.shade600),
              ),
              if (debugMode) SelectableText(houseID())
            ],
          ),
          SizedBox(
            height: 24,
          ),
          _buildCity(),
          _buildMetro(),
          _buildDistance(),
          _buildAddress(),
          _buildPrice(),
          _buildRooms(),
          _buildInfo(),
          SizedBox(
            height: 75,
          ),
        ],
      ),
    );
  }

  Widget _buildInfo() {
    return ExpansionTile(
      leading: Icon(Partnerum.info, color: Theme.of(context).primaryColor),
      title: Text(Strings.pages.housesPage.newVersionHouseDetails.info.l(context)),
      children: <Widget>[
        Container(
          margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: Align(
            alignment: Alignment.topLeft,
            child: houseInfo().isNotEmpty ? Text(houseInfo()) : Text(Strings.pages.housesPage.newVersionHouseDetails.notStated.l(context)),
          ),
        ),
      ],
    );
  }

  Widget _buildRooms() {
    return ListTile(
      leading: Icon(Partnerum.rooms, color: Theme.of(context).primaryColor),
      title: Text(Strings.pages.housesPage.newVersionHouseDetails.roomsAmount.l(context)),
      subtitle: Text(roomsAmount() == 5 ? Strings.pages.housesPage.newVersionHouseDetails.moreThan4.l(context) : roomsAmount().toString()),
    );
  }

  Widget _buildPrice() {
    return ListTile(
      leading: Icon(Partnerum.ruble, color: Theme.of(context).primaryColor),
      title: Text(Strings.pages.housesPage.newVersionHouseDetails.price.l(context)),
      subtitle: Text(
        '${Strings.pages.housesPage.newVersionHouseDetails.priceFrom.l(context)} ${startPrice()} ${Strings.pages.housesPage.newVersionHouseDetails.priceTo.l(context)} ${endPrice()} ${Strings.pages.housesPage.newVersionHouseDetails.priceMeasure.l(context)}',
      ),
    );
  }

  Widget _buildAddress() {
    return ListTile(
      leading: Icon(Partnerum.address, color: Theme.of(context).primaryColor),
      title: Text(Strings.pages.housesPage.newVersionHouseDetails.address.l(context)),
      subtitle: Text('${address()}'),
    );
  }

  Widget _buildDistance() {
    if (isAnyStations()) {
      return ListTile(
        leading: Icon(Partnerum.walk, color: Theme.of(context).primaryColor),
        title: Text(Strings.pages.housesPage.newVersionHouseDetails.timeToMetro.l(context)),
        subtitle: Text('${timeToMetro()} ${Strings.pages.housesPage.newVersionHouseDetails.timeMeasure.l(context)}'),
      );
    } else {
      return Container();
    }
  }

  Widget _buildCity() {
    return ListTile(
      leading: Icon(Partnerum.city, color: Theme.of(context).primaryColor),
      title: Text(Strings.pages.housesPage.newVersionHouseDetails.city.l(context)),
      subtitle: Text('${city()}'),
    );
  }

  Widget _buildMetro() {
    return isAnyStations()
        ? ExpansionTile(
            leading: Icon(Partnerum.metro, color: Theme.of(context).primaryColor),
            title: Text('${Strings.pages.housesPage.newVersionHouseDetails.metroStations.l(context)} (${stationsAmount()})'),
            children: <Widget>[
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Wrap(
                    runSpacing: 2,
                    spacing: 2,
                    children: <Widget>[
                      for (var i = 0; i < stationsAmount(); i++)
                        Chip(
                            padding: EdgeInsets.zero,
                            label: Text(
                              '${stationByIndex(i)}',
                              style: TextStyle(fontSize: 14, color: Colors.grey),
                            ))
                    ],
                  ),
                ),
              ),
            ],
          )
        : Container();
  }
}
