import 'package:flutter/material.dart';
import 'package:micro_utils/data_helper.dart';
import 'package:db_partnerum/db_partnerum.dart';
import 'package:partnerum/utils/brain_module.dart';

@DataHelper()
class HouseDetailsHelper {
  HouseInformation houseInformation;

  Future<List<PhotoImage>> photosList(HouseInformation houseInformation) async {
    var imagesList = <PhotoImage>[];
    for (var photo in houseInformation?.photos?.values) {
      imagesList.add(await root.imagesContainer[photo.value].future);
    }
    return imagesList;
  }

  int photosAmount() {
    // return 0;
    var photos = houseInformation?.photos;
    if (photos == null) {
      return 0;
    }
    // return 1;
    return photos.length;
  }

  String photoByIndex(int index) {
    return '';
    // return house.houseInformation.photos.list[index].value;
  }

  String houseInfo() {
    var description = houseInformation.description;
    return ((description == null) || (description.value == null) || (description.value == 'null')) ? '' : description.value;
  }

  int roomsAmount() {
    var rooms = houseInformation.rooms;
    return ((rooms == null) || (rooms.value == null) || (rooms.value == 'null')) ? 0 : rooms.value.toInt();
  }

  String startPrice() {
    var startPrice = houseInformation.priceRange.startPosition;
    return ((startPrice == null) || (startPrice.value == null) || (startPrice.value == 'null')) ? '' : startPrice.value;
  }

  String endPrice() {
    var endPrice = houseInformation.priceRange.endPosition;
    return ((endPrice == null) || (endPrice.value == null) || (endPrice.value == 'null')) ? '' : endPrice.value;
  }

  String address() {
    var address = houseInformation.address;
    return ((address == null) || (address.value == null) || (address.value == 'null')) ? '' : address.value;
  }

  bool isAnyStations() {
    // return false;
    return houseInformation.stations != null;
  }

  int timeToMetro() {
    var timeToMetro = houseInformation?.timeToMetro?.value?.inMinutes;
    if (timeToMetro == null) return 0;
    return timeToMetro;

    // return ((timeToMetro == null) ||
    //         (timeToMetro == null) ||
    //         (timeToMetro == 'null'))
    //     ? ''
    //     : timeToMetro.value.inMinutes;
  }

  String city() {
    var city = houseInformation.city;
    return ((city == null) || (city.value == null) || (city.value == 'null')) ? '' : city.value;
  }

  int stationsAmount() {
    // return 0;
    return houseInformation?.stations?.values?.length ?? 0;
  }

  String stationByIndex(int index) {
    // return 'St';
    return houseInformation.stations.values.toList()[index].key;
  }

  // List photosList(House house) {
  //   return [];
  //   // return house.houseInformation.photos.list;
  // }

  String houseID() {
    return houseInformation.key;
  }

  Future<void> toggleAutoBookingSetup(datesAutoBooking) async {
    await root.userData[mindClass.user.uid].apartmentsAutoBookingDates.set(!datesAutoBooking);
  }

  Future<bool> addApartmentBookingDates(String apartmentId, DateTimeRange dateTimeRange, List<BookingPeriod> bookingPeriods) async {
    bool checkDatesIntersection(List<BookingPeriod> bookedDates, DateTimeRange bookingDates) {
      for (var date in bookedDates) {
        var startDate = date.checkIn.value;
        var endDate = date.checkOut.value;
        print(startDate.inMilliseconds);
        print(endDate.inMilliseconds);
        print(bookingDates.end.millisecondsSinceEpoch);
        print(bookingDates.start.millisecondsSinceEpoch);
        if (!((startDate.inMilliseconds > bookingDates.end.millisecondsSinceEpoch) ||
            (endDate.inMilliseconds < bookingDates.start.millisecondsSinceEpoch))) {
          return true;
        }
      }
      return false;
    }

    if (checkDatesIntersection(bookingPeriods, dateTimeRange)){
      return true;
    }
    else {
      var bookingDates = root.userData[mindClass.user.uid].houses[apartmentId].bookingDates.push;
      await bookingDates.checkIn.set(dateTimeRange.start.millisecondsSinceEpoch);
      await bookingDates.checkOut.set(dateTimeRange.end.millisecondsSinceEpoch);
      return false;
    }
  }

  Future<void> removeBookingPeriod(String apartmentId, String bookingPeriodId) async {
    await root.userData[mindClass.user.uid].houses[apartmentId].bookingDates[bookingPeriodId].remove();
  }

  Stream<List<BookingPeriod>> bookingDatesStream(String hoseId) {
    return db?.root?.userData[mindClass.user.uid]?.houses[hoseId]?.bookingDates?.stream?.map((event) => event?.values?.toList() ?? []) ?? [];
  }
}
