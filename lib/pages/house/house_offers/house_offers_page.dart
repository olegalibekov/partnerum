import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:db_partnerum/db_partnerum.dart';
import 'package:diffutil_sliverlist/diffutil_sliverlist.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:flutter/material.dart' hide ExpansionTile;

import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:partnerum/head_module/gen_extensions/generated_str_extension.dart';
import 'package:partnerum/models/houses_requests_model.dart';

import 'package:partnerum/orders/orders/orders_page.dart';
import 'package:partnerum/utils/brain_module.dart';
import 'package:partnerum/utils/theme_constants.dart';
import 'package:partnerum/utils/utils.dart';

import 'package:scroll_to_index/scroll_to_index.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'dart:math' as math;
import 'package:partnerum/head_module/gen_extensions/generated_files_extension.dart';

import 'house_offers_helper.dart';

class SMapEntry<V1, V2> {
  V1 v1;

  V2 v2;

  factory SMapEntry(V1 key, V2 value) = SMapEntry<V1, V2>._;

  SMapEntry._(this.v1, this.v2);

  String toString() => "SMapEntry(${v1.toString()}: ${v2.toString()})";
}

class ChoosedList extends StatefulWidget {
  const ChoosedList({Key key, @required this.cardForm, @required this.houses}) : super(key: key);

  final bool cardForm;
  final List<House> houses;

  @override
  _ChoosedListState createState() => _ChoosedListState();
}

class _ChoosedListState extends State<ChoosedList> {
  int _selectedElement = -1;

  @override
  void initState() {
    mindClass.eventSteam.outStream.listen((event) {
      if (event == 'removeRoundClick') {
        setState(() {
          _selectedElement = -1;
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        child: Row(children: [
          if (!widget.cardForm) SizedBox(width: 16),
          for (var i = 0, l = widget.houses.length; i < l; i++)
            if (true) //TODO mindClass.requested(widget.houses[i].offerType)
              Row(mainAxisSize: MainAxisSize.min, children: [
                AnimatedContainer(
                    decoration: BoxDecoration(
                        border: Border.all(width: 2, style: BorderStyle.solid, color: _selectedElement == i ? Colors.orange : goodGreen),
                        borderRadius: BorderRadius.circular(30)),
                    padding: EdgeInsets.all(1),
                    duration: kThemeAnimationDuration,
                    child: GestureDetector(
                        onTap: () async {
                          if (!widget.cardForm) {
                            setState(() {
                              if (_selectedElement == i) {
                                _selectedElement = -1;
                              } else {
                                vibrate();
                                /*
                                mindClass.updateEvent(
                                    "scrollToElement${widget.houses[i].complexId}");

                                 */
                                _selectedElement = i;
                              }
                            });
                          }
                        },
                        child: CircleAvatar(
                          backgroundColor: Colors.grey.shade100,
                          /*
                            backgroundImage: CachedNetworkImageProvider(widget.houses[i].houseInformation.photos.list.first)

                             */
                        ))),
                spacer(4, 0)
              ])
        ]));
  }
}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  _SliverAppBarDelegate({@required this.minHeight, @required this.maxHeight, @required this.child});

  final double minHeight;
  final double maxHeight;
  final Widget child;

  @override
  double get minExtent => minHeight;

  @override
  double get maxExtent => math.max(maxHeight, minHeight);

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    return SizedBox.expand(child: child);
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return maxHeight != oldDelegate.maxHeight || minHeight != oldDelegate.minHeight || child != oldDelegate.child;
  }
}

class HouseOffersPage extends StatefulWidget {
  HouseOffersPage({Key key, this.house}) : super(key: key);

  final House house;

  @override
  _HouseOffersPageState createState() => _HouseOffersPageState();
}

class _HouseOffersPageState extends State<HouseOffersPage> with HouseOffersHelper {
  Stream<List<HouseRequestInfo>> houseRequestsIdStream;

  @override
  void initState() {
    super.initState();
    _autoScrollController.addListener(_scrollListener);
    _subscription = mindClass.eventSteam.outStream.listen((value) async {
      if (value is String && value.contains('scrollToElement')) {
        var scrollIndex = value.replaceFirst('scrollToElement', '');
        await _autoScrollController.scrollToIndex(scrollIndex.hashCode, preferPosition: AutoScrollPosition.begin);
        _clicked = true;
      }
      if (value == 'updateView') _loaderVisibility.value = false;
    });
    mindClass.eventSteam.outStream.listen((value) {
      if (value == 'updateView') {
        setState(() {
          print('up');
          isLoading = false;
        });
      }
      if (value == 'housesRequestUpdate') _filterOpened.notifyListeners();
    });

    houseRequestsIdStream = widget.house?.houseRequests?.stream?.map((event) => event?.values?.toList())?.asBroadcastStream();
  }

  @override
  void dispose() {
    _subscription.cancel();
    _autoScrollController.removeListener(_scrollListener);
    super.dispose();
  }

  bool isLoading = false;

  bool isArchive = false;
  final ValueNotifier<bool> _loaderVisibility = ValueNotifier<bool>(true);
  final ValueNotifier<bool> _filterOpened = ValueNotifier<bool>(false);
  final AutoScrollController _autoScrollController = AutoScrollController();
  bool _clicked = false;

  _scrollListener() {
    _filterOpened.value = false;
    if (_clicked) mindClass.updateEvent('removeRoundClick');
  }

  List<SMapEntry<String, bool>> sortingRequestList = [
    SMapEntry(Strings.pages.housesPage.houseOffersPage.newRequests, true),
    SMapEntry(Strings.pages.housesPage.houseOffersPage.acceptedRequests, true),
    SMapEntry(Strings.pages.housesPage.houseOffersPage.bookedRequests, true),
    SMapEntry(Strings.pages.housesPage.houseOffersPage.declinedRequests, true),
  ];

  void _onReorder(int oldIndex, int newIndex) {
    if (newIndex > oldIndex) {
      newIndex -= 1;
    }

    final item = sortingRequestList.removeAt(oldIndex);
    sortingRequestList.insert(newIndex, item);
    _filterOpened.notifyListeners();
    _reorderScrollController.animateTo(0, duration: kThemeChangeDuration, curve: Curves.linear);
  }

  final ScrollController _reorderScrollController = ScrollController();
  StreamSubscription _subscription;
  String _selectedOfferType = Strings.pages.housesPage.houseOffersPage.active;

  filterHeader() {
    return ValueListenableBuilder(
      builder: (BuildContext context, value, Widget child) {
        const max = kToolbarHeight * 8;
        return TweenAnimationBuilder(
            builder: (context, _value, child) {
              return SliverPersistentHeader(
                pinned: true,
                delegate: _SliverAppBarDelegate(
                  minHeight: _value,
                  maxHeight: _value,
                  child: child,
                ),
              );
            },
            tween: !value ? Tween<double>(begin: max, end: 0) : Tween<double>(begin: 0, end: max),
            duration: kThemeChangeDuration,
            child: SingleChildScrollView(
                child: AnimatedContainer(
                    duration: kThemeAnimationDuration,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.2),
                          spreadRadius: 1,
                          blurRadius: 1,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(mainAxisSize: MainAxisSize.min, children: [
                          // SwitchListTile(
                          //     title: Text(Strings.pages.housesPage.houseOffersPage.hideArchivedRequests.l(context)),
                          //     value: false,
                          //     onChanged: (value) {
                          //       // _filterList.value.onlyNew = value;
                          //       _filterOpened.value = _filterOpened.value;
                          //       _filterOpened.notifyListeners();
                          //       // _filterList.notifyListeners();
                          //     }),
                          ListTile(
                            title: Text(Strings.pages.housesPage.houseOffersPage.showRequests.l(context)),
                          ),
                          ChipTheme(
                              data: Theme.of(context).chipTheme.copyWith(
                                  backgroundColor: Colors.white,
                                  selectedColor: Colors.orange.withOpacity(0.1),
                                  disabledColor: Colors.white,
                                  pressElevation: 0,
                                  checkmarkColor: Colors.orange),
                              child: Align(
                                  alignment: Alignment.topRight,
                                  child: SingleChildScrollView(
                                      scrollDirection: Axis.horizontal,
                                      child: Wrap(spacing: 4, children: [
                                        /*
                                        for (String offerType
                                            in mindClass.offersTypes)
                                          InputChip(
                                            selected:
                                                _selectedOfferType == offerType,
                                            label: Text(offerType),
                                            onPressed: () {
                                              if (_selectedOfferType ==
                                                  offerType) return;
                                              _selectedOfferType = offerType;
                                              _filterOpened.value =
                                                  _filterOpened.value;
                                              _filterOpened.notifyListeners();
                                            },
                                          )

                                         */
                                      ])))),
                          Divider(),
                          ListTile(
                            title: Text(Strings.pages.housesPage.houseOffersPage.orderOfRequests.l(context)),
                          ),
                          Divider(
                            height: 1,
                          ),
                          Container(
                              constraints: BoxConstraints(maxHeight: max - kToolbarHeight * 4),
                              child: ReorderableListView(
                                  onReorder: _onReorder,
                                  scrollController: _reorderScrollController,
                                  scrollDirection: Axis.vertical,
                                  children: List.generate(sortingRequestList.length, (index) {
                                    return CheckboxListTile(
                                      key: Key('$index'),
                                      secondary: Icon(Icons.menu),
                                      title: Text('${sortingRequestList[index].v1.l(context)}'),
                                      value: sortingRequestList[index].v2,
                                      onChanged: (bool value) {
                                        sortingRequestList[index].v2 = value;
                                        _filterOpened.notifyListeners();
                                      },
                                    );
                                  })))
                        ])))));
      },
      valueListenable: _filterOpened,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Material(
        child: SlidingUpPanel(
            color: Colors.transparent,
            minHeight: kToolbarHeight,
            maxHeight: 300,
            panelBuilder: (ScrollController sc) => Container(
                decoration: BoxDecoration(color: Colors.white, borderRadius: sheetRadius),
                clipBehavior: Clip.antiAlias,
                child: Column(mainAxisSize: MainAxisSize.min, children: [
                  Padding(
                    padding: const EdgeInsets.all(16).copyWith(bottom: 8, top: 8),
                    child: Text(
                      Strings.pages.housesPage.houseOffersPage.activeRequests.l(context),
                      textAlign: TextAlign.start,
                      style: TextStyle(fontSize: 26, color: Colors.black),
                    ),
                  ),
                ])),
            body: Scaffold(
                backgroundColor: Colors.white,
                body: Stack(children: <Widget>[
                  // Container(color: Colors.grey, width: 20, height: 40,)
                  StreamBuilder<List<HouseRequestInfo>>(
                      // initialData: [0],
                      // future: mindClass.userTopicHousesFuture(widget.house.houseId),
                      stream: houseRequestsIdStream,
                      builder: (BuildContext context, AsyncSnapshot rootSnapshot) {
                        if (rootSnapshot.connectionState != ConnectionState.waiting) {
                          Timer(kThemeChangeDuration, () => _loaderVisibility.value = false);
                        }
                        return AnimatedSwitcher(
                            duration: Duration(milliseconds: 600),
                            child: ScrollConfiguration(
                                behavior: noHighlightScrollBehavior(context),
                                child: RefreshIndicator(
                                    displacement: 80,
                                    onRefresh: () {
                                      _loaderVisibility.value = true;
                                      setState(() {});
                                      return Future.delayed(Duration(milliseconds: 300));
                                    },
                                    child: CustomScrollView(controller: _autoScrollController, slivers: [
                                      SliverAppBar(
                                        bottom: PreferredSize(
                                            preferredSize: Size.fromHeight(2),
                                            child: ValueListenableBuilder(
                                              valueListenable: _loaderVisibility,
                                              builder: (BuildContext context, bool value, Widget child) => AnimatedOpacity(
                                                  duration: Duration(milliseconds: 300), opacity: value ? 1 : 0, child: linearProgressIndicator),
                                            )),
                                        pinned: true,
                                        title: Align(
                                          alignment: Alignment.center,
                                          child: AnimatedSwitcher(
                                            duration: Duration(milliseconds: 300),
                                            child: Text(
                                                !isArchive
                                                    ? Strings.pages.housesPage.houseOffersPage.requests.l(context)
                                                    : Strings.pages.housesPage.houseOffersPage.archivedRequests.l(context),
                                                key: Key(isArchive.toString())),
                                            // child: !isArchive
                                            //     ? Text(Strings.pages.housesPage.houseOffersPage.requests.l(context),
                                            //         key: ValueKey<bool>(
                                            //             !isArchive))
                                            //     : Text(Strings.pages.housesPage.houseOffersPage.archivedRequests.l(context),
                                            //         key: ValueKey<bool>(
                                            //             isArchive)),
                                          ),
                                        ),
                                        actions: [
                                          IconButton(icon: Icon(Icons.filter_list), onPressed: () => _filterOpened.value = !_filterOpened.value),
                                          IconButton(
                                              icon: Icon(!isArchive ? Icons.archive : Icons.unarchive),
                                              onPressed: () {
                                                if (isArchive == true) {
                                                  isArchive = false;
                                                } else {
                                                  isArchive = true;
                                                }
                                                setState(() {});
                                              }),
                                        ],
                                      ),
                                      filterHeader(),
                                      ValueListenableBuilder(
                                          builder: (BuildContext context, value, child) {
                                            if (rootSnapshot.hasData && !rootSnapshot.hasError && rootSnapshot.data.length > 0) {
                                              if (rootSnapshot.data.first.runtimeType == 0.runtimeType) {
                                                return SliverToBoxAdapter(
                                                    child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                                                  Container(
                                                      margin: EdgeInsets.all(20),
                                                      child: Stack(children: <Widget>[
                                                        Column(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                                                          Padding(
                                                            padding: const EdgeInsets.only(top: 16.0),
                                                            child: Center(
                                                              child: Container(
                                                                  width: 300,
                                                                  height: 300,
                                                                  child: SvgPicture.asset(files.images.undraw.postOnlineSvg)),
                                                            ),
                                                          ),
                                                          Padding(
                                                              padding: const EdgeInsets.only(top: 16.0),
                                                              child: Text(Strings.pages.housesPage.houseOffersPage.emptyRequestsList.l(context),
                                                                  textAlign: TextAlign.center,
                                                                  style: TextStyle(
                                                                    color: Colors.black,
                                                                    fontSize: 14.0,
                                                                  )))
                                                        ])
                                                      ]))
                                                ]));
                                              }
                                              // var houseRequests = List<HousesRequest>.from(snapshot.data);
                                              // var houseRequests =
                                              //     snapshot.data;

                                              //   initHouseRequests  () async {
                                              //       var tempHouseRequests = snapshot.data;
                                              //       // print('HouseRequests: ${houseRequests.length}');
                                              //   if(snapshot.hasData) {
                                              //     for (var houseRequestIndex = 0;
                                              //     houseRequestIndex <
                                              //         tempHouseRequests
                                              //             .length; houseRequestIndex++) {
                                              //
                                              //
                                              //       var requestId = snapshot
                                              //           .data[houseRequestIndex];
                                              //       // print("")
                                              //       await offerObjectFuture(
                                              //           requestId
                                              //               .value)
                                              //           .then((value) {
                                              //
                                              //         if (value
                                              //             .requestCopy
                                              //             .isArchived.value ==
                                              //             false) {
                                              //           print('Archived archived');
                                              //           houseRequests.add(
                                              //               requestId);
                                              //         };
                                              //       });
                                              //
                                              //     }
                                              //   }
                                              // };
                                              //
                                              // initHouseRequests();

                                              // print('House requests length: ${houseRequests.length}');
                                              houseRequests = (rootSnapshot.data).where((e) =>
                                                  ((isArchive == false) && (e.isArchived.value != !isArchive)) ||
                                                  ((isArchive == true) && (e.isArchived.value == isArchive))).toList();

                                              archiveItem(int index) async {
                                                //return false;
                                                /*
                                                var result =
                                                    await mindClass.archiveRequest(houseRequests[index].requestId, !houseRequests[index].archived);
                                                //await Future.delayed(Duration(milliseconds: 600));
                                                return result;
                                                */
                                              }

                                              // action(int index) {
                                              //   return Builder(builder:
                                              //       (BuildContext
                                              //           buildContext) {
                                              //     return IconSlideAction(
                                              //         caption: isArchive
                                              //             ? Strings.globalOnes.unarchive.l(context)
                                              //             : Strings
                                              //                 .globalOnes
                                              //                 .archive
                                              //                 .l(context),
                                              //         color:
                                              //             Colors.grey[200],
                                              //         icon: isArchive
                                              //             ? Icons.unarchive
                                              //             : Icons.archive,
                                              //         onTap: () {
                                              //           Slidable.of(
                                              //                   buildContext)
                                              //               .dismiss();
                                              //         });
                                              //   });
                                              // }
/*
                                              if (mindClass.offersTypes.first != _selectedOfferType) {
                                                houseRequests
                                                    .removeWhere((element) => element.archived == (mindClass.offersTypes[1] == _selectedOfferType));
                                              }
*/

                                              // return SliverToBoxAdapter(child: Container(color: Colors.purple, width: 100, height: 150,),);

                                              Builder action(int index) {
                                                return Builder(builder: (BuildContext buildContext) {
                                                  return IconSlideAction(
                                                      caption:
                                                          isArchive ? Strings.globalOnes.unarchive.l(context) : Strings.globalOnes.archive.l(context),
                                                      color: Colors.transparent,
                                                      foregroundColor: blackColor,
                                                      icon: isArchive ? Icons.unarchive : Icons.archive,
                                                      onTap: () {
                                                        Slidable.of(buildContext).dismiss();
                                                      });
                                                });
                                              }

                                              //     ?.map((event) => event?.values?.toList())
                                              //     ?.asBroadcastStream()
                                              return DiffUtilSliverList<HouseRequestInfo>(
                                                items: houseRequests,
                                                /*equalityChecker: (house1, house2) => house1.complexId == house2.complexId,*/
                                                equalityChecker: (house1, house2) => house1 == house2,
                                                builder: (context, housesRequest) {
                                                  // var offerObjectStream = offerObjectLoader(housesRequest.offerId.value);
                                                  var offerToRoomObject = root.chatTopic.offer2Room[housesRequest.offerId.value];
                                                  var userDataObject = root.userData[housesRequest.requesterId.value];
                                                  return AutoScrollTag(
                                                    /*key: Key(housesRequest.complexId),*/
                                                    key: Key(housesRequest.offerId.value),
                                                    /*index: housesRequest.complexId.hashCode,*/
                                                    index: housesRequest.offerId.value.hashCode,
                                                    controller: _autoScrollController,
                                                    child: StreamBuilder(
                                                        stream: offerToRoomObject.stream.asBroadcastStream(),
                                                        initialData: offerToRoomObject,
                                                        builder: (context, offerObjectSnapshot) {
                                                          OfferObject offerObject = offerObjectSnapshot.data;
                                                          var ownerStatus = offerObject
                                                              ?.conversationUsers?.usersStatuses?.ownerUserStatus?.value
                                                              ?.split(';')
                                                              ?.first;
                                                          var isNew = ownerStatus == null;
                                                          var isAccepted = ownerStatus == 'request_accept';
                                                          var isBooked = ownerStatus == 'request_booked';
                                                          var isDenied = ownerStatus == 'request_denied';

                                                          if ((sortingRequestList[0].v2 && isNew) ||
                                                              (sortingRequestList[1].v2 && isAccepted) ||
                                                              (sortingRequestList[2].v2 && isBooked) ||
                                                              (sortingRequestList[3].v2 && isDenied)) {
                                                            // var requestUserStream =
                                                            //     requestUserLoader(offerObjectSnapshot.data.conversationUsers.requestUser.value);
                                                            return StreamBuilder(
                                                                stream: userDataObject.stream.asBroadcastStream(),
                                                                initialData: userDataObject,
                                                                builder: (BuildContext context, AsyncSnapshot<User> userSnapshot) {

                                                                 if(userSnapshot.hasData) {
                                                                   return Slidable(
                                                                       enabled: true,
                                                                       actionPane: SlidableDrawerActionPane(),
                                                                       key: Key(housesRequest.offerId.value),
                                                                       dismissal: SlidableDismissal(
                                                                           onWillDismiss: (item) async {
                                                                             //   /*
                                                                             //   return await archiveItem(index);
                                                                             //    */
                                                                             var offerObject = await offerObjectFuture(housesRequest.offerId.value);
                                                                             var isArchivedValue = offerObject?.requestCopy?.isArchived?.value;
                                                                             if (isArchivedValue == null || !isArchivedValue) {
                                                                               await archiveRequest(offerObject, housesRequest);
                                                                             } else {
                                                                               await setUnArchived(offerObject, housesRequest);
                                                                             }
                                                                             return true;
                                                                           },
                                                                           onDismissed: (actionType) {
                                                                             setState(() {
                                                                               houseRequests.removeWhere((element) => element.offerId.value == housesRequest.offerId.value);
                                                                               // print(
                                                                               //     'Requests before deletion: ${requests}');
                                                                               // requests.removeAt(index);
                                                                               // print(
                                                                               //     'Requests after deletion: ${requests}');
                                                                             });
                                                                           },
                                                                           child: SlidableDrawerDismissal()),
                                                                       actions: [action(0)],
                                                                       secondaryActions: [action(0)],
                                                                       actionExtentRatio: 0.25,
                                                                       child: Padding(
                                                                           padding: EdgeInsets.symmetric(
                                                                             horizontal: 16.0,
                                                                           ).add(EdgeInsets.only(top: 16)),
                                                                           child: LayoutBuilder(builder: (BuildContext _context, constraints) {

                                                                             // return Container(width: 100, height: 200, color: Colors.red);
                                                                             // print('House request: ${housesRequest}');

                                                                             return HouseRequestCard(
                                                                               offerObject: offerObjectSnapshot.data,
                                                                               isArchive: isArchive,
                                                                               house: widget.house,
                                                                               onSend: () {
                                                                                 setState(() {
                                                                                   isLoading = true;
                                                                                 });
                                                                               },
                                                                               onArchive: () {
                                                                                 Slidable.of(_context).dismiss();
                                                                               },
                                                                               requestUser: userSnapshot?.data,
                                                                             );
                                                                           })));
                                                                 }

                                                                 return SizedBox.shrink();

                                                                });
                                                          }
                                                          return SizedBox.shrink();
                                                        }));
                                                },
                                                insertAnimationBuilder: (context, animation, child) => FadeTransition(
                                                  opacity: animation,
                                                  child: child,
                                                ),
                                                removeAnimationBuilder: (context, animation, child) => SizeTransition(
                                                  sizeFactor: animation,
                                                  child: child,
                                                ),
                                                removeAnimationDuration: const Duration(milliseconds: 300),
                                                insertAnimationDuration: const Duration(milliseconds: 300),
                                              );

                                              // return SliverList(
                                              //     delegate:
                                              //         SliverChildBuilderDelegate(
                                              //             (context, index) {
                                              //   return Slidable(
                                              //       actionPane:
                                              //           SlidableDrawerActionPane(),
                                              //       actionExtentRatio: 0.25,
                                              //       child: Padding(
                                              //           padding: EdgeInsets
                                              //               .symmetric(
                                              //             horizontal: 16.0,
                                              //           ).add(
                                              //               EdgeInsets.only(
                                              //                   top: 16)),
                                              //           child: LayoutBuilder(
                                              //               builder: (BuildContext
                                              //                       _context,
                                              //                   constraints) {
                                              //             return Column(
                                              //                 mainAxisSize:
                                              //                     MainAxisSize
                                              //                         .min,
                                              //                 children: [
                                              //                   HouseRequestCard(
                                              //                       housesRequest:
                                              //                           houseRequests[
                                              //                               index],
                                              //                       isArchive:
                                              //                           isArchive,
                                              //                       house: widget
                                              //                           .house,
                                              //                       onSend:
                                              //                           () {
                                              //                         setState(
                                              //                             () {
                                              //                           isLoading =
                                              //                               true;
                                              //                         });
                                              //                       },
                                              //                       onArchive:
                                              //                           () {
                                              //                         Slidable.of(_context)
                                              //                             .dismiss();
                                              //                       }),
                                              //                   //RateWidget(),
                                              //                 ]);
                                              //           })));
                                              // },
                                              //             childCount:
                                              //                 houseRequests
                                              //                     .length));
                                            } else {
                                              return SliverToBoxAdapter(
                                                  child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                                                Container(
                                                    margin: EdgeInsets.all(20),
                                                    child: Column(
                                                        mainAxisSize: MainAxisSize.max,
                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                        children: <Widget>[
                                                          Padding(
                                                            padding: const EdgeInsets.only(top: 16.0),
                                                            child: Container(
                                                                width: 300,
                                                                height: 300,
                                                                // child: UnDraw(
                                                                //   illustration:
                                                                //       UnDrawIllustration.post_online,
                                                                //   color:
                                                                //       Theme.of(context).primaryColor,
                                                                // )
                                                                child: SvgPicture.asset(files.images.undraw.postOnlineSvg)),
                                                          ),
                                                          Padding(
                                                              padding: const EdgeInsets.only(top: 16.0),
                                                              child: Text(Strings.pages.housesPage.houseOffersPage.emptyRequestsList.l(context),
                                                                  textAlign: TextAlign.center,
                                                                  style: TextStyle(
                                                                    color: Colors.black,
                                                                    fontSize: 14.0,
                                                                  )))
                                                        ]))
                                              ]));
                                            }
                                          },
                                          valueListenable: _filterOpened),
                                      SliverToBoxAdapter(child: spacer(0, kToolbarHeight))
                                    ]))));
                      })
                ]))));
  }
}

class ListViewCard extends StatefulWidget {
  final int index;
  final Key key;
  final List<String> listItems;

  ListViewCard(this.listItems, this.index, this.key);

  @override
  _ListViewCard createState() => _ListViewCard();
}

class _ListViewCard extends State<ListViewCard> {
  @override
  Widget build(BuildContext context) {
    return Card(
        margin: EdgeInsets.all(4),
        color: Colors.white,
        child: InkWell(
            splashColor: Colors.blue,
            onTap: () => Fluttertoast.showToast(msg: 'Item ${widget.listItems[widget.index]} selected.', toastLength: Toast.LENGTH_SHORT),
            child: Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
              Flexible(
                  child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                Container(
                    padding: const EdgeInsets.all(8.0),
                    alignment: Alignment.topLeft,
                    child: Text('Title ${widget.listItems[widget.index]}',
                        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16), textAlign: TextAlign.left, maxLines: 5)),
                Container(
                    padding: const EdgeInsets.all(8.0),
                    alignment: Alignment.topLeft,
                    child: Text('Description ${widget.listItems[widget.index]}',
                        style: TextStyle(fontWeight: FontWeight.normal, fontSize: 16), textAlign: TextAlign.left, maxLines: 5))
              ])),
              Padding(padding: const EdgeInsets.fromLTRB(0, 0, 10, 0), child: Icon(Icons.reorder, color: Colors.grey, size: 24.0))
            ])));
  }
}
