import 'dart:async';

import 'package:db_partnerum/db_partnerum.dart';
import 'package:micro_utils/data_helper.dart';
import 'package:partnerum/utils/brain_module.dart';

@DataHelper()
class HouseOffersHelper {
  List<HouseRequestInfo> houseRequests = [];

  // Future<List> houseOffers(House house) =>
  //     house?.houseRequests?.future
  //         ?.then((value) => value?.values?.toList() ?? []) ??
  //     [];

  OfferObject offerObjectLoader(String offerId) {
    return root.chatTopic.offer2Room[offerId];
  }

  User requestUserLoader(userId) {
    return root.userData[userId];
  }

  Future<OfferObject> offerObjectFuture(String objectID) =>
      root.chatTopic.offer2Room[objectID].future;

  // void setRequestDenyReason(OfferObject offerObject, String denyReason) async {
  //   await offerObject.conversationUsers.usersStatuses.requestDenyReason.set(denyReason);
  // }

  void setOwnerStatus(OfferObject offerObject, String status) async {
    await offerObject.conversationUsers.usersStatuses.ownerUserStatus
        .set(status);

    // root.chatTopic.offer2Room[offerObject.key].conversationUsers.usersStatuses
    //     .ownerUserStatus = status;
  }

  FutureOr<void> archiveRequest(
      OfferObject offerObject, HouseRequestInfo houseRequestInfo) async {
    var conversationUsers = offerObject.conversationUsers;
    if (conversationUsers.usersStatuses.requestUserStatus.value ==
            'house_request_booked' &&
        conversationUsers.usersStatuses.ownerUserStatus.value
                .split(';')
                .first ==
            'request_booked') {
    } else {
      setOwnerStatus(offerObject, 'request_denied');
    }
    offerObject.requestCopy.isArchived.set(true);
    await houseRequestInfo.isArchived.set(true);
  }

  FutureOr<void> setUnArchived(
      OfferObject offerObject, HouseRequestInfo houseRequestInfo) async {
    offerObject.requestCopy.isArchived.set(false);
    await houseRequestInfo.isArchived.set(false);
  }

  String currentUserType(OfferObject offerObject) {
    var ownerUserId =
        offerObject?.conversationUsers?.ownerUser?.value?.split(';')?.first;
    var requestUserId = offerObject?.conversationUsers?.requestUser?.value;
    if (mindClass.user.uid == ownerUserId) return 'ownerUser';
    if (mindClass.user.uid == requestUserId) return 'requestUser';
    return null;
    // return 'requestUser';
  }

  String requestorId(OfferObject offerObject) {
    return offerObject?.conversationUsers?.requestUser?.value;
  }

  List<String> ownerId(OfferObject offerObject) {
    return offerObject?.conversationUsers?.ownerUser?.value?.split(';');
  }

  String yourStatus(OfferObject offerObject) {
    var ownerStatus =
        offerObject?.conversationUsers?.usersStatuses?.ownerUserStatus?.value;
    String currentUserStatus;
    if (ownerStatus != null && currentUserType(offerObject) == 'ownerUser') {
      currentUserStatus = ownerStatus.split(';').first;
    } else if (ownerStatus == null &&
        currentUserType(offerObject) == 'ownerUser') {
      currentUserStatus = 'Не указано';
    } else {
      currentUserStatus =
          offerObject.conversationUsers.usersStatuses.requestUserStatus.value;
    }
    return currentUserStatus;
  }

  String oppositeUserStatus(OfferObject offerObject) {
    var ownerStatus =
        offerObject?.conversationUsers?.usersStatuses?.ownerUserStatus?.value;
    String oppositeUserStatusValue;
    if (currentUserType(offerObject) == 'ownerUser') {
      oppositeUserStatusValue = offerObject
          ?.conversationUsers?.usersStatuses?.requestUserStatus?.value;
    } else if (ownerStatus != null &&
        currentUserType(offerObject) == 'requestUser') {
      oppositeUserStatusValue = ownerStatus.split(';').first;
    }

    // var currentUserStatus = currentUserType(offerObject) == 'ownerUser'
    //     ? offerObject.conversationUsers.usersStatuses.requestUserStatus.value
    //     : offerObject.conversationUsers.usersStatuses.ownerUserStatus.value
    //         .split(';')
    //         .first;
    return oppositeUserStatusValue;
  }

  Future<String> createChat(String ownerId, String offerObjectId) async {
    var existChat = await root?.chatTopic?.offer2Room[offerObjectId]?.conversationUsers?.chatId?.future;
    if (existChat?.value == null) {
      var newChat = root.chatTopic.userToUser.push;
      await newChat.header.type.set('offer2room');

      newChat.header.offerObjectId = offerObjectId;
      root.chatTopic.offer2Room[offerObjectId].conversationUsers.chatId = newChat.key;
      return newChat.key;
    }
    else{
      return existChat.value;
    }
  }

  void enterInChat(String chatId) {
    root.userData[mindClass.user.uid].activeChats[chatId].set(true);
    root.userData[mindClass.user.uid].userChats.otr[chatId].status.set('read');
    root.userData[mindClass.user.uid].unreadChats[chatId].remove();
  }

  Future<bool> apartmentsAutoBookingBool(String apartmentId) async {
    var autoBooking = await root
        .userData[mindClass.user.uid].apartmentsAutoBookingDates.future;
    return autoBooking.value;
  }

  Future<void> addApartmentBookingDates(
      String apartmentId, DatesRange datesRange) async {
    var bookingDates =
        root.userData[mindClass.user.uid].houses[apartmentId].bookingDates.push;
    bookingDates.checkIn = datesRange.first.value;
    bookingDates.checkOut = datesRange.last.value;
  }

  String bookingPeriodString(DatesRange datesRange) {
    var firstDate = datesRange.first.value;
    var lastDate = datesRange.last.value;
    var period = '${(firstDate == 'null') ? 'N/A' : firstDate}' +
        ' - ' +
        '${(lastDate == 'null') ? 'N/A' : lastDate}';
    return period;
  }

  Future<List<PhotoImage>> photosList(HouseInformation houseInformation) async {
    var imagesList = <PhotoImage>[];
    for (var photo in houseInformation?.photos?.values) {
      imagesList.add(await root.imagesContainer[photo.value].future);
    }
    return imagesList;
  }
}
