import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:micro_utils/data_helper.dart';
import 'package:db_partnerum/db_partnerum.dart';
import 'package:partnerum/utils/brain_module.dart';
import 'package:path/path.dart' as path;
import 'package:http/http.dart' as http;

@DataHelper()
class HouseCreateHelper {
  var timeValue = 5;

  TextEditingController offerCityNameController = TextEditingController();
  TextEditingController offerMetroStationController = TextEditingController();
  TextEditingController offerAddressNameController = TextEditingController();
  TextEditingController offerStartPriceController = TextEditingController();
  TextEditingController offerEndPriceController = TextEditingController();
  TextEditingController offerInfoController = TextEditingController();
  int roomsValue = 1;

  HouseInformation houseInformation;
  Future<HouseInformation> futureHouseInfo;
  List<File> imageList = [];
  List<PhotoImage> urlImages = [];

  String roomsCount = '1';

  void initHouse(String houseID, HouseInformation prevHouseInfo) {
    if (houseID.isNotEmpty) {
      houseInformation = root.userData[mindClass.user.uid].houses[houseID].houseInformation;
      print('futureHouseInfo');
      futureHouseInfo = root.userData[mindClass.user.uid].houses[houseID].houseInformation.future.then((value) async {
        urlImages = [];
        print('FUTURETHEN');
        var photosIds = value?.photos?.values?.map((e) => e.value)?.toList();
        print('photosIds: ${photosIds}');
        for (var photo in photosIds){
          urlImages.add(await root.imagesContainer[photo].future);
          print('photo: ${photo}');
        }
        return value;
      });
      // loadPhotos(prevHouseInfo);
    } else {
      houseInformation = root.userData[mindClass.user.uid].houses.push.houseInformation;
    }
    houseInformation.settings.packet.on();
    // setRoomsNumber(1);
  }

  init(String houseID, HouseInformation houseInformation) {
    initHouse(houseID, houseInformation);
  }

  downgradeUsagePlaces(PhotoImage img, String houseId) {
    var imgId = img.key;
    // print(img.dbPath);
    // print(houseId);
    // print(imgId);
    root.imagesContainer[imgId].usagePlacesAmount.set(img.usagePlacesAmount.value - 1);
    db.root.userData[mindClass.user.uid].houses[houseId].houseInformation.photos[imgId].remove();
  }

  void setStartPrice(int price) {
    houseInformation.priceRange.startPosition = price;
  }

  void setEndPrice(int price) {
    houseInformation.priceRange.endPosition = price;
  }

  void setRoomsValue(int roomsValue) {
    houseInformation.rooms = roomsValue;
  }

  void setCity(String cityName) {
    houseInformation.city = cityName;
  }

  void setDescription(String description) {
    houseInformation.description = description;
  }

  void setAddress(String address) {
    houseInformation.address = address;
  }

  void setRoomsNumber(int value) {
    houseInformation.rooms = value;
    // house.houseInformation.rooms = 5;
  }

  void setStations(String value) {
    var stationsList = value.split(', ');
    var stations = houseInformation.stations;
    // var dbStations = stations.keys.toList();
    // for (var station in stationsList){
    //   if (dbStations.contains(station)){
    //     stationsList.remove(station);
    //   }
    // }
    // stations.clear();
    // stations.remove();
    // var stationNum = 0;
    for (var metroStation in stationsList) {
      stations[metroStation] = metroStation;
      // stationNum++;
    }
    /*
    for (var i = stationNum; stationNum < oldStationsLength; i++){
      stations[i.toString()].remove();
    }

     */
    // house.houseInformation.rooms = 5;
  }

  void setTimeToMetro() {
    houseInformation.timeToMetro = timeValue * 60000;
  }

  Future<void> uploadHouse() async {
    var imageNum = 0;
    for (var image in imageList) {

      var img = root.imagesContainer.push;
      await img.usagePlacesAmount.set(1);
      houseInformation.photos[img.key].set(img.key);
      // print('image.path: ${image.path.replaceAll('.jpg', 'full.jpg')}');
      // print('path.basename(image.path): ${path.basename(image.path)}');

      var smallImage8List = await testCompressFile(image, 10);
      img.smallImg.value = FileWrapper(path.basename(image.path), smallImage8List);
      var bigImage8List = await testCompressFile(image, 100);
      img.bigImg.value = FileWrapper(path.basename(image.path), bigImage8List);

    }
    setCity(offerCityNameController.text);
    setAddress(offerAddressNameController.text);
    setStartPrice(int.parse(offerStartPriceController.text));
    setEndPrice(int.parse(offerEndPriceController.text));
    setRoomsValue(roomsValue);
    setDescription(offerInfoController.text);
    setStations(offerMetroStationController.text);
    setRoomsNumber(roomsValue);
    setTimeToMetro();
    // house.houseInformation.photos
    await houseInformation.upload();
  }

  String houseStations(HouseInformation houseInformation) {
    var stations = houseInformation?.stations?.values
            ?.map((e) => e.key)
            ?.toList() ??
        [];
    return stations.join(', ');
  }

  /*
  Future<Directory> getTemporaryDirectory() async {
    return Directory.systemTemp;
  }

  void _testCompressFile() async {
    final img = AssetImage("img/img.jpg");
    print("pre compress");
    final config = new ImageConfiguration();

    AssetBundleImageKey key = await img.obtainKey(config);
    final ByteData data = await key.bundle.load(key.name);
    final dir = await path_provider.getTemporaryDirectory();
    print('dir = $dir');

    File file = createFile("${dir.absolute.path}/test.png");
    file.writeAsBytesSync(data.buffer.asUint8List());

    final result = await testCompressFile(file);

    if (result == null) return;

    ImageProvider provider = MemoryImage(result);
    this.provider = provider;
    setState(() {});
  }

  File createFile(String path) {
    final file = File(path);
    if (!file.existsSync()) {
      file.createSync(recursive: true);
    }

    return file;
  }

  Future<String> getExampleFilePath() async {
    final img = AssetImage("img/img.jpg");
    print("pre compress");
    final config = new ImageConfiguration();

    AssetBundleImageKey key = await img.obtainKey(config);
    final ByteData data = await key.bundle.load(key.name);
    final dir = await path_provider.getTemporaryDirectory();

    File file = createFile("${dir.absolute.path}/test.png");
    file.createSync(recursive: true);
    file.writeAsBytesSync(data.buffer.asUint8List());
    return file.absolute.path;
  }

  void getFileImage() async {
    final img = AssetImage("img/img.jpg");
    print("pre compress");
    final config = new ImageConfiguration();

    AssetBundleImageKey key = await img.obtainKey(config);
    final ByteData data = await key.bundle.load(key.name);
    final dir = await path_provider.getTemporaryDirectory();

    File file = createFile("${dir.absolute.path}/test.png");
    file.writeAsBytesSync(data.buffer.asUint8List());

    final targetPath = dir.absolute.path + "/temp.jpg";
    final imgFile = await testCompressAndGetFile(file, targetPath);

    if (imgFile == null) {
      return;
    }

    provider = FileImage(imgFile);
    setState(() {});
  }
*/
  Future<Uint8List> testCompressFile(File file, int quality) async {
    var result = await FlutterImageCompress.compressWithFile(
      file.path,
      quality: quality,
      rotate: 0,
    );
    return result;
  }
/*
  Future<File?> testCompressAndGetFile(File file, String targetPath) async {
    print("testCompressAndGetFile");
    final result = await FlutterImageCompress.compressAndGetFile(
      file.absolute.path,
      targetPath,
      quality: 90,
      minWidth: 1024,
      minHeight: 1024,
      rotate: 90,
    );

    print(file.lengthSync());
    print(result?.lengthSync());

    return result;
  }

  Future testCompressAsset(String assetName) async {
    print("testCompressAsset");
    final list = await FlutterImageCompress.compressAssetImage(
      assetName,
      minHeight: 1920,
      minWidth: 1080,
      quality: 96,
      rotate: 135,
    );

    if (list == null) return;

    this.provider = MemoryImage(Uint8List.fromList(list));
    setState(() {});
  }

  Future compressListExample() async {
    final data = await rootBundle.load("img/img.jpg");

    final memory = await testComporessList(data.buffer.asUint8List());

    setState(() {
      this.provider = MemoryImage(memory);
    });
  }

  Future<Uint8List> testComporessList(Uint8List list) async {
    final result = await FlutterImageCompress.compressWithList(
      list,
      minHeight: 1080,
      minWidth: 1080,
      quality: 96,
      rotate: 270,
      format: CompressFormat.webp,
    );
    print(list.length);
    print(result.length);
    return result;
  }

  void writeToFile(List<int> list, String filePath) {
    final file = File(filePath);
    file.writeAsBytes(list, flush: true, mode: FileMode.write);
  }

  void _compressAssetAndAutoRotate() async {
    final result = await FlutterImageCompress.compressAssetImage(
      R.IMG_AUTO_ANGLE_JPG,
      minWidth: 1000,
      quality: 95,
      // autoCorrectionAngle: false,
    );

    if (result == null) return;

    final u8list = Uint8List.fromList(result);
    this.provider = MemoryImage(u8list);
    setState(() {});
  }

  void _compressPngImage() async {
    final result = await FlutterImageCompress.compressAssetImage(
      R.IMG_HEADER_PNG,
      minWidth: 300,
      minHeight: 500,
    );

    if (result == null) return;

    final u8list = Uint8List.fromList(result);
    this.provider = MemoryImage(u8list);
    setState(() {});
  }

  void _compressTransPNG() async {
    final bytes =
    await getAssetImageUint8List(R.IMG_TRANSPARENT_BACKGROUND_PNG);
    final result = await FlutterImageCompress.compressWithList(
      bytes,
      minHeight: 100,
      minWidth: 100,
      format: CompressFormat.png,
    );

    final u8list = Uint8List.fromList(result);
    this.provider = MemoryImage(u8list);
    setState(() {});
  }

  void _restoreTransPNG() async {
    this.provider = AssetImage(R.IMG_TRANSPARENT_BACKGROUND_PNG);
    setState(() {});
  }

  void _compressImageAndKeepExif() async {
    final result = await FlutterImageCompress.compressAssetImage(
      R.IMG_AUTO_ANGLE_JPG,
      minWidth: 500,
      minHeight: 600,
      // autoCorrectionAngle: false,
      keepExif: true,
    );

    if (result == null) return;

    this.provider = MemoryImage(Uint8List.fromList(result));
    setState(() {});

    // final dir = (await path_provider.getTemporaryDirectory()).path;
    // final f = File("$dir/tmp.jpg");
    // f.writeAsBytesSync(result);
    // print("f.path = ${f.path}");
  }

  void _compressHeicExample() async {
    print("start compress");
    final logger = TimeLogger();
    logger.startRecoder();
    final tmpDir = (await getTemporaryDirectory()).path;
    final target = "$tmpDir/${DateTime.now().millisecondsSinceEpoch}.heic";
    final srcPath = await getExampleFilePath();
    final result = await FlutterImageCompress.compressAndGetFile(
      srcPath,
      target,
      format: CompressFormat.heic,
      quality: 90,
    );

    if (result == null) return;

    print("Compress heic success.");
    logger.logTime();
    print("src, path = $srcPath length = ${File(srcPath).lengthSync()}");
    print(
        "Compress heic result path: ${result.absolute.path}, size: ${result.lengthSync()}");
  }

  void _compressAndroidWebpExample() async {
    // Android compress very nice, but the iOS encode UIImage to webp is slow.
    final logger = TimeLogger();
    logger.startRecoder();
    print("start compress webp");
    final quality = 90;
    final tmpDir = (await getTemporaryDirectory()).path;
    final target =
        "$tmpDir/${DateTime.now().millisecondsSinceEpoch}-$quality.webp";
    final srcPath = await getExampleFilePath();
    final result = await FlutterImageCompress.compressAndGetFile(
      srcPath,
      target,
      format: CompressFormat.webp,
      minHeight: 800,
      minWidth: 800,
      quality: quality,
    );

    if (result == null) return;

    print("Compress webp success.");
    logger.logTime();
    print("src, path = $srcPath length = ${File(srcPath).lengthSync()}");
    print(
        "Compress webp result path: ${result.absolute.path}, size: ${result.lengthSync()}");

    provider = FileImage(result);
    setState(() {});
  }
*/
}
