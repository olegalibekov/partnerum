import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:micro_utils/data_helper.dart';
import 'package:db_partnerum/db_partnerum.dart';
import 'package:partnerum/utils/brain_module.dart';

@DataHelper()
class RateHelper {
  double rating = 0;
  TextEditingController commentEditingController;
  TextEditingController reviewEditingController;

  Future<Review> offerReviewFuture(String messageID) async {
    var review = await root
        .chatTopic.userToUser[messageID].header.offerObjectId.future
        .then((value) => root.chatTopic.offer2Room[value.value].review.future);
    return review;
  }

  void uploadReview(Review review) {
    review.authorId = mindClass.user.uid;
    review.ratingAndReview = '$rating;${reviewEditingController.text}';
    // review.reviewAnswer = commentEditingController.text;
    review.timestamp = DateTime.now().millisecondsSinceEpoch;
  }

  void answerToReview(Review review) {
    review.reviewAnswer = commentEditingController.text;
    review.reviewAnswerTimestamp = DateTime.now().millisecondsSinceEpoch;
  }

  void denyReview(Review review) {
    review.remove();
  }

  List<dynamic> splitRatingAndReview(String value) {
    if (value == null || value.isEmpty) return [null, null];

    var idx = value.indexOf(';');
    var parts = <dynamic>[
      value.substring(0, idx).trim(),
      value.substring(idx + 1).trim()
    ];

    parts[0] = double.parse(parts[0]);
    return parts;
  }
}
