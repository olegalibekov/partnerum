import 'dart:async';

import 'package:async/async.dart';
import 'package:db_partnerum/db_partnerum.dart';
import 'package:flutter/material.dart';

import 'package:flutter_slidable/flutter_slidable.dart';

import 'package:flutter_svg/svg.dart';
import 'package:partnerum/head_module/gen_extensions/generated_str_extension.dart';

import 'package:partnerum/utils/brain_module.dart';
import 'package:partnerum/utils/theme_constants.dart';
import 'package:partnerum/widgets/house_card/house_card_widget.dart';

import 'houses_helper.dart';

class HousesPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _HousesPage();
}

class _HousesPage extends State<HousesPage> with AutomaticKeepAliveClientMixin, HousesHelper /*
        _HousesPageHelper
         */
{
  StreamSubscription subscription;
  Stream housesStreamLoader;


  @override
  void dispose() {
    subscription.cancel();
    manualUpdate.close();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    initStreams();
    housesStreamLoader = root.userData[mindClass.user.uid].houses.stream.map((event) => event?.values?.toList() ?? []).asBroadcastStream();
    // housesStreamLoader.first.then((value) => print('Stream loader first value: $value'));

    subscription = mindClass.eventSteam.outStream.listen((value) {
      if (value == 'updateView') {
        setState(() {
          print('up');
        });
      }
      if (value == 'updateHousesPage') setState(() {});
    });
    manualUpdate.add(false);
  }

  bool isArchive = false;

  toggleArchive() {
    isArchive = !isArchive;
    manualUpdate.add(isArchive);
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<House>>(
        // stream: houseLoader(),
        stream: housesStreamLoader,
        initialData: [],
        builder: (context, snapshot) {
          Key getKey() {
            if (snapshot.hasData) {
              var houses = snapshot.data;

              if (houses.isNotEmpty) {
                return Key('>zero_houses${houses.length}' + isArchive.toString());
              } else {
                return Key('zero_houses');
              }
            }
            return Key('loading');
          }

          return AnimatedSwitcher(
              duration: kThemeAnimationDuration,
              child: Scaffold(
                  key: getKey(),
                  backgroundColor: Colors.white,
                  body: SafeArea(
                      child: RefreshIndicator(
                          displacement: 80,
                          onRefresh: () async {
                            manualUpdate.add(archive);
                            // return Future.value(null);
                            return Future.delayed(Duration(milliseconds: 300));
                          },
                          child: ScrollConfiguration(
                              behavior: noHighlightScrollBehavior(context),
                              child: CustomScrollView(slivers: [
                                SliverAppBar(
                                    pinned: true,
                                    centerTitle: true,
                                    title: getSwitcher(
                                        child: Text('${Strings.pages.housesPage.myFlats.l(context)}${isArchive ? ': ${Strings.pages.housesPage.archive.l(context)}' : ''}',
                                            style: TextStyle(fontSize: 20), key: Key(isArchive.toString()))),
                                    leading: !isArchive
                                        ? Container()
                                        : IconButton(
                                            icon: Icon(Icons.arrow_back),
                                            onPressed: () {
                                              toggleArchive();
                                              setState(() {});
                                            }),
                                    actions: <Widget>[
                                      isArchive
                                          ? Container()
                                          : IconButton(
                                              icon: Icon(Icons.archive),
                                              onPressed: () {
                                                toggleArchive();
                                                setState(() {});
                                              })
                                    ]),
                                Builder(builder: (context) {
                                  if (snapshot.hasData) {
                                    var houses = snapshot.data
                                        .where((element) =>
                                            ((element.houseInformation.isArchived.value == isArchive) && (isArchive == true)) ||
                                            ((element.houseInformation.isArchived.value != !isArchive) && (isArchive == false)))
                                        .toList().reversed.toList();
                                    // print(houses.runtimeType);
                                    if (houses.isNotEmpty) {
                                      return SliverList(
                                          delegate: SliverChildBuilderDelegate((context, index) {
                                            Builder action(int index) {
                                              return Builder(builder: (BuildContext buildContext) {
                                                return IconSlideAction(
                                                    caption: isArchive ? Strings.globalOnes.unarchive.l(context) : Strings.globalOnes.archive.l(context),
                                                    icon: isArchive ? Icons.unarchive : Icons.archive,
                                                    onTap: () {
                                                      Slidable.of(buildContext).dismiss();
                                                    });
                                              });
                                            }

                                            if (index == 0) {
                                              return Align(
                                                  alignment: Alignment.centerLeft, child: Text('', style: TextStyle(fontSize: 26, color: Colors.black)));
                                            }
                                            Future archiveItem(House house, bool archive) async {
                                              await switchArchive(house, archive);
                                              //return false;
                                              /*
                                              var result = await mindClass
                                                  .archiveHouse(houseId, !archive);


                                              //await Future.delayed(Duration(milliseconds: 600));
                                              return result;
                                              */
                                            }

                                            if(index == houses.length + 1) {
                                              return Padding(padding: EdgeInsets.all(15.0),);
                                            }
                                            var housesIndex = index - 1;
                                            return FutureBuilder(
                                              future: housePhotos(houses[housesIndex]),
                                              builder: (context, photoSnapshot) {
                                                var photos = photoSnapshot.data;
                                                return Slidable(
                                                  actionPane: SlidableDrawerActionPane(),
                                                  actionExtentRatio: 0.25,
                                                  key: Key(houses[housesIndex].key),
                                                  dismissal: SlidableDismissal(
                                                    onWillDismiss: (item) async {
                                                      return await archiveItem(houses[housesIndex], isArchived(houses[housesIndex]));
                                                    },
                                                    onDismissed: (actionType) {
                                                      setState(() {
                                                        houses.removeAt(housesIndex);
                                                      });
                                                    },
                                                    child: SlidableDrawerDismissal(),
                                                  ),
                                                  actions: <Widget>[action(index)],
                                                  secondaryActions: <Widget>[action(index)],
                                                  child: Padding(
                                                      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                                                      child: HouseCard(house: houses[housesIndex], photos: photos ?? [])),
                                                );
                                              }
                                            );},
                                              childCount: 2 + houses.length));
                                    } else {
                                      return SliverToBoxAdapter(
                                          child: Container(
                                              margin: EdgeInsets.all(20),
                                              child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                                                _buildZeroOffers(),
                                                Padding(
                                                    padding: const EdgeInsets.only(top: 16.0),
                                                    child: Text(
                                                        Strings.pages.housesPage.emptyScreenMessage.l(context),
                                                        textAlign: TextAlign.center,
                                                        style: TextStyle(color: Colors.black, fontSize: 14.0)))
                                              ])));
                                    }
                                  }
                                  return SliverToBoxAdapter(
                                      child: Stack(children: <Widget>[
                                    ((!snapshot.hasData && (snapshot.data != null))
                                        ? Align(alignment: Alignment.topCenter, child: SizedBox(height: 2, child: linearProgressIndicator))
                                        : Container()),
                                    Container(
                                        margin: EdgeInsets.all(20),
                                        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                                          _buildZeroOffers(),
                                          Padding(
                                              padding: const EdgeInsets.only(top: 16.0),
                                              child: Text(
                                                  Strings.pages.housesPage.emptyScreenMessage.l(context),
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 14.0,
                                                  )))
                                        ]))
                                  ]));
                                })
                              ]))))));
        });
  }

  Widget _buildZeroOffers() {
    return Padding(
        padding: const EdgeInsets.only(top: 16.0),
        child: Center(
            child: Container(width: 300, height: 300, child: SvgPicture.asset('assets/images/zero_offers.svg', semanticsLabel: 'Zero offers'))));
  }

  @override
  bool get wantKeepAlive => true;
}

/*
class _HousesPageHelper {
  StreamController manualUpdate = StreamController();
  Stream manualUpdateStream;
  Stream houses;
  List<House> lastHousesList;
  bool archive = false;

  initStreams() {
    manualUpdateStream = manualUpdate.stream.asBroadcastStream();
  }

  Stream<List<House>> houseLoader() async* {
    // here was code
  }
}

 */
