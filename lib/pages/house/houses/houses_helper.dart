import 'dart:async';
import 'package:micro_utils/data_helper.dart';
import 'package:db_partnerum/db_partnerum.dart';
import 'package:partnerum/utils/brain_module.dart';

@DataHelper()
class HousesHelper {
  StreamController manualUpdate = StreamController();
  Stream manualUpdateStream;
  Stream houses;
  List lastHousesList;
  bool archive = false;

  initStreams() {
    manualUpdateStream = manualUpdate.stream.asBroadcastStream();
  }

  Stream houseLoader() => root.userData[mindClass.user.uid].houses.stream
      .map((event) => event?.values?.toList() ?? []);

  bool isArchived(House house) {
    if (house.houseInformation.isArchived.value == true) {
      return true;
    }
    return false;
  }

  Future<bool> switchArchive(House house, bool archive) async {
    await house.houseInformation.isArchived.set(!archive);
    return true;
  }

  Future<Map> photosList(List<House> housesList) async {
    var imagesList = {};
    for (var house in housesList) {
      var housePhotos = [];
      var houseInformation = house?.houseInformation;
      for (var photo in houseInformation?.photos?.values) {
        housePhotos.add(await root.imagesContainer[photo.value].future);
      }
      imagesList[house.key] = housePhotos;
    }
    return imagesList;
  }

  Future<List> housePhotos(House house) async {
    var housePhotos = [];
    var houseInformation = house?.houseInformation;
    for (var photo in houseInformation?.photos?.values) {
      housePhotos.add(await root.imagesContainer[photo.value].future);
    }
    return housePhotos;
  }
}
