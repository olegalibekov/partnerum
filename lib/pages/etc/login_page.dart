import 'package:db_partnerum/db_partnerum.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:partnerum/head_module/gen_extensions/generated_str_extension.dart';
import 'package:partnerum/utils/brain_module.dart';
import 'package:url_launcher/url_launcher.dart';

const privacyUrl = 'https://partnerum.com/privacy/';

///TODO change with flutter_serve
/// text & icons
class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool _isLoading = false;
  bool isOtpSent = false;
  final _phone = MaskedTextController(mask: '+7 (000) 000-00-00');
  final _otp = TextEditingController();
  final _phoneForm = GlobalKey<FormState>();

  final _phoneFocusNode = FocusNode();
  final _otpFocusNode = FocusNode();

  var verificationId;

  String _message = 'Для входа в приложение введите свой номер телефона с кодом страны \n(Россия: +7)';

  final String _phoneNumber = 'assets/images/phone_number.svg';

  @override
  Widget build(BuildContext context) {
    return Scaffold(

        ///TODO look at global theme
        backgroundColor: Colors.white,
        body: SafeArea(
            child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: SingleChildScrollView(
                    child: Form(
                        key: _phoneForm,
                        child: Column(mainAxisAlignment: MainAxisAlignment.center, mainAxisSize: MainAxisSize.min, children: <Widget>[
                          Container(margin: EdgeInsets.only(bottom: 50.0), child: _buildLogoField()),
                          Container(
                              margin: EdgeInsets.symmetric(horizontal: 20),
                              child: Text(_message, style: TextStyle(fontSize: 14), textAlign: TextAlign.center)),
                          Container(margin: EdgeInsets.symmetric(horizontal: 20), child: _buildPhoneField(context)),
                          FlatButton(
                            child: Text(Strings.pages.loginPage.agreeWithRules.l(context),
                                style: TextStyle(color: Colors.blue, fontSize: 10), textAlign: TextAlign.center),
                            onPressed: () => _launchPrivacyUrl(),
                          ),
                          isOtpSent ? _getOtpBox() : SizedBox(height: 30),
                          _buildButton(context),
                          SizedBox(height: 40),
                          _isLoading
                              ? SizedBox(
                                  height: 100,
                                  width: 100,
                                  child: CircularProgressIndicator(
                                      valueColor: AlwaysStoppedAnimation<Color>(Theme.of(context).primaryColor), strokeWidth: 5.0),
                                )
                              : SizedBox()
                        ]))))));
  }

  _getOtpBox() {
    return Column(children: <Widget>[
      Container(margin: EdgeInsets.only(top: 10), child: Center(child: _buildResetPhone())),
      Text(Strings.pages.loginPage.or.l(context), textAlign: TextAlign.center),
      Container(margin: EdgeInsets.only(bottom: 20.0), child: Center(child: _buildResetOpt(context))),
      Container(margin: EdgeInsets.symmetric(horizontal: 20.0), child: _buildOptInputField(context))
    ]);
  }

  Widget _buildLogoField() {
    return Container(width: 200, height: 200, child: SvgPicture.asset(_phoneNumber, semanticsLabel: 'Phone Number'));
  }

  Widget _buildButton(BuildContext context) {
    return Container(
        height: 50,
        width: 200,
        child: FlatButton(
            onPressed: () => isOtpSent ? _verifyOtp(context) : _sendOtp(false, context),
            color: Theme.of(context).primaryColor,
            child: Text(isOtpSent ? Strings.pages.loginPage.enter.l(context) : Strings.pages.loginPage.getCode.l(context), style: TextStyle(fontSize: 16, color: Colors.white)),
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(1.0))));
  }

  Widget _buildPhoneField(BuildContext context) {
    return TextFormField(
        controller: _phone,
        focusNode: _phoneFocusNode,
        maxLines: 1,
        textInputAction: TextInputAction.done,
        keyboardType: TextInputType.phone,
        enabled: !isOtpSent,
        autofocus: false,
        onFieldSubmitted: (term) {
          SystemChannels.textInput.invokeMethod('TextInput.hide');
          isOtpSent ? _verifyOtp(context) : _sendOtp(false, context);
        },
        decoration: InputDecoration(labelText: Strings.pages.loginPage.phone.l(context), hintText: Strings.pages.loginPage.phoneNumber.l(context), prefixIcon: Icon(Icons.phone, color: Colors.black)),
        validator: (value) => value.isEmpty ? Strings.pages.loginPage.notEmptyPhoneNumber.l(context) : _checkValidation(value));
  }

  Widget _buildOptInputField(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 20),
        child: TextFormField(
            controller: _otp,
            focusNode: _otpFocusNode,
            maxLines: 1,
            maxLength: 6,
            textInputAction: TextInputAction.done,
            keyboardType: TextInputType.number,
            autofocus: false,
            onFieldSubmitted: (term) {
              SystemChannels.textInput.invokeMethod('TextInput.hide');
              isOtpSent ? _verifyOtp(context) : _sendOtp(false, context);
            },
            decoration: InputDecoration(labelText: Strings.pages.loginPage.code.l(context), hintText: Strings.pages.loginPage.askForEnterCode.l(context)),
            validator: (value) => value.isEmpty
                ? Strings.pages.loginPage.oneOffCodeEmptyIssue.l(context)
                : (value.length != 6 ? Strings.pages.loginPage.oneOffCodeIncompleteIssue.l(context) : null)));
  }

  Widget _buildResetOpt(BuildContext context) {
    return FlatButton(
        child: Text(Strings.pages.loginPage.sendCodeAgain.l(context), style: TextStyle(color: Theme.of(context).primaryColor)),
        onPressed: () {
          _sendOtp(true, context);
        });
  }

  Widget _buildResetPhone() {
    return FlatButton(
        child: Text(Strings.pages.loginPage.changeNumber.l(context)),
        onPressed: () {
          setState(() {
            _message = Strings.pages.loginPage.changeNumberMessage.l(context);
            _otp.clear();
            isOtpSent = false;
            _isLoading = false;
            FocusScope.of(context).requestFocus(_phoneFocusNode);
          });
        });
  }

  void _sendOtp(checkForm, BuildContext context) {
    if (!_isLoading && (checkForm || _phoneForm.currentState.validate())) {
      SystemChannels.textInput.invokeMethod('TextInput.hide');

      setState(() {
        _otp.clear();
      });
      setState(() {
        _isLoading = true;
      });

      final PhoneVerificationCompleted verificationCompleted = (AuthCredential phoneAuthCredential) async {
        print('launch');
        await mindClass.launchBrain(user: (await mindClass.auth.signInWithCredential(phoneAuthCredential)).user);

        setState(() {
          Navigator.pushNamedAndRemoveUntil(context, 'home', (Route<dynamic> route) => false);
          _message = 'Sign in succeed!';
          debugPrint('Sign up succedded');
          _isLoading = false;
        });
      };

      final PhoneVerificationFailed verificationFailed = (FirebaseAuthException authException) {
        setState(() {
          _message = '${authException.message}';
          _isLoading = false;
        });
      };

      final PhoneCodeSent codeSent = (String verificationId, [int forceResendingToken]) async {
        this.verificationId = verificationId;
        setState(() {
          _message = Strings.pages.loginPage.oneOffCodeSentToNumber.l(context);
          isOtpSent = true;
          _isLoading = false;
          FocusScope.of(context).requestFocus(_otpFocusNode);
        });
      };

      final PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout = (String verificationId) {
        this.verificationId = verificationId;
        _isLoading = false;
      };

      mindClass.auth.verifyPhoneNumber(
          phoneNumber: _phone.text,
          timeout: Duration(seconds: 60),
          verificationCompleted: verificationCompleted,
          verificationFailed: verificationFailed,
          codeSent: codeSent,
          codeAutoRetrievalTimeout: codeAutoRetrievalTimeout);
    }
  }

  void _verifyOtp(BuildContext context) async {
    print('_verifyOtp');
    if (!_isLoading && _phoneForm.currentState.validate()) {
      setState(() {
        _isLoading = true;
      });

      SystemChannels.textInput.invokeMethod('TextInput.hide');

      final credential = PhoneAuthProvider.credential(
        verificationId: verificationId,
        smsCode: _otp.text,
      );
      await mindClass.launchBrain(user: (await mindClass.auth.signInWithCredential(credential)).user);

      await root.userData[mindClass.user.uid].userPhone.set(mindClass.user.phoneNumber);

      Navigator.pushNamedAndRemoveUntil(context, 'home', (Route<dynamic> route) => false);
    }
  }

  String _checkValidation(String value) {
    return value.length < 10
        ? Strings.pages.loginPage.askForCorrectPhoneNumber.l(context)
        : value[0] == '\+'
            ? null
            : Strings.pages.loginPage.askForCorrectPhoneCode.l(context);
  }

  void _launchPrivacyUrl() async {
    if (await canLaunch(privacyUrl)) {
      await launch(privacyUrl);
    } else {
      throw '${Strings.pages.loginPage.website.l(context)} - $privacyUrl - ${Strings.pages.loginPage.notFound.l(context)}';
    }
  }
}
