import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:db_partnerum/db_partnerum.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:partnerum/head_module/gen_extensions/generated_str_extension.dart';
import 'package:partnerum/models/rooms_model.dart';
import 'package:partnerum/utils/brain_module.dart';
import 'package:partnerum/utils/theme_constants.dart';
import 'package:partnerum/widgets/partnerum_icons.dart';
import 'chats_helper.dart';
import '../messages/messages_page.dart';

class ChatsPage extends StatefulWidget {
  /*
  final User user = mindClass.user;

   */
  ChatsPage();

  @override
  _ChatsPageState createState() => _ChatsPageState();
}

class _ChatsPageState extends State<ChatsPage> with ChatsHelper {
  _ChatsPageState();

  @override
  void initState() {
    mindClass.eventSteam.outStream.listen((event) {
      if (event == 'update_messages') {
        setState(() {});
      }
    });
    super.initState();
  }

  final ValueNotifier<bool> _loaderVisibility = ValueNotifier<bool>(true);

  @override
  Widget build(BuildContext context) {
    var isLoading = false;

    var rooms = [];
    return Scaffold(
        backgroundColor: Colors.white,
        body: Stack(children: <Widget>[
          Builder(
            builder: (BuildContext context) {
              return StreamBuilder<UserChats>(
                  // initialData: [],
                  stream: chats(),
                  builder: (context, chatsSnapshot) {
                    // if (chatsSnapshot.hasData) {
                    //   print('yep, there is some data: ${chatsSnapshot.data.first}');
                    // }

                    getKey() {
                      if (!chatsSnapshot.hasData) return Key('error_messages');
                      else{
                        var utuCompanions = chatsSnapshot?.data?.utu?.values?.map((e) {
                          utuList.add(e.key);
                          return e;
                        })?.toList() ?? [];

                        var otrCompanions = chatsSnapshot?.data?.otr?.values?.map((e) {
                          otrList.add(e.key);
                          return e;
                        })?.toList() ?? [];

                        if (utuCompanions.isEmpty && otrCompanions.isEmpty) {
                          return Key('zero_messages');
                        } else {
                          rooms = [];
                          rooms.addAll(utuCompanions);
                          rooms.addAll(otrCompanions);
                          return Key('${rooms.length}_messages');
                        }
                      }

                    }

                    if (chatsSnapshot.hasData && !chatsSnapshot.hasError) {}
                    return AnimatedSwitcher(
                        duration: kThemeAnimationDuration,
                        child: ScrollConfiguration(
                            behavior: noHighlightScrollBehavior(context),
                            child: RefreshIndicator(
                                displacement: 80,
                                key: getKey(),
                                onRefresh: () {
                                  setState(() {});
                                  return Future.delayed(Duration(milliseconds: 300));
                                },
                                child: CustomScrollView(slivers: [
                                  SliverAppBar(
                                      pinned: true,
                                      title: Text(Strings.pages.chatsPage.titleDialogs.l(context), style: TextStyle(fontSize: 20)),
                                      automaticallyImplyLeading: false,
                                      centerTitle: true,
                                      iconTheme: IconThemeData(color: Colors.black),
                                      textTheme: TextTheme(title: TextStyle(color: Colors.black, fontSize: 20.0)),
                                      bottom: PreferredSize(
                                          preferredSize: Size.fromHeight(2),
                                          child: ValueListenableBuilder(
                                              valueListenable: _loaderVisibility,
                                              builder: (BuildContext context, bool value, Widget child) => AnimatedOpacity(
                                                  duration: Duration(milliseconds: 300), opacity: value ? 1 : 0, child: linearProgressIndicator))),
                                      backgroundColor: Colors.white),
                                  Builder(builder: (BuildContext context) {
                                    if ((chatsSnapshot.data == null)) {
                                      return noChatsFound(context);
                                      return SliverToBoxAdapter(child: Container());
                                      // return SliverToBoxAdapter(child: linearProgressIndicator);
                                      //   loadingChats(context);
                                    }
                                    // if (!chatsSnapshot.hasData) {
                                    //   Timer(kThemeChangeDuration, () => _loaderVisibility.value = false);
                                    //   return noChatsFound(context);
                                    // }

                                    if (!chatsSnapshot.hasData) {
                                      Timer(kThemeChangeDuration, () => _loaderVisibility.value = false);
                                      return noChatsFound(context);
                                    } else {
                                      // List rooms = chatsSnapshot.data;
                                      if (rooms.isNotEmpty) {
                                        var companionsId = rooms.map((e) => e.companionId.value).toList();
                                        var interlocutorsInfo = rooms.map((e) => (e).companionInfo).toList();
                                        return Builder(
                                          builder: (BuildContext context) {
                                            if (rooms.isNotEmpty) {
                                              Timer(kThemeChangeDuration, () => _loaderVisibility.value = false);
                                              return SliverList(
                                                  delegate: SliverChildBuilderDelegate(
                                                (context, index) => Card(
                                                    clipBehavior: Clip.antiAlias,
                                                    margin: EdgeInsets.all(16).copyWith(bottom: 0),
                                                    child: ListTile(

                                                        // contentPadding: EdgeInsets.all(4),
                                                        title: Row(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                                                          FutureBuilder(
                                                            future: usernameLoaderFuture(rooms[index].companionId.value),
                                                            builder: (context, usernameSnapshot) {
                                                                var username = usernameSnapshot.hasData
                                                                    ? usernameSnapshot.data.value
                                                                    : interlocutorsInfo[index]?.username?.value;
                                                                return Flexible(
                                                                  child: Text(
                                                                      '${(username) ?? Strings.globalOnes.anonymousUser.l(context)}',
                                                                      overflow: TextOverflow.ellipsis));
                                                            }
                                                          ),
                                                        ]),
                                                        trailing: FutureBuilder(
                                                            future: timestampFuture(rooms[index].key),
                                                            builder: (context, timestampSnapshot) {
                                                              DbDuration timestamp = timestampSnapshot.data;
                                                              if (timestampSnapshot.hasData && !timestampSnapshot.hasError && timestamp != null) {
                                                                return timestamp.value.inMilliseconds != 0
                                                                    ? Text(
                                                                        DateFormat('yyyy.MM.dd HH:mm').format(
                                                                            DateTime.fromMillisecondsSinceEpoch(timestamp.value.inMilliseconds ?? 0)),
                                                                        style: Theme.of(context).textTheme.caption)
                                                                    : noDatePlug(context);
                                                              }
                                                              return noDatePlug(context);
                                                            }),
                                                        subtitle: Text('${Strings.pages.chatsPage.topic.l(context)} #${rooms[index].hashCode}'),
                                                        leading: FutureBuilder<DbObj>(
                                                          future: loadImageFuture((rooms[index])?.companionId?.value),
                                                          builder: (BuildContext context, AsyncSnapshot<dynamic> avatarSnapshot) {
                                                            return (((avatarSnapshot?.data)?.url != null) && (avatarSnapshot.hasData))
                                                                ? CachedNetworkImage(
                                                                    imageUrl: avatarSnapshot.data.url,
                                                                    httpHeaders: avatarSnapshot.data.syncHttpHeaders,
                                                                    imageBuilder: (context, imageProvider) => decoratedImage(imageProvider),
                                                                    placeholder: (context, url) => Theme.of(context).platform == TargetPlatform.iOS
                                                                        ? CupertinoActivityIndicator()
                                                                        : CircularProgressIndicator(),
                                                                    errorWidget: (context, url, error) => noImageIcon(context))
                                                                : noImageIcon(context);
                                                          },
                                                        ),
                                                        onTap: () async {
                                                          enterInChat(rooms[index].key);
                                                          await Navigator.push(
                                                              context,
                                                              CupertinoPageRoute(
                                                                  builder: (context) => Messages(
                                                                        messageInstanceId: rooms[index].key,
                                                                        finalButton: true,
                                                                      )));
                                                        })),
                                                childCount: rooms.length,
                                              ));
                                            }
                                            return SliverToBoxAdapter(child: Container());
                                          },
                                        );
                                        return FutureBuilder<List<UserInformation>>(
                                          future: interlocutorNames(companionsId),
                                          builder: (BuildContext context, AsyncSnapshot interlocutorsNamesSnapshot) {
                                            if (interlocutorsNamesSnapshot.data != null && interlocutorsNamesSnapshot.hasData) {
                                              var interlocutorsInfo = interlocutorsNamesSnapshot?.data;
                                              return Builder(
                                                builder: (BuildContext context) {
                                                  if (rooms.isNotEmpty) {
                                                    Timer(kThemeChangeDuration, () => _loaderVisibility.value = false);
                                                    return SliverList(
                                                        delegate: SliverChildBuilderDelegate(
                                                      (context, index) => Card(
                                                          clipBehavior: Clip.antiAlias,
                                                          margin: EdgeInsets.all(16).copyWith(bottom: 0),
                                                          child: ListTile(

                                                              // contentPadding: EdgeInsets.all(4),
                                                              title: Row(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                                                                Flexible(
                                                                    child: Text(
                                                                        '${(interlocutorsInfo[index]?.username?.value) ?? Strings.globalOnes.anonymousUser.l(context)}',
                                                                        overflow: TextOverflow.ellipsis)),
                                                              ]),
                                                              trailing: FutureBuilder(
                                                                  future: root.chatTopic.userToUser[rooms[index].key].timestamp.future,
                                                                  builder: (context, timestampSnapshot) {
                                                                    DbDuration timestamp = timestampSnapshot.data;
                                                                    if (timestampSnapshot.hasData &&
                                                                        !timestampSnapshot.hasError &&
                                                                        timestamp != null) {
                                                                      return timestamp.value.inMilliseconds != 0
                                                                          ? Text(
                                                                              DateFormat('yyyy.MM.dd HH:mm').format(
                                                                                  DateTime.fromMillisecondsSinceEpoch(
                                                                                      timestamp.value.inMilliseconds ?? 0)),
                                                                              style: Theme.of(context).textTheme.caption)
                                                                          : noDatePlug(context);
                                                                    }
                                                                    return noDatePlug(context);
                                                                  }),
                                                              subtitle: Text('${Strings.pages.chatsPage.topic.l(context)} #${rooms[index].hashCode}'),
                                                              leading: (interlocutorsInfo[index]?.avatar?.url != null)
                                                                  ? CachedNetworkImage(
                                                                      imageUrl: interlocutorsInfo[index].avatar.url,
                                                                      httpHeaders: interlocutorsInfo[index].avatar.syncHttpHeaders,
                                                                      imageBuilder: (context, imageProvider) => decoratedImage(imageProvider),
                                                                      placeholder: (context, url) => Theme.of(context).platform == TargetPlatform.iOS
                                                                          ? CupertinoActivityIndicator()
                                                                          : CircularProgressIndicator(),
                                                                      errorWidget: (context, url, error) => noImageIcon(context))
                                                                  : noImageIcon(context),
                                                              onTap: () async {
                                                                enterInChat(rooms[index].key);
                                                                await Navigator.push(
                                                                    context,
                                                                    CupertinoPageRoute(
                                                                        builder: (context) => Messages(
                                                                              messageInstanceId: rooms[index].key,
                                                                              finalButton: true,
                                                                            )));
                                                              })),
                                                      childCount: rooms.length,
                                                    ));
                                                  }
                                                  return SliverToBoxAdapter(child: Container());
                                                },
                                              );

                                              // print('Interlocutor name: ${interlocutorsNames}');
                                              return StreamBuilder<UnreadChatsMap>(
                                                  stream: unreadChatsStream(),
                                                  builder: (context, unreadChats) {
                                                    if (rooms.isNotEmpty) {
                                                      Timer(kThemeChangeDuration, () => _loaderVisibility.value = false);
                                                      return SliverList(
                                                          delegate: SliverChildBuilderDelegate(
                                                        (context, index) => Card(
                                                            clipBehavior: Clip.antiAlias,
                                                            margin: EdgeInsets.all(16).copyWith(bottom: 0),
                                                            child: ListTile(

                                                                // contentPadding: EdgeInsets.all(4),
                                                                title: Row(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                                                                  Flexible(
                                                                      child: Text(
                                                                          '${(interlocutorsInfo[index]?.username?.value) ?? Strings.globalOnes.anonymousUser.l(context)}',
                                                                          overflow: TextOverflow.ellipsis)),
                                                                  // if (!unreadChats.hasError &&
                                                                  //     unreadChats.hasData &&
                                                                  //     unreadChats?.data[rooms[index]?.status?.value]?.value != null)
                                                                  //   ClipOval(child: Container(width: 8, height: 8, color: Colors.red)),
                                                                  // SizedBox(width: 8),
                                                                ]),
                                                                trailing: FutureBuilder(
                                                                    future: root.chatTopic.userToUser[rooms[index].key].timestamp.future,
                                                                    builder: (context, timestampSnapshot) {
                                                                      DbDuration timestamp = timestampSnapshot.data;
                                                                      if (timestampSnapshot.hasData &&
                                                                          !timestampSnapshot.hasError &&
                                                                          timestamp != null) {
                                                                        return timestamp.value.inMilliseconds != 0
                                                                            ? Text(
                                                                                DateFormat('yyyy.MM.dd HH:mm').format(
                                                                                    DateTime.fromMillisecondsSinceEpoch(
                                                                                        timestamp.value.inMilliseconds ?? 0)),
                                                                                style: Theme.of(context).textTheme.caption)
                                                                            : noDatePlug(context);
                                                                      }
                                                                      return noDatePlug(context);
                                                                    }),
                                                                subtitle: Text('${Strings.pages.chatsPage.topic.l(context)} #${rooms[index].hashCode}'
                                                                    /*
                                                                              '${Strings.pages.chatsPage.topic.l(context)} #${rooms[index]
                                                                                  .messageInstanceId
                                                                                  .hashCode}'

                                                                           */
                                                                    ),
                                                                // leading:
                                                                // CircleAvatar(
                                                                //   backgroundColor: Colors.orangeAccent,
                                                                //   /*
                                                                //           backgroundImage:
                                                                //           CachedNetworkImageProvider(
                                                                //               rooms[index]
                                                                //                   .possibleUser
                                                                //                   .userInfo
                                                                //                   .avatar
                                                                //           )
                                                                //
                                                                //            */
                                                                // ),
                                                                leading: (interlocutorsInfo[index]?.avatar?.url != null)
                                                                    ? CachedNetworkImage(
                                                                        imageUrl: interlocutorsInfo[index].avatar.url,
                                                                        httpHeaders: interlocutorsInfo[index].avatar.syncHttpHeaders,
                                                                        imageBuilder: (context, imageProvider) => decoratedImage(imageProvider),
                                                                        placeholder: (context, url) =>
                                                                            Theme.of(context).platform == TargetPlatform.iOS
                                                                                ? CupertinoActivityIndicator()
                                                                                : CircularProgressIndicator(),
                                                                        errorWidget: (context, url, error) => noImageIcon(context))
                                                                    : noImageIcon(context),
                                                                onTap: () async {
/*
                                                                            await mindClass
                                                                                .readMessageNotification(
                                                                                rooms[index]
                                                                                    .key);
                                                                            */
                                                                  enterInChat(rooms[index].key);
                                                                  await Navigator.push(
                                                                      context,
                                                                      CupertinoPageRoute(
                                                                          builder: (context) => Messages(
                                                                                messageInstanceId: rooms[index].key,
                                                                                finalButton: true,
                                                                                /*
                                                                      chatRoom: rooms[
                                                                          index]

                                                                       */
                                                                              )));
                                                                })),
                                                        childCount: rooms.length,
                                                      ));
                                                    }
                                                    return SliverToBoxAdapter(child: Container());

                                                    /*
                                                return FutureBuilder(
                                                  initialData: [],
                                                  future: companionNames(rooms),
                                                  builder: (context, companionsSnapshot) {
                                                    if (companionsSnapshot.hasData && !companionsSnapshot.hasError) {
                                                      List companions = companionsSnapshot.data;

                                                      // print('Unread chats data: ${unreadChats.data.values}');
                                                      // print('Companions: ${companions.first}');
                                                      if (companions.isNotEmpty) {
                                                        Timer(kThemeChangeDuration, () => _loaderVisibility.value = false);
                                                        return SliverList(
                                                            delegate: SliverChildBuilderDelegate(
                                                          (context, index) => Card(
                                                              clipBehavior: Clip.antiAlias,
                                                              margin: EdgeInsets.all(16).copyWith(bottom: 0),
                                                              child: ListTile(
                                                                  title: Row(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                                                                    Text('${rooms[index].companionName.value}'),
                                                                    if (true
                                                                    /*
                                                                                !rooms[index]
                                                                                    .read

                                                                                 */
                                                                    )
                                                                      // -MYkRYtuYTusGGz4UC3T
                                                                      if (!unreadChats.hasError && unreadChats.data[rooms[index]].value != null)
                                                                        ClipOval(child: Container(width: 8, height: 8, color: Colors.red)),
                                                                    SizedBox(width: 8),
                                                                    FutureBuilder(
                                                                        future: root.chatTopic.userToUser[rooms[index]].timestamp.future,
                                                                        builder: (context, timestampSnapshot) {
                                                                          DbDuration timestamp = timestampSnapshot.data;
                                                                          if (timestampSnapshot.hasData &&
                                                                              !timestampSnapshot.hasError &&
                                                                              timestamp != null) {
                                                                            return Flexible(
                                                                                child: Text(
                                                                                    timestamp.value.inMilliseconds != 0
                                                                                        ? DateFormat('yyyy.MM.dd HH:mm').format(
                                                                                            DateTime.fromMillisecondsSinceEpoch(
                                                                                                timestamp.value.inMilliseconds ?? 0
                                                                                                /*
                                                                                                    rooms[index]
                                                                                                        .timestamp

                                                                                                     */
                                                                                                ))
                                                                                        : 'null',
                                                                                    style: Theme.of(context).textTheme.caption));
                                                                          }
                                                                          return Container();
                                                                        })
                                                                  ]),
                                                                  subtitle: Text('${Strings.pages.chatsPage.topic.l(context)} #${rooms[index].hashCode}'
                                                                      /*
                                                                              '${Strings.pages.chatsPage.topic.l(context)} #${rooms[index]
                                                                                  .messageInstanceId
                                                                                  .hashCode}'

                                                                           */
                                                                      ),
                                                                  leading: CircleAvatar(
                                                                    backgroundColor: Colors.orangeAccent,
                                                                    /*
                                                                              backgroundImage:
                                                                              CachedNetworkImageProvider(
                                                                                  rooms[index]
                                                                                      .possibleUser
                                                                                      .userInfo
                                                                                      .avatar
                                                                              )

                                                                               */
                                                                  ),
                                                                  onTap: () async {
/*
                                                                            await mindClass
                                                                                .readMessageNotification(
                                                                                rooms[index]
                                                                                    .key);
                                                                            */
                                                                    enterInChat(rooms[index]);
                                                                    await Navigator.push(
                                                                        context,
                                                                        CupertinoPageRoute(
                                                                            builder: (context) => Messages(
                                                                                  messageInstanceId: rooms[index],
                                                                                  finalButton: true,
                                                                                  /*
                                                                      chatRoom: rooms[
                                                                          index]

                                                                       */
                                                                                )));
                                                                  })),
                                                          childCount: companions.length,
                                                        ));
                                                      }
                                                      return SliverToBoxAdapter(child: Container());
                                                      // : SliverToBoxAdapter(child: linearProgressIndicator);
                                                      // loadingChats(context);
                                                    }
                                                    return SliverToBoxAdapter(child: Container());
                                                  },
                                                );
                                                */
                                                  });
                                            }
                                            return SliverToBoxAdapter(child: SizedBox.shrink());
                                            // return SizedBox.shrink();
                                          },
                                        );
                                      } else {
                                        Timer(kThemeChangeDuration, () => _loaderVisibility.value = false);
                                        return noChatsFound(context);
                                      }
                                    }
                                  })
                                ]))));
                  });
            },
          ),
          /*
          StreamBuilder(
              stream: mindClass.eventSteam.outStream.asBroadcastStream(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                return FutureBuilder(
                    // initialData: [],
                    future: chats(),
                    builder: (context, chatsSnapshot) {
                      // if (chatsSnapshot.hasData) {
                      //   print('yep, there is some data: ${chatsSnapshot.data.first}');
                      // }
                      getKey() {
                        if (!chatsSnapshot.hasData) return Key('error_messages');

                        if (chatsSnapshot.data.isEmpty) {
                          return Key('zero_messages');
                        } else {
                          var rooms = chatsSnapshot.data;
                          return Key('${rooms.length}_messages');
                        }
                      }

                      if (chatsSnapshot.hasData && !chatsSnapshot.hasError) {
                        print('Chats: ${chatsSnapshot.data}');
                      }
                      return AnimatedSwitcher(
                          duration: kThemeAnimationDuration,
                          child: ScrollConfiguration(
                              behavior: noHighlightScrollBehavior(context),
                              child: RefreshIndicator(
                                  displacement: 80,
                                  key: getKey(),
                                  onRefresh: () {
                                    setState(() {});
                                    return Future.delayed(Duration(milliseconds: 300));
                                  },
                                  child: CustomScrollView(slivers: [
                                    SliverAppBar(
                                        pinned: true,
                                        title: Text(Strings.pages.chatsPage.titleDialogs.l(context), style: TextStyle(fontSize: 20)),
                                        automaticallyImplyLeading: false,
                                        centerTitle: true,
                                        iconTheme: IconThemeData(color: Colors.black),
                                        textTheme: TextTheme(title: TextStyle(color: Colors.black, fontSize: 20.0)),
                                        bottom: PreferredSize(
                                            preferredSize: Size.fromHeight(2),
                                            child: ValueListenableBuilder(
                                                valueListenable: _loaderVisibility,
                                                builder: (BuildContext context, bool value, Widget child) => AnimatedOpacity(
                                                    duration: Duration(milliseconds: 300), opacity: value ? 1 : 0, child: linearProgressIndicator))),
                                        backgroundColor: Colors.white),
                                    Builder(builder: (BuildContext context) {
                                      if (chatsSnapshot.data == null) {
                                        return noChatsFound(context);
                                        return SliverToBoxAdapter(child: Container());
                                        // return SliverToBoxAdapter(child: linearProgressIndicator);
                                        //   loadingChats(context);
                                      }
                                      // if (!chatsSnapshot.hasData) {
                                      //   Timer(kThemeChangeDuration, () => _loaderVisibility.value = false);
                                      //   return noChatsFound(context);
                                      // }

                                      if (!chatsSnapshot.hasData) {
                                        Timer(kThemeChangeDuration, () => _loaderVisibility.value = false);
                                        return noChatsFound(context);
                                      } else {
                                        List rooms = chatsSnapshot.data;

                                        var companionsId = rooms.map((e) => e.companionId.value).toList();

                                        return FutureBuilder<List<UserInformation>>(
                                          future: interlocutorNames(companionsId),
                                          builder: (BuildContext context, AsyncSnapshot interlocutorsNamesSnapshot) {
                                            if (interlocutorsNamesSnapshot.data != null && interlocutorsNamesSnapshot.hasData) {
                                              var interlocutorsInfo = interlocutorsNamesSnapshot?.data;

                                              // print('Interlocutor name: ${interlocutorsNames}');
                                              return StreamBuilder<UnreadChatsMap>(
                                                  stream: unreadChatsStream(),
                                                  builder: (context, unreadChats) {
                                                    if (rooms.isNotEmpty) {
                                                      Timer(kThemeChangeDuration, () => _loaderVisibility.value = false);
                                                      return SliverList(
                                                          delegate: SliverChildBuilderDelegate(
                                                        (context, index) => Card(
                                                            clipBehavior: Clip.antiAlias,
                                                            margin: EdgeInsets.all(16).copyWith(bottom: 0),
                                                            child: ListTile(
                                                                title: Row(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                                                                  Text(
                                                                      '${(interlocutorsInfo[index]?.username?.value) ?? Strings.globalOnes.anonymousUser.l(context)}'),
                                                                  // if (true
                                                                  /*
                                                                                !rooms[index]
                                                                                    .read

                                                                                 */
                                                                  // )
                                                                  // -MYkRYtuYTusGGz4UC3T
                                                                  if (!unreadChats.hasError &&
                                                                      unreadChats.hasData &&
                                                                      unreadChats?.data[rooms[index]?.status?.value]?.value != null)
                                                                    ClipOval(child: Container(width: 8, height: 8, color: Colors.red)),
                                                                  SizedBox(width: 8),
                                                                  FutureBuilder(
                                                                      future: root.chatTopic.userToUser[rooms[index].key].timestamp.future,
                                                                      builder: (context, timestampSnapshot) {
                                                                        DbDuration timestamp = timestampSnapshot.data;
                                                                        if (timestampSnapshot.hasData &&
                                                                            !timestampSnapshot.hasError &&
                                                                            timestamp != null) {
                                                                          return Flexible(
                                                                              child: Text(
                                                                                  timestamp.value.inMilliseconds != 0
                                                                                      ? DateFormat('yyyy.MM.dd HH:mm').format(
                                                                                          DateTime.fromMillisecondsSinceEpoch(
                                                                                              timestamp.value.inMilliseconds ?? 0
                                                                                              /*
                                                                                                    rooms[index]
                                                                                                        .timestamp

                                                                                                     */
                                                                                              ))
                                                                                      : '',
                                                                                  style: Theme.of(context).textTheme.caption));
                                                                        }
                                                                        return Container();
                                                                      })
                                                                ]),
                                                                subtitle: Text('${Strings.pages.chatsPage.topic.l(context)} #${rooms[index].hashCode}'
                                                                    /*
                                                                              '${Strings.pages.chatsPage.topic.l(context)} #${rooms[index]
                                                                                  .messageInstanceId
                                                                                  .hashCode}'

                                                                           */
                                                                    ),
                                                                // leading:
                                                                // CircleAvatar(
                                                                //   backgroundColor: Colors.orangeAccent,
                                                                //   /*
                                                                //           backgroundImage:
                                                                //           CachedNetworkImageProvider(
                                                                //               rooms[index]
                                                                //                   .possibleUser
                                                                //                   .userInfo
                                                                //                   .avatar
                                                                //           )
                                                                //
                                                                //            */
                                                                // ),
                                                                leading: (interlocutorsInfo[index]?.avatar?.url != null)
                                                                    ? CachedNetworkImage(
                                                                        imageUrl: interlocutorsInfo[index].avatar.url,
                                                                        httpHeaders: interlocutorsInfo[index].avatar.syncHttpHeaders,
                                                                        imageBuilder: (context, imageProvider) => decoratedImage(imageProvider),
                                                                        placeholder: (context, url) =>
                                                                            Theme.of(context).platform == TargetPlatform.iOS
                                                                                ? CupertinoActivityIndicator()
                                                                                : CircularProgressIndicator(),
                                                                        errorWidget: (context, url, error) => Container(
                                                                            width: 40,
                                                                            height: 40,
                                                                            decoration: BoxDecoration(
                                                                                border: Border.all(color: Theme.of(context).primaryColor),
                                                                                shape: BoxShape.circle),
                                                                            child: Center(
                                                                                child:
                                                                                    Icon(Icons.info_outline, color: Theme.of(context).primaryColor))))
                                                                    : CircleAvatar(
                                                                        backgroundColor: Colors.orangeAccent,
                                                                      ),
                                                                onTap: () async {
/*
                                                                            await mindClass
                                                                                .readMessageNotification(
                                                                                rooms[index]
                                                                                    .key);
                                                                            */
                                                                  enterInChat(rooms[index].key);
                                                                  await Navigator.push(
                                                                      context,
                                                                      CupertinoPageRoute(
                                                                          builder: (context) => Messages(
                                                                                messageInstanceId: rooms[index].key,
                                                                                finalButton: true,
                                                                                /*
                                                                      chatRoom: rooms[
                                                                          index]

                                                                       */
                                                                              )));
                                                                })),
                                                        childCount: rooms.length,
                                                      ));
                                                    }
                                                    return SliverToBoxAdapter(child: Container());

                                                    /*
                                                return FutureBuilder(
                                                  initialData: [],
                                                  future: companionNames(rooms),
                                                  builder: (context, companionsSnapshot) {
                                                    if (companionsSnapshot.hasData && !companionsSnapshot.hasError) {
                                                      List companions = companionsSnapshot.data;

                                                      // print('Unread chats data: ${unreadChats.data.values}');
                                                      // print('Companions: ${companions.first}');
                                                      if (companions.isNotEmpty) {
                                                        Timer(kThemeChangeDuration, () => _loaderVisibility.value = false);
                                                        return SliverList(
                                                            delegate: SliverChildBuilderDelegate(
                                                          (context, index) => Card(
                                                              clipBehavior: Clip.antiAlias,
                                                              margin: EdgeInsets.all(16).copyWith(bottom: 0),
                                                              child: ListTile(
                                                                  title: Row(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                                                                    Text('${rooms[index].companionName.value}'),
                                                                    if (true
                                                                    /*
                                                                                !rooms[index]
                                                                                    .read

                                                                                 */
                                                                    )
                                                                      // -MYkRYtuYTusGGz4UC3T
                                                                      if (!unreadChats.hasError && unreadChats.data[rooms[index]].value != null)
                                                                        ClipOval(child: Container(width: 8, height: 8, color: Colors.red)),
                                                                    SizedBox(width: 8),
                                                                    FutureBuilder(
                                                                        future: root.chatTopic.userToUser[rooms[index]].timestamp.future,
                                                                        builder: (context, timestampSnapshot) {
                                                                          DbDuration timestamp = timestampSnapshot.data;
                                                                          if (timestampSnapshot.hasData &&
                                                                              !timestampSnapshot.hasError &&
                                                                              timestamp != null) {
                                                                            return Flexible(
                                                                                child: Text(
                                                                                    timestamp.value.inMilliseconds != 0
                                                                                        ? DateFormat('yyyy.MM.dd HH:mm').format(
                                                                                            DateTime.fromMillisecondsSinceEpoch(
                                                                                                timestamp.value.inMilliseconds ?? 0
                                                                                                /*
                                                                                                    rooms[index]
                                                                                                        .timestamp

                                                                                                     */
                                                                                                ))
                                                                                        : 'null',
                                                                                    style: Theme.of(context).textTheme.caption));
                                                                          }
                                                                          return Container();
                                                                        })
                                                                  ]),
                                                                  subtitle: Text('${Strings.pages.chatsPage.topic.l(context)} #${rooms[index].hashCode}'
                                                                      /*
                                                                              '${Strings.pages.chatsPage.topic.l(context)} #${rooms[index]
                                                                                  .messageInstanceId
                                                                                  .hashCode}'

                                                                           */
                                                                      ),
                                                                  leading: CircleAvatar(
                                                                    backgroundColor: Colors.orangeAccent,
                                                                    /*
                                                                              backgroundImage:
                                                                              CachedNetworkImageProvider(
                                                                                  rooms[index]
                                                                                      .possibleUser
                                                                                      .userInfo
                                                                                      .avatar
                                                                              )

                                                                               */
                                                                  ),
                                                                  onTap: () async {
/*
                                                                            await mindClass
                                                                                .readMessageNotification(
                                                                                rooms[index]
                                                                                    .key);
                                                                            */
                                                                    enterInChat(rooms[index]);
                                                                    await Navigator.push(
                                                                        context,
                                                                        CupertinoPageRoute(
                                                                            builder: (context) => Messages(
                                                                                  messageInstanceId: rooms[index],
                                                                                  finalButton: true,
                                                                                  /*
                                                                      chatRoom: rooms[
                                                                          index]

                                                                       */
                                                                                )));
                                                                  })),
                                                          childCount: companions.length,
                                                        ));
                                                      }
                                                      return SliverToBoxAdapter(child: Container());
                                                      // : SliverToBoxAdapter(child: linearProgressIndicator);
                                                      // loadingChats(context);
                                                    }
                                                    return SliverToBoxAdapter(child: Container());
                                                  },
                                                );
                                                */
                                                  });
                                            }
                                            return SliverToBoxAdapter(child: SizedBox.shrink());
                                            // return SizedBox.shrink();
                                          },
                                        );
                                      }
                                    })
                                  ]))));
                    });
              }),
          */
          // Loading
          Positioned(
              child: isLoading
                  ? Container(
                      color: Colors.white.withOpacity(0.8),
                      child: Center(
                        child: Theme.of(context).platform == TargetPlatform.iOS ? CupertinoActivityIndicator() : CircularProgressIndicator(),
                      ))
                  : Container())
        ]));
  }

  void choiceAction(String choice) {
    if (choice == Constants.ToBlock.l(context)) {
      _toBlock();
    }
  }

  Future<Future<Null>> _toBlock() async {
    return showDialog<Null>(
        context: context,
        barrierDismissible: false, // user must tap button!

        builder: (BuildContext context) {
          return SimpleDialog(title: Text(Strings.pages.chatsPage.block.l(context)), children: <Widget>[
            ListTile(
                leading: Icon(Icons.info, color: Colors.black),
                title: Text(Strings.pages.chatsPage.attention.l(context)),
                subtitle: Text(Strings.pages.chatsPage.blockingMessage.l(context))),
            Row(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop();
//                    _addBlockData();
                    _onPressBlock();
                  },
                  child: Text(Strings.pages.chatsPage.yes.l(context), style: TextStyle(color: Colors.redAccent))),
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text(Strings.pages.chatsPage.no.l(context), style: TextStyle(color: Colors.black)))
            ])
          ]);
        });
  }

  void _onPressBlock() {
    Scaffold.of(context).showSnackBar(SnackBar(
        backgroundColor: Colors.black, content: Text(Strings.pages.chatsPage.reviewMessage.l(context), style: TextStyle(color: Colors.yellow))));
  }
}

class Constants {
  static String ToBlock = Strings.pages.chatsPage.topic;

  static List<String> choices = <String>[ToBlock];
}
