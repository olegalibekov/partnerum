import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:micro_utils/data_helper.dart';
import 'package:db_partnerum/db_partnerum.dart';
import 'package:partnerum/head_module/gen_extensions/generated_str_extension.dart';
import 'package:partnerum/pages/request/share.dart';
import 'package:partnerum/utils/brain_module.dart';

@DataHelper()
class MessagesHelper {
  // Stream<List<Message>> chatByID(MessagesMap messagesRoot) =>
  //     messagesRoot.stream
  //         .map((event) => event?.values?.toList() ?? []);

  MessagesMap dbMessages;

  var chat = root.chatTopic.userToUser;

  Stream<List<Message>> chatByID(String chatID) {
    var res = chat[chatID]
        .messages
        .stream
        .map((event) => event?.values?.toList() ?? []);
    return res;
  }

  // root.chatTopic.userToUser[chatID].messages.stream
  //     .map((event) => event?.values?.toList() ?? []);

  // Future<List<Message>> chatByID(String chatID) =>
  //     root.chatTopic.userToUser[chatID].messages.future
  //         .then((value) => value.values.toList());

  void addMessage(String chatID, String message, String companionID) {
    print('messageInstanceID: ${chatID}');
    var newMessage = dbMessages.push;
    // newMessage.packet = true;

    newMessage.message = message;
    newMessage.timestamp.updateTimestamp();
    newMessage.uid = mindClass.user.uid;
  }

  addNewContact(String chatId) async {
    var users = (await root.chatTopic.userToUser[chatId].users.future
        .then((value) => value.values.toList()));

    print('users: ${users}');

    var firstUser = (await db?.root?.userData[users[0].key]
            ?.availableUsers[users[1].key]?.userInformation?.uid?.future)
        ?.value;
    if (firstUser == null) {
      var user1Name =
          (await root.userData[users[1].key].userInformation.username.future)
              .value;
      var newAvUser = root.userData[users[0].key].availableUsers[users[1].key];
      newAvUser.userInformation.uid = users[1].key;
      newAvUser.userInformation.username = user1Name;
    }
    var secondUser = (await db?.root?.userData[users[1].key]
            ?.availableUsers[users[0].key]?.userInformation?.uid?.future)
        ?.value;
    if (secondUser == null) {
      var user2Name =
          (await root.userData[users[0].key].userInformation.username.future)
              .value;
      var newAvUser = root.userData[users[1].key].availableUsers[users[0].key];
      newAvUser.userInformation.uid = users[0].key;
      newAvUser.userInformation.username = user2Name;
    }
  }

  void deactivateChat(String chatId) {
    root.userData[mindClass.user.uid].activeChats[chatId].remove();
  }

  Future<void> updateStatus(
      String offerObjectId, String userType, String status) async {
    print('userType: ${userType}');
    var userStatutes = root
        .chatTopic.offer2Room[offerObjectId].conversationUsers.usersStatuses;
    if (userType == 'ownerUser') {
      await userStatutes.ownerUserStatus.set(status);
    } else if (userType == 'requestUser') {
      await userStatutes.requestUserStatus.set(status);
    }
  }

  // List<String> stationsToList(StationsMap stationsMap) {
  //   var list = (stationsMap.values.map((e) => e.value)).toList().toString();
  //
  //   // print('Stations to list: ${list}');
  //   // final stringNoBrackets = list.substring(1, list.length - 1);
  //   // final stringToList = stringNoBrackets.split(', ');
  //
  //   return stringToList;
  // }

  String messageOfSharing(
      HouseInformation houseInformation, BuildContext context) {
    var listOfStations = (houseInformation.stations.values.map((e) => e.key)).toList();

    var export = '';
    if (houseInformation.description.value.isNotEmpty) {
      export +=
          '${Strings.pages.requestOfferPage.requestOfferHelper.aboutThePlace.l(context)}: ${houseInformation.description.value}\n';
    }
    export +=
        '${Strings.pages.requestOfferPage.requestOfferHelper.city.l(context)}: ${houseInformation.city.value}\n';
    if (listOfStations != null && listOfStations.isNotEmpty) {
      export +=
          '${Strings.pages.requestOfferPage.requestOfferHelper.nearbyStations.l(context)}: ${listOfStations.join(', ')}\n';
      export +=
          '${houseInformation.timeToMetro.value.inMilliseconds} ${Strings.pages.requestOfferPage.requestOfferHelper.timeToMetro.l(context)}\n';
    }
    if (houseInformation.address.value != null &&
        houseInformation.address.value.isNotEmpty &&
        houseInformation.address.value !=
            Strings.pages.requestOfferPage.requestOfferHelper.noAddress
                .l(context)) {
      export +=
          '${Strings.pages.requestOfferPage.requestOfferHelper.address.l(context)}: ${houseInformation.address.value}\n';
    }
    if (houseInformation.rooms.value != 5) {
      export +=
          '${Strings.pages.requestOfferPage.requestOfferHelper.rooms.l(context)}: ${houseInformation.rooms.value}\n';
    } else {
      export +=
          '${Strings.pages.requestOfferPage.requestOfferHelper.moreThanXRooms.l(context)}\n';
    }
    return export;
  }

  Future<List<Uint8List>> uint8ListOfImages(requestImages) async {
    if (requestImages == null || requestImages.isEmpty) return [];
    var imagesUint8List = <Uint8List>[];
    for (DbString requestImage in requestImages) {
      var imageFuture =
          await root?.imagesContainer[requestImage.value]?.bigImg?.future;
      var imageBytes = imageFuture?.uint8list;

      if (imageBytes != null) imagesUint8List.add(imageBytes);
    }
    return imagesUint8List;
  }

  Future<void> shareApartment(
      BuildContext context, String offerObjectId) async {
    var houseInformation =
        await root.chatTopic.offer2Room[offerObjectId].houseCopy.future;
    var requestImages = houseInformation?.photos?.values?.toList();
    var imagesUint8List = await uint8ListOfImages(requestImages);
    await share(
        messageOfSharing(houseInformation, context), imagesUint8List, context);
  }

  Future<String> currentUserType(String offerObjectId) async {
    var ownerUserFuture = await root.chatTopic.offer2Room[offerObjectId]
        ?.conversationUsers?.ownerUser?.future;
    var ownerUserId = ownerUserFuture.value?.split(';')?.first;
    if (mindClass.user.uid == ownerUserId) return 'ownerUser';
    return 'requestUser';
  }
}
