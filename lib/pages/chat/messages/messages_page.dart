import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:full_screen_image/full_screen_image.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:partnerum/head_module/gen_extensions/generated_str_extension.dart';
import 'package:partnerum/main.dart';
import 'package:partnerum/models/notification_model_original.dart';
import 'package:partnerum/models/rooms_model.dart';
import 'package:partnerum/pages/house/rate/rate_widget.dart';
import 'package:partnerum/pages/notifications/message_instance_page.dart';
import 'package:partnerum/pages/request/suitable_apartments/suitable_apartments_page.dart';
import 'package:partnerum/utils/brain_module.dart';
import 'package:partnerum/utils/router.dart';
import 'package:partnerum/utils/theme_constants.dart';
import 'package:scroll_to_index/scroll_to_index.dart';
import 'package:db_partnerum/db_partnerum.dart';
import 'messages_helper.dart';
import 'package:path/path.dart' as path;

class Messages extends StatefulWidget {
  Messages(
      {Key key, this.messageInstanceId, this.isOwner = false, this.finalButton = false, this.userId, this.chatRoom, this.notificationInternal, this.bookedPeriods = const [], this.bookingPeriod})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _Messages();
  final String messageInstanceId;
  final String userId;
  final bool isOwner;
  final bool finalButton;
  final ChatRoom chatRoom;
  final NotificationInternal notificationInternal;
  List<BookingPeriod> bookedPeriods;
  final DatesRange bookingPeriod;
}

class _Messages extends State<Messages> with AutomaticKeepAliveClientMixin, TickerProviderStateMixin<Messages>, MessagesHelper {
  Stream<List<Message>> messagesStream;

  Future getImage() async {
    var file = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (file != null) return file;

    return null;

    // var imageFile = await ImagePicker.pickImage(source: ImageSource.gallery);
    // print('image: ${imageFile.path}');
    // if (imageFile != null) {
    //   return await uploadFile(imageFile);
    // }
    // return null;
  }

  Future uploadFile(File imageFile) async {
    var newMessage = dbMessages.push;

    newMessage.uid = mindClass.user.uid;
    newMessage.timestamp = DateTime.now().millisecondsSinceEpoch;

    var image8List = await testCompressFile(imageFile);
    var newImage = newMessage.images.push;
    await newImage.set(FileWrapper(path.basename(imageFile.path), image8List));
  }

  Future<Uint8List> testCompressFile(File file) async {
    var result = await FlutterImageCompress.compressWithFile(
      file.path,
      quality: 10,
      rotate: 0,
    );
    return result;
  }

  drawCard(Message message, BuildContext buildContext) {
    if (message.images.isEmpty) {
      var currentTime = DateTime.now().millisecondsSinceEpoch;
      var showTime = (message.timestamp.value.inMilliseconds != 0) ? message.timestamp.value.inMilliseconds : currentTime;
      // that simple message
      // if (message.isEvent())
      //   return Padding(
      //     padding: const EdgeInsets.all(8.0),
      //     child: Container(
      //       decoration: BoxDecoration(
      //           color: Theme.of(context).primaryColor.withOpacity(0.5),
      //           borderRadius: BorderRadius.circular(10)),
      //       constraints: BoxConstraints(
      //           maxWidth: MediaQuery.of(buildContext).size.width * 0.8,
      //           minWidth: MediaQuery.of(buildContext).size.width * 0.2),
      //       child: Card(
      //         child: Text("test"),
      //       ),
      //     ),
      //   );
      if ((message?.uid?.value?.isNotEmpty ?? false)) {
        return Padding(
            padding: (message.isEvent.value == true) ? const EdgeInsets.all(16.0) : const EdgeInsets.all(4.0),
            child: Container(
                decoration: BoxDecoration(
                    color: (message.isEvent.value == true)
                        ? Theme.of(context).primaryColor.withOpacity(0.3)
                        : Theme.of(context).primaryColor.withOpacity(0.8),
                    borderRadius: BorderRadius.circular(10)),
                constraints:
                    BoxConstraints(maxWidth: MediaQuery.of(buildContext).size.width * 0.8, minWidth: MediaQuery.of(buildContext).size.width * 0.2),
                child: ((message.isEvent.value == true))
                    ? ListTile(
                        leading: Icon(Icons.info_outline),
                        title: Text('${Strings.pages.messagesPage.card.event.l(context)}: ${message.message.value}'),
                        subtitle: Text(DateFormat('kk:mm').format(DateTime.fromMillisecondsSinceEpoch(showTime))))
                    : Container(
                        child: Stack(alignment: Alignment.centerRight, children: <Widget>[
                        Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                            child: Column(mainAxisSize: MainAxisSize.min, crossAxisAlignment: CrossAxisAlignment.end, children: [
                              Flexible(
                                  child: Text(message?.message?.value ?? '',
                                      softWrap: true, style: Theme.of(buildContext).textTheme.bodyText2.copyWith(color: Colors.white))),
                              Text(DateFormat('kk:mm').format(DateTime.fromMillisecondsSinceEpoch(showTime)),
                                  style: Theme.of(context).textTheme.caption.copyWith(color: Colors.transparent))
                            ])),
                        Positioned(
                            bottom: 8,
                            right: 8,
                            child: Text(DateFormat('kk:mm').format(DateTime.fromMillisecondsSinceEpoch(showTime)),
                                style: Theme.of(context).textTheme.caption.copyWith(color: Colors.white70)))
                      ]))));
      } else {
        return Container();
      }
    } else {
      print('message.images.values: ${message.images.values}');
      return Container(
          //color: Colors.red,
          constraints: BoxConstraints(maxWidth: MediaQuery.of(buildContext).size.width * 0.8),
          child: ClipRRect(
              borderRadius: BorderRadius.circular(16),
              child: Card(
                  clipBehavior: Clip.antiAlias,
                  child: Column(children: <Widget>[
                    FullScreenWidget(
                        child: Hero(
                            /*
                            tag: '${message.url}smallImage',

                           */
                            tag: 'smallImage',
                            child: Center(
                                child: InteractiveViewer(
                                    maxScale: 10,
                                    minScale: 0.0001,
                                    child: Container(
                                      // color: Colors.green,
                                      height: MediaQuery.of(buildContext).size.height * 0.2,
                                      width: double.maxFinite,
                                      child: CachedNetworkImage(
                                          imageUrl: message.images.values.first.url,
                                          httpHeaders: message.images.values.first.syncHttpHeaders,
                                          // placeholder: (context, url) =>
                                          // Theme.of(context).platform == TargetPlatform.iOS
                                          //     ? CupertinoActivityIndicator()
                                          //     : CircularProgressIndicator(),
                                          errorWidget: (context, url, error) =>
                                              Center(child: Icon(Icons.info_outline, color: Theme.of(context).primaryColor))),
                                    )
                                    /*
                                    CachedNetworkImage(
                                        height: MediaQuery.of(buildContext)
                                                .size
                                                .height *
                                            0.2,
                                        width: double.maxFinite,
                                        fit: BoxFit.cover,
                                        /*
                                        imageUrl: message.url,

                                         */
                                        placeholder: (context, url) =>
                                            getShimmer(buildContext))

                                     */
                                    )))),
//                Text(DateTime.fromMillisecondsSinceEpoch(message.timestamp)
//                    .toIso8601String())
                  ]))));
    }
  }

  TextEditingController textEditingController = TextEditingController();
  Timer endedScroll;

  String offerObjectId;
  String userType;

  @override
  void initState() {
    //
    // chatByID(widget.messageInstanceId).listen((event) {
    //   print(event.length);
    // });
    // Future.delayed(Duration(seconds: 5))
    //     .then((_) => chatByID(widget.messageInstanceId).listen((event) {
    //           print(event.length);
    //         }));

    // messagesRoot =
    //     root.chatTopic.userToUser[widget.messageInstanceId].messages;
    dbMessages = root.chatTopic.userToUser[widget.messageInstanceId].messages;
    root.chatTopic.userToUser[widget.messageInstanceId].header.offerObjectId.future
        .then((value) => offerObjectId = value.value)
        .then((value) => currentUserType(offerObjectId).then((value) => userType = value));

    // print(offerObjectId);
    // offerObj =
    //     root.chatTopic.offer2Room[offerObjectId];

    super.initState();
    messagesStream = dbMessages.stream.map((event) => event?.values?.toList() ?? []);

    mindClass.ramData['chat_page'] = widget.messageInstanceId;
    _scrollController = ScrollController();
    _scrollController.addListener(() {});
  }

  toBottom([updateState = false]) async {
    await _scrollController.animateTo(0, curve: Curves.bounceIn, duration: kThemeAnimationDuration);
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
    mindClass.ramData['chat_page'] = '';
  }

  final bool _isVisible = false;
  final ValueNotifier<bool> _loadingMessages = ValueNotifier<bool>(true);
  List<Message> messages;
  ScrollController _scrollController;

  // MessagesMap messagesRoot;
  final ValueNotifier<bool> _filterOpened = ValueNotifier<bool>(false);

  ValueListenableBuilder<bool> filterHeader() {
    return ValueListenableBuilder(
        valueListenable: _filterOpened,
        builder: (BuildContext context, value, Widget child) {
          return FutureBuilder(
              future: chat[widget.messageInstanceId].users.future.then((value) => value.values.toList()),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  print('snapshot.data[0]: ${snapshot.data[0].key}');
                  return AnimatedSize(
                      key: ValueKey('actions'),
                      duration: kThemeChangeDuration,
                      vsync: this,
                      child: SizedBox(
                          height: value ? 0 : null,
                          child: SingleChildScrollView(
                              child: AnimatedContainer(
                                  duration: kThemeAnimationDuration,
                                  decoration: BoxDecoration(color: Colors.white, boxShadow: [
                                    BoxShadow(
                                        color: Colors.grey.withOpacity(0.2),
                                        spreadRadius: 1,
                                        blurRadius: 1,
                                        offset: Offset(0, 3) // changes position of shadow
                                        )
                                  ]),
                                  child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Container(
                                          width: double.maxFinite,
                                          child: ChipTheme(
                                              data: Theme.of(context).chipTheme.copyWith(pressElevation: 0),
                                              child: Wrap(children: [
                                                if (messages.isNotEmpty)
                                                  ActionChip(
                                                      backgroundColor: Colors.transparent,
                                                      padding: EdgeInsets.all(5),
                                                      onPressed: () async {
                                                        var bookedList = widget.bookedPeriods;
                                                        var newBookingDates = widget.bookingPeriod;
                                                        var ifAnyIntersections = checkDatesIntersection(bookedList, newBookingDates);
                                                        print('ifAnyIntersections: ${ifAnyIntersections}');
                                                        if (ifAnyIntersections) {
                                                          await Fluttertoast.showToast(
                                                              textColor: Colors.white,
                                                              backgroundColor: Colors.black54,
                                                              msg: 'Бронирование недоступно из-за переечения дат в заявке',
                                                              toastLength: Toast.LENGTH_SHORT,
                                                              gravity: ToastGravity.BOTTOM,
                                                              timeInSecForIosWeb: 1);
                                                        }
                                                        else {
                                                          await updateStatus(offerObjectId, userType, 'request_booked');
                                                        }
                                                      },
                                                      label: Text(Strings.pages.messagesPage.actions.book.l(context))),
                                                if (messages.isNotEmpty)
                                                  RateWidget.showRateChip(widget.messageInstanceId, mindClass.user.uid == snapshot.data[0].key),
                                                if (messages.isNotEmpty)
                                                  ActionChip(
                                                      backgroundColor: Colors.transparent,
                                                      padding: EdgeInsets.all(5),
                                                      onPressed: () async {},
                                                      label: Text(Strings.pages.messagesPage.actions.complain.l(context))),
                                                /*
                                            if (widget.userId == mindClass.user.uid)

                                             */
                                                ActionChip(
                                                    backgroundColor: Colors.transparent,
                                                    padding: EdgeInsets.all(5),
                                                    onPressed: () async {
                                                      await shareApartment(context, offerObjectId);
                                                    },
                                                    label: Text(Strings.pages.messagesPage.actions.shareWithClient.l(context))),
                                                ActionChip(
                                                    backgroundColor: Colors.transparent,
                                                    padding: EdgeInsets.all(5),
                                                    onPressed: () async {
                                                      await updateStatus(offerObjectId, userType,
                                                          userType == 'ownerUser' ? 'request_denied' : 'house_request_canceled');
                                                    },
                                                    label: Text(Strings.pages.messagesPage.actions.refuseOffer.l(context))),
                                                if (messages.isNotEmpty)
                                                  RateWidget.showRatedChip(widget.messageInstanceId, mindClass.user.uid == snapshot.data[0].key)
                                              ]))))))));
                }
                return Container();
              });
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        floatingActionButton: AnimatedOpacity(
            duration: Duration(milliseconds: 300),
            opacity: _isVisible ? 1 : 0,
            child: IgnorePointer(
                ignoring: !_isVisible,
                child: Padding(
                    padding: const EdgeInsets.only(bottom: 80),
                    child: FloatingActionButton(
                        mini: true,
                        onPressed: toBottom,
                        child: Icon(Icons.keyboard_arrow_down, color: Theme.of(context).primaryColor, size: 24 + 6.0))))),
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
        appBar: AppBar(
          centerTitle: true,
          elevation: 1,
          title: Text('Чат'),
          actions: <Widget>[
            if (widget.chatRoom != null)
              Tooltip(
                  message: widget.chatRoom.possibleUser.userInfo.username,
                  child: SizedBox(
                      height: kToolbarHeight / 2,
                      width: kToolbarHeight / 2,
                      child: CircleAvatar(
                          backgroundColor: Colors.orangeAccent,
                          backgroundImage: CachedNetworkImageProvider(widget.chatRoom.possibleUser.userInfo.avatar)))),
            if (!widget.isOwner)
              IconButton(
                  icon: Icon(Icons.info_outline, color: blackColor),
                  onPressed: () {
                    Navigator.push(context, CupertinoPageRoute(builder: (context) => InstancePage(offerObjectId: offerObjectId)));
                    if (offerObjectId == null) {
                      Fluttertoast.showToast(
                        textColor: Colors.white,
                        backgroundColor: Colors.black54,
                        msg: 'Идентификатор загружается. Повторите снова.',
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.BOTTOM,
                        timeInSecForIosWeb: 1,
                      );
                    } else {
                      Navigator.push(context, CupertinoPageRoute(builder: (context) => InstancePage(offerObjectId: offerObjectId)));
                    }
                  }),
            IconButton(
                icon: Icon(Icons.more, color: blackColor),
                onPressed: () {
                  _filterOpened.value = !_filterOpened.value;
                })
          ],
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              deactivateChat(widget.messageInstanceId);
              Navigator.pop(context);
            },
          ),
        ),
        body: Stack(children: <Widget>[
          // FutureBuilder<List<Message>>(
          StreamBuilder<List<Message>>(
              initialData: [],
              // future: chatByID(widget.messageInstanceId),
              // stream: messagesRoot.stream
              //     .map((event) => event?.values?.toList() ?? []),
              // stream: chatByID(widget.messageInstanceId),
              stream: messagesStream,
              builder: (context, asyncSnapshot) {
                messages = [];
                if (asyncSnapshot.data != null && asyncSnapshot.hasData && asyncSnapshot.data.isNotEmpty) {
                  messages = asyncSnapshot.data;
                  Timer(Duration(milliseconds: 32), () => _filterOpened.notifyListeners());
                }

                SliverList getMessagesList(List<Message> data) {
                  var bad = data.length < 20;
                  data = data.reversed.toList();

                  return SliverList(
                      delegate: SliverChildBuilderDelegate((context, int index) {
                    return Row(
                        mainAxisAlignment: ((data[index].isEvent.value == true))
                            ? MainAxisAlignment.center
                            : ((data[index].uid.value == mindClass.user.uid))
                                ? MainAxisAlignment.end
                                : MainAxisAlignment.start,
                        children: <Widget>[drawCard(data[index], context)]);
                  }, childCount: data.length));
                }

                if (_loadingMessages.value) _loadingMessages.value = false;
                return ScrollConfiguration(
                    behavior: noHighlightScrollBehavior(context),
                    child: CustomScrollView(controller: _scrollController, reverse: true, physics: AlwaysScrollableScrollPhysics(), slivers: [
                      SliverToBoxAdapter(
                          child: Padding(
                              padding: const EdgeInsets.only(top: 16),
                              child: TextField(
                                  controller: textEditingController,
                                  enableInteractiveSelection: true,
                                  decoration: InputDecoration(
                                      hintText: 'Введите сообщение',
                                      prefixIcon: IconButton(
                                          icon: Icon(Icons.image),
                                          onPressed: () async {
                                            // var str = await getImage();
                                            var image = await getImage();

                                            await uploadFile(image);
                                            setState(() {});

                                            if (widget.messageInstanceId == 'none' && widget.userId != null) {
                                              var messageInstanceId = 'text';
                                              /*
                                                      await mindClass.pureChat(
                                                          widget.userId,
                                                          null,
                                                          {'url': str});

                                                   */
                                              await Navigator.pushReplacement(
                                                  context, FadeRoute(page: Messages(messageInstanceId: messageInstanceId, finalButton: false)));
                                            } else {
                                              setState(() {
                                                _loadingMessages.value = true;
                                              });
                                              /*
                                                  await mindClass
                                                      .sendMessageToInstanceId(
                                                          messageInstanceId: widget
                                                              .messageInstanceId,
                                                          object: {'url': str});

                                                   */
                                            }
                                            toBottom();
                                          }),
                                      suffixIcon: IconButton(
                                          icon: Icon(Icons.send),
                                          onPressed: () async {
                                            if (textEditingController.text.isEmpty) {
                                              return;
                                            }

                                            if (widget.messageInstanceId == 'none' && widget.userId != null) {
                                              var messageInstanceId = 'message';
                                              /*
                                                      await mindClass.pureChat(
                                                          widget.userId, null, {
                                                    'message':
                                                        textEditingController
                                                            .text
                                                  });

                                                   */
                                              await Navigator.pushReplacement(
                                                  context, FadeRoute(page: Messages(messageInstanceId: messageInstanceId, finalButton: false)));
                                            } else {
                                              if (messages.isEmpty) {
                                                print('messages.isEmpty');
                                                addNewContact(widget.messageInstanceId);
                                              }
                                              addMessage(widget.messageInstanceId, textEditingController.text, widget.userId);
                                              setState(() {
                                                _loadingMessages.value = true;
                                              });

                                              /*
                                                  await mindClass.sendMessage(
                                                      messageInstanceId: widget
                                                          .messageInstanceId,
                                                      object: {
                                                        'message':
                                                            textEditingController
                                                                .text
                                                      });

                                                   */
                                            }
                                            textEditingController.clear();
                                          }))))),
                      if (debugMode) SliverToBoxAdapter(child: SelectableText(widget.messageInstanceId)),
                      messages.isNotEmpty
                          ? getMessagesList(messages)
                          : SliverFillRemaining(
                              child: Padding(
                                  padding: const EdgeInsets.only(top: 64),
                                  child: SizedBox(
                                      height: 100,
                                      child: Center(
                                          child: Text(Strings.pages.messagesPage.chattingBegin.l(context),
                                              style: Theme.of(context).textTheme.caption)))))
                    ]));
              }),
          filterHeader()
        ]));
  }

  bool once = true;

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

  bool checkDatesIntersection(List<BookingPeriod> bookedDates, DatesRange bookingDates) {
    if ((bookedDates == null) || (bookingDates == null)){
      return false;
    }
    for (var date in bookedDates){
      var startDate = date.checkIn.value;
      var endDate = date.checkOut.value;
      if (!((startDate > bookingDates.last.value) || (endDate < bookingDates.first.value))){
        return true;
      }
    }
    return false;
  }

}
