import 'dart:async';

import 'package:diffutil_sliverlist/diffutil_sliverlist.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:partnerum/models/rooms_model.dart';
import 'package:partnerum/utils/brain_module.dart';
import 'package:partnerum/utils/theme_constants.dart';
import '../../utils/admin_extension.dart';

class AdminPage extends StatefulWidget {
  @override
  _AdminPageState createState() => _AdminPageState();
}

class _AdminPageState extends State<AdminPage>
    with TickerProviderStateMixin<AdminPage>, pageState {
  final StreamController streamController = StreamController();
  Stream streamListen;

  @override
  void initState() {
    streamListen = streamController.stream;
    mindClass.setStreamController(streamController);
    mindClass.update();
    super.initState();
  }

  @override
  void dispose() {
    streamController.close();
    streamListen = null;
    super.dispose();
  }

  sliverDivider() => SliverToBoxAdapter(
          child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        child: Divider(),
      ));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: StreamBuilder(
            stream: streamListen,
            builder: (context, snapshot) {
              var _isLoading = snapshot.hasData && snapshot.data == 'upload';
              var _isLoadingDone = snapshot.hasData && snapshot.data == 'done';
              return ScrollConfiguration(
                  behavior: noHighlightScrollBehavior(context),
                  child: CustomScrollView(slivers: [
                    SliverAppBar(
                        backgroundColor: Colors.white,
                        pinned: true,
                        title: Text('Админ Панель'),
                        centerTitle: true,
                        textTheme: TextTheme(
                            title: TextStyle(
                                color: Colors.black, fontSize: 20.0))),
                    SliverToBoxAdapter(
                        child: AnimatedSize(
                            vsync: this,
                            duration: kThemeAnimationDuration,
                            child: SizedBox(
                                height: _isLoading ? 3 : 0,
                                child: linearProgressIndicator))),
                    SliverToBoxAdapter(
                        child: SwitchListTile(
                            title: Text('Показывать в глобальных контактах'),
                            value: mindClass.globalContactMode,
                            onChanged: (value) =>
                                mindClass.globalContactMode = value)),
                    SliverToBoxAdapter(
                        child: AnimatedSize(
                            vsync: this,
                            duration: kThemeAnimationDuration,
                            child: SizedBox(
                                height: mindClass.globalContactMode ? null : 0,
                                child: Padding(
                                    padding: const EdgeInsets.all(16.0)
                                        .copyWith(top: 8),
                                    child: FormField<String>(builder:
                                        (FormFieldState<String> state) {
                                      return InputDecorator(
                                          decoration: InputDecoration(
                                              // labelStyle: textStyle,
                                              errorStyle: TextStyle(
                                                  color: Colors.redAccent,
                                                  fontSize: 16.0),
                                              hintText: 'Please select expense',
                                              border: OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          5.0))),
                                          isEmpty: false,
                                          child: DropdownButtonHideUnderline(
                                              child: DropdownButton<String>(
                                                  value: mindClass
                                                      .globalContactRole,
                                                  isDense: true,
                                                  onChanged: (String newValue) {
                                                    setState(() {
                                                      mindClass
                                                              .globalContactRole =
                                                          newValue;
                                                      state.didChange(newValue);
                                                    });
                                                  },
                                                  items:
                                                      Roles.map((String value) {
                                                    return DropdownMenuItem<
                                                            String>(
                                                        value: value,
                                                        child: Text(value));
                                                  }).toList())));
                                    }))))),
                    sliverDivider(),
                    SliverToBoxAdapter(
                        child: SwitchListTile(
                            title: Text(
                                '${!adminsListVisible ? 'Показать' : 'Скрыть'} список админов'),
                            value: adminsListVisible,
                            onChanged: (value) => setState(() {
                                  adminsListVisible = value;
                                }))),
                    DiffUtilSliverList<PossibleAdmin>(
                        items: adminsListVisible
                            ? mindClass.adminList()
                            : <PossibleAdmin>[],
                        builder: (context, user) => ListTile(
                            subtitle: Text(user.humanReadableLevel),
                            onTap: () {},
                            title: Row(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                      '${user.userInfo.username.isEmpty ? user.uid.hashCode.toString() : user.userInfo.username}'),
                                  if (DateTime.now().millisecondsSinceEpoch -
                                          user.userInfo.lastOnline <=
                                      60000)
                                    ClipOval(
                                        child: Container(
                                            width: 8,
                                            height: 8,
                                            color: Colors.greenAccent))
                                ])),
                        insertAnimationBuilder: (context, animation, child) =>
                            FadeTransition(
                              opacity: animation,
                              child: child,
                            ),
                        removeAnimationBuilder: (context, animation, child) =>
                            SizeTransition(sizeFactor: animation, child: child),
                        removeAnimationDuration:
                            const Duration(milliseconds: 300),
                        insertAnimationDuration:
                            const Duration(milliseconds: 300)),
                    sliverDivider(),
                    SliverToBoxAdapter(
                        child: SwitchListTile(
                            title: Text(
                                '${!usersListVisible ? 'Показать' : 'Скрыть'} список пользователей'),
                            value: usersListVisible,
                            onChanged: (value) => setState(() {
                                  usersListVisible = value;
                                }))),
                    DiffUtilSliverList<PossibleUser>(
                        items: usersListVisible
                            ? mindClass.usersList()
                            : <PossibleUser>[],
                        builder: (context, user) => ListTile(
                            subtitle: Text(user.uid),
                            onTap: () {},
                            title: Row(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                      '${user.userInfo.username.isEmpty ? user.uid.hashCode.toString() : user.userInfo.username}'),
                                  if (DateTime.now().millisecondsSinceEpoch -
                                          user.userInfo.lastOnline <=
                                      60000)
                                    ClipOval(
                                        child: Container(
                                            width: 8,
                                            height: 8,
                                            color: Colors.greenAccent))
                                ])),
                        insertAnimationBuilder: (context, animation, child) =>
                            FadeTransition(opacity: animation, child: child),
                        removeAnimationBuilder: (context, animation, child) =>
                            SizeTransition(sizeFactor: animation, child: child),
                        removeAnimationDuration:
                            const Duration(milliseconds: 300),
                        insertAnimationDuration:
                            const Duration(milliseconds: 300)),
                    sliverDivider()
                  ]));
            }));
  }
}

mixin pageState {
  bool adminsListVisible = false, usersListVisible = false;
}
