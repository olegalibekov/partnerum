import 'package:flutter/material.dart';

class EndTopFloatingActionButtonLocation extends FloatingActionButtonLocation {
  const EndTopFloatingActionButtonLocation(this.show);

  final bool show;

  @override
  Offset getOffset(ScaffoldPrelayoutGeometry scaffoldGeometry) {
    return Offset(
        _endOffset(scaffoldGeometry), _straddleAppBar(scaffoldGeometry, show));
  }

  @override
  String toString() => 'FloatingActionButtonLocation.endTop';

  double _leftOffset(ScaffoldPrelayoutGeometry scaffoldGeometry,
      {double offset = 0.0}) {
    return kFloatingActionButtonMargin +
        scaffoldGeometry.minInsets.left -
        offset;
  }

  double _rightOffset(ScaffoldPrelayoutGeometry scaffoldGeometry,
      {double offset = 0.0}) {
    return scaffoldGeometry.scaffoldSize.width -
        kFloatingActionButtonMargin -
        scaffoldGeometry.minInsets.right -
        scaffoldGeometry.floatingActionButtonSize.width +
        offset;
  }

  double _endOffset(ScaffoldPrelayoutGeometry scaffoldGeometry,
      {double offset = 0.0}) {
    assert(scaffoldGeometry.textDirection != null);
    switch (scaffoldGeometry.textDirection) {
      case TextDirection.rtl:
        return _leftOffset(scaffoldGeometry, offset: offset);
      case TextDirection.ltr:
        return _rightOffset(scaffoldGeometry, offset: offset);
    }
    return null;
  }

  double _straddleAppBar(
      ScaffoldPrelayoutGeometry scaffoldGeometry, bool show) {
    final fabHalfHeight = scaffoldGeometry.floatingActionButtonSize.height + 16;

    return show
        ? scaffoldGeometry.contentBottom - fabHalfHeight
        : scaffoldGeometry.contentBottom + fabHalfHeight;
  }
}
