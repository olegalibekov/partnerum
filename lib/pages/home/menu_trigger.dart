import 'package:db_partnerum/db_partnerum.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:partnerum/pages/chat/messages/messages_page.dart';
import 'package:partnerum/utils/brain_module.dart';
import 'package:partnerum/utils/theme_constants.dart';

import 'home_helper.dart';

class HomePageMenu {
  static const int requestMenu = 0;
  static const int homeMenu = 1;
  static const int notificationsMenu = 2;
  static const int messagesMenu = 3;
  static const int profileMenu = 4;

  static bool isFabButton(int currentMenu, [bool extra = true]) {
    switch (currentMenu) {
      case requestMenu:
        return extra;
      case homeMenu:
      case messagesMenu:

        ///here we can click button and write a message to a friend?

        return true;
        break;
      case profileMenu:
        return mindClass.isAdmin;
        break;
      default:
        return false;
        break;
    }
  }

  static Future runButton(BuildContext context, int currentMenu, data, [callback]) async {
    switch (currentMenu) {
      case requestMenu:
        return Navigator.pushNamed(context, 'request_create/$data').then((v) async {
          await Future.delayed(Duration(seconds: 2));
          mindClass.updateEvent('updateRequestScreen');
        });
        break;
      case homeMenu:
        return Navigator.pushNamed(context, 'house_create/$data').then((v) async {
          await Future.delayed(Duration(seconds: 2));
          mindClass.updateEvent('updateHousesPage');
        });
        break;
      case profileMenu:
        /*
        if ((await mindClass.userInfo()).isAdmin) {

         */
        if (false) {
          return Navigator.pushNamed(context, 'admin_mode').then((v) {
            if (callback != null) callback();
          });
        }
        break;
      case messagesMenu:
        //support + all our parsed friends

        if (data != null && data.runtimeType == GlobalKey<ScaffoldState>().runtimeType) {
          await showModalBottomSheet(
              elevation: 1,
              isScrollControlled: true,
              shape: RoundedRectangleBorder(borderRadius: borderRadius),
              clipBehavior: Clip.antiAlias,
              context: (data as GlobalKey<ScaffoldState>).currentContext,
              builder: (context) {

                return FutureBuilder(
                  initialData: [],
                  future: HomeHelper.chatUsers(),
                  builder: (context, snapshot) {
                    return DraggableScrollableSheet(
                        expand: false,
                        maxChildSize: .7, //MediaQuery.of(context).size.height*0.5,
                        builder: (BuildContext context, ScrollController scrollController) {
                          bool nReady = false;
                          if (snapshot.hasData) {
                            // DbList<UserInformation> users = snapshot.data;
                            var users = snapshot.data;
                            if(snapshot.connectionState == ConnectionState.waiting) {
                              return AnimatedSwitcher(
                                  child: Stack(key: Key('contacts_loading'), children: <Widget>[
                                    Positioned(top: 0, left: 0, right: 0, child: SizedBox(height: 2, child: linearProgressIndicator)),
                                    Padding(
                                        padding: const EdgeInsets.all(2.0),
                                        child: Container(decoration: BoxDecoration(borderRadius: BorderRadius.circular(16), color: Colors.white))),
                                    Center(child: Text('Загрузка доступных контактов...'))
                                  ]),
                                  duration: Duration(milliseconds: 300));

                              return Center(child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  CircularProgressIndicator(),
                                  SizedBox(height: 12),
                                  Text('Загрузка пользователей')
                                ],
                              ));
                            }
                            if (snapshot.connectionState == ConnectionState.done && users.length <= 0) {
                              return AnimatedSwitcher(
                                  duration: Duration(milliseconds: 300),
                                  child: Center(key: Key('contacts_error'), child: Text('Ошибка. Контакты не обнаружены')));
                            }
                            return AnimatedSwitcher(
                                duration: Duration(milliseconds: 300),
                                child: ListView.builder(
                                    key: Key('contacts'),
                                    physics: BouncingScrollPhysics(),
                                    controller: scrollController,
                                    itemCount: users.length,
                                    itemBuilder: (BuildContext context, int index) {
                                      return ListTile(
                                          title:
                                          Row(mainAxisSize: MainAxisSize.min, crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                                            Text('${HomeHelper.nameByIndex(users, index)}'
                                              /*
                                                          '${users[index].userInfo.username.isEmpty ? users[index].uid.hashCode.toString() : users[index].userInfo.username}'

                                                         */
                                            ),
                                            if (DateTime.now().millisecondsSinceEpoch - HomeHelper.lastOnlineByIndex(users, index).inMilliseconds <=
                                                60000)
                                              ClipOval(child: Container(width: 8, height: 8, color: Colors.greenAccent))
                                          ]),
                                          subtitle: Text(HomeHelper.roleByIndex(users, index)
                                            /*
                                                    users[index].userInfo.role

                                                   */
                                          ),
                                          onTap: () async {
                                            var companion = HomeHelper.idByIndex(users, index);
                                            var chatID =
                                                users[index]?.chatId?.value ?? await HomeHelper.newChat(companion, users[index].userInformation.username.value);
                                            print('this is chatID: ${chatID}');
                                            await Navigator.pushReplacement(
                                                context,
                                                CupertinoPageRoute(
                                                    builder: (context) => Messages(
                                                      userId: companion,
                                                      /*
                                                                  users[index]
                                                                          .uid

                                                                   */
                                                      finalButton: false,
                                                      messageInstanceId: chatID,
                                                    )));
                                          });
                                    }));
                          }

                          return AnimatedSwitcher(
                              child: Stack(key: Key('contacts_loading'), children: <Widget>[
                                // Positioned(top: 0, left: 0, right: 0, child: SizedBox(height: 2, child: linearProgressIndicator)),
                                // Padding(
                                //     padding: const EdgeInsets.all(2.0),
                                //     child: Container(decoration: BoxDecoration(borderRadius: BorderRadius.circular(16), color: Colors.white))),
                                Center(child: Text('Доступные контакты не найдены'))
                              ]),
                              duration: Duration(milliseconds: 300));
                        });
                  }
                );
              }).then((value) async {
            await Future.delayed(Duration(seconds: 2));
            return mindClass.updateEvent('update_messages');
          });

        }
        break;
      default:
        break;
    }
  }
}
