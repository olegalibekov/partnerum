import 'package:after_layout/after_layout.dart';
import 'package:db_partnerum/db_partnerum.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:partnerum/head_module/gen_extensions/generated_str_extension.dart';
import 'package:partnerum/models/notification_model_original.dart';
import 'package:partnerum/pages/chat/chats/chats_page.dart';

import 'package:partnerum/pages/profile/profile/profile_page.dart';
import 'package:partnerum/pages/request/requests/requests_page.dart';
import 'package:partnerum/tools/firebase_notifcation_handler.dart';

import 'home_button_location.dart';

import 'package:partnerum/pages/house/houses/houses_page.dart';

import 'package:partnerum/pages/notifications/notifications/notifications_page.dart';

import 'package:partnerum/tools/firebase_notification_handler_original.dart';

import 'package:partnerum/utils/brain_module.dart';
import 'package:partnerum/utils/theme_constants.dart';
import 'package:partnerum/widgets/partnerum_icons.dart';

import 'home_helper.dart';
import 'menu_trigger.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with
        AfterLayoutMixin<HomePage>,
        SingleTickerProviderStateMixin,
        BottomHelper {
  int _page = 0;
  List<Widget> _pagesList;
  Widget _mainPage;
  FloatingActionButtonLocation _buttonLocation;

  Widget _scrollingList(ScrollController sc) {
    return ListView.builder(
      controller: sc,
      itemCount: 50,
      itemBuilder: (BuildContext context, int i) {
        return Container(
            padding: const EdgeInsets.all(12.0), child: Text('$i'));
      },
    );
  }

  Future fabClick() async => await HomePageMenu.runButton(
      context, _page, _page == 3 ? _globalKey : '');

  final GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    //onPageChanged
    return Scaffold(
        key: _globalKey,
        body: AnimatedSwitcher(
          duration: Duration(milliseconds: 300),
          child: _mainPage,
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
        floatingActionButton: isButtonShow
            ? FloatingActionButton(
                onPressed: fabClick,
                child: Stack(children: <Widget>[
                  Icon(
                      _page == HomePageMenu.profileMenu
                          ? Icons.lock_open :
                      _page == HomePageMenu.messagesMenu ? Icons.book : Icons.add,
                      size: 30,
                      color: blackColor),
                  if (_page != HomePageMenu.profileMenu)
                    Positioned(
                        right: 0,
                        child: ClipOval(
                            child: Container(
                                padding: EdgeInsets.all(1),
                                color: Colors.red,
                                constraints: BoxConstraints(
                                    minWidth: 13, minHeight: 13))))
                ]))
            : Container(width: 0, height: 0),
        bottomNavigationBar: Theme(
            data: Theme.of(context).copyWith(
                canvasColor: Colors.transparent,
                primaryColor: Colors.redAccent,
                textTheme: Theme.of(context)
                    .textTheme
                    .copyWith(caption: TextStyle(color: Colors.black))),
            child: Builder(builder: (BuildContext context) {
              Widget notificationBubble([active = false]) {
                // return Container();
                return StreamBuilder(

                    // future: mindClass.getNotifications(false),
                    stream: db
                        .root.userData[mindClass.user.uid].notifications.stream
                        .map((event) => event.values.toList()),
                    builder: (BuildContext context, AsyncSnapshot snapshot) {
                      if (snapshot.hasData && snapshot.data.length > 0) {
                        List<UserNotification> notifications = snapshot.data;

                        var showCircle = false;

                        for (var userNotification in notifications) {
                          if (userNotification?.read?.value != null && !userNotification.read.value) {
                            showCircle = true;
                            break;
                          }
                        }

                        return showCircle
                            ? Positioned(
                                right: 0,
                                child: ClipOval(
                                    child: Container(
                                        padding: EdgeInsets.all(1),
                                        color: active ? blackColor : Colors.red,
                                        constraints: BoxConstraints(
                                            minWidth: 13, minHeight: 13))))
                            : Container(width: 0, height: 0);
                      } else {
                        return Container(width: 0, height: 0);
                      }
                    });
              }

              /*
              // FutureBuilder<List<NotificationInternal>> notificationBubble(
              FutureBuilder<List<NotificationInternal>> notificationBubble(
                  [active = false]) {

                return FutureBuilder(
                  /*
                    future: mindClass.getNotifications(false),

                   */
                    future: root.userData[mindClass.user.uid].notifications.future,
                    builder: (BuildContext context, AsyncSnapshot snapshot) {
                      if (snapshot.hasData && snapshot.data.length > 0) {
                        return Positioned(
                            right: 0,
                            child: ClipOval(
                                child: Container(
                                    padding: EdgeInsets.all(1),
                                    color: active ? blackColor : Colors.red,
                                    constraints: BoxConstraints(
                                        minWidth: 13, minHeight: 13))));
                      } else {
                        return Container(width: 0, height: 0);
                      }
                    });


              }
*/
              Positioned messageBubble([active = false]) {
                // print('active: ${active}');
                return Positioned(
                    right: 0,
                    child: ClipOval(
                        child: Container(
                            padding: EdgeInsets.all(1),
                            color: active ? blackColor : Colors.red,
                            constraints:
                                BoxConstraints(minWidth: 13, minHeight: 13))));
/*
                return FutureBuilder(
                    future: mindClass.getMessagesNotifications(),
                    builder: (BuildContext context, AsyncSnapshot snapshot) {
                      if (snapshot.hasData && snapshot.data.length > 0) {
                        return Positioned(
                            right: 0,
                            child: ClipOval(
                                child: Container(
                                    padding: EdgeInsets.all(1),
                                    color: active ? blackColor : Colors.red,
                                    constraints: BoxConstraints(
                                        minWidth: 13, minHeight: 13))));
                      } else {
                        return Container(width: 0, height: 0);
                      }
                    });
  */
              }

              return StreamBuilder(
                  stream: numberOfUnreadMessages(),
                  builder: (context, unreadMessagesSnapshot) {
                    return BottomAppBar(
                        color: Colors.transparent,
                        shape: isButtonShow ? CircularNotchedRectangle() : null,
                        clipBehavior: Clip.antiAlias,
                        child: BottomNavigationBar(
                            backgroundColor: Colors.transparent,
                            selectedItemColor: Theme.of(context).primaryColor,
                            unselectedItemColor: blackColor,
                            showSelectedLabels: false,
                            showUnselectedLabels: false,
                            items: [
                              bottomButton(
                                  ico: Partnerum.offer,
                                  titleText: Strings
                                      .pages.homePage.bottomBar.requestText
                                      .l(context)),
                              // BottomNavigationBarItem(
                              //     backgroundColor: Colors.white,
                              //     icon: Icon(Partnerum.offer, size: 25.0),
                              //     title: Text(
                              //         Strings.pages.homePage.bottomBar.requestText
                              //             .l(context),
                              //         style: TextStyle(
                              //             color: Theme.of(context).primaryColor,
                              //             fontSize: 14.0))),
                              bottomButton(
                                  ico: Partnerum.order,
                                  titleText: Strings
                                      .pages.homePage.bottomBar.housesText
                                      .l(context)),
                              // BottomNavigationBarItem(
                              //     backgroundColor: Colors.white,
                              //     icon: Icon(Partnerum.order, size: 25.0),
                              //     title: Text(
                              //         Strings.pages.homePage.bottomBar.housesText
                              //             .l(context),
                              //         style: TextStyle(
                              //           color: Theme.of(context).primaryColor,
                              //           fontSize: 14.0,
                              //         ))),
                              BottomNavigationBarItem(
                                  activeIcon: Stack(children: <Widget>[
                                    Icon(Icons.notifications, size: 25.0),
                                    notificationBubble(true)

                                    /*
                                  notificationBubble(true)

                                   */
                                  ]),
                                  icon: Stack(children: <Widget>[
                                    Icon(Icons.notifications, size: 25.0),
                                    notificationBubble()
                                    /*
                                  notificationBubble()

                                   */
                                  ]),
                                  title: Text(
                                      Strings.pages.homePage.bottomBar
                                          .notificationsText
                                          .l(context),
                                      style: TextStyle(
                                          color: Theme.of(context).primaryColor,
                                          fontSize: 14.0)),
                                  backgroundColor: Colors.white),
                              BottomNavigationBarItem(
                                  backgroundColor: Colors.white,
                                  activeIcon: Stack(children: <Widget>[
                                    Icon(Partnerum.help, size: 25.0),
                                    if ((unreadMessagesSnapshot?.data?.length ??
                                            0) >
                                        0)
                                      messageBubble(true)
                                    // messageBubble((unreadMessagesSnapshot?.data?.length ?? 0) > 0)
                                  ]),
                                  icon: Stack(children: <Widget>[
                                    Icon(Partnerum.help, size: 25.0),
                                    if ((unreadMessagesSnapshot?.data?.length ??
                                            0) >
                                        0)
                                      messageBubble(false)
                                    // messageBubble((unreadMessagesSnapshot?.data?.length ?? 0) > 0)
                                  ]),
                                  title: Text(
                                      Strings.pages.homePage.bottomBar.chat
                                          .l(context),
                                      style: TextStyle(
                                          color: Theme.of(context).primaryColor,
                                          fontSize: 14.0))),
                              bottomButton(
                                  ico: Partnerum.profile,
                                  titleText: Strings
                                      .pages.homePage.bottomBar.profileText
                                      .l(context)),
                              // BottomNavigationBarItem(
                              //     backgroundColor: Colors.white,
                              //     icon: Icon(
                              //       Partnerum.profile,
                              //       size: 25.0,
                              //     ),
                              //     title: Text(
                              //         Strings.pages.homePage.bottomBar.profileText
                              //             .l(context),
                              //         style: TextStyle(
                              //             color: Theme.of(context).primaryColor,
                              //             fontSize: 14.0))),
                              BottomNavigationBarItem(
                                  icon: SizedBox(width: 25, height: 25),
                                  title: Container(child: Text('f')))
                            ],
                            //fixedColor: Colors.white,
                            type: BottomNavigationBarType.shifting,
                            onTap: navigationTapped,
                            currentIndex: _page));
                  });
            })

            // Container(color: Colors.orangeAccent, width: 200, height: 10),
            ));
  }

  BottomNavigationBarItem bottomButton({IconData ico, String titleText}) {
    return BottomNavigationBarItem(
        backgroundColor: Colors.white,
        icon: Icon(ico, size: 25.0),
        title: Text(titleText,
            style: TextStyle(
                color: Theme.of(context).primaryColor, fontSize: 14.0)));
  }

  void onPageChanged(int page) {
    setState(() {
      _page = page;
    });
  }

  bool isButtonShow = true;

  void navigationTapped(int page) {
    if (page == 5) {
      return;
    }
    if (page == _page) return;
    //pageController.jumpToPage(page);
    setState(() {
      _page = page;
      _mainPage = _pagesList[page];
      if (isButtonShow != HomePageMenu.isFabButton(_page)) {
        isButtonShow = HomePageMenu.isFabButton(_page);
        _buttonLocation = EndTopFloatingActionButtonLocation(isButtonShow);
      }
    });
  }

  // FirebaseNotifications firebaseNotifications;

  redrawPages() {
    _pagesList = [
      RequestsPage(hideFab: () {
        setState(() {
          if (isButtonShow != HomePageMenu.isFabButton(_page, false)) {
            isButtonShow = HomePageMenu.isFabButton(_page, false);
            _buttonLocation = EndTopFloatingActionButtonLocation(isButtonShow);
          }
        });
      }, showFab: () {
        setState(() {
          if (isButtonShow != HomePageMenu.isFabButton(_page, true)) {
            isButtonShow = HomePageMenu.isFabButton(_page, true);
            _buttonLocation = EndTopFloatingActionButtonLocation(isButtonShow);
          }
        });
      }),
      HousesPage(),
      NotificationsPage(),
      ChatsPage(),
      ProfilePage()
    ];
  }

  @override
  void initState() {
    super.initState();
    FlutterStatusbarcolor.setStatusBarColor(Colors.white);
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
    redrawPages();
    _mainPage = _pagesList[HomePageMenu.requestMenu];
    _buttonLocation =
        EndTopFloatingActionButtonLocation(HomePageMenu.isFabButton(_page));
    mindClass.eventSteam.outStream.listen((value) {
      if (value == 'updateView' || value == 'updateBottom') setState(() {});
    });
    //pageController =  PageController();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    mindClass.setContext(context);
    FirebaseNotifications(context);
  }
}
