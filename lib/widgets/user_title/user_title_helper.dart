import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:micro_utils/data_helper.dart';
import 'package:db_partnerum/db_partnerum.dart';
import 'package:partnerum/utils/brain_module.dart';

@DataHelper()
class UserTitleHelper {

  Future<User> userByID(String userID){
    return root.userData[userID].future;
  }

  Widget decoratedImage(ImageProvider imageProvider) {
    return Container(
        width: 50,
        height: 50,
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
                image: imageProvider,
                fit: BoxFit.cover,
                colorFilter:
                ColorFilter.mode(Colors.white, BlendMode.colorBurn))));
  }

  String confirmCheck(UserInformation userInformation) {
    if (userInformation?.isConfirmed?.value ?? false) {
      return 'Верефицирован';
    } else {
      return 'Неверефицирован';
    }
  }

}
