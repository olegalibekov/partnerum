import 'package:cached_network_image/cached_network_image.dart';
import 'package:db_partnerum/db_partnerum.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:partnerum/head_module/gen_extensions/generated_str_extension.dart';
import 'package:partnerum/pages/house/house_details/house_details_page.dart';
import 'package:partnerum/orders/order_detail/order_detail.dart';
import 'package:partnerum/pages/house/house_offers/house_offers_page.dart';
import 'package:partnerum/utils/brain_module.dart';
import 'package:partnerum/utils/theme_constants.dart';
import 'package:partnerum/widgets/partnerum_icons.dart';
import 'package:shimmer/shimmer.dart';

import 'house_card_helper.dart';

class HouseCard extends StatelessWidget with HouseCardHelper {
  final House house;
  final bool noEdit;
  final List photos;

  const HouseCard({Key key, this.house, this.noEdit = false, this.photos}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _buildOrderItem(context, house.houseInformation);
  }

  Widget _buildOrderItem(BuildContext context, HouseInformation houseInformation) {
    return GestureDetector(
        onTap: () {
          Navigator.push(context,
              CupertinoPageRoute(builder: (context) => OfferDetailsPage(houseInformation: houseInformation, edit: !noEdit, houseId: house.key)));
        },
        child: Card(
            clipBehavior: Clip.antiAlias,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(16))),
            child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
              // Container(color: Colors.black, width: 100, height: 20,)
              Padding(padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8), child: _buildCity(context, houseInformation)),

              LayoutBuilder(builder: (context, constraints) {
                return SizedBox(
                    height: MediaQuery.of(context).size.height * 1 / 6,
                    child: Builder(
                      builder: (BuildContext context) {
                        return AnimatedSwitcher(
                          duration: kThemeAnimationDuration,
                          child: photos == null || photos.isEmpty
                              ? Shimmer.fromColors(
                                  baseColor: Colors.grey[300],
                                  highlightColor: Colors.grey[100],
                                  child: Padding(
                                      padding: EdgeInsets.only(right: 8),
                                      child: ClipRRect(
                                          borderRadius: BorderRadius.circular(16),
                                          child: Stack(children: <Widget>[Container(width: constraints.maxWidth, color: Colors.black)]))))
                              : ListView(physics: BouncingScrollPhysics(), scrollDirection: Axis.horizontal, shrinkWrap: true, children: [
                                  for (var photo in photos)
                                    Padding(
                                        padding: EdgeInsets.only(right: 8),
                                        child: ClipRRect(
                                            borderRadius: BorderRadius.circular(16),
                                            child: Stack(children: <Widget>[
                                              CachedNetworkImage(
                                                  imageUrl: photo.smallImg.url,
                                                  httpHeaders: photo.smallImg.syncHttpHeaders,
                                                  fit: BoxFit.cover,
                                                  width: constraints.maxWidth),
                                              Positioned.fill(child: Container(color: Colors.black.withOpacity(0.05)))
                                            ])))
                                ]),
                        );
                      },
                    ));
              }),

              ListTile(
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[],
                  ),
                  subtitle: Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                    spacer(0, 8),
                    // _buildCity(context, house),

                    _buildMetro(context, houseInformation),
                    spacer(0, 8),
                    _buildPrice(context, houseInformation),
                    spacer(0, 8)
//                _buildGuests(document),
//                _buildRooms(document),
                  ])),

              _buildBtns(context, house)
            ])));
  }

  Widget _buildBtns(BuildContext context, House house) {
    return Container(
        width: double.maxFinite,
        child: Column(children: <Widget>[
          Container(
              width: double.maxFinite,
              child: Wrap(alignment: WrapAlignment.end, children: <Widget>[
                if (!noEdit)
                  TextButton(
                    onPressed: () {
                      // print('HouseRequests: ${house?.houseRequests?.values?.first.}');
                      if (!isAnyOffers(house)) {
                        Fluttertoast.showToast(
                          textColor: Colors.white,
                          backgroundColor: Colors.black54,
                          msg: Strings.pages.housesPage.housesCard.nobodyInterestedIn.l(context),
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.BOTTOM,
                          timeInSecForIosWeb: 1,
                        );
                      } else {
                        Navigator.push(context, CupertinoPageRoute(builder: (context) => HouseOffersPage(house: house)));
                      }
                    },
                    child: Row(mainAxisSize: MainAxisSize.min, crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                      Text(Strings.pages.housesPage.housesCard.offerText.l(context),
                          style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 16)),
                      Builder(
                        builder: (BuildContext context) {
                          if (isAnyOffers(house)) {
                            return ClipOval(child: Container(width: 6, height: 6, color: Colors.red));
                          }
                          return SizedBox(width: 0, height: 0);
                        },
                      )
                    ]),
                  )
              ]))
        ]));
  }

  Widget _buildPrice(BuildContext context, HouseInformation houseInformation) {
    return Row(children: <Widget>[
      Icon(Partnerum.ruble, color: Colors.grey, size: 14),
      Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Text(
              '${Strings.pages.housesPage.housesCard.price.from.l(context)} ${startPrice(houseInformation)} ${Strings.pages.housesPage.housesCard.price.to.l(context)} ${endPrice(houseInformation)} ${Strings.pages.housesPage.housesCard.price.measure.l(context)}',
              style: TextStyle(fontSize: 14, color: Colors.grey)))
    ]);
  }

  Widget _buildMetro(BuildContext context, HouseInformation houseInformation) {
    return Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
      Icon(Partnerum.metro, color: Colors.grey, size: 14),
      Flexible(
          child: Padding(
              padding: const EdgeInsets.only(left: 8),
              child: Container(child: Builder(builder: (context) {
                var stations = getStations(houseInformation);
                return Text(stations,
                    softWrap: true, maxLines: 2, overflow: TextOverflow.ellipsis, style: TextStyle(fontSize: 14, color: Colors.grey));
              }))))
    ]);
  }

  Widget _buildCity(BuildContext context, HouseInformation houseInformation) {
    // return Chip(
    //   label: Text(house.info.city.trim(),
    //       style: Theme.of(context)
    //           .chipTheme
    //           .labelStyle
    //           .copyWith(color: Colors.green)),
    //   padding: EdgeInsets.zero,
    //   shape: StadiumBorder(side: BorderSide(color: Colors.green)),
    //   backgroundColor: Colors.white,
    //   elevation: 0,
    // );
    return Text('${city(houseInformation)}', style: TextStyle(fontSize: 16, color: Colors.black, fontWeight: FontWeight.bold));
  }
}
