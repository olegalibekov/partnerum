import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:micro_utils/data_helper.dart';
import 'package:db_partnerum/db_partnerum.dart';
import 'package:partnerum/utils/brain_module.dart';

@DataHelper()
class HouseCardHelper {

  Future<List<PhotoImage>> photosList(HouseInformation houseInformation) async {
    var imagesList = <PhotoImage>[];
    for (var photo in houseInformation?.photos?.values){
      imagesList.add(await root.imagesContainer[photo.value].future);
    }
    return imagesList;
  }


  /*
  Stream<List> photosList(House house) async* {
    // return [];
    var photosList = await house.houseInformation.photos.future.then((value) => value?.values?.toList());
    yield photosList;
    print(photosList.length);
    for (var item in photosList) {
      await item.future;
      yield photosList;
    }
  }

   */

  String startPrice(HouseInformation houseInformation) =>
      houseInformation.priceRange.startPosition.value;

  String endPrice(HouseInformation houseInformation) =>
      houseInformation.priceRange.endPosition.value;

  // String stationsNearby(House house) =>
  //     house.houseInformation.stations.list.join(', ');

  String stationsNearby(House house) {
    var emptyList = <String>['-'];
    var st = house?.houseInformation?.stations?.values
            ?.map((event) => event.key)
            ?.toList() ??
        emptyList;
    // var stations = (st == null || st.isEmpty) ? emptyList : st.values.map((event) => event.key).toList();
    // return stations.join(', ');
    return st.join(', ');
    // return emptyList.join(', ');
  }

  Future<List<String>> futureStatons(HouseInformation houseInformation) {
    return houseInformation.stations.future
        .then((value) => value.values.map((e) => e.key).toList());
  }

  String getStations(HouseInformation houseInformation) {
    var stationsValues = houseInformation.stations.values;
    if (stationsValues != null) {
      return stationsValues.toList().map((e) => e.key).join(', ');
    }
    return '';
  }

  String city(HouseInformation houseInformation) => houseInformation.city.value;

  bool isAnyOffers(House house){
    return house?.houseRequests?.values?.isNotEmpty ?? false;
  }

}
