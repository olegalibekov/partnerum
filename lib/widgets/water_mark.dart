import 'package:flutter/material.dart';

class WaterMark extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: 100,
        height: 100,
        child: Container(color: Colors.red.withOpacity(0.1)));
  }
}
