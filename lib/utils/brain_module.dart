import 'dart:async' show FutureOr, Stream, StreamController, Timer;
import 'dart:collection';
import 'dart:convert';
import 'dart:ui';
import 'package:async/async.dart' show StreamGroup;
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:partnerum/models/rooms_model.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:partnerum/models/city_models.dart';
import 'package:db_partnerum/db_partnerum.dart' hide User;
import 'package:partnerum/tools/firebase_notifcation_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'theme_constants.dart';

enum Classic { remove, add }

abstract class EventsList {
  static const String went_offline =
      'WENT_OFFLINE'; //когда уходит в оффлайн оьновляется бар. предыщий прогресс обновлется, а новые штуки не обновляются.
  static const String went_online = 'WENT_ONLINE';

  //new block for page loading animation
  static const String reset_animation = 'RESET_ANIMATION';
  static const String one_more_point_load = 'ONE_MORE_POINT_LOAD';
  static const String close_page = 'CLOSE_PAGE';
  static const String close_page_ready = 'CLOSE_PAGE_READY';

  static const String start_loading = 'START_LOADING';
  static const String stop_loading = 'STOP_LOADING';
  static const String start_end = 'START_END';
  static const String stop_end = 'STOP_END';
//static const String
}

enum TypesOfLoad { offlineMode, loginPage, mainScreen, walkPage }

class DatabasePaths {
  static const String requests = 'requests';
  static const String houses = 'houses';
  static const String pure_messages = 'pure_messages';
  static const String timestamp = 'timestamp';
  static const String user_data = 'user_data';
  static const String message_data = 'message_data';
  static const String request_offers = 'request_offers';
  static const String request_offered_houses = 'request_offered_houses';
  static const String notifications = 'notifications';
}

var extra;

extension ExtraFuncs on StreamController {
  set value(value) => add(value);

  Stream get outStream {
    extra ??= stream.asBroadcastStream();
    return extra;
  }
}

class MindClass with DatabaseModule, DataUtil, ExtraSearch {
  static final MindClass _mindClass = MindClass._internal();
  final Map ramData = {};
  final eventSteam = StreamController<Object>();

  @override
  FirebaseAuth auth;

  @override
  User user;

  bool isAdmin = false;

  MindClass._internal() {
    eventSteam.value = '';
  }

  factory MindClass() {
    return _mindClass;
  }

  updateEvent(event) {
    eventSteam.add(event);
    // eventSteam.value = event;
  }

  BuildContext context;

  setContext(BuildContext buildContext) async {
    context = buildContext;
    /*
    if (!(await userInfo()).isUsernameExist()) {
      await Future.delayed(Duration(seconds: 1));
      showModalBottomSheet(
          shape: RoundedRectangleBorder(borderRadius: borderRadius),
          context: context,
          builder: (_context) {
            return Padding(
              padding: const EdgeInsets.all(16.0),
              child: Container(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    ListTile(
                      title: Text('Добавьте в Вашу анкету Имя'),
                      subtitle: Text(
                          'Вы не заполнили профиль, хотите это сделать сейчас ?'),
                    ),
                    SizedBox(
                      height: 16 * 2.0,
                    ),
                    Container(
                      width: double.maxFinite,
                      child: Wrap(
                        alignment: WrapAlignment.end,
                        spacing: 8,
                        children: <Widget>[
                          FlatButton(
                            onPressed: () => Navigator.pop(_context),
                            color: Colors.grey[300],
                            child: Text(
                              'Позже',
                              style: TextStyle(
                                fontSize: 16,
                                color: Colors.white
                              )
                            )
                          ),
                          FlatButton(
                            color: Theme.of(context).primaryColor,
                            child: Text(
                              'Да, заполню',
                              style: TextStyle(
                                fontSize: 16,
                                color: Colors.white
                              )
                            ),
                            onPressed: () => Navigator.pushReplacementNamed(
                                _context, 'edit_profile')
                          )
                        ]
                      )
                    )
                  ]
                )
              )
            );
          });
    }

     */
  }

  Timer _onlineUpdater;
  DbDuration _onlineUpdaterFuture;
  SharedPreferences shared;

  void onlineUpdater() async {
    if (_onlineUpdater == null || !_onlineUpdater.isActive) {
      _onlineUpdater = Timer.periodic(Duration(seconds: 60), (timer) async {
        if (_onlineUpdaterFuture == null) {
          _onlineUpdaterFuture = await updateUserOnline();
          _onlineUpdaterFuture = null;
        }
      });
    }
  }

  // onlineUpdater() async {
  //   if (_onlineUpdater == null || !_onlineUpdater.isActive) {
  //     _onlineUpdater = Timer.periodic(Duration(seconds: 5), (timer) async {
  //       if (_onlineUpdaterFuture == null) {

  //         /*
  //         _onlineUpdaterFuture = await this.updateLastOnline();
  //          */
  //         _onlineUpdaterFuture = null;
  //       }
  //     });
  //   }
  // }

  Future<void> launchBrain(
      {GlobalKey<ScaffoldState> key, BuildContext buildContext, user}) async {
    // await FirebaseAuth.instance.useEmulator('http://192.168.0.103:9099');
    auth = FirebaseAuth.instanceFor(app: await Firebase.initializeApp());
    shared = await SharedPreferences.getInstance();
    if (user == null) {
      currentUser();
    } else {
      this.user = user;
    }
    if (this.user != null) {
      await mindClass?.user?.reload();
      db.realtimeIdTokenGenerator = this.user.getIdToken;
      db.streamCacheOn = true;
      db.etagCacheOn = true;
      db.asyncMode = true;
      db.optimizationMode = true;

      db.saver = shared.setString;
      db.getter = (String key) => Future.value(shared.getString(key));

      await db.init(this.user.uid);
    }
    if (this.user == null) return;
    db.isWeb = kIsWeb;
    getCities();
    //context = buildContext;
    //
    eventSteam.outStream.listen((val) {});
    if (this.user != null) {
      onlineUpdater();
      /*
      isAdmin = (await mindClass.userInfo()).isAdmin;

       */
      //await FirebaseDatabase.instance.setPersistenceCacheSizeBytes(20);
      //rootReference.setPriority(priority)
    }
    //onReady(TypesOfLoad.mainScreen);
//    user == null
//        ? onReady(buildContext, TypesOfLoad.walkPage)
//        : onReady(buildContext, TypesOfLoad.mainScreen);
//
//    auth.onAuthStateChanged.listen((FirebaseUser user) async {
//      await currentUser();
//    });
  }

  onReady(BuildContext context, TypesOfLoad typeOfLoad) {
    switch (typeOfLoad) {
      case TypesOfLoad.offlineMode:
        Navigator.pushNamedAndRemoveUntil(
            context, 'offline', (Route<dynamic> route) => false);
        break;
      case TypesOfLoad.loginPage:
        Navigator.pushNamed(context, 'login');
        break;
      case TypesOfLoad.mainScreen:
        Navigator.pushNamedAndRemoveUntil(
            context, 'home', (Route<dynamic> route) => false);
        break;
      case TypesOfLoad.walkPage:
        Navigator.pushNamedAndRemoveUntil(
            context, 'walk', (Route<dynamic> route) => false);
        break;
    }
  }

  dispose() {
    eventSteam.close();
  }

  Future<void> deleteUser() async {
    var onlineUpdater = mindClass?._onlineUpdater;
    if (onlineUpdater != null) onlineUpdater.cancel();

    await user?.delete();
    user = null;
  }

  Future<UserCredential> reauthenticateViaPhone(
      String verificationId, String smsCode) async {
    var credential = PhoneAuthProvider.credential(
        verificationId: verificationId, smsCode: smsCode);

    return await mindClass.user.reauthenticateWithCredential(credential);
  }

// String flushbarSMSCode;
// Future reauthenticateCurrentAccount(
//   Future<Function> confirmCodeAlert) async {
//   await auth.verifyPhoneNumber(
//     phoneNumber: user.phoneNumber,
//     codeSent: (String verificationId, int resendToken) async {
//       // Update the UI - wait for the user to enter the SMS code
//
//       await confirmCodeAlert;
//
//       var credential = PhoneAuthProvider.credential(
//           verificationId: verificationId, smsCode: flushbarSMSCode);
//
//       // Sign the user in (or link) with the credential
//       // await auth.signInWithCredential(credential);
//
//       await user.reauthenticateWithCredential(credential);
//     },
//     verificationFailed: (FirebaseAuthException error) {},
//     codeAutoRetrievalTimeout: (String verificationId) {},
//     verificationCompleted: (PhoneAuthCredential phoneAuthCredential) {},
//   );
// }
}

final mindClass = MindClass();

class ExtraSearch {
  List<JsonCity> cities;
  Map<String, bool> cityMetro = new Map();

  Future<List<JsonCity>> getCities() async {
    if (true) {
      var jsonString = await rootBundle.loadString('assets/langs/cities.json');

      List<dynamic> jsonMap = json.decode(jsonString);
      var _cities = <JsonCity>[];
      jsonMap.forEach((jsonValue) {
        _cities.add(JsonCity.fromJson(jsonValue));
        cityMetro[_cities.last.name] = true;
      });

      cities = _cities;
    }
    return cities;
  }

  Future<JsonCity> getCityMetros(String city) async {
    if (cities == null) getCities();
    for (var i = 0, l = cities.length; i < l; i++) {
      if (cities[i].name == city) return cities[i];
    }
    return null;
  }

  Future<Map<String, Lines>> getRounds(String city) async {
    if (city.toLowerCase() != 'Москва'.toLowerCase()) return null;
    if (cities == null) getCities();
    JsonCity _city;
    for (var i = 0, l = cities.length; i < l; i++) {
      if (cities[i].name == city) {
        // print('here1');
        _city = cities[i];
        break;
      }
    }
    var lines = <String, Lines>{};

    for (var j = 0, l = _city.lines.length; j < l; j++) {
      _city.lines[j].stations.forEach((value) {
        var _station = value;
        _station.color = Color(int.parse('0xff' + _city.lines[j].hexColor));
        _station.number = _city.lines[j].id;
        rounds.forEach((v) {
          if (v.stations.toLowerCase().contains(_station.name.toLowerCase())) {
            if (!lines.containsKey(v.name)) lines[v.name] = Lines(v.name);
            lines[v.name].stations.add(_station);
          }
        });
      });
    }

    return lines;
  }

  Map<String, Lines> getRoundsAsync(String city, JsonCity jsonCity) {
    if (city.toLowerCase() != 'Москва'.toLowerCase()) return null;
    var _city = jsonCity;
    // for (var i = 0, l = jsonCitiy.length; i < l; i++) {
    //   if (jsonCitiy[i].name == city) {
    //     // print('here1');
    //     _city = jsonCitiy[i];
    //     break;
    //   }
    // }
    var lines = <String, Lines>{};

    for (var j = 0, l = _city.lines.length; j < l; j++) {
      _city.lines[j].stations.forEach((value) {
        var _station = value;
        _station.color = Color(int.parse('0xff' + _city.lines[j].hexColor));
        _station.number = _city.lines[j].id;
        rounds.forEach((v) {
          if (v.stations.toLowerCase().contains(_station.name.toLowerCase())) {
            if (!lines.containsKey(v.name)) lines[v.name] = Lines(v.name);
            lines[v.name].stations.add(_station);
          }
        });
      });
    }
    // return {'test': Lines('name')};
    return lines;
  }

  Future<List<Stations>> getMetroFromCity(String city) async {
    JsonCity _city = await getCityMetros(city);
    var allStations = <Stations>[];
    /*
    for (var i = 0, l = _city.lines.length; i < l; i++) {
      _city.lines[i].stations.forEach((value) {
        Stations _station = value;
//        if (_city.lines[i].hexColor != null &&
//            _city.lines[i].hexColor.isNotEmpty)
        _station.color = Color(int.parse('0xff' + _city.lines[i].hexColor));
        _station.number = _city.lines[i].id;
        allStations.add(value);
      });
    }

     */
    allStations.sort((s1, s2) {
      return s1.name.compareTo(s2.name);
    });
    /*
    return allStations;

     */
    return [];
  }

  List<String> search(String search, List<String> array) {
    var searchAnswer = <String>[];
    for (var i = 0, l = array.length; i < l; i++) {
      if (array[i].contains(search) || search.contains(array[i])) {
        searchAnswer.add(array[i]);
      }
    }
    return searchAnswer;
  }
}

///Модуль для аутентификации и простого взаимодействия с базой данных

class DatabaseModule {
  FirebaseAuth auth;
  User user;

/*
  DatabaseReference rootReference = FirebaseDatabase.instance.reference();


 */
  String currentNotificationToken = '';

  signOut() async {
    /*
    await addNotificationToken(currentNotificationToken, null);
     */

    var onlineUpdater = mindClass?._onlineUpdater;
    if (onlineUpdater != null) onlineUpdater.cancel();
    await root.userData[mindClass.user.uid].userInformation
        .tokens[currentNotificationToken]
        .remove();
    var shared = await SharedPreferences.getInstance();
    await shared.clear();
    await auth.signOut();
    user = null;
  }

  User currentUser() {
    user = auth.currentUser;
    return auth.currentUser;
  }

  Future<Map> getFType(String messageInstanceId) async {
    if (checkUser()) return null;
    /*
    var offer = (await rootReference
            .child(DatabasePaths.message_data)
            .child(messageInstanceId)
            .child('header')
            .child('fType')
            .once())
        .value as Map;
    return offer;

     */
  }

  Future<List<PossibleUser>> getPossibleUsers() async {
    if (checkUser()) return [];
    Future<List<PossibleUser>> getUsersInfo(List keys) async {
      var clearedKeys = Set<String>.from(keys);
      var users = <PossibleUser>[];
      for (var key in clearedKeys) {
        /*
        users.add(PossibleUser(key, await userInfo(key)));

         */
      }
      return users;
    }

    Future<List<PossibleUser>> getAdminInfo(Map contacts) async {
      var users = <PossibleUser>[];
      for (var contact in contacts.entries) {
        /*
        users.add(PossibleUser(contact.key, await userInfo(contact.key)));

         */
        users.last.userInfo.role = contact.value;
      }
      return users;
    }

    var possibleU = <PossibleUser>[];
    /*
    var globalContacts = (await rootReference.child('global_contacts').once())
        .value; // id: {user_info}
    if (globalContacts != null && (globalContacts as Map).keys.isNotEmpty) {
      possibleU.addAll(await getAdminInfo(Map.from(globalContacts)));
    }
    var localContactsRequests = (await rootReference
            .child(DatabasePaths.user_data)
            .child(user.uid)
            .child('chats')
            .once())
        .value;



    if (localContactsRequests !=
        null) if (Map.from(localContactsRequests).isNotEmpty) {
      //var keys = Map.from(localContactsRequests).keys.toList();

      possibleU.addAll(await getUsersInfo(
          Map.from(localContactsRequests).keys.map<String>((v) {
        if (v.runtimeType == ''.runtimeType)
          return v.split(';')[0];
        else
          return '';
      }).toList()));
    }
*/
    return possibleU;

//    var pureList = rootReference
//        .child(DatabasePaths.user_data)
//        .child(user.uid)
//        .child(DatabasePaths.message_data)
//        .child(DatabasePaths.pure_messages);
  }

  static const String houses_messages = 'houses_messages';
  static const String pure_message = 'pure_message';

//  archiveHousesRequest(String ownerId, String requestId, bool archive) async {
//    print(ownerId);
//    print(requestId);
//    print(user.uid);
//    if (_checkUser()) return false;
//    DatabaseReference databaseReference = rootReference
//        .child(DatabasePaths.user_data)
//        .child(user.uid)
//        .child(DatabasePaths.message_data)
//        .child(DatabasePaths.requests)
//        .child(ownerId)
//        .child(requestId);
//    await databaseReference.update({'archive': archive});
//    return true;
//  }

  Map<String, House> formatUserHouses(dynamic value) {
    var houses = Map<String, dynamic>.from(value);
    var trueRequests = houses.map((v1, v2) {
      /*
      House _house = House.fromJson(v2);
      _house.houseId = v1;

      return MapEntry(v1, _house);
       */
    });
    return trueRequests;
  }

  searchForApartment() async {
    var queryText = 's1';
//    var databaseReference = await rootReference.child(DatabasePaths.houses).orderByChild("city").equalTo("Coc").orderByChild("stations") .startAt(queryText)
//        .endAt(queryText+"\uf8ff").once();
  }

  Future<bool> isSubscribedRequestOffer(String userId, String houseId) async {
    /*
    var databaseReference = rootReference
        .child(DatabasePaths.user_data)
        .child(user.uid)
        .child(DatabasePaths.message_data)
        .child(userId)
        .child(houseId); //answer is String of messageInstance
    DataSnapshot snapshot = await databaseReference.once();
    if (snapshot.value == null) return false;
    return true;

     */
  }

  //TODO
  Timer countDownTimer;

  isReadyToPost() {}

  Future<int> getUserTopicHousesFutureS(String houseId) async {
    /*
    if (checkUser()) return null;
    if (houseId.isEmpty) return null;
    var data = (await rootReference
            .child(DatabasePaths.user_data)
            .child(user.uid)
            .child(DatabasePaths.message_data)
            .child('houses_by_house')
            .child(houseId)
            .once())
        .value;
    if (data != null) {
      var userList = Map.from(data);
      var count = 0;

      userList.forEach((uid, offersList) {
        count += Map.from(offersList).length;
      });
      return count;
    } else {
      return 0;
    }

     */
  }

  Stream getUserTopicHousesStream() {
    /*
    if (checkUser()) return null;
    return rootReference
        .child(DatabasePaths.user_data)
        .child(user.uid)
        .child(DatabasePaths.message_data)
        .child(DatabasePaths.houses)
        .onValue
        .map((data) {
      if (data.snapshot.value != null) {
        var userList = Map.from(data.snapshot.value);
        var answer = [];
        userList.forEach((ownerUid, requestsList) {
          Map.from(requestsList).forEach((requestId, instanceId) {
            var instanceMap = Map.from(instanceId);
            print(instanceMap);
            answer.add([
              ownerUid,
              requestId,
              instanceMap.keys.first,
              instanceMap.values.first
            ]);
          });
        });
        return answer;
      }
      return [];
    });

     */
  }

  Future<List> getUserTopics(String requestId) async {
    /*
    if (checkUser()) return [];
    var data = (await rootReference
            .child(DatabasePaths.user_data)
            .child(user.uid)
            .child(DatabasePaths.message_data)
            .child(DatabasePaths.requests)
            .child(requestId)
            .once())
        .value;
    if (data == null) return [];
    var userList = Map.from(data);
    var answer = [];
    userList.forEach((uid, housesList) {
      Map.from(housesList).forEach((houseId, messageInstanceId) {
        answer.add([uid, houseId, messageInstanceId]);
      });
    });

    return answer;

     */
  }

  Stream getUserTopicsStream(String requestId) {
    /*
    if (checkUser()) return null;
    return rootReference
        .child(DatabasePaths.user_data)
        .child(user.uid)
        .child(DatabasePaths.message_data)
        .child(DatabasePaths.requests)
        .child(requestId)
        .onValue
        .map((data) {
      if (data.snapshot.value != null) {
        var userList = Map.from(data.snapshot.value);
        var answer = [];
        userList.forEach((uid, housesList) {
          Map.from(housesList).forEach((houseId, messageInstanceId) {
            answer.add([uid, houseId, messageInstanceId]);
          });
        });
        return answer;
      }
      return [];
    });

     */
  }

  dynamic getRequestOfferedHousesStream(String requestId) {
    /*
    if (checkUser()) return null;
    return rootReference
        .child(DatabasePaths.user_data)
        .child(user.uid)
        .child(DatabasePaths.request_offered_houses)
        .child(requestId)
        .onValue;

     */
  }

  Map<String, House> allHouses = {};

  bool checkUser() {
    return user == null;
  }

  bool isUserExist() {
    return user == null;
  }

  Future updateUserOnline() {
    return root.userData[mindClass.user.uid].userInformation.lastOnline
        .updateTimestamp();
  }
}

class DataUtil {
  // userOnlineUpdate() {
  //   var timer = Timer.periodic(Duration(milliseconds: 500), (timer) {
  //     root.userData['test'].userInformation.lastOnline.updateTimestamp();
  //   });
  // }
/*
  static isKeyExist() async {

    Database db = database();
    DatabaseReference ref = db.ref("emails/" + key.replaceAll(".", "_"));
    var data = (await ref.once('value')).snapshot.val();
    html.window.localStorage["name"] = data;
    mindClass.updateEvent(EventsList.stop_loading);

    return data != null;
  }

  static tryToComeIn(func) async {
    mindClass.updateEvent(EventsList.start_loading);
    String key = html.window.localStorage["key"] ?? "";
    var usr = (await mindClass.auth.currentUser());
    if (usr != null) {
      if (!usr.isAnonymous) {
        mindClass.updateEvent(EventsList.stop_loading);
        func();
        return true;
      }
    }

    if (key.isEmpty) {
      mindClass.updateEvent(EventsList.stop_loading);
      return false;
    }
    Database db = database();
    DatabaseReference ref = db.ref("attempts/");
    //var id = (await ref.(key.replaceAll(".", "_"))).;

    String string = key.replaceAll(".", "_");
    ThenableReference childRef = ref.push();
    await childRef.update({'key': string}).catchError((e) {});
    ref.child(childRef.key).onChildAdded.listen((data) {
      ref.child(childRef.key).child("token").once('value').then((data) async {
        if (data.snapshot.exists()) {
          String token = data.snapshot.val();
          FirebaseUser user =
              (await mindClass.auth.signInWithCustomToken(token: token)).user;
          if (user.email != null) {
            mindClass.updateEvent(EventsList.stop_loading);
            func();
          }
        }
      });
    });
    //if(data!=null) getKeyData((){});
  }

  //lastDataWrite to db;
  //special key that activate count and we would wait for update

  static Future getKeyData(Function func) async {
    mindClass.updateEvent(EventsList.start_loading);
    String key = html.window.localStorage["key"];
    if (key.isEmpty) {
      mindClass.updateEvent(EventsList.stop_loading);
      return false;
    }
    Database db = database();
    DatabaseReference ref = db.ref("data/" + key.replaceAll(".", "_"));
    Map data = Map.from((await ref.once('value')).snapshot.val());
    if (data != null) {
      html.window.localStorage["womenBirth"] = data["womenBirth"];
      html.window.localStorage["_womenBirth"] =
          lF(html.window.localStorage["womenBirth"]).toString();

      html.window.localStorage["menBirth"] = data["menBirth"];
      html.window.localStorage["_menBirth"] =
          lF(html.window.localStorage["menBirth"], "optional", "-").toString();

      html.window.localStorage["lastPeriodDate"] = data["lastPeriodDate"];
      html.window.localStorage["_lastPeriodDate"] =
          lF(html.window.localStorage["lastPeriodDate"]).toString();

      html.window.localStorage["averageMenstruationDay"] =
      data["averageMenstruationDay"];
      html.window.localStorage["averagePeriod"] = data["averagePeriod"];

      html.window.localStorage["chronicles"] = "-";
      if (data["chronicles"].toString() == "true") {
        html.window.localStorage["chronicles"] = "+";
      }
      html.window.localStorage["_chronicles"] =
          lF(html.window.localStorage["chronicles"].toString(), "+", "-")
              .toString();
      mindClass.updateEvent(EventsList.stop_loading);
      func();
    }
  }

  static lF(String t, [String opt, String opt2]) {
    if (t == null) return true;
    if (t.isEmpty) return true;
    return t.replaceAll(RegExp(r'[0-9]'), "#") == "##/##/####" ||
        t == opt ||
        t == opt2;
  }

  static loadDays() async {
    mindClass.updateEvent(EventsList.start_loading);
    String key = html.window.localStorage["key"];
    if (key.isEmpty) {
      mindClass.updateEvent(EventsList.stop_loading);
      return false;
    }
    Database db = database();
    DatabaseReference ref = db.ref("data/" + key.replaceAll(".", "_") + "/");
    var timeStamp = DateTime.now().millisecondsSinceEpoch.toString();
    DatabaseReference ref0 = db.ref("requests/");

    var chron = false;
    if (html.window.localStorage["chronicles"] !=
        null) if (html.window.localStorage["chronicles"] == "+") chron = true;
    var done = (await ref.set({
      "womenBirth": html.window.localStorage["womenBirth"],
      "menBirth": html.window.localStorage["menBirth"],
      "lastPeriodDate": html.window.localStorage["lastPeriodDate"],
      "averageMenstruationDay":
      html.window.localStorage["averageMenstruationDay"],
      "averagePeriod": html.window.localStorage["averagePeriod"],
      "chronicles": chron,
      "timestamp": timeStamp
    }));
    ref0.update({
      key.replaceAll(".", "_") + ";" + timeStamp.toString():
      html.window.localStorage['name']
    });
    mindClass.updateEvent(EventsList.stop_loading);
  }

   */
}

class DataDrive {
  final Map _o = {};

  DataDrive() {}

  Object operator [](Object key) => _o[key];

  void operator []=(Object key, Object value) {
    _o[key] = value;
    print('Key: "$key" updated with the value "$value"');
  }
}
