import 'package:cached_network_image/cached_network_image.dart';
import 'package:db_partnerum/db_partnerum.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

// import 'package:partnerum/models/houses_model.dart';
// import 'package:partnerum/models/requests_model.dart';
import 'package:partnerum/models/rooms_model.dart';
import 'package:partnerum/models/user_info_model.dart';
import 'package:partnerum/pages/request/request_offer/request_offer_page.dart';
import 'package:partnerum/pages/request/suitable_apartments/suitable_apartments_page.dart';
import 'package:partnerum/widgets/user_title/user_tile.dart';
import 'package:shimmer/shimmer.dart';

import 'brain_module.dart';

ThemeData mainTheme = ThemeData(
    brightness: Brightness.light,
    primaryColor: Color(0xFFFF6F00),
    accentColor: Color(0xFFFF6F00),
    chipTheme: ThemeData().chipTheme.copyWith(backgroundColor: Colors.white),
    appBarTheme: AppBarTheme(
        brightness: Brightness.light,
        color: Colors.white,
        textTheme: TextTheme(title: TextStyle(color: Colors.black, fontSize: 22)),
        iconTheme: IconThemeData(color: blackColor),
        actionsIconTheme: IconThemeData(color: blackColor)),
    iconTheme: IconThemeData(color: blackColor),
    tabBarTheme: TabBarTheme(labelColor: Colors.black),
    floatingActionButtonTheme: FloatingActionButtonThemeData(
      backgroundColor: Colors.white,
      // shape: RoundedRectangleBorder(
      //     borderRadius: BorderRadius.all(Radius.circular(16.0)))
    ),

    //colorScheme: Theme.of(context).colorScheme.copyWith(
    //  secondary: Color(0xFFFF6F00),
    //),
    fontFamily: 'Roboto');
Radius radius = Radius.circular(16.0);
BorderRadius borderRadius = BorderRadius.only(topLeft: radius, topRight: radius);
Widget linearProgressIndicator = PreferredSize(preferredSize: Size.fromHeight(2), child: LinearProgressIndicator(backgroundColor: blackColor));

Color blackColor = Color(0xff3D3D57);
Color goodGreen = Color(0xff81c784).withOpacity(0.5);
DateFormat dateFormat = DateFormat('dd.MM.yyyy');

AnimatedSwitcher getSwitcher({Widget child}) {
  return AnimatedSwitcher(duration: Duration(milliseconds: 300), child: child);
}

SizedBox spacer([double spaceX = 8, double spaceY = 8]) => SizedBox(width: spaceX, height: spaceY);
BorderRadiusGeometry sheetRadius = BorderRadius.only(topLeft: Radius.circular(16.0), topRight: Radius.circular(16.0));
Duration animationDuration = Duration(milliseconds: 300);

openUserInfo({String uid, UserInformation userInformation, BuildContext context}) {
  showModalBottomSheet(
      elevation: 1,
      isScrollControlled: true,
      shape: RoundedRectangleBorder(borderRadius: borderRadius),
      clipBehavior: Clip.antiAlias,
      context: context,
      builder: (context) {
        return DraggableScrollableSheet(
            expand: false,
            maxChildSize: .7, //MediaQuery.of(context).size.height*0.5,
            builder: (BuildContext context, ScrollController scrollController) {
              return CustomScrollView(controller: scrollController, slivers: [
                SliverToBoxAdapter(child: UserTile(userInformation: userInformation, click: false,)
                    //   child: Container(
                    // color: Colors.yellow,
                    // width: 100,
                    // height: 200,
                    // )
                    /*child: UserTile(uid: uid, user: user, click: false)*/
                    ),
                SliverToBoxAdapter(child: Padding(padding: const EdgeInsets.symmetric(horizontal: 16.0), child: Divider())),
                /*if (user.rating.reviews?.isNotEmpty ?? false)*/
                if (userInformation?.rating?.value == null)
                  SliverToBoxAdapter(
                      child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16.0).copyWith(top: 8),
                          child: Text('У этого пользователя ещё нет отзывов', style: Theme.of(context).textTheme.headline6))),
                if (userInformation?.rating?.value != null)
                  SliverToBoxAdapter(
                      child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16.0).copyWith(top: 8),
                          child: Text('Отзывы', style: Theme.of(context).textTheme.headline6))),
                if (userInformation?.rating?.value != null)
                FutureBuilder<List<UsersReview>>(
                  future: root.userData[uid].reviews.future.then((value) => value.values.toList()),
                  initialData: [],
                  builder: (context, reviewsSnapshot) {
                    return SliverList(
                          delegate: SliverChildBuilderDelegate((context, index) {
                        return Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisSize: MainAxisSize.min, children: [
                          ListTile(
                              /*title: user.rating.reviews[index].humanReadableRating, */
                              title: Text((reviewsSnapshot.data[index].mark.value - 0.01)?.toStringAsFixed(2)),
                              // title: Text('title'),
                              /*subtitle: Text(user.rating.reviews[index].reviewText)*/
                              subtitle: Text(reviewsSnapshot.data[index].reviewText.value)),
                              // subtitle: Text('subtitle')),
                          /*if (user.rating.reviews[index].reviewAnswer.isNotEmpty)*/
                          if (reviewsSnapshot.data[index]?.answerText?.value?.isNotEmpty ?? false)
                          // if (true)
                            Padding(
                                padding: const EdgeInsets.all(16.0).copyWith(top: 0),
                                child: Container(
                                    width: double.maxFinite,
                                    decoration: BoxDecoration(color: Colors.grey.shade300, borderRadius: BorderRadius.circular(16)),
                                    child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Column(mainAxisSize: MainAxisSize.min, crossAxisAlignment: CrossAxisAlignment.start, children: [
                                          Text('Ответ на отзыв', style: Theme.of(context).textTheme.caption),
/*                                    Text(user.rating.reviews[index].reviewAnswer,
                                            style: Theme.of(context).textTheme.bodyText2.copyWith(color: Colors.grey.shade600))*/
                                          Text(reviewsSnapshot.data[index].answerText.value,
                                              style: Theme.of(context).textTheme.bodyText2.copyWith(color: Colors.grey.shade600))
                                        ]))))
                        ]);
                      },
                              /*    childCount: user.rating.reviews?.length ?? 0*/
                              childCount: reviewsSnapshot?.data?.length ?? 0));
                    }
                )
              ]);
            });
      });
}

Shimmer getShimmer(BuildContext buildContext) {
  return Shimmer.fromColors(
      baseColor: Colors.grey[300],
      highlightColor: Colors.grey[100],
      enabled: true,
      child: Container(width: double.maxFinite, height: MediaQuery.of(buildContext).size.height * 0.15, color: Colors.white));
}

ScrollBehavior noHighlightScrollBehavior(BuildContext context) => ScrollBehavior()..buildViewportChrome(context, null, AxisDirection.down);
