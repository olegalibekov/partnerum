import 'package:flutter/material.dart';
import 'package:partnerum/pages/admin/admin_page.dart';

import 'package:partnerum/pages/etc/login_page.dart';
import 'package:partnerum/pages/home/home_page.dart';
import 'package:partnerum/pages/house/house_create/house_create_page.dart';

import 'package:partnerum/pages/request/request_create/request_create_page.dart';
import 'package:partnerum/pages/welcome/walk_page.dart';
import '../pages/profile/profile_edit/profile_edit_page.dart';

import 'fluro/fluro.dart';

class FluroRouter {
  static FRouter router = FRouter();

  static final Handler _homePage = Handler(
      handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
          HomePage());

  static final Handler _editProfilePage = Handler(
      handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
          ProfileEditPage());

  static final Handler _loginPage = Handler(
      handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
          LoginPage());

  static final Handler _walkPage = Handler(
      handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
          WalkPage());

  static final Handler _requestCreatePage = Handler(
      handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
          RequestCreate(requestId: params["requestId"][0]));

  static final Handler _houseCreatePage = Handler(
      handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
          HouseCreatePage(houseId: params["houseId"][0]));

  static final Handler _adminPage = Handler(
      handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
          AdminPage());

  static void setupRouter() {
    router.define('$LoginPage',
        handler: _loginPage, transitionType: TransitionType.cupertino);
    router.define('home',
        handler: _homePage, transitionType: TransitionType.cupertino);
    router.define('edit_profile',
        handler: _editProfilePage, transitionType: TransitionType.cupertino);
    router.define('walk',
        handler: _walkPage, transitionType: TransitionType.cupertino);
    router.define('request_create/:requestId',
        handler: _requestCreatePage, transitionType: TransitionType.cupertino);
    router.define('house_create/:houseId',
        handler: _houseCreatePage, transitionType: TransitionType.cupertino);
    router.define('admin_mode',
        handler: _adminPage, transitionType: TransitionType.cupertino);
  }
}

class FadeRoute extends PageRouteBuilder {
  final Widget page;

  FadeRoute({this.page})
      : super(
          pageBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
          ) =>
              page,
          transitionsBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
            Widget child,
          ) =>
              FadeTransition(
            opacity: animation,
            child: child,
          ),
        );
}
