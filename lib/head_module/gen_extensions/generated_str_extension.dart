/*Внимание! В данном файле нельзя производить изменения вручную. Т.к. генератор просто удалит их.
              Используйте плагин flutter_serve для внесения изменений: flutter pub run flutter_serve*/
import '_generated_str_extension.dart';
import '../../utils/app_localizations.dart';

extension Languages on String {
  ///Функция активации перевода
  l(context, [List args = const []]) {
    return AppLocalizations.of(context).t(this, args);
  }
}

final _Strings Strings = _Strings();

class _Strings {
  ///Partnerum
  String get appName => "app_name"; /*Partnerum*/
  Tooltips get tooltips => Tooltips();
  GlobalOnes get globalOnes => GlobalOnes();
  Common get common => Common();
  Pages get pages => Pages();
}
