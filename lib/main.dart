import 'package:db_partnerum/db_partnerum.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:partnerum/utils/brain_module.dart';
import 'package:flutter/services.dart';
import 'pages/partnerum_app_page.dart';
import 'tools/firebase_notifcation_handler.dart';
import 'utils/router.dart';

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {}

Future<void> main() async {
  ///Загрузка плагинов до старта интерфейса
  WidgetsFlutterBinding.ensureInitialized();

  ///Установка вращения только в портретном режиме
  await SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

  ///Инициализиаця роутера
  FluroRouter.setupRouter();
  await mindClass.launchBrain();

  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

  runApp(PartnerumApp());
}

bool debugMode = false;
