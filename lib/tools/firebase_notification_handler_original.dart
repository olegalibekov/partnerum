// import 'dart:io';
//
// import 'package:db_partnerum/db_partnerum.dart';
// import 'package:firebase_messaging/firebase_messaging.dart';
// import 'package:flushbar/flushbar.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:partnerum/models/notification_model_original.dart';
// import 'package:partnerum/pages/notifications/message_instance_page.dart';
// import 'package:partnerum/pages/chat/messages/messages_page.dart';
// import 'package:partnerum/utils/brain_module.dart';
//
// class FirebaseNotifications {
//   static const String houses_topic = "houses_topic";
//
//   FirebaseMessaging _firebaseMessaging;
//   final BuildContext _buildContext;
//
//   FirebaseNotifications(this._buildContext) {
//     setUpFirebase();
//     subscribeChannel(houses_topic);
//   }
//
//   subscribeChannel(String topic, [bool subscribe = true]) {
//     if (subscribe) {
//       _firebaseMessaging.subscribeToTopic(topic);
//     } else {
//       _firebaseMessaging.unsubscribeFromTopic(topic);
//     }
//   }
//
//   void setUpFirebase() {
//     _firebaseMessaging = FirebaseMessaging();
//     firebaseCloudMessagingListeners();
//   }
//
//   void firebaseCloudMessagingListeners() {
//     if (Platform.isIOS) iOS_Permission();
// /*
//     _firebaseMessaging
//         .getToken()
//         .then((token) {
//           print('@@@token: $token');
//           return root.userData[mindClass.user.uid](token);
//         });
// */
//     _firebaseMessaging.configure(
//         onMessage: (Map<String, dynamic> message) async {
//       print('hey');
//       var notificationMessage = await NotificationInternal.fromJson(message);
//       if (notificationMessage.type != NotificationType.message) {
//         mindClass.eventSteam.value = 'updateBottom';
//         Flushbar(
//             flushbarPosition: FlushbarPosition.TOP,
//             title: notificationMessage.title,
//             message: notificationMessage.body,
//             margin: EdgeInsets.all(8),
//             borderRadius: 8,
//             flushbarStyle: FlushbarStyle.FLOATING,
//             icon: Icon(Icons.notifications_none,
//                 size: 28.0, color: Theme.of(_buildContext).primaryColor),
//             duration: Duration(seconds: 8),
//             //mainButton: FlatButton(onPressed: (){}, child: Text("Открыть".toUpperCase())),
//             onTap: (data) async {
//               /*
//               await mindClass.readNotification(
//                   notificationMessage.key);
//               launchNotification(notificationMessage, _buildContext);
//
//                */
//             })
//           ..show(_buildContext);
//       } else {
//         mindClass.eventSteam.value = notificationMessage.messageInstanceId;
//       }
//     }, onResume: (Map<String, dynamic> message) async {
//       print('on resume $message');
//       var notificationMessage = await NotificationInternal.fromJson(message);
//       launchNotification(notificationMessage, _buildContext);
//     }, onLaunch: (Map<String, dynamic> message) async {
//       print('on launch $message');
//       var notificationMessage = await NotificationInternal.fromJson(message);
//       launchNotification(notificationMessage, _buildContext);
//     });
//   }
//
//   void iOS_Permission() {
//     _firebaseMessaging.requestNotificationPermissions(
//         IosNotificationSettings(sound: true, badge: true, alert: true));
//     _firebaseMessaging.onIosSettingsRegistered
//         .listen((IosNotificationSettings settings) {
//       print('Settings registered: $settings');
//     });
//   }
// }
//
// //
// // void launchPage(NotificationMessage notificationMessage, BuildContext context) {
// //   mindClass.readNotification(notificationMessage.data.key);
// //   if (mindClass.ramData['last_notification_instance'] ==
// //       notificationMessage.data.link) return;
// //   mindClass.ramData['last_notification_instance'] =
// //       notificationMessage.data.link;
// //   // launchNotification(
// //   //     notificationMessage.data.link, context, notificationMessage.notification);
// // }
//
// void launchNotification(
//     NotificationInternal notificationInternal, BuildContext context) {
//   switch (notificationInternal.type) {
//     case NotificationType.message:
//       Navigator.push(
//           context,
//           CupertinoPageRoute(
//               builder: (context) => Messages(
//                   messageInstanceId: notificationInternal.messageInstanceId,
//                   notificationInternal: notificationInternal))).then((v) {
//         mindClass.ramData['last_notification_instance'] = '';
//       });
//       break;
//     case NotificationType.not_message:
//       Navigator.push(
//           context,
//           CupertinoPageRoute(
//               builder: (context) => InstancePage(
//                   messageInstanceId: notificationInternal.messageInstanceId,
//                   notification: notificationInternal))).then((v) {
//         mindClass.ramData['last_notification_instance'] = '';
//       });
//       break;
//   }
// }
