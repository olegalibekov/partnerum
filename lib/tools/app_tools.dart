import 'dart:async';
import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:partnerum/tools/progressdialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

Widget appTextField(
    {IconData textIcon,
    String textHint,
    bool isPassword,
    double sidePadding,
    TextInputType textType,
    TextEditingController controller}) {
  sidePadding ??= 0.0;
  textHint ??= '';
  //textType == null ? textType == TextInputType.text : textType;

  return Padding(
    padding: EdgeInsets.only(left: sidePadding, right: sidePadding),
    child: Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(15.0))
      ),
      child: TextField(
        controller: controller,
        obscureText: isPassword ??= false,
        keyboardType: textType ??= TextInputType.text,
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: textHint,
          prefixIcon: textIcon == null ? Container() : Icon(textIcon)
        )
      )
    )
  );
}

Widget appButton(
    {String btnTxt,
    double btnPadding,
    Color btnColor,
    VoidCallback onBtnclicked}) {
  btnTxt ??= 'App Button';
  btnPadding ??= 0.0;
  btnColor ??= Colors.black;

  return Padding(
    padding: EdgeInsets.all(btnPadding),
    child: RaisedButton(
      color: Colors.white,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15.0))),
      onPressed: onBtnclicked,
      child: Container(
        height: 50.0,
        child: Center(
          child: Text(
            btnTxt,
            style: TextStyle(color: btnColor, fontSize: 18.0)
          )
        )
      )
    )
  );
}

Widget productTextField(
    {String textTitle,
    String textHint,
    double height,
    TextEditingController controller,
    TextInputType textType,
    int maxLines}) {
  textTitle ??= 'Enter Title';
  textHint ??= 'Enter Hint';
  height ??= 50.0;
  //height !=null

  return Column(
    //mainAxisAlignment: MainAxisAlignment.start,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          textTitle,
          style: TextStyle(fontWeight: FontWeight.w700, color: Colors.white)
        )
      ),
      Padding(
        padding: const EdgeInsets.only(left: 10.0, right: 10.0),
        child: Container(
          height: height,
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: Colors.white),
              borderRadius: BorderRadius.all(Radius.circular(4.0))),
          child: Padding(
            padding: const EdgeInsets.only(left: 8.0, right: 8.0),
            child: TextField(
              controller: controller,
              keyboardType: textType ??= TextInputType.text,
              maxLines: maxLines ??= null,
              decoration:
                  InputDecoration(border: InputBorder.none, hintText: textHint)
            )
          )
        )
      )
    ]
  );
}

Widget productDropDown(
    {String textTitle,
    String selectedItem,
    List<DropdownMenuItem<String>> dropDownItems,
    ValueChanged<String> changedDropDownItems}) {
  textTitle ??= 'Enter Title';

  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    mainAxisAlignment: MainAxisAlignment.start,
    children: <Widget>[
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          textTitle,
          style: TextStyle(fontWeight: FontWeight.w700, color: Colors.black)
        )
      ),
      Padding(
        padding: const EdgeInsets.only(left: 8.0, right: 8.0),
        child: Container(
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: Colors.grey),
              borderRadius: BorderRadius.all(Radius.circular(4.0))),
          child: Padding(
            padding: const EdgeInsets.only(left: 8.0, right: 8.0),
            child: DropdownButtonHideUnderline(
                child: DropdownButton(
              value: selectedItem,
              items: dropDownItems,
              onChanged: changedDropDownItems
            ))
          )
        )
      )
    ]
  );
}

Widget MultiImagePickerMap(
    {Map<int, File> imageList,
    VoidCallback addNewImage(int position),
    VoidCallback removeNewImage(int position)}) {
  var imageLength = imageList.isEmpty ? 1 : imageList.length + 1;
  var isNull = imageList.isEmpty ? true : false;

  print('Image length is $imageLength');

  return Padding(
    padding: const EdgeInsets.only(left: 15.0, right: 15.0),
    child: SizedBox(
      height: 150.0,
      child: ListView.builder(
          itemCount: imageLength,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            return imageList.isEmpty || imageList[index] == null
                ? Padding(
                    padding: EdgeInsets.only(left: 3.0, right: 3.0),
                    child: GestureDetector(
                      onTap: () {
                        addNewImage(index);
                      },
                      child: Container(
                        width: 150.0,
                        height: 150.0,
                        child: Stack(
                          alignment: Alignment.center,
                          children: <Widget>[
                            Icon(
                              Icons.image,
                              size: 150.0,
                              color: Theme.of(context).primaryColor
                            ),
                            Icon(
                              Icons.add_circle,
                              size: 25.0,
                              color: Colors.white
                            )
                          ]
                        ),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(15.0))
                        )
                      )
                    )
                  )
                : Padding(
                    padding: EdgeInsets.only(left: 3.0, right: 3.0),
                    child: Stack(
                      children: <Widget>[
                        Container(
                          width: 150.0,
                          height: 150.0,
                          decoration: BoxDecoration(
                              color: Colors.grey.withAlpha(100),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15.0)),
                              image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: FileImage(imageList[index])))
                        ),
                        Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: CircleAvatar(
                            backgroundColor: Colors.red[600],
                            child: IconButton(
                                icon: Icon(
                                  Icons.clear,
                                  color: Colors.white
                                ),
                                onPressed: () {
                                  removeNewImage(index);
                                })
                          )
                        )
                      ]
                    )
                  );
          })
    )
  );
}

Widget MultiImagePickerList(
    {List<File> imageList = const [],
      VoidCallback removeNewImage(int position),
      List<String> urlImages = const []}) {
  print(urlImages);
  return Padding(
    padding: const EdgeInsets.only(left: 15.0, right: 15.0),
    child: imageList.isEmpty && urlImages.isEmpty
        ? Container()
        : SizedBox(
      height: 150.0,
      child: ListView.builder(
          itemCount: imageList.length + urlImages.length,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            if (index + 1 > imageList.length) {
              return Padding(
                padding: EdgeInsets.only(left: 3.0, right: 3.0),
                child: Stack(
                  children: <Widget>[
                    Container(
                      width: 150.0,
                      height: 150.0,
                      decoration: BoxDecoration(
                        color: Colors.grey,
                        borderRadius:
                        BorderRadius.all(Radius.circular(15.0))
                      )
                    ),
                    Opacity(
                      opacity: .95,
                      child: Container(
                        width: 150.0,
                        height: 150.0,
                        decoration: BoxDecoration(
                          color: Colors.grey.withAlpha(100),
                          borderRadius:
                          BorderRadius.all(Radius.circular(15.0))
                        ),
                        child: CachedNetworkImage(
                          imageUrl: urlImages[index - imageList.length]
                        )
                      )
                    ),
                    Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: CircleAvatar(
                        backgroundColor: Colors.red[600],
                        child: IconButton(
                          icon: Icon(
                            Icons.clear,
                            color: Colors.white
                          ),
                          onPressed: () {
                            //removeOldImage(index);
                          }
                        )
                      )
                    )
                  ]
                )
              );
            }
            return Padding(
              padding: EdgeInsets.only(left: 3.0, right: 3.0),
              child: Stack(
                children: <Widget>[
                  Container(
                    width: 150.0,
                    height: 150.0,
                    decoration: BoxDecoration(
                      color: Colors.grey,
                      borderRadius:
                      BorderRadius.all(Radius.circular(15.0))
                    )
                  ),
                  Opacity(
                    opacity: .95,
                    child: Container(
                      width: 150.0,
                      height: 150.0,
                      decoration: BoxDecoration(
                          color: Colors.grey.withAlpha(100),
                          borderRadius:
                          BorderRadius.all(Radius.circular(15.0)),
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: FileImage(imageList[index])))
                    )
                  ),
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: CircleAvatar(
                      backgroundColor: Colors.red[600],
                      child: IconButton(
                        icon: Icon(
                          Icons.clear,
                          color: Colors.white
                        ),
                        onPressed: () {
                          removeNewImage(index);
                        }
                      )
                    )
                  )
                ]
              )
            );
          })
    )
  );
}

Widget MultiTagPickerList(
    {List<dynamic> tagList, VoidCallback removeNewTag(int position)}) {
  return tagList == null || tagList.isEmpty
      ? Container()
      : Container(
          height: 50.0,
          margin: EdgeInsets.symmetric(horizontal: 15.0),
          child: ListView.builder(
              shrinkWrap: true,
              physics: ClampingScrollPhysics(),
              itemCount: tagList.length,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                return Wrap(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Chip(
                          backgroundColor: Colors.white,
                          label: Text(
                            tagList[index],
                            style: TextStyle(
                              color: Colors.black
                            )
                          ),
                          deleteIcon: Icon(
                            Icons.delete,
                            color: Colors.red,
                            size: 20
                          ),
                          onDeleted: () {
                            removeNewTag(index);
                          }
                        )
                      ]
                    )
                  ]
                );

//              return  Padding(
//                padding:  EdgeInsets.only(left: 3.0, right: 3.0),
//                child:  Stack(
//                  children: <Widget>[
//                     Container(
//                      width: 150.0,
//                      height: 150.0,
//                      decoration:  BoxDecoration(
//                          color: Colors.grey.withAlpha(100),
//                          borderRadius:  BorderRadius.all(
//                               Radius.circular(15.0)),),
//                      child:  Text(tagList[index]),
//                    ),
//                     Padding(
//                      padding: const EdgeInsets.all(5.0),
//                      child:  CircleAvatar(
//                        backgroundColor: Colors.red[600],
//                        child:  IconButton(
//                            icon:  Icon(
//                              Icons.clear,
//                              color: Colors.white,
//                            ),
//                            onPressed: () {
//                              removeNewTag(index);
//                            }),
//                      ),
//                    )
//                  ],
//                ),
//              );
              })
        );
}

Widget buildImages({int index, Map imagesMap}) {
  return imagesMap.isEmpty
      ? Container(
          width: 150.0,
          height: 150.0,
          child: Stack(
            alignment: Alignment.center,
            children: <Widget>[
              Icon(
                Icons.image,
                size: 100.0,
                color: Colors.white
              ),
              Icon(
                Icons.add_circle,
                color: Colors.grey
              )
            ]
          ),
          decoration: BoxDecoration(
            color: Colors.grey.withAlpha(100)
          )
        )
      : imagesMap[index] != null
          ? Container(
              width: 150.0,
              height: 150.0,
              decoration: BoxDecoration(
                  color: Colors.grey.withAlpha(100),
                  image: DecorationImage(
                      fit: BoxFit.cover, image: FileImage(imagesMap[index])))
            )
          : Container(
              width: 150.0,
              height: 150.0,
              child: Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  Icon(
                    Icons.image,
                    size: 100.0,
                    color: Colors.white
                  ),
                  Icon(
                    Icons.add_circle,
                    color: Colors.grey
                  )
                ]
              ),
              decoration: BoxDecoration(
                color: Colors.grey.withAlpha(100)
              )
            );
}

List<DropdownMenuItem<String>> buildAndGetDropDownItems(List size) {
  var items = <DropdownMenuItem<String>>[];
  for (String size in size) {
    items.add(DropdownMenuItem(value: size, child: Text(size)));
  }
  return items;
}

showSnackBar(String message, final scaffoldKey) {
  scaffoldKey.currentState.showSnackBar(SnackBar(
    backgroundColor: Colors.red[600],
    content: Text(
      message,
      style: TextStyle(color: Colors.white)
    )
  ));
}

displayProgressDialog(BuildContext context) {
  Navigator.of(context).push(PageRouteBuilder(
      opaque: false,
      pageBuilder: (BuildContext context, _, __) {
        return ProgressDialog();
      }));
}

closeProgressDialog(BuildContext context) {
  Navigator.of(context).pop();
}

writeDataLocally({String key, String value}) async {
  var saveLocal = SharedPreferences.getInstance();
  final localData = await saveLocal;
  localData.setString(key, value);
}

writeBoolDataLocally({String key, bool value}) async {
  var saveLocal = SharedPreferences.getInstance();
  final localData = await saveLocal;
  localData.setBool(key, value);
}

Future getDataLocally({String key}) async {
  var saveLocal = SharedPreferences.getInstance();
  final localData = await saveLocal;
  return localData.get(key);
}

Future<String> getStringDataLocally({String key}) async {
  var saveLocal = SharedPreferences.getInstance();
  final localData = await saveLocal;
  return localData.getString(key);
}

Future<bool> getBoolDataLocally({String key}) async {
  var saveLocal = SharedPreferences.getInstance();
  final localData = await saveLocal;
  return localData.getBool(key) ?? false;
}

clearDataLocally() async {
  var saveLocal = SharedPreferences.getInstance();
  final localData = await saveLocal;
  localData.clear();
}
