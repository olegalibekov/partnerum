import "package:flutter/material.dart";

class Walkthrough {
  IconData icon;
  String title;
  Color color;
  String description;
  Widget extraWidget;
  String illustrationPath;
  // UnDrawIllustration illustration;

  Walkthrough(
      {this.icon,
      this.title,
      this.color,
      this.description,
      this.extraWidget,
      this.illustrationPath}) {
    extraWidget ??= Container();
  }
}
