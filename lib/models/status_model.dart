import 'package:db_partnerum/src/models/constants/status_constants.dart';

class StatusModel {
  static String convertStatus(String status) {
    if (status == StatusConstants.houseRequestSended) return 'Заявка подана';
    if (status == StatusConstants.houseRequestCanceled) return 'Заявка отменена';
    if (status == StatusConstants.requestBooked) return 'Забронировано';
    if (status == StatusConstants.requestAccept) return 'Заявка одобрена';
    if (status == StatusConstants.requestDenied) return 'Заявка отклонена';
    return 'Статус не найден';
  }
}
