import 'package:partnerum/widgets/user_title/user_tile.dart';

class UserModel {
  String avatar = '';
  String rawUsername = '';
  int lastOnline;
  String document = 'empty';
  bool isConfirmed = false;
  bool isAdmin = false;
  String role = "Пользователь";
  String uid;
  Rating rating;

  get username => rawUsername.trim();

  UserModel({this.avatar, this.rawUsername, this.lastOnline, this.document});
/*
  UserModel.fromJson(dynamic json)
      : this._fromJson(json == null ? {} : Map.from(json));
*/
  isUsernameExist() {
    return rawUsername != 'Имя не введено';
  }

  isConfirmedHumanReadable() {
    if (isConfirmed)
      return "Верефицирован";
    else
      return "Неверефицирован";
  }
/*
  UserModel._fromJson(Map<String, dynamic> json) {
    avatar = json['avatar'] ?? '';
    rawUsername = json['username'] ?? 'Имя не введено';
    lastOnline = json['last_online'] ?? -1;
    document = json['document'] ?? '';
    isConfirmed = json['confirmed'] ?? false;
    isAdmin = json["admin"] ?? false;
    rating = Rating.fromJson(json['rating'], json['reviews']);
  }
*/
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['avatar'] = this.avatar;
    data['username'] = this.rawUsername;
    data['last_online'] = this.lastOnline;
    data['document'] = this.document;
    data["admin"] = this.isAdmin;
    return data;
  }

  @override
  String toString() {
    return toJson().toString();
  }
}
