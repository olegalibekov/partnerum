import 'requests_model.dart';
import 'houses_model.dart';
// import 'package:db_partnerum/db_partnerum.dart' hide Request;

class Header {
  final String messageInstanceId;
  House house;
  Request request;
  Status status;

  Header(this.messageInstanceId, {this.house, this.request, this.status});

  static Future<Header> fromJson(
      String messageInstanceId, Map json) async {
    var house =
        json['house'] != null ? new House.fromJson(json['house']) : null;
    var request =
        json['request'] != null ? new Request.fromJson(json['request']) : null;

    Status status = await Status.loadStatus(messageInstanceId, json);
    return Header(messageInstanceId,
        house: house, request: request, status: status);
  }
}
