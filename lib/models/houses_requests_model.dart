import 'package:db_partnerum/db_partnerum.dart';

import 'houses_model.dart' hide House;

class HousesRequest {
  final String requestOwnerId;
  final String requestId, messageInstanceId;

  bool archived = false;

  String requestStateType;
  Request request;

  Status status;

  get complexId => "$requestOwnerId-$requestId-$messageInstanceId";

  addRequestsFromMap(dynamic map) {
    Map.from(map).forEach((v1, v2) {});
  }

  String getMixedId() {
    return '$requestOwnerId;$requestId';
  }

  HousesRequest(
    this.requestOwnerId,
    this.requestId,
    this.messageInstanceId, //this.requestStateType, this.request,
  ) {
//    if (this.requestHouses.containsKey('archive')) {
//      archive = this.requestHouses['archive'];
//      this.requestHouses.remove('archive');
//    }
  }

  @override
  String toString() {
    return 'ownerId -> $requestOwnerId; requestId -> $requestId; messageInstanceId -> $messageInstanceId\n ${request.toString()} ${archived}';
    return super.toString();
  }
}
