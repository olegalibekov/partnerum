import 'user_info_model.dart';

class PossibleUser {
  final UserModel userInfo;
  final String uid;

  PossibleUser(this.uid, this.userInfo);
}

class PossibleAdmin extends PossibleUser {
  PossibleAdmin(String uid, UserModel userInfo) : super(uid, userInfo);
  int level = 3;

  get humanReadableLevel {
    String humanReadableLevel;
    switch (level) {
      case 0:
        humanReadableLevel = "Полный доступ ко всем данным";
        break;
      case 1:
        humanReadableLevel = "Частиный доступ ко всем данным";
        break;
      case 3:
        humanReadableLevel = "Стандартный доступ ко всем данным";
        break;
    }
    humanReadableLevel += "\n->Уровень $level";
    return humanReadableLevel;
  }
}

class ChatRoom {
  final PossibleUser possibleUser;
  final String messageInstanceId;
  bool read = false;
  int timestamp = 1;
  String key;

  ChatRoom(this.possibleUser, this.messageInstanceId);
//final User userInfo;

}
