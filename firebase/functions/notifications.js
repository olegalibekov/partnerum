const admin = require("firebase-admin");
const db = admin.database();
const fcm = admin.messaging();
const functions = require('firebase-functions');
const send_request = "send_request";
const confirm_request = "confirm_request";
const deny_request = "deny_request";
const refuse_request = "refuse_request";
const book = "book";
const rate = "rate";
const rate_answer = "rate_answer";

function isUserNameEmpty(data) {
    return !data;
}

function remove(array, element) {
    const index = array.indexOf(element);
    if (index > -1) {
        array.splice(index, 1);
    }
}

async function doIfFinallyBooked(statusHouse, statusReview, messageInstanceId) {
    const databaseReference = db.ref(`/message_data/${messageInstanceId}/`);
    let houseId = (await databaseReference.child("header").child("house").child("id").once("value")).val();
    let requestId = (await databaseReference.child("header").child("request").child("id").once("value")).val();

    if (statusHouse === book && statusReview === book) {
        let users = (await databaseReference.child("header").child("users").once("value")).val();
        let houseUid;
        let requestUid;
        if (users) {
            let uids = Object.keys(users);
            for (let uid in uids) {
                if (users[uids[uid]] === "house")
                    houseUid = uids[uid];
                else
                    requestUid = uids[uid];
            }
        }
        let automatic_house_booking_dates_place = (await db.ref(`/user_data/${houseUid}/user_info/automatic_house_booking_dates_place/`)).val();
        if (automatic_house_booking_dates_place === null || automatic_house_booking_dates_place === true) {


            let houseInfoReference = databaseReference.child("header").child("house").child("info");
            let photos = (await houseInfoReference.child("photo_urls").once("value")).val();
            let image;
            if (!photos)
                photos = [];
            else
                image = photos[0];

            const databaseReferenceH = db.ref(`/user_data/${houseUid}/houses/${houseId}/info/booked_dates/`); ///TODO оповещение о добавлении в случае, если всё удалось.
            const dates_range = (await db.ref(`/user_data/${requestUid}/requests/${requestId}/info/dates_range/`).on('value')).val();
            const start = new Date(dates_range[0]).getTime();
            const end = new Date(dates_range[1]).getTime();
            await databaseReferenceH.set({[`${start}-${end}`]: 0});
            await sendNotificationToType(houseUid, messageInstanceId, "Добавлено в бронированные даты квартиры", "", image);
        }
    }
}

module.exports.catchRequestStatus = functions.database.ref('/message_data/{messageInstanceId}/header/status/request/')
    .onWrite(async (change, context) => {
            let request = change.after.val();
            let house = (await db.ref(`/message_data/${context.params.messageInstanceId}/header/status/house/`).once('value')).val();
            let requestBefore = change.before.val();
            //automatic_house_booking_dates_place
            await statusNotifications(context, change, request, requestBefore, house);
            await doIfFinallyBooked(house, request, context.params.messageInstanceId);
        }
    );

async function chatReady(house,context) {
    if (house === confirm_request) {
        let users = (await db.ref(`/message_data/${context.params.messageInstanceId}`).child("header").child("users").once("value")).val();
        if (users) {
            users = Object.keys(users);
        }
        remove(users, context.auth.uid);
        let anotherUid = users[0];
        await db.ref(`/user_data/${anotherUid}`).child("chats").child(`${context.auth.uid};${context.params.messageInstanceId}`).set(false);
        await db.ref(`/user_data/${context.auth.uid}`).child("chats").child(`${anotherUid};${context.params.messageInstanceId}`).set(false);
    }
    if(house === refuse_request){

    }
}

///TODO create special module that looks if it possible to create element if dates is booked
module.exports.catchHouseStatus = functions.database.ref('/message_data/{messageInstanceId}/header/status/house/')
    .onWrite(async (change, context) => {
            let house = change.after.val();
            let request = (await db.ref(`/message_data/${context.params.messageInstanceId}/header/status/request/`).once('value')).val();
            let houseBefore = change.before.val();

            await chatReady(house,context);
            await statusNotifications(context, change, house, houseBefore, request);
            await doIfFinallyBooked(house, request, context.params.messageInstanceId);
        }
    );
module.exports.catchMessage = functions.database.ref('/message_data/{messageInstanceId}/body/{messageId}/')
    .onWrite(async (change, context) => {
            if (!change.after.exists()) return null;


            const databaseReference = db.ref(`/message_data/${context.params.messageInstanceId}/`);

            let users = await databaseReference.child("header").child("users").once("value");
            users = users.val();
            if (users) {
                users = Object.keys(users);
            }
            let anotherUid = getAnotherUid(users, context);

            let request = change.after.val();
            await statusNotifications();

        }
    );


async function statusNotifications(context, change, value1, value1Before, value2) {
    const databaseReference = db.ref(`/message_data/${context.params.messageInstanceId}/`);

    let users = (await databaseReference.child("header").child("users").once("value")).val();
    if (users) {
        users = Object.keys(users);
    }
    remove(users, context.auth.uid);

    let anotherUid = users[0];
    let anotherUser = (await db.ref(`/user_data/${context.auth.uid}/user_info/`).once('value')).val();

    let address, metroStations, city, photos;


    let data = {};
    let houseInfoReference = databaseReference.child("header").child("house").child("info");
    let header = (await (databaseReference.child("header").once('value'))).val();
    address = (await houseInfoReference.child("address").once("value")).val();
    metroStations = (await houseInfoReference.child("stations").once("value")).val();
    city = (await houseInfoReference.child("stations").once("value")).val();
    photos = (await houseInfoReference.child("photo_urls").once("value")).val();
    if (photos)
        data.image = photos[0];
    if (address === "Адрес не указан") {
        if (metroStations) {
            address = `у метро ${metroStations[0]}`
            for (let i = 1; i < metroStations.length - 1; i++) {
                address += `, ${metroStations[i]}`;
            }
            address += ` и ${metroStations[metroStations.length - 1]}`
        } else if (city) {
            address = `в городе ${city}`;
        }
    } else {
        address = `по адресу: ${address}`
    }
    if (formNotificationText(value1, value1Before, value2, data, anotherUser, address) === "error") return;
    data.messageInstanceId = context.params.messageInstanceId;
    data.key = await writeNotification(context.auth.uid, anotherUid, context.params.messageInstanceId, data, false, header);
    await sendNotificationToType(anotherUid, context.params.messageInstanceId, data.title, data.body, data.image, data);

}


function formNotificationText(value, valueBefore, anotherValue, data, anotherUser, address) {
    if (!data.reason) {
        data.reason = 'Без причины';
    }
    if (value === send_request && valueBefore === deny_request) {
        // Повторно подал заявку
        data.title = "Повторно подал заявку";
        data.body = `${isUserNameEmpty(anotherUser['username']) ? 'Анонимный пользователь' : `Пользователь ${anotherUser['username']}`}`
        data.body += ` повторно подал заявку на вашу квартиру ${address}`;
        return;
    }

    if (value === deny_request && anotherValue === book && valueBefore === book) {
        // Отменил бронь и заявку
        data.title = "Отменили бронь и заявку";
        data.body = `${isUserNameEmpty(anotherUser['username']) ? 'Анонимный пользователь' : `Пользователь ${anotherUser['username']}`}`
        data.body += ` отменил бронь и заявку вашей квартиры ${address}`;
        return;
    }
    if (value === send_request && anotherValue === book && valueBefore === book) {
        // Отменил бронь
        data.title = "Отменили бронь";
        data.body = `${isUserNameEmpty(anotherUser['username']) ? 'Анонимный пользователь' : `Пользователь ${anotherUser['username']}`}`
        data.body += ` отменил бронь квартиры ${address}`;
        return;
    }
    if (value === book && anotherValue === book && valueBefore !== book) {
        // Возобновление брони
        data.title = "Бронь возобновили";
        data.body = `${isUserNameEmpty(anotherUser['username']) ? 'Анонимный пользователь' : `Пользователь ${anotherUser['username']}`}`
        data.body += ` возобновил бронь квартиры ${address}`;
        return;
    }
    if (value === confirm_request && anotherValue === book && valueBefore === book) {
        // Отменил предложение брони
        data.title = "Отменил предложение брони";
        data.body = `${isUserNameEmpty(anotherUser['username']) ? 'Анонимный пользователь' : `Пользователь ${anotherUser['username']}`}`
        data.body += ` отменил предложение брони квартиры ${address}`;
        return;
    }
    if (value === refuse_request && anotherValue === book && valueBefore === book) {
        //Отменили бронь и отказались от сотрудничества
        data.title = "Полностью отказались от сотрудничества";
        data.body = `${isUserNameEmpty(anotherUser['username']) ? 'Анонимный пользователь' : `Пользователь ${anotherUser['username']}`}`
        data.body += ` отменил бронь, и отказался сотрудничать, квартиры ${address}`;
        return;
    }
    if (value === deny_request && (anotherValue === confirm_request || !anotherValue)) {
        // Отменил заявку
        data.title = "Отменили заявку";
        data.body = `${isUserNameEmpty(anotherUser['username']) ? 'Анонимный пользователь' : `Пользователь ${anotherUser['username']}`}`
        data.body += ` отменил заявку на вашу квартиру ${address}`;
        return;
    }

    if (value === deny_request && anotherValue === book) {
        // Отменил предложение брони и заявку
        data.title = "Отозвали заявку и бронь";
        data.body = `${isUserNameEmpty(anotherUser['username']) ? 'Анонимный пользователь' : `Пользователь ${anotherUser['username']}`}`
        data.body += ` отозвал заявку и бронь на вашу квартиру ${address}`;
        return;
    }
    if (value === send_request && !anotherValue) {
        // Подал заявку
        data.title = "Подали заявку";
        data.body = `${isUserNameEmpty(anotherUser['username']) ? 'Анонимный пользователь' : `Пользователь ${anotherUser['username']}`}`
        data.body += ` подал заявку на вашу квартиру ${address}`;
        return;
    }
    if (value === send_request && anotherValue === confirm_request) {
        // Повторно подал заявку
        data.title = "Повторно подал заявку";
        data.body = `${isUserNameEmpty(anotherUser['username']) ? 'Анонимный пользователь' : `Пользователь ${anotherUser['username']}`}`
        data.body += ` повторно подал заявку на вашу квартиру ${address}`;
        return;
    }


    if (value === send_request && anotherValue === book) {
        // Отменил предложение брони
        data.title = "Отменили предложение брони";
        data.body = `${isUserNameEmpty(anotherUser['username']) ? 'Анонимный пользователь' : `Пользователь ${anotherUser['username']}`}`
        data.body += ` отменили предложение брони вашей квартиры ${address}`;
        return;
    }

    if (value === book && anotherValue === confirm_request) {
        // Предложение о брони
        data.title = "Вам предложили бронь";
        data.body = `${isUserNameEmpty(anotherUser['username']) ? 'Анонимный пользователь' : `Пользователь ${anotherUser['username']}`}`
        data.body += ` хочет взять в бронь вашу квартиру ${address}`;
        return;
    }

    if (value === confirm_request && anotherValue !== deny_request) {
        // Заявка на квартиру одобрена. Можно приступить к обсуждению деталей
        data.title = "Заявка одобрена";
        data.body = `${isUserNameEmpty(anotherUser['username']) ? 'Анонимный пользователь' : `Пользователь ${anotherUser['username']}`}`
        data.body += ` одобрил вашу заявку на квартиру ${address}. Можно приступать к обсуждению деталей.`;
        return;
    }
    if (value === confirm_request && (valueBefore === deny_request)) {
        // Заявка на квартиру вновь одобрена. Можно повторно приступить к обсуждению деталей
        data.title = "Заявка вновь одобрена";
        data.body = `${isUserNameEmpty(anotherUser['username']) ? 'Анонимный пользователь' : `Пользователь ${anotherUser['username']}`}`
        data.body += ` вновь одобрил вашу заявку на квартиру ${address}. Можно приступать к обсуждению деталей.`;
        return;
    }


    if (value === refuse_request && anotherValue === send_request) {
        // Заявку отклонили
        data.title = "Заявку отклонили";
        data.body = `${isUserNameEmpty(anotherUser['username']) ? 'Анонимный пользователь' : `Пользователь ${anotherUser['username']}`}`
        data.body += ` отклонил вашу заявку на квартиру ${address}. По причине:${data.reason}`;
        return;
    }

    if (value === refuse_request && anotherValue === book) {
        //Отменили предложение брони и отказались от сотрудничества
        data.title = "Отменили предложение брони и отказались от сотрудничества";
        data.body = `${isUserNameEmpty(anotherUser['username']) ? 'Анонимный пользователь' : `Пользователь ${anotherUser['username']}`}`
        data.body += ` отклонил вашу заявку на квартиру ${address}. По причине:${data.reason}`;
        return;

    }
    if (value === book && anotherValue === book) {
        //Бронь подтверждена
        data.title = "Бронь согласована";
        data.body = `${isUserNameEmpty(anotherUser['username']) ? 'Анонимный пользователь' : `Пользователь ${anotherUser['username']}`}`
        data.body += ` согласовал предложение брони квартиры ${address}`;
    }

    if (value === rate) {
        //Оценили работу с Вами и Вашей квартирой.
        data.title = "Работу с Вами оценили";
        data.body = `${isUserNameEmpty(anotherUser['username']) ? 'Анонимный пользователь' : `Пользователь ${anotherUser['username']}`}`
        data.body += ` оценил работу с Вами. Комментарий: ${data.comment}`;
        data.image = "";
    }
    if (value === rate_answer) {
        data.title = "На Ваш комментарий ответили";
        data.body = `${isUserNameEmpty(anotherUser['username']) ? 'Анонимный пользователь' : `Пользователь ${anotherUser['username']}`}`
        data.body += ` ответил Вам в комментарии: ${data.comment}`;
        data.image = "";
    }

    return "error";
    if (value === deny_request && anotherValue === refuse_request) {
        // Не может быть такого. Ничего не присылаем.
    }
    if (value === book && !anotherValue) {
        // *
    }
    if (value === book && anotherValue === refuse_request) {
        // *
    }
    if (value === confirm_request && (anotherValue === deny_request || !anotherValue)) {
        //*
    }
    if (value === refuse_request && (anotherValue === deny_request || !anotherValue)) {
        // *
    }
    if (value === book && (anotherValue === send_request || anotherValue === deny_request)) {
        //*
    }
    if (value === send_request && anotherValue === refuse_request) {
        // *Ничего не присылаем..
    }
    /// простите, запрос некорректный.

}

async function writeNotification(currentUid, anotherUid, messageInstanceId, data, isPoorMessage = false, header) {
    let notificationId = db.ref(`/user_data/${anotherUid}/message_notification_list/`);
    if (isPoorMessage) {
        notificationId = notificationId.child(`${currentUid};${messageInstanceId}`);
        data.type = "message";
    } else {
        notificationId = notificationId.push();
        data.type = "!message";
    }
    await notificationId.set({
        time: admin.database.ServerValue.TIMESTAMP
    });
    if (header)
        data['header'] = JSON.stringify(header);
    await notificationId.update(data);
    return notificationId.key;
}

async function userTokens(uid) {
    let tokens = await db.ref(`/user_data/${uid}/notifications/`).once("value");
    tokens = tokens.val();
    if (tokens)
        tokens = Object.keys(tokens);
    else tokens = null;
    return tokens;
}

async function sendNotificationToType(uid, messageInstanceId, title, body, iconUrl, data) {
    let tokensUid = await userTokens(uid);
    if (!tokensUid) return;


    const payload = {
        "notification": {
            "title": title,
            "body": body,
            "sound": "default",
            "click_action": 'FLUTTER_NOTIFICATION_CLICK',
        }
    };
    if (iconUrl) {
        payload.notification.image = iconUrl;
        data.image = iconUrl;
    }
    if (data)
        payload.data = data;

    return fcm.sendToDevice(tokensUid, payload, {priority: 'high'}).then(function (response) {
        console.log('Notification sent successfully:', response);
    })
        .catch(function (error) {
            console.log('Notification sent failed:', error);
        });

}

module.exports.sendNotificationToType = sendNotificationToType;