const admin = require("firebase-admin");
const db = admin.database();
const fcm = admin.messaging();
const functions = require('firebase-functions');


const sendNotificationToType = require('./notifications').sendNotificationToType;

//check if smth wrong

function remove(array, element) {
    const index = array.indexOf(element);
    if (index > -1) {
        array.splice(index, 1);
    }
}

module.exports.catchUserReview = functions.database.ref('/user_data/{userId}/user_info/reviews/{messageInstanceId}/')
    .onWrite(async (change, context) => {

            const reviewsRef = db.ref(`/user_data/${context.params.userId}/user_info/reviews/`);

            const reviews = (await reviewsRef.once("value")).val();
            let rating = 0;
            let rating_form;
            if (reviews) {
                let reviewsMessageInstanceIds = Object.keys(reviews);
                for (let i = 0; i < reviewsMessageInstanceIds.length; i++) {
                    rating += reviews[reviewsMessageInstanceIds[i]]["rating"];
                }
                rating = rating / reviewsMessageInstanceIds.length;
                rating_form = `${rating}_${reviewsMessageInstanceIds.length}`;
            }

            await db.ref(`/user_data/${context.params.userId}/user_info/rating/`).set(rating_form);
        }
    ); //this woudl count only user reviews
//one more functions shows only for current house reviews

module.exports.catchReview = functions.database.ref('/message_data/{messageInstanceId}/header/review/{uid}/')
    .onWrite(async (change, context) => {
            let review = change.after.val();
            let currentUid = context.params.uid;
            await newTaskReview(review, currentUid, context.params.messageInstanceId);
            //await handleReview(review, currentUid, context.params.messageInstanceId);
        }
    );

async function handleReview(review, currentUid, messageInstanceId) {
    const databaseReference = db.ref(`/message_data/${messageInstanceId}/`);

    let users = (await databaseReference.child("header").child("users").once("value")).val();

    /// TODO wait for review if wasn't then publish. //отправить оповещение о том, что проверка началась и если не успеют проверить зв 30 минут, то это их проблемы.
    if (users) {
        let uids = Object.keys(users);
        remove(uids, currentUid);

        if (users[uids[0]] === "house") {
            //write to house & user
            let houseId = (await databaseReference.child("header").child("house").child("id").once("value")).val();
            await db.ref(`/user_data/${uids[0]}/houses/${houseId}/reviews/${messageInstanceId}/`).set(review);
        }
        //write to user
        await db.ref(`/user_data/${uids[0]}/user_info/reviews/${messageInstanceId}/`).set(review);
        await sendNotificationToType(uids[0], messageInstanceId, 'Новый отзыв', 'Вам написали новый отзыв');
    }
    await sendNotificationToType(currentUid, messageInstanceId, 'Ваш отзыв одобрен', 'Мы опубликовали Ваш отзыв!^^');
}

async function newTaskReview(review, currentUid, messageInstanceId) {
    let timestamp = new Date((await db.ref(`/task_to_accomplish/`).once("value")).val());
    timestamp.setMinutes(timestamp.getMinutes() + 58); //58
    await db.ref(`/actions/reviews/${messageInstanceId}`).set({
        'timestamp': timestamp.getTime(),
        'review': review,
        'current_uid': currentUid,
        'messageInstanceId': messageInstanceId
    });
    await sendNotificationToType(currentUid, messageInstanceId, 'Сделан запрос на публикацию отзыва', 'В пределах ~60 минут мы опубликуем Ваш отзыв, сейчас он проходит проверку.');
}

async function doStaff() {
    let timestamp = (await db.ref(`/task_to_accomplish/`).once("value")).val();


    let actionsList = (await db.ref(`/actions/reviews/`).once("value")).val();
    if (!actionsList) return;
    let actionsKeys = Object.keys(actionsList);

    for (let i = 0; i < actionsKeys.length; i++) {
        let actionKey = actionsKeys[i];
        if (actionsList[actionKey]['timestamp'] <= timestamp) {
            let action = actionsList[actionKey];
            console.log(action);
            console.log('hi');
            await handleReview(action['review'], action['current_uid'], action['messageInstanceId']);
            await db.ref(`/actions/reviews/${actionKey}`).remove();
        }
    }
}

module.exports.scheduledFunction = functions.pubsub.schedule('every 30 minutes').onRun(async (context) => {
    await doStaff();
    await db.ref(`/task_to_accomplish/`).set(admin.database.ServerValue.TIMESTAMP);
    return null;
});

///special table with request for review on users actions
///tasks_to_accomplish -> every30 = timestamp;
