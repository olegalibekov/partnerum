const algoliasearch = require('algoliasearch');


const algolia = algoliasearch(
    process.env.ALGOLIA_APP_ID,
    process.env.ALGOLIA_API_KEY
);


const index = algolia.initIndex(process.env.ALGOLIA_INDEX_NAME);
index.setSettings({
    paginationLimitedTo: 10000,
    hitsPerPage: 1000,
    attributesForFaceting: [
        'stations',
        'city'
    ],
    attributesToHighlight: [
        'none'
    ],
    attributesToRetrieve: [
        'objectId'
    ]
});

module.exports.search = async function (facetList, price_range) {
    console.log("cool");
    let options = {
        "analytics": true,
        //"attributesToRetrieve": "*",
        //"attributesToSnippet": "*:20",
        //"getRankingInfo": true,
        //"snippetEllipsisText": "…",
        //"responseFields": "*",
        // "enableABTest": false,
        //"filters": ,
        "facets": "*,stations,city",
        "facetFilters": facetList,
        "numericFilters": [

            [`price_start:${price_range[0]} TO ${price_range[1]}`,
                `price_end:${price_range[0]} TO ${price_range[1]}`
            ]
            //'price < 1000'
        ]
    };
    console.log();
    console.log("cool_end");
    return index.search("", options);
}
module.exports.index = index;